<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import= "java.util.Enumeration "%>

<html>

<head>

<title>IntellectFlow</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<style>

.index_login_text{

	color: #4c4c4c;

	font-family: Tahoma, Helvetica, sans-serif;

	font-size: 9pt; 

	font-weight: normal;

	text-decoration: none;

}

.ST77 {

font-family: Tahoma, Helvetica, sans-serif;

font-size: 8pt;

font-weight: normal;

color: #333333

}

</style>


</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-repeat:no-repeat; scroll:no;" scroll="no" Scrolling="no" background="images/loginCrop.png" >
<br>
<br>
<br>
<table width="70%" align=center> 
<th align=center colspan='2'>
 <font color="blue">Excel Comparator Tool </font><br><br>
 </th>
    <tr>
		<td>
			<b><i>Useful Steps</i></b><br>	
			1) Ensure that file open in MS Excel format(extn should be .xls)<br>
			2) Steps for using the Excel Comparator Tool: <br>
			&nbsp; a) Open CompareFiles.bat<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; It contains a file in the following format:<br>
		    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2pt"> java -jar ExcelComparatorTool.jar GoodFileWithPath.xls BadFileWithPath.xls NumberOfSheetsToCompare >> OutputFile.txt<br></font>
	 
		&nbsp; b) Replace the names of GoodFileWithPath.xls and BadFileWithPath.xls with your respective file names.<br>
		&nbsp; c) Replace the NumberOfSheetsToCompare with your relevant value.<br>
	 
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; For example: <br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2pt"> java -jar ExcelComparatorTool.jar</font> <font size="1.8pt">C:/Users/shruti.gupta/Desktop/FORMAT_GENERATION_TOOL_3.0_SFGT_Super_Final_Ph3.xls <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; D:/FORMAT_GENERATION_TOOL_3.0_SFGT_Super_Final_Ph3.xls  9  >> FileDifferences.txt</font> <br>
	 
	&nbsp; d) Save the file. Open command prompt and execute the .bat file <br><br>
		 
		</td>
		</tr>
		<tr>
		<td>
         <a href="/FormatGenByTemplate/ExcelComparatorTool.zip" download="ExcelComparatorTool.zip">ExcelComparator</a><br><br></td>
        </tr>
		<tr>
		<td>
			<strong>NOTE:</strong> This program will do a basic comparison of the 2 versions of excel sheet(.xls format) like format of columns, <br>
			length of text data etc.and display it in a text file (FileDifferences.txt) in case of a mismatch.<br><br>
			
	 </td>
	 </tr>
	
  </table>

</body>

</html>

