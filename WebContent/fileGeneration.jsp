<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ page import="java.sql.*" %>
	<%@ page import="com.intellect.dao.*" %>
	<%@ page import= "java.util.Map"%>
	<%@ page import= "java.util.Map.*" %>
	
<html>
<head>
<title>IntellectFlow</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="scripts/FGApp.css" type="text/css">
<script>

function FGSubmitForm(){
	if(document.getElementById("Mul_FILE_VAR_REQ").checked){
		var chks=document.getElementsByName("fileVar");
		for(var i=0;i<chks.length;i++){
			if(chks[i].checked){
				return true;
			}	
		}	
		alert("Please select atleast one Rules for creating variations ");
		return false;
	}
	
	return true;
}
function enableDisable(){
	if(document.getElementById("Mul_FILE_VAR_REQ").checked){
		document.getElementById("rules").hidden=false;
	}else{
		document.getElementById("rules").hidden=true;
		var chks=document.getElementsByName("fileVar");
		for(var i=0;i<chks.length;i++){
			chks[i].checked=false;	
		}	
	}

	return true;
}

</script>
</head>
<style>

</style>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"
	background="images/loginCrop.png"
	style="background-repeat: no-repeat;" >
<center>

<FORM method="POST" name="frm" action="tranUrl"
	onsubmit="return FGSubmitForm()" enctype="multipart/form-data">

<table  width="70%" height="300px" style="padding: 50px;">

	<tr>
		<td><b>Please Choose Files <font color="red">*</font></b></td>
		<td ><input type="file" name="fileName" style="color: green; "/></td>	
	</tr>
					
	<tr>
					  <td><b> Format Name <font color="red">*</font> </b></td>
					  <td><select name="sheetName" required  >
                                    <option value="">Select Sheet</option>
                                <option value="all">ALL</option>
                                <option value="ANSI">ANSI</option>
                                <option value="Native Formats">Native Formats</option>
                                <option value="SWIFT">SWIFT</option>
                                <option value="PAIN 001">PAIN 001</option>
                                <option value="PAIN 008">PAIN 008</option>
                                <option value="PAIN 001_Remit_check">PAIN 001_Remit_check</option>
                                <option value="PAIN 008_remit">PAIN 008_remit</option>
                                <option value="Interac">INTERAC</option>
                                <option value="ISOREMIT">ISOREMIT</option>
                                <option value="CAMT">CAMT</option>
                                </select></td></tr>
                                
                                
                            
					  <tr>  <td><b> Suite Name <font color="red">*</font> </b></td>
					  <td><select Name ="suiteName" id ="suiteName" required >
                      <option value ="" >Select Suite </option>
                        	 <option value="Business Validations">Business Validations</option>
                        	 <option value="Manual Action">Manual Action</option>
                         	 <option value="Sanity Suite">Sanity Suite</option>
                         	 <option value="Smoke Suite">Smoke Suite</option>
                       	     <option value="Phase III Regression Suite">Phase III Regression Suite</option>
                       	      <option value="Phase I And II E2E Suite">Phase I And II E2E Suite</option>
                        	 <option value="Phase III E2E Suite">Phase III E2E Suite</option>
                        	 <option value="Phase II Regression Suite">Phase II Regression Suite</option>
                        	 
						</select>	
						</td></tr>
                                
                   <tr >
						<td><b>Modular Testing </b></td>
						<td><input type="checkbox" id="MODULAR" name="MODULAR" value="Y" >Yes<br></td>

					
					</tr>
						
<tr><td ><b>Multiple File variations required?</b></td><td><input type='Checkbox' id="Mul_FILE_VAR_REQ" onclick="enableDisable();">Yes</td></tr>
<tr id="rules" hidden><td><b>Rules for creating variations</b></td><td><input type="checkbox" name="fileVar" value="Missing">Missing Tags<br>
                                                            <input type="checkbox" name="fileVar" value="Duplicate">Duplicate Tags<br>
                                                            <input type="checkbox" name="fileVar" value="Invalid">Invalid Tags<br>
                                                            <input type="checkbox" name="fileVar" value="Order Change">Change Order of Tags</td></tr>

					<tr>	<td ><input type="submit"
							value="Generate Formats" class='butn' /></td>
					</tr>
					
				</table>
</center>

</form>


</body>

</html>