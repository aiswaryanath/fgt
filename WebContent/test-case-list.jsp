<%@page import="java.util.Map.Entry"%>
<%@ page import= "com.intellect.service.ReadRegSuitFile "%>
<%@ page import= "com.intellect.property.FormatConstants "%>
<%@ page import= "java.util.Map"%>
<%@ page import= "java.util.List"%>
<%@ page import= "java.util.ArrayList"%>
<%@ page import= "com.intellect.dao.ReadTemplate"%>
<%@ page import= "java.net.URLDecoder"%>
<html>
<head>
<title>IntellectFlow</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
var checkedData = [];
$(document).ready(function() {
$('#select-all').click(function(event) {   
    if(this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;                        
        });
    }else{
    	$(':checkbox').each(function() {
            this.checked = false;                        
        });
    }
});
});

function validateForm(){
	if(checkedData.length==0){
		alert("You must select at least one test case!");
		return false;
	}else{
		return true;
	}
}

function storeCheckedTestCase(chkVal){
	if(document.getElementById(chkVal).checked){
		checkedData.push(chkVal);
	}else{
		const index = checkedData.indexOf(chkVal);
		checkedData.splice(index, 1);
	}
}

function checkUncheckFormatTestCases(formatName){
	var testCaseCnt=document.getElementById(formatName).value;
	if(testCaseCnt){
		for(i=0;i<=parseInt(testCaseCnt);i++){
			if(document.getElementById(formatName+"_"+i).checked){
				const index = checkedData.indexOf(formatName+"_"+i);
				checkedData.splice(index, 1);
				document.getElementById(formatName+"_"+i).checked=false;
			}else{
				checkedData.push(formatName+"_"+i);
				document.getElementById(formatName+"_"+i).checked=true;
			}
		}
	}
}
</script>
</head>

<body>
<FORM method="POST" name="formname" action ="regressionUrl" onsubmit="return validateForm()">
	 <% 
	 	
	 	String regSuiteName = URLDecoder.decode(request.getParameter("regSuiteName"),"UTF-8");
	 	//out.print("regSuiteName : "+regSuiteName);
	 	String fileName="";	
	 	Map<String,List<String>> testCases=null;
	 	ReadRegSuitFile readRegSuitFile = new ReadRegSuitFile();
	 	fileName= new ReadTemplate().getRegressionSuiteInputFile(regSuiteName);
	 	//out.print("fileName : "+fileName);
	 	testCases=readRegSuitFile.readAllTestCase(fileName);
	 	if(testCases != null && !testCases.isEmpty()){
	 		List<String> sheetNames=new ArrayList<String>();
	 %>
	 <input type="hidden" name="fileName" value="<%= fileName%>">
	 <input type="hidden" name="regSuiteName" value="<%= regSuiteName%>">
	<table>
	<tr>
	<td>
	<input type="checkbox" name="select-all" id="select-all" onclick="storeCheckedTestCase('select-all')" /><b>Select All</b>
	</td></tr>
	<%
	for(Entry<String,List<String>> entrySet : testCases.entrySet()){
		sheetNames.add(entrySet.getKey());
	}
	for(int i=0; i<sheetNames.size();){
		if(i>0){
	%>
	<tr></tr>
	<tr></tr>
	<% }%>
	<tr>
	  <td>
	  <table>
	  <tr><td><input type="checkbox"  id="<%= sheetNames.get(i) %>" 
	  		value="<%=testCases.get(sheetNames.get(i)).size()-1 %>"
	  		onclick="checkUncheckFormatTestCases('<%= sheetNames.get(i) %>')" /><b><%= sheetNames.get(i) %></b></td></tr>
	  <%
	  	for(int j=0;j<testCases.get(sheetNames.get(i)).size();j++){
	  		String testCase=testCases.get(sheetNames.get(i)).get(j);
	  		String id=sheetNames.get(i)+"_"+j;
	  %>
	  <tr><td>
	  <input type="checkbox"  name="<%= sheetNames.get(i) %>" value="<%= testCase %>" id="<%= id %>"
	  			onclick="storeCheckedTestCase('<%= id %>')"> <%= testCase %></td></tr>
	  <%	
	  	}
	  	i++;
	  %>
	  </table>
	  </td>
	  <!-- <td></td><td></td><td></td><td></td> -->
	  <% if(i<sheetNames.size()){ %>
	  <td>
	  <table>
	  <tr><td>
	  <input type="checkbox"  id="<%= sheetNames.get(i) %>" 
	  		value="<%=testCases.get(sheetNames.get(i)).size()-1 %>"
	  		onclick="checkUncheckFormatTestCases('<%= sheetNames.get(i) %>')" /><b><%= sheetNames.get(i) %></b></td></tr>
	  <% 
	  for(int j=0;j<testCases.get(sheetNames.get(i)).size();j++){
	  		String testCase=testCases.get(sheetNames.get(i)).get(j);
	  		String id=sheetNames.get(i)+"_"+j;
	  %>
	  <tr><td>
	  <input type="checkbox" class='{roles: true}' name="<%= sheetNames.get(i) %>" value="<%= testCase %>" id="<%= id %>"
	  		onclick="storeCheckedTestCase('<%= id %>')"> <%= testCase %></td></tr>
	  <%	
	  	}
	  	i++;
	  %>
	  </table>
	  </td>
	  <%} %>
	 <!--  <td></td><td></td><td></td><td></td> -->
	  <% if(i<sheetNames.size()){ %>
	  <td>
	  <table>
	  <tr><td>
	  <input type="checkbox"  id="<%= sheetNames.get(i) %>" 
	  		value="<%=testCases.get(sheetNames.get(i)).size()-1 %>"
	  		onclick="checkUncheckFormatTestCases('<%= sheetNames.get(i) %>')" /><b><%= sheetNames.get(i) %></b></td></tr>
	  <% 
	  for(int j=0;j<testCases.get(sheetNames.get(i)).size();j++){
	  		String testCase=testCases.get(sheetNames.get(i)).get(j);
	  		String id=sheetNames.get(i)+"_"+j;
	  %>
	  <tr><td>
	  <input type="checkbox" class='{roles: true}' name="<%= sheetNames.get(i) %>" value="<%= testCase %>" id="<%= id %>"
	  		onclick="storeCheckedTestCase('<%= id %>')"> <%= testCase %></td></tr>
	  <%	
	  	}
	  	i++;
	  %>
	  </table>
	  </td>
	  <%} %>
	 </tr>
  <% } %>
  </table>
  <br><br><br/>
  <input type="submit" value="Submit">
  <%} %>
</form>
</body>

</html>