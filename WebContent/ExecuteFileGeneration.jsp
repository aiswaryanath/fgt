<%@page import="java.util.Map.Entry"%>
<%@ page import= "java.util.Map"%>
<%@ page import= "com.intellect.dao.ReadTemplate"%>
<html>
<style>
.resp {
   float: center;
   margin: 10px;
   padding: 10px;
   border: 0px solid black;
    
   position: relative;
}
</style>
	<script type="text/javascript">
        function call()
		{
           
 			var types = document.getElementsByName('regType');
 			var regSuiteName;
 			for(var i = 0; i < types.length; i++){
 			    if(types[i].checked){
 			    	regSuiteName = types[i].value;
 			    }
 			}
 			
 			if(regSuiteName){
 				 window.location.replace("test-case-list.jsp?regSuiteName="+regSuiteName);
 			}else{
 				alert("Please select regression suite.");
 			}
        }
        
        function get_action() {
            return tranUrl;
        }
    </script>
    
	<body>
	
	<form >
	<div class="resp">
		<h2 align="center">File Generation Tool</h2>
	</div>
	<div class="resp">
		<table style="width:50%"  border="1" align="center">
		<%
			ReadTemplate readTemplate = new ReadTemplate();
			Map<String,String> data=readTemplate.readRegressionSuitesConfigData();
			for(Entry<String,String> entrySet : data.entrySet()){%>
				<tr>
				<td>
					  <input type="radio" name="regType" value="<%=entrySet.getKey() %>" ><%=entrySet.getKey() %><br>
				</td>
			</tr>
			<%}
		%>			
		<tr align="center">
			<td>
				<input type="button" value="Next" onclick='call()' style="font-size:10pt;color:white;background-color:blue;border:2px solid #336600;padding:3px">
			</td>
		</tr>
		</table>
	</div>
	   
	</form>
	</body>
</html>