/********************************************************************************************************/
/* Copyright �  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/* 													 													*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : FormatConstants.java                                           					    */
/*                                                                                      				*/
/********************************************************************************************************/
/*  			 Author					         |        	  Date   			|	  Version           */
/********************************************************************************************************/
/* 	         Veera Kumar Reddy.V  		         |			20:05:2016			|		1.0	            */
/********************************************************************************************************/
/* 																									    */
/* 	Description  : To declare all Constant values and Getting all property file values.                 */
/*        																								*/
/********************************************************************************************************/

package com.intellect.property;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class FormatConstants {

	// 9206 706 806
	static Properties prop;

	public static String tempFileName;
	public static String inputFile;
	public static final String Mandatory;
	public static final String outputFile;
	public static final String tmpSheetName;
	public static final String swiftSheetName;
	public static final String ansiSheetName;
	public static final String pain1SheetName;
	public static final String pain8SheetName;
	public static final String InteracSheetName;
	public static final String IsoRemitSheetName;
	//jo changed here
	public static final String CAMT_SHEET_NAME;
	public static final String PAIN8_REMIT_SHEET_NAME;
	public static final String PAIN1_REMIT_SHEET_NAME;
	public static final String folderNameSheetName;
	public static final String fieldsRowNo;
	public static final String painFieldsRowNo;
	public static final String ansiFieldsRowNo;
	public static final String dbTableName;
	public static final String fileNamesDbName;
	public static final String auditTableName;
	public static final String recordsStartRow;
	public static final String swiftRecordsStartRow;
	public static final String ansiRecordsStartRow;
	public static final String pain1RecordsStartedRowNo;
	public static final String Pain8RecordsStartedRowNo;
	public static final String CAMTStartedRowNo;
	public static final String formatColumn;
	public static final String sectionRow;
	public static final String dbDriver;
	public static final String dbUrl;
	public static final String dbUser;
	public static final String dbPwd;
	public static final String cibc1464 = "CIBC1464";
	public static final String nachaIat94 = "NACHAIAT94BYTE";
	public static final String cibc80Byte = "CIBC80BYTE";
	public static final String cpa005 = "CPA005";
	public static final String nacha94 = "NACHA94";
	public static final String mt101 = "MT101";
	public static final String mt104 = "MT104";
	public static final String cmo103 = "CMO103";
	public static final String pain001 = "Pain001v03";
	public static final String pain008 = "Pain008v02";
	public static final String x820 = "X820";
	public static final String pain001remit = "pain001cnr";
	public static final String pain008remit = "pain008cnr";
	public static final String interac = "EMTXML";
	public static final String isomerit = "remt001v02";
	public static final String ISOPAIN001 = "ISOPAIN001";
	public static final String ISOPAIN008 = "ISOPAIN008";
	public static final String Interacxml = "Interacxml";
	public static final String ISO_REMITxml = "ISO_REMITxml";
	public static final String ISOPAIN001_Remit = "ISOPAIN001_Remit";
	public static final String ISOPAIN008_Remit = "ISOPAIN008_Remit";
	//jo added here
	public static final String CAMT = "Camt055v07";
	public static final String ALL = "all";
	public static String inputFileCompleteRegression;
	public static String inputFileModulerRegression;
	public static String inputFileMiniRegression;
	public static String inputFileSpecificRegression;
	public static final String swiftAppliedRulePosition;
	public static final String nativeAppliedRulePosition;
	public static final String ansiAppliedRulePosition;
	public static final String pain001AppliedRulePosition;
	public static final String pain008AppliedRulePosition;
	public static final String CAMTAppliedRulePosition;

	public static final String swiftModularPosition;
	public static final String nativeModularPosition;
	public static final String ansiModularPosition;
	public static final String pain001ModularPosition;
	public static final String pain008ModularPosition;
	//JO ADDED HERE
	public static final String CAMTModularPosition;

	public static final String pain1remitModularPosition;
	public static final String pain8remitModularPosition;
	public static final String isoremitModularPosition;
	public static final String interacModularPosition;

	public static final String interacAppliedRulePosition;
	public static final String isoremitAppliedRulePosition;
	public static final String pain8remitAppliedRulePosition;
	public static final String pain1remitAppliedRulePosition;

	public static final String ftsChannel;
	public static final String scaChannel;
	public static final String fttChannel;
	public static final String ndmChannel;
	public static final String FILE = "FILE";
	public static final String BATCH = "BATCH";
	public static final String TRANSACTION = "TRANSACTION";
	public static final Map<String,Integer> sheetwiseReadIndex = new HashMap<String,Integer>();
	public static final Map<String,Integer> sheetwiseModularPosition = new HashMap<String,Integer>();
	public static final String testcaseTableName;
	public static final String testcaseAuditTableName;
	public static final String testSuiteConfigTable;
	public static final String MOVING_SCRIPT_FILE_NAME ;
	public static final String MOVING_SCRIPT_FILE_EXECUTION_PATH ;
	public static final String shellScriptOn;
	static {
		prop = new Properties();
		try {
			prop = PropertyFileReading
					.getProperties("/properties/formats_property_file.properties");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	static {
		MOVING_SCRIPT_FILE_NAME = prop.getProperty("MOVING_SCRIPT_FILE_NAME");
		MOVING_SCRIPT_FILE_EXECUTION_PATH = prop.getProperty("MOVING_SCRIPT_FILE_EXECUTION_PATH");
		tempFileName = "tmp";
		inputFile = prop.getProperty("INPUT_FILE");
		outputFile = prop.getProperty("OUTPUT_FILE");
		tmpSheetName = prop.getProperty("TEMPLATE_SHEET_NAME");
		swiftSheetName = prop.getProperty("SWIFT_SHEET_NAME");
		ansiSheetName = prop.getProperty("ANSI_SHEET_NAME");
		pain1SheetName = prop.getProperty("PAIN1_SHEET_NAME");

		pain8SheetName = prop.getProperty("PAIN8_SHEET_NAME");
		InteracSheetName = prop.getProperty("INTRAC_SHEET_NAME");
		IsoRemitSheetName = prop.getProperty("ISOREMIT_SHEET_NAME");
		
		//Jo-Changed here
		CAMT_SHEET_NAME = prop.getProperty("CAMT_SHEET_NAME");
		PAIN8_REMIT_SHEET_NAME = prop.getProperty("PAIN8_REMIT_SHEET_NAME");
		PAIN1_REMIT_SHEET_NAME = prop.getProperty("PAIN1_REMIT_SHEET_NAME");
		inputFileCompleteRegression = prop.getProperty("INPUT_FILE_COMPLETE");
		inputFileModulerRegression = prop.getProperty("INPUT_FILE_MODULER");
		inputFileMiniRegression = prop.getProperty("INPUT_FILE_MINI");
		inputFileSpecificRegression = prop.getProperty("INPUT_FILE_SPECIFIC");
		folderNameSheetName = prop.getProperty("FOLDER_NAME_SHEET_NAME");
		fieldsRowNo = prop.getProperty("FIELDS_ROW_NO");
		shellScriptOn=prop.getProperty("SHELL_SCRIPT_ON");
		painFieldsRowNo = prop.getProperty("PAIN_FIELDS_ROW_NO");
		ansiFieldsRowNo = prop.getProperty("ANSI_FIELDS_ROW_NO");
		dbTableName = prop.getProperty("DB_TABLE_NAME");
		fileNamesDbName = prop.getProperty("FILE_NAMES_DB_NAME");
		auditTableName = prop.getProperty("AUDIT_TABLE_NAME");
		testcaseTableName = prop.getProperty("TEST_CASE_TABLE_NAME");
		testSuiteConfigTable= prop.getProperty("TEST_SUITE_CONFIG_TABLE");
		testcaseAuditTableName = prop.getProperty("TEST_CASE_AUDIT_TABLE_NAME");
		recordsStartRow = prop.getProperty("RECORDS_STARTED_ROW_NO");
		swiftRecordsStartRow = prop.getProperty("SWIFT_RECORDS_STARTED_ROW_NO");
		ansiRecordsStartRow = prop.getProperty("ANSI_RECORDS_STARTED_ROW_NO");
		pain1RecordsStartedRowNo = prop
				.getProperty("PAIN1_RECORDS_STARTED_ROW_NO");
		Pain8RecordsStartedRowNo = prop
				.getProperty("");
		//JO ADDED HRE
		CAMTStartedRowNo = prop
				.getProperty("CAMT_STARTED_ROW_NO");
		
		formatColumn = prop.getProperty("FORMAT_NEEDED_COLUMN_NO");
		sectionRow = prop.getProperty("SECTION_ROW_NO");

		swiftAppliedRulePosition = prop
				.getProperty("SWIFT_APPLIED_RULE_POSITION");
		nativeAppliedRulePosition = prop
				.getProperty("NATIVE_APPLIED_RULE_POSITION");
		ansiAppliedRulePosition = prop
				.getProperty("ANSI_APPLIED_RULE_POSITION");
		pain001AppliedRulePosition = prop
				.getProperty("PAIN001_APPLIED_RULE_POSITION");
		pain008AppliedRulePosition = prop
				.getProperty("PAIN008_APPLIED_RULE_POSITION");
		//JO CHANGED HERE
		CAMTAppliedRulePosition = prop
				.getProperty("CAMT_APPLIED_RULE_POSITION");

		swiftModularPosition = prop.getProperty("SWIFT_MODULAR_POSITION");
		nativeModularPosition = prop.getProperty("NATIVE_MODULAR_POSITION");
		ansiModularPosition = prop.getProperty("ANSI_MODULAR_POSITION");
		pain001ModularPosition = prop.getProperty("PAIN001_MODULAR_POSITION");
		pain008ModularPosition = prop.getProperty("PAIN008_MODULAR_POSITION");
		//JO ADDED HERE
		CAMTModularPosition = prop.getProperty("CAMT_MODULAR_POSITION");

		pain1remitModularPosition = prop
				.getProperty("PAIN001REMIT_MODULAR_POSITION");
		pain8remitModularPosition = prop
				.getProperty("PAIN008REMIT_MODULAR_POSITION");
		isoremitModularPosition = prop.getProperty("ISOREMIT_MODULAR_POSITION");
		interacModularPosition = prop.getProperty("INTERAC_MODULAR_POSITION");

		interacAppliedRulePosition = prop
				.getProperty("INTERAC_APPLIED_RULE_POSITION");
		isoremitAppliedRulePosition = prop
				.getProperty("ISOREMIT_APPLIED_RULE_POSITION");
		pain8remitAppliedRulePosition = prop
				.getProperty("PAIN008REMIT_APPLIED_RULE_POSITION");
		pain1remitAppliedRulePosition = prop
				.getProperty("PAIN001REMIT_APPLIED_RULE_POSITION");

		dbDriver = prop.getProperty("dbDriver");
		dbUrl = prop.getProperty("oracle.dburl");
		dbUser = prop.getProperty("dbUSER");
		dbPwd = prop.getProperty("dbPWD");

		ftsChannel = "fts";
		scaChannel = "sca";
		fttChannel = "ftt";
		ndmChannel = "ndm";
		Mandatory = "M";
		
		sheetwiseReadIndex.put(tmpSheetName, Integer.parseInt(recordsStartRow));
		sheetwiseReadIndex.put(swiftSheetName, Integer.parseInt(swiftRecordsStartRow));
		sheetwiseReadIndex.put(pain1SheetName, Integer.parseInt(pain1RecordsStartedRowNo));
		sheetwiseReadIndex.put(pain8SheetName, Integer.parseInt(pain1RecordsStartedRowNo));
		sheetwiseReadIndex.put(PAIN8_REMIT_SHEET_NAME, Integer.parseInt(pain1RecordsStartedRowNo));
		sheetwiseReadIndex.put(PAIN1_REMIT_SHEET_NAME, Integer.parseInt(pain1RecordsStartedRowNo));
		sheetwiseReadIndex.put(ansiSheetName, Integer.parseInt(ansiRecordsStartRow));
		sheetwiseReadIndex.put(InteracSheetName, Integer.parseInt(pain1RecordsStartedRowNo));
		sheetwiseReadIndex.put(IsoRemitSheetName, Integer.parseInt(pain1RecordsStartedRowNo));
		//JO ADDED HERE
		sheetwiseReadIndex.put(CAMT_SHEET_NAME, Integer.parseInt(CAMTStartedRowNo));
		
		
		
		sheetwiseModularPosition.put(tmpSheetName, Integer.parseInt(nativeModularPosition));
		sheetwiseModularPosition.put(swiftSheetName, Integer.parseInt(swiftModularPosition));
		sheetwiseModularPosition.put(pain1SheetName, Integer.parseInt(pain001ModularPosition));
		sheetwiseModularPosition.put(pain8SheetName, Integer.parseInt(pain008ModularPosition));
		sheetwiseModularPosition.put(PAIN8_REMIT_SHEET_NAME, Integer.parseInt(pain8remitModularPosition));
		sheetwiseModularPosition.put(PAIN1_REMIT_SHEET_NAME, Integer.parseInt(pain1remitModularPosition));
		sheetwiseModularPosition.put(ansiSheetName, Integer.parseInt(ansiModularPosition));
		sheetwiseModularPosition.put(InteracSheetName, Integer.parseInt(interacModularPosition));
		sheetwiseModularPosition.put(IsoRemitSheetName, Integer.parseInt(isoremitModularPosition));
		//JO ADDED HERE
		sheetwiseModularPosition.put(CAMT_SHEET_NAME, Integer.parseInt(CAMTModularPosition));
	}
}
