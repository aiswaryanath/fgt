/********************************************************************************************************/
/* Copyright �  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/* 													 													*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : PropertyFileReading.java                                           				    */
/*                                                                                      				*/
/********************************************************************************************************/
/*  			 Author					         |        	  Date   			|	  Version           */
/********************************************************************************************************/
/* 	         Veera Kumar Reddy.V  		         |			20:05:2016			|		1.0	            */
/********************************************************************************************************/
/* 																									    */
/* 	Description  : To Load property file.                       						                */
/*        																								*/
/********************************************************************************************************/

package com.intellect.property;

import java.util.Properties;

public class PropertyFileReading {

	/**
	 * @param args
	 */
	public PropertyFileReading() {
	}

	public static Properties getProperties(String fileName) throws Exception {
		Properties prop = new Properties();
		try {
			// returns the ClassLoader object associated with this Class
			ClassLoader clasLoader = Class.forName(
					"com.intellect.property.PropertyFileReading")
					.getClassLoader();
			prop.load(clasLoader.getResourceAsStream(fileName));

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return prop;
	}
}