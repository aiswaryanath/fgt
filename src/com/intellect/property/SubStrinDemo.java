package com.intellect.property;

public class SubStrinDemo {
	
	private String build80LineString(String str){
		String retString = null;
		StringBuffer stringBuffer = new StringBuffer();
		int i = 0;
		int j =80;
		int strLength = str.length();
		if (strLength > 80) {
			while (strLength > 80) {
				stringBuffer.append(str.substring(i, j));//(0,80),(81,161)
				stringBuffer.append("\n");
				strLength = strLength - 80;
				i = i+ 80;
				j = i + 80;
			}
		} else if(strLength < 80) {
			j = strLength;
			stringBuffer.append(str.substring(i, j));
		}
		retString = stringBuffer.toString();
		return retString;
	}
	public static void main(String[] args) {
		String str = "ISA*00*     *00*Sec1Info34*ZZ*BILLCATTXXXXXXX*ZZ*CIBCCATTXXXXXXX*160922*1415*U*00401*820000014*1*P*!&GS*RA*BILLCATTXXXXXXX*CIBCCATTXXXXXXX*20160922*201001*820001001*X*004010&ST*820*TXNREF001&BPR*C*100*D*ACH*CBC*04*11042*DA*1111042*1480000011*001000412*04*021998765*DA*000036532724*20160902&NTE*SPH*200&TRN*1*ANSI31AUG001&CUR*BK*USD*1.0456&REF*EF*SALARY*DEALREF001&N1*PR*BILL PAY CEU Auto&N2*199 Bay Street &N3*Commerce Court West &N4*TORONTO*ON*M5L1A2*CA&N1*PE*LAXMAN PALLIKONDA&N2*MUCHKUR CHOWK&N3*BHEEMGAL STREET&N4*TELANGANA*TN*10013*IN&ENT*ENTNO1&RMR*IV*ANSI820EDI001*PI*100*100*1.00&REF*PO*100000820*3232&ADX*100*01&NTE*SPH*SAL&SE*19*TXNREF001&GE*1*820001001&IEA*1*820000014&";
		SubStrinDemo subStrinDemo = new SubStrinDemo();
		System.out.println(subStrinDemo.build80LineString(str));
	}
}
