package com.intellect.property;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.intellect.dao.ReadTemplate;

public class JulianDate {

	public static String convertToJulian() throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
		Calendar c = Calendar.getInstance();
		String dateOutput = dateFormat.format(new Date());
		// System.out.println("jULIAN DATE CONVERTER1 ::"+dateOutput);
		int year = c.get(Calendar.YEAR);
		String syear = String.format("%04d", year).substring(2);
		// System.out.println("jULIAN DATE CONVERTER2 ::"+syear);
		int century = Integer.parseInt(String.valueOf((year)).substring(2));
		// System.out.println("jULIAN DATE CONVERTER3 ::"+century);
		c.setTime(dateFormat.parse(dateOutput));
		dateOutput = dateFormat.format(c.getTime()); // dt is now the new date
		// System.out.println("jULIAN DATE CONVERTER4 ::"+dateOutput);
		return ReadTemplate.dateToJulian(dateOutput);
	}

	public static String convertDate() throws ParseException {
		String DATE_FORMAT = "yyMMdd";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		Calendar c1 = Calendar.getInstance(); // today
		return sdf.format(c1.getTime());
	}

	public static void main(String args[]) throws ParseException {
		convertToJulian();

	}
}
