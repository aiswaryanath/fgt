package com.intellect.pojo;

import com.sun.jmx.snmp.Timestamp;

public class TestToolFileNamePOJO {

	private String testCaseid;
	private String formatName;
	private String channel;
	private String lsi;
	private String clientRefNum;
	private String bicCode;
	private String bbdId;
	private String custRef;
	private String uniqueId;
	private String fileName;
	private String timeStamp ;
	private String appliedRules;
	private String errorCodes;
	private String modifiedField;
	private String folderName;
	private String suiteName;
	
	
	public String getTestCaseid() {
		return testCaseid;
	}
	public void setTestCaseid(String testCaseid) {
		this.testCaseid = testCaseid;
	}
	public String getFormatName() {
		return formatName;
	}
	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getLsi() {
		return lsi;
	}
	public void setLsi(String lsi) {
		this.lsi = lsi;
	}
	public String getClientRefNum() {
		return clientRefNum;
	}
	public void setClientRefNum(String clientRefNum) {
		this.clientRefNum = clientRefNum;
	}
	public String getBicCode() {
		return bicCode;
	}
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}
	public String getBbdId() {
		return bbdId;
	}
	public void setBbdId(String bbdId) {
		this.bbdId = bbdId;
	}
	public String getCustRef() {
		return custRef;
	}
	public void setCustRef(String custRef) {
		this.custRef = custRef;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getAppliedRules() {
		return appliedRules;
	}
	public void setAppliedRules(String appliedRules) {
		this.appliedRules = appliedRules;
	}
	public String getErrorCodes() {
		return errorCodes;
	}
	public void setErrorCodes(String errorCodes) {
		this.errorCodes = errorCodes;
	}
	public String getModifiedField() {
		return modifiedField;
	}
	public void setModifiedField(String modifiedField) {
		this.modifiedField = modifiedField;
	}
	public String getFolderName() {
		return folderName;
	}
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	public String getSuiteName() {
		return suiteName;
	}
	public void setSuiteName(String suiteName) {
		this.suiteName = suiteName;
	}
	@Override
	public String toString() {
		return "TestToolFileNameDao [testCaseid=" + testCaseid
				+ ", formatName=" + formatName + ", channel=" + channel
				+ ", lsi=" + lsi + ", clientRefNum=" + clientRefNum
				+ ", bicCode=" + bicCode + ", bbdId=" + bbdId + ", custRef="
				+ custRef + ", uniqueId=" + uniqueId + ", fileName=" + fileName
				+ ", timeStamp=" + timeStamp + ", appliedRules=" + appliedRules
				+ ", errorCodes=" + errorCodes + ", modifiedField="
				+ modifiedField + ", folderName=" + folderName + ", suiteName="
				+ suiteName + "]";
	}
	
	
}
