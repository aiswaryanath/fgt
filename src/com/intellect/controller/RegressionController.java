/********************************************************************************************************/
/* Copyright @  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/*				 									 													*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : AllFormatsGenerationController.java                                 					*/
/*                                                                                      				*/
/********************************************************************************************************/
/* 				 Author					          |        	  Date   			|	  Version           */
/********************************************************************************************************/
/*         	Surendra Reddy.M 	             	  |			20:05:2016			|		1.0	            */
/*			Upendar CH							  |         18:07:2017          |  
/********************************************************************************************************/
/* 																										*/
/* 	Description  : Main Controller 																		*/
/*        			 																					*/
/********************************************************************************************************/

package com.intellect.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.intellect.dao.CallShellScriptUtil;
import com.intellect.dao.ReadTemplate;
import com.intellect.db.DBUtility;
import com.intellect.property.FormatConstants;
import com.intellect.service.FormatGenerateService;
import com.intellect.service.UpdateFormatXls;
import com.intellect.service.UpdateRegSuitFile;

public class RegressionController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8122195968427646769L;
	private static org.apache.log4j.Logger log = Logger
			.getLogger(RegressionController.class);

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("Hello this is GET Method");
		}
		process(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("Hello this is POST Method");
		}
		process(req, res);
	}

	public void process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("Hello this is process Method");
		}
		response.setContentType("text/html");
		String fileName = null, error = "error";
		String modular="N";
		String regSuiteName = null;
		char[] variants = null;
		Connection connection = null;
		ReadTemplate readTemplate = new ReadTemplate();
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
				.format(Calendar.getInstance().getTime());
		try {

			regSuiteName = request.getParameter("regSuiteName");
			if(variants==null){
				 variants = new char[0];
				
				}
			List<String> selectedItems= new ArrayList<String>();
			fileName=request.getParameter("fileName") ;
			String[] nativeTestCases=request.getParameterValues(FormatConstants.tmpSheetName);
			if(nativeTestCases == null){
				nativeTestCases = new String[]{};
			}else{
				selectedItems.add(FormatConstants.tmpSheetName);
			}
			String[] swiftTestCases=request.getParameterValues(FormatConstants.swiftSheetName);
			if(swiftTestCases == null){
				swiftTestCases = new String[]{};
			}else{
				selectedItems.add(FormatConstants.swiftSheetName);
			}
			String[] pain1TestCases=request.getParameterValues(FormatConstants.pain1SheetName);
			if(pain1TestCases == null){
				pain1TestCases = new String[]{};
			}else{
				selectedItems.add(FormatConstants.pain1SheetName);
			}
			String[] pain8TestCases=request.getParameterValues(FormatConstants.pain8SheetName);
			if(pain8TestCases == null){
				pain8TestCases = new String[]{};
			}else{
				selectedItems.add(FormatConstants.pain8SheetName);
			}
			String[] pain8RemitTestCases=request.getParameterValues(FormatConstants.PAIN8_REMIT_SHEET_NAME);
			if(pain8RemitTestCases == null){
				pain8RemitTestCases = new String[]{};
			}else{
				selectedItems.add(FormatConstants.PAIN8_REMIT_SHEET_NAME);
			}
			//jo added here
			String[] CAMTTestCases=request.getParameterValues(FormatConstants.CAMT_SHEET_NAME);
			if(CAMTTestCases == null){
				CAMTTestCases = new String[]{};
			}else{
				selectedItems.add(FormatConstants.CAMT_SHEET_NAME);
			}
			String[] pain1RemitTestCases=request.getParameterValues(FormatConstants.PAIN1_REMIT_SHEET_NAME);
			if(pain1RemitTestCases == null){
				pain1RemitTestCases = new String[]{};
			}else{
				selectedItems.add(FormatConstants.PAIN1_REMIT_SHEET_NAME);
			}
			String[] ansiTestCases=request.getParameterValues(FormatConstants.ansiSheetName);
			if(ansiTestCases == null){
				ansiTestCases = new String[]{};
			}else{
				selectedItems.add(FormatConstants.ansiSheetName);
			}
			String[] interacTestCases=request.getParameterValues(FormatConstants.InteracSheetName);
			if(interacTestCases == null){
				interacTestCases = new String[]{};
			}else{
				selectedItems.add(FormatConstants.InteracSheetName);
			}
			String[] isoRemitTestCases=request.getParameterValues(FormatConstants.IsoRemitSheetName);
			if(isoRemitTestCases == null){
				isoRemitTestCases = new String[]{};
			}else{
				selectedItems.add(FormatConstants.IsoRemitSheetName);
			}
			
			Map<String,List<String>> selectedTestCase = new HashMap<String,List<String>>();
			selectedTestCase.put(FormatConstants.tmpSheetName, Arrays.asList(nativeTestCases));
			selectedTestCase.put(FormatConstants.swiftSheetName, Arrays.asList(swiftTestCases));
			selectedTestCase.put(FormatConstants.pain1SheetName, Arrays.asList(pain1TestCases));
			selectedTestCase.put(FormatConstants.pain8SheetName, Arrays.asList(pain8TestCases));
			selectedTestCase.put(FormatConstants.PAIN8_REMIT_SHEET_NAME, Arrays.asList(pain8RemitTestCases));
			selectedTestCase.put(FormatConstants.PAIN1_REMIT_SHEET_NAME, Arrays.asList(pain1RemitTestCases));
			selectedTestCase.put(FormatConstants.ansiSheetName, Arrays.asList(ansiTestCases));
			selectedTestCase.put(FormatConstants.InteracSheetName, Arrays.asList(interacTestCases));
			selectedTestCase.put(FormatConstants.IsoRemitSheetName, Arrays.asList(isoRemitTestCases));
			//jo changed here
			selectedTestCase.put(FormatConstants.CAMT_SHEET_NAME, Arrays.asList(CAMTTestCases));
			if(!selectedTestCase.isEmpty()){
				UpdateRegSuitFile updateRegSuitFile = new UpdateRegSuitFile();
				updateRegSuitFile.updateFGTFlag(fileName, selectedTestCase);
				modular="Y";
			}

			RequestDispatcher requestDispatcher = null;

			if (log.isDebugEnabled()) {
				log.debug("Input file  :" + fileName);
			}
			
			FormatGenerateService generateFormatService = new FormatGenerateService();
			//added by abhishek

			UpdateFormatXls objUpdateFormatXls  = new UpdateFormatXls();


			objUpdateFormatXls.updateFilesXls(fileName, FormatConstants.ALL);
			
			int returnValue = generateFormatService.generateFilesService(
					fileName,selectedItems ,modular,variants,regSuiteName);
			
			if (log.isDebugEnabled()) {
				log.debug("return value:" + returnValue);
			}
			connection = DBUtility.openConnection();
			String filePath = fileName.substring(0,fileName.lastIndexOf("/")); 
			if (returnValue == 1) {
				if(FormatConstants.shellScriptOn.equalsIgnoreCase("Y")){
					String pScriptFileName =FormatConstants.MOVING_SCRIPT_FILE_NAME;
					String pScriptPath =FormatConstants.MOVING_SCRIPT_FILE_EXECUTION_PATH;
					log.debug("script filename:::: "+pScriptFileName+", scriptfilepath :::: "+pScriptPath);
					System.out.println("script filename:::: "+pScriptFileName+",scriptfilepath :::: "+pScriptPath);
					CallShellScriptUtil.callShellScript(pScriptFileName, pScriptPath);
					if (log.isDebugEnabled()) {
						log.debug("Successfully Generated");
					}
				}
				for (Map.Entry<String, List<String>> me : selectedTestCase.entrySet()) {
					List<String> valueList = me.getValue();
					System.out.println("valueList ::"+valueList);
					if(valueList != null && !valueList.isEmpty()){
						for (String selectedItem : valueList) {
							System.out.println("selectedItem ::"+selectedItem);
							readTemplate.insertIntoTestCasesAuditTable(selectedItem,regSuiteName,fileName,filePath,timeStamp,connection);
						}
					}
				}
				requestDispatcher = request.getRequestDispatcher("success.jsp");
				requestDispatcher.forward(request, response);
			} else if (returnValue == -1) {
				if (log.isDebugEnabled()) {
					log.debug("Error page");
				}
				requestDispatcher = request.getRequestDispatcher("error.jsp");
				request.setAttribute(error,"1. Please Check Property File(Any Property value is null/Empty)");
				requestDispatcher.forward(request, response);
			} else if (returnValue == -2) {
				if (log.isDebugEnabled()) {
					log.debug("Error page");
				}
				requestDispatcher = request.getRequestDispatcher("error.jsp");
				request.setAttribute(error,"1. Your Selected Template( Excel ) File does not have Your required Sheets. Pls Check");
				requestDispatcher.forward(request, response);
			} else if (returnValue == -3) {
				if (log.isDebugEnabled()) {
					log.debug("Error page");
				}
				requestDispatcher = request.getRequestDispatcher("error.jsp");
				request.setAttribute(error,"1. Your Selected Template( Excel ) File does not have Records (or) Pls check in Property file mismatch Record Started Row number");
				requestDispatcher.forward(request, response);
			} else if (returnValue == -4) {
				if (log.isDebugEnabled()) {
					log.debug("Error page");
				}
				requestDispatcher = request.getRequestDispatcher("error.jsp");
				request.setAttribute(error,"1. Format Name is null in Data Base");
				requestDispatcher.forward(request, response);
			} else if (returnValue == -5) {
				if (log.isDebugEnabled()) {
					log.debug("Error page");
				}
				requestDispatcher = request.getRequestDispatcher("error.jsp");
				request.setAttribute(error,"1. Your Selected Template( Excel ) File does not have Fields Names Data (or) Pls check in Property file mismatch Fields Row number ");
				requestDispatcher.forward(request, response);
			} else if (returnValue == -6) {
				if (log.isDebugEnabled()) {
					log.debug("Error page");
				}
				requestDispatcher = request.getRequestDispatcher("error.jsp");
				request.setAttribute(error,"1. Your Selected Template( Excel ) File does not have Section Names Data (or) Pls check in Property file mismatch Section Row number");
				requestDispatcher.forward(request, response);
			} else if (returnValue == -7) {
				if (log.isDebugEnabled()) {
					log.debug("Error page");
				}
				requestDispatcher = request.getRequestDispatcher("error.jsp");
				request.setAttribute(error,"1. Sheet Names are not Matched with property file details <br> 2. Your Selected Template( Excel ) File does not have Fields Names Data, Section Names Data and Records Data (or) Pls check in Property file mismatch data. <br> 3. Your selected wrong Template file.");
				requestDispatcher.forward(request, response);
			} else if (returnValue == -8) {
				if (log.isDebugEnabled()) {
					log.debug("Error page");
				}
				requestDispatcher = request.getRequestDispatcher("error.jsp");
				request.setAttribute(error,"1. Your selected Template file Entire row is EMPTY pls check in Template file. (for ex: File Header, Detail Record, File Trailer,Please Select Correct File" +
						")");
				requestDispatcher.forward(request, response);
			} else if (returnValue == -9) {
				if (log.isDebugEnabled()) {
					log.debug("Error page");
				}
				requestDispatcher = request.getRequestDispatcher("error.jsp");
				request.setAttribute(error,"1. Please write Folder Name in Config Sheet.");
				requestDispatcher.forward(request, response);
			} else if (returnValue == -10) {
				if (log.isDebugEnabled()) {
					log.debug("Error page");
				}
				requestDispatcher = request.getRequestDispatcher("error.jsp");
				request.setAttribute(error,"1. DB Connection returns Null.");
				requestDispatcher.forward(request, response);
			}
			else {
				if (log.isDebugEnabled()) {
					log.debug("Failure state");
				}
			}
			if (log.isDebugEnabled()) {
				log.debug("Failure state");
				log.debug("Please Uploaded Excel File only.");
			}
		} catch (FileNotFoundException e) {
			if (log.isDebugEnabled()) {
				System.out.println("Message when file not found");
				log.error("FileNotFoundException While loading the Excel file in Controller class");
			}
			log.fatal("Exception ",e); //FG_1.1 (S)
		} catch (Exception e) {

			RequestDispatcher requestDispatcher = null;
			String message = null;

			StringWriter errors = new StringWriter();

			e.printStackTrace(new PrintWriter(errors));

			message = errors.toString();
			System.out.println("Message:::"+message);
			requestDispatcher = request.getRequestDispatcher("failure.jsp?message="+message);
			requestDispatcher.forward(request, response);
		     
			if (log.isDebugEnabled()) {
				log.error("Exception While loading the Excel file in Controller class");
			}
			log.fatal("Exception ",e);//FG_1.1 (S)
		}finally {
			DBUtility.closeConnection(connection);
		}
	}
}