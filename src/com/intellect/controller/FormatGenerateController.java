/********************************************************************************************************/
/* Copyright @  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/*				 									 													*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : AllFormatsGenerationController.java                                 					*/
/*                                                                                      				*/
/********************************************************************************************************/
/* 				 Author					          |        	  Date   			|	  Version           */
/********************************************************************************************************/
/*         	Surendra Reddy.M 	             	  |			20:05:2016			|		1.0	            */
/*			Upendar CH							  |         18:07:2017          |  		1.1
 * 			Gokaran Tiwari						  |			22:03:2018			|		1.2 Regression 
 * 																						Suite implementation
 */
 /********************************************************************************************************/
/* 																										*/
/* 	Description  : Main Controller 																		*/
/*        			 																					*/
/********************************************************************************************************/

package com.intellect.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.intellect.dao.CallShellScriptUtil;
import com.intellect.property.FormatConstants;
import com.intellect.service.FormatGenerateService;
import com.intellect.service.UpdateFormatXls;

public class FormatGenerateController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5114633772148314167L;
	private static org.apache.log4j.Logger log = Logger
			.getLogger(FormatGenerateController.class);

//	public void doGet(HttpServletRequest req, HttpServletResponse res)
//			throws ServletException, IOException {
//	//	if (log.isDebugEnabled()) {
//			log.info("Hello this is GET Method");
//		//}
//		process(req, res);
//	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
//		if (log.isDebugEnabled()) {
			log.info("Hello this is POST Method");
			System.out.println("Hello this is POST Method");
//		}
		process(req, res);
	}

	public void process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		if (log.isDebugEnabled()) {
			log.info("Hello this is process Method");
//		}
		response.setContentType("text/html");
		String fileName = null, error = "error";
		String modular = "N";
		String tempModular = null;
		try {
			// For uploading purpose of input file
			ServletFileUpload servletFileUpload = new ServletFileUpload(
					new DiskFileItemFactory());
			List<FileItem> fileItemsList = servletFileUpload.parseRequest(request);
			FileItem fileItem = (FileItem) fileItemsList.get(0);
			System.out.println(fileItem);
			log.info("FILETEM ash:"+fileItem);
			FileItem fileItem2 = (FileItem) fileItemsList.get(1);
			System.out.println("FILEITEM2"+fileItem2);
			log.info("FILEITEM2 ash:"+fileItem2);
			// Change 1.2
			String suiteName="";
			if(fileItemsList.size()>2){
				suiteName=fileItemsList.get(2).getString();
			}
			Iterator<FileItem> iter = fileItemsList.iterator();

			while (iter.hasNext()) {
				FileItem item = iter.next();

				if (item.isFormField()) {
					String name = item.getFieldName();
					String value = item.getString();
					if (name.equals("MODULAR")) {
						modular = value;
						tempModular = value;
						log.debug("modular=====" + modular);
					}else if(name.equalsIgnoreCase("suiteName")){ // Change 1.2
						iter.remove();
					}

				}
			}
			// FG_1.1 (S)
			char[] variants = null;
			if (tempModular == null) {
				variants = new char[fileItemsList.size() - 2];
				for (int i = 2; i < fileItemsList.size(); i++) {
					variants[i - 2] = ((FileItem) fileItemsList.get(i))
							.getString().charAt(0);

				}
			} else {
				variants = new char[fileItemsList.size() - 3];
				for (int i = 3; i < fileItemsList.size(); i++) {
					variants[i - 3] = ((FileItem) fileItemsList.get(i))
							.getString().charAt(0);
				}
			}
			// FG_1.1 (E)

			String tempFileName = FormatConstants.tempFileName;
			String inputFile_Path = FormatConstants.inputFile;
			RequestDispatcher requestDispatcher = null;

			if (fileItem.getName().toString().endsWith(".xls")) {

				fileName = inputFile_Path + tempFileName + ".xls";
				File file = new File(inputFile_Path);
				if (!file.exists()) {
					file.mkdirs();
				}
				// Store input template file as temp file
				fileItem.write(new File(fileName));
				FormatGenerateService generateFormatService = new FormatGenerateService();
				UpdateFormatXls objUpdateFormatXls = new UpdateFormatXls();
				objUpdateFormatXls.updateFilesXls(fileName,
						fileItem2.getString());
				List<String> selectedItems= new ArrayList<String>();
				//selectedItems.add(fileItem2.getString());
				selectedItems.add(fileItem2.getString());
				log.info("ash selected items"+selectedItems.toString());
				int returnValue = generateFormatService.generateFilesService(fileName, selectedItems, modular, variants,suiteName);// Change 1.2
				if (log.isDebugEnabled()) {
					log.debug("return value:" + returnValue);
				}
				if (returnValue == 1) {
					if (log.isDebugEnabled()) {
						log.debug("Successfully Generated");
					}
					if(FormatConstants.shellScriptOn.equalsIgnoreCase("Y")){
						String pScriptFileName =FormatConstants.MOVING_SCRIPT_FILE_NAME;
						String pScriptPath =FormatConstants.MOVING_SCRIPT_FILE_EXECUTION_PATH;
						log.debug("script filename:::: "+pScriptFileName+", scriptfilepath :::: "+pScriptPath);
						System.out.println("script filename:::: "+pScriptFileName+",scriptfilepath :::: "+pScriptPath);
						CallShellScriptUtil.callShellScript(pScriptFileName, pScriptPath);
						if (log.isDebugEnabled()) {
							log.debug("Successfully Generated");
						}
					}
					requestDispatcher = request
							.getRequestDispatcher("success.jsp");
					requestDispatcher.forward(request, response);
				} else if (returnValue == -1) {
					if (log.isDebugEnabled()) {
						log.debug("Error page");
					}
					requestDispatcher = request
							.getRequestDispatcher("error.jsp");
					request.setAttribute(error,
							"1. Please Check Property File(Any Property value is null/Empty)");
					requestDispatcher.forward(request, response);
				} else if (returnValue == -2) {
					if (log.isDebugEnabled()) {
						log.debug("Error page");
					}
					requestDispatcher = request
							.getRequestDispatcher("error.jsp");
					request.setAttribute(
							error,
							"1. Your Selected Template( Excel ) File does not have Your required Sheets. Pls Check");
					requestDispatcher.forward(request, response);
				} else if (returnValue == -3) {
					if (log.isDebugEnabled()) {
						log.debug("Error page");
					}
					requestDispatcher = request
							.getRequestDispatcher("error.jsp");
					request.setAttribute(
							error,
							"1. Your Selected Template( Excel ) File does not have Records (or) Pls check in Property file mismatch Record Started Row number");
					requestDispatcher.forward(request, response);
				} else if (returnValue == -4) {
					if (log.isDebugEnabled()) {
						log.debug("Error page");
					}
					requestDispatcher = request
							.getRequestDispatcher("error.jsp");
					request.setAttribute(error,
							"1. Format Name is null in Data Base");
					requestDispatcher.forward(request, response);
				} else if (returnValue == -5) {
					if (log.isDebugEnabled()) {
						log.debug("Error page");
					}
					requestDispatcher = request
							.getRequestDispatcher("error.jsp");
					request.setAttribute(
							error,
							"1. Your Selected Template( Excel ) File does not have Fields Names Data (or) Pls check in Property file mismatch Fields Row number ");
					requestDispatcher.forward(request, response);
				} else if (returnValue == -6) {
					if (log.isDebugEnabled()) {
						log.debug("Error page");
					}
					requestDispatcher = request
							.getRequestDispatcher("error.jsp");
					request.setAttribute(
							error,
							"1. Your Selected Template( Excel ) File does not have Section Names Data (or) Pls check in Property file mismatch Section Row number");
					requestDispatcher.forward(request, response);
				} else if (returnValue == -7) {
					if (log.isDebugEnabled()) {
						log.debug("Error page");
					}
					requestDispatcher = request
							.getRequestDispatcher("error.jsp");
					request.setAttribute(
							error,
							"1. Sheet Names are not Matched with property file details <br> 2. Your Selected Template( Excel ) File does not have Fields Names Data, Section Names Data and Records Data (or) Pls check in Property file mismatch data. <br> 3. Your selected wrong Template file.");
					requestDispatcher.forward(request, response);
				} else if (returnValue == -8) {
					if (log.isDebugEnabled()) {
						log.debug("Error page");
					}
					requestDispatcher = request
							.getRequestDispatcher("error.jsp");
					request.setAttribute(
							error,
							"1. Your selected Template file Entire row is EMPTY pls check in Template file. (for ex: File Header, Detail Record, File Trailer,....)");
					requestDispatcher.forward(request, response);
				} else if (returnValue == -9) {
					if (log.isDebugEnabled()) {
						log.debug("Error page");
					}
					requestDispatcher = request
							.getRequestDispatcher("error.jsp");
					request.setAttribute(error,
							"1. Please write Folder Name in Config Sheet.");
					requestDispatcher.forward(request, response);
				} else if (returnValue == -10) {
					if (log.isDebugEnabled()) {
						log.debug("Error page");
					}
					requestDispatcher = request
							.getRequestDispatcher("error.jsp");
					request.setAttribute(error,
							"1. DB Connection returns Null.");
					requestDispatcher.forward(request, response);
				} else {
					if (log.isDebugEnabled()) {
						log.debug("Failure state");
					}
				}
			} else {
				if (log.isDebugEnabled()) {
					log.debug("Failure state");
					log.debug("Please Uploaded Excel File only.");
				}
			}
		} catch (FileNotFoundException e) {
			if (log.isDebugEnabled()) {
				System.out.println("Message when file not found");
				log.error("FileNotFoundException While loading the Excel file in Controller class");
			}
			throw new IOException(e
					+ " : xls file not found at the specified path");

		} catch (Exception e) {

			/*
			 * Code changes for error message
			 */

			RequestDispatcher requestDispatcher = null;
			String message = null;

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			message = errors.toString();
			System.out.println("Message:::" + message);
			requestDispatcher = request
					.getRequestDispatcher("failure.jsp?message=" + message);
			requestDispatcher.forward(request, response);

			if (log.isDebugEnabled()) {
				log.error("Exception While loading the Excel file in Controller class");
			}
			log.fatal("Exception ", e);// FG_1.1 (S)
		}
	}
}