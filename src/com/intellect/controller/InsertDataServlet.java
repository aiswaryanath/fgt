package com.intellect.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.intellect.db.DBUtility;

/**
 * Servlet implementation class InsertDataServlet
 * 
 */
public class InsertDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static org.apache.log4j.Logger log = Logger
			.getLogger(InsertDataServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InsertDataServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("Hello this is GET Method ");

		}
		process(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			System.out.println("Hello this is POST Method");
		}
		process(request, response);

	}

	public void process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("Hello this is process Method");
		}
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String sheetName = request.getParameter("sheetName");
		String formatName = request.getParameter("formatName");
		String OriginatorID = request.getParameter("OriginatorID");
		String CustomerRefNum = request.getParameter("CustomerRefNum");
		String tc1 = request.getParameter("tc1");
		String tc2 = request.getParameter("tc2");
		String tc3 = request.getParameter("tc3");
		String tc4 = request.getParameter("tc4");

		String originatorLabel = request.getParameter("OriginatorIDLabel");
		String customerRefNumLabel = request
				.getParameter("CustomerRefNumLabel");

		// Enumeration<String> parameterNames = request.getParameterNames();
		// while (parameterNames.hasMoreElements()) {
		// System.out.println((String) parameterNames.nextElement());
		// log.debug((String) parameterNames.nextElement());
		// }

		int i = 0;
		int j = 0;
		try {
			conn = DBUtility.openConnection();
			String query = "INSERT INTO TEST_TOOL_ALTER_VALUES (SHEET_NAME,FORMAT_NAME,KEY_TO_REPLACE,VALUE_TO_REPLACE,TESTCASEKEY1,TESTCASEKEY2,TESTCASEKEY3,TESTCASEKEY4) values(?, ?, ? , ?,?,?,?,? )";
			log.debug("query :::" + query);
			pstmt = conn.prepareStatement(query); // create a statement
			if (!"".equals(OriginatorID)) {
				pstmt.setString(1, sheetName); // set input parameter 1
				pstmt.setString(2, formatName); // set input parameter 2
				pstmt.setString(3, originatorLabel); // set input parameter 3
				pstmt.setString(4, OriginatorID);// set input parameter 4
				pstmt.setString(5, tc1); // set input parameter 5
				pstmt.setString(6, tc2); // set input parameter 6
				pstmt.setString(7, tc3); // set input parameter 7
				pstmt.setString(8, tc4); // set input parameter 8

				i = pstmt.executeUpdate();
				// System.out.println("i ::::"+i);
			}
			if (!"".equals(CustomerRefNum)) {
				pstmt.setString(1, sheetName); // set input parameter 1
				pstmt.setString(2, formatName); // set input parameter 2
				pstmt.setString(3, customerRefNumLabel); // set input parameter
															// 3
				pstmt.setString(4, CustomerRefNum);// set input parameter 4
				pstmt.setString(5, tc1); // set input parameter 5
				pstmt.setString(6, tc2); // set input parameter 6
				pstmt.setString(7, tc3); // set input parameter 7
				pstmt.setString(8, tc4); // set input parameter 8

				j = pstmt.executeUpdate();
				// System.out.println("j ::::"+j);
			}

			if (i != 0 && j != 0) {
				out.println("<font size=" + "4" + " color=" + "black"
						+ "><b>Record has been inserted </b>");
				// out.println("Record has been inserted");
			} else {
				out.println("<font size=" + "4" + " color=" + "black"
						+ "><b>Failed to insert the data </b>");
				// out.println("failed to insert the data");
			}

		} catch (SQLException e) {
			RequestDispatcher requestDispatcher = null;
			String message = null;

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			message = errors.toString();
			System.out.println("Message:::" + message);
			requestDispatcher = request
					.getRequestDispatcher("failure.jsp?message=" + message);
			requestDispatcher.forward(request, response);

			log.debug("Exception ", e);// FG_1.1 (S)
		} finally {
			if (conn != null) {
				// closes the database connection
				try {
					conn.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}

		}

	}

}
