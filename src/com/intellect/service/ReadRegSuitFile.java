/*********************************************************************************************************/
/* Copyright @  2016 Intellect Design Arena Ltd. All rights reserved                    				 */
/*                                                                                      				 */
/*********************************************************************************************************/
/*  Application  : 	Intellect File Generation Tool	   														 */
/*  Module Name  :	Generating Files for Testing														 */
/* 	  Author			     |        Date   			|	  Version     |   Description   	         */
/*********************************************************************************************************/
/* 	  Gokaran Tiwari		 |		21:02:2018			|	   1.0	      |   New file    						
/*********************************************************************************************************/
/*																										 */
/* 	Description  :  Reads regression suit file(excel file) and returns all testcase id across format */
/*        																								 */
/*********************************************************************************************************/


package com.intellect.service;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.intellect.property.FormatConstants;

public class ReadRegSuitFile {
	private static org.apache.log4j.Logger log = Logger.getLogger(ReadRegSuitFile.class);
	
	
	public Map<String,List<String>> readAllTestCase(String fileName) throws Exception{
		log.debug("inside method readAllTestCase !!!!!!!!!!");
		log.debug("fileName : "+fileName);
		Map<String,List<String>> uniqueTestCases=new HashMap<String,List<String>>();
		FileInputStream inputStream=null;
		Workbook workbook = null;
		try{
			inputStream = new FileInputStream(fileName);
			workbook = WorkbookFactory.create(inputStream);
			for(Entry<String, Integer> entrySet : FormatConstants.sheetwiseReadIndex.entrySet()){
				log.debug("sheet name : "+entrySet.getKey()+" , record started : "+entrySet.getValue());
				Sheet sheet=workbook.getSheet(entrySet.getKey());
				int recordStarted=entrySet.getValue();
				int lastPhysicalRowNo = sheet.getPhysicalNumberOfRows();
				List<String> sheetwiseTestCase = new ArrayList<String>();
				for(int i=recordStarted;i<lastPhysicalRowNo;i++){
					Row row = sheet.getRow(i);
					String testCaseCellValue="";
					if(row!= null && row.getCell(0) != null){
						testCaseCellValue=row.getCell(0).getStringCellValue();
					}
					if(testCaseCellValue != null && !testCaseCellValue.isEmpty()){
						if(!sheetwiseTestCase.contains(testCaseCellValue)){
							sheetwiseTestCase.add(testCaseCellValue);
						}
						log.debug("testcase name : "+testCaseCellValue);
					}
				}
				if(!sheetwiseTestCase.isEmpty()){
					uniqueTestCases.put(entrySet.getKey(), sheetwiseTestCase);
				}
			}
		}catch(Exception e){
			log.error("Error occured in ReadRegSuitFile.readAllTestCase() : "+e.getMessage(), e);
			throw e;
		}finally {
			if(inputStream != null){
				inputStream.close();
			}
		}
		return uniqueTestCases;
	}
}
