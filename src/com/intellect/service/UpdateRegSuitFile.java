/*********************************************************************************************************/
/* Copyright @  2016 Intellect Design Arena Ltd. All rights reserved                    				 */
/*                                                                                      				 */
/*********************************************************************************************************/
/*  Application  : 	Intellect File Generation Tool	   														 */
/*  Module Name  :	Generating Files for Testing														 */
/* 	  Author			     |        Date   			|	  Version     |   Description   	         */
/*********************************************************************************************************/
/* 	  Gokaran Tiwari		 |		21:02:2018			|	   1.0	      |   New file    						
/*********************************************************************************************************/
/*																										 */
/* 	Description  :  Updates regression suit file(excel file) for given testcases  */
/*        																								 */
/*********************************************************************************************************/

package com.intellect.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.intellect.property.FormatConstants;

public class UpdateRegSuitFile {

	private static org.apache.log4j.Logger log = Logger.getLogger(UpdateRegSuitFile.class);
	
	public void updateFGTFlag(String fileName, Map<String,List<String>> testCases) throws IOException{
		log.debug("Inside method updateFGTFlag!!!!!!!!!!!");
		log.debug("fileName : "+fileName);
		log.debug("testCases : "+testCases);
		File file=null;
		FileInputStream inputStream=null;
		Workbook workbook = null;
		FileOutputStream fileOutputStream=null;
		try{
			file = new File(fileName);
			inputStream = new FileInputStream(file);
			workbook = WorkbookFactory.create(inputStream);
			if(testCases != null && !testCases.isEmpty()){
				for(Entry<String,List<String>> entrySet : testCases.entrySet()){
					log.debug("Sheet Name : "+entrySet.getKey());
					Sheet sheet=workbook.getSheet(entrySet.getKey());
					int recordStarted=FormatConstants.sheetwiseReadIndex.get(entrySet.getKey());
					int lastPhysicalRowNo = sheet.getPhysicalNumberOfRows();
					log.debug("recordStarted : "+recordStarted+" , lastPhysicalRowNo : "+lastPhysicalRowNo);
					int continueIndex=-1;
					for(int i=recordStarted;i<lastPhysicalRowNo;i++){
						Row row = sheet.getRow(i);
						String testCaseCellValue="";
						if(row!= null && row.getCell(0) != null){
							testCaseCellValue=row.getCell(0).getStringCellValue();
						}
						if(!testCaseCellValue.isEmpty() && entrySet.getValue().contains(testCaseCellValue)){
							continueIndex=i;
							for(;continueIndex<lastPhysicalRowNo;continueIndex++){
								Row nextRow=sheet.getRow(continueIndex+1);
								if(nextRow != null && nextRow.getCell(0) != null 
										&& nextRow.getCell(0).getStringCellValue() != null 
										&& !nextRow.getCell(0).getStringCellValue().isEmpty()){
									break;
								}
							}
						}
						Cell flagCell=row.getCell(FormatConstants.sheetwiseModularPosition.get(entrySet.getKey()));
						if(flagCell == null || (flagCell != null && 
								flagCell.getStringCellValue().equalsIgnoreCase("FGT FLAG Y/N") 
								|| flagCell.getStringCellValue().equalsIgnoreCase("FGT FLAG")))
							continue;
						if(continueIndex>=i){
							flagCell.setCellValue("Y");
						}else{
							flagCell.setCellValue("N");
						}
					}
				}
				if(inputStream != null){
					inputStream.close();
				}
				fileOutputStream = new FileOutputStream(fileName);
				workbook.write(fileOutputStream);
				if(fileOutputStream != null){
					fileOutputStream.close();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(inputStream != null){
				inputStream.close();
			}
			if(fileOutputStream != null){
				fileOutputStream.close();
			}
		}
	}
}