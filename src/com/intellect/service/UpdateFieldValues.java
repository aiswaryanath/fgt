package com.intellect.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.intellect.db.DBUtility;

public class UpdateFieldValues {
	private static org.apache.log4j.Logger log = Logger
			.getLogger(UpdateFieldValues.class);

	public static Map update(Map cellDataMap, String formatName,
			String sheet_name) {

		Map<Integer, String> cellDataMapCopy = new HashMap<Integer, String>();
		try {
			cellDataMapCopy.putAll(cellDataMap);

			Map<Integer, String> newFieldsFromDB = getValidatedFieldsFromDB(
					formatName, sheet_name, (String) cellDataMap.get(0));
			Set<Integer> keySet = newFieldsFromDB.keySet();
			for (Integer orderSeq : keySet) {
				cellDataMap.put(orderSeq, newFieldsFromDB.get(orderSeq));
			}
			return cellDataMap;
		} catch (Exception e) {
			return cellDataMapCopy;
		}
	}

	// To get validated field name with values from DB
	private static Map<Integer, String> getValidatedFieldsFromDB(
			String formatName, String sheet_name, String testCaseID)
			throws SQLException {

		LinkedHashMap<Integer, String> dbValidatedFieldsMap = null;
		Connection con = null;
		con = DBUtility.openConnection();
		String query = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			query = "SELECT ORDER_SEQUENCE,VALUE_TO_REPLACE FROM TEST_TOOL_ALTER_VALUES WHERE FORMAT_NAME ='"
					+ formatName
					+ "' and  sheet_name ='"
					+ sheet_name
					+ "' "
					+ " AND UPPER('"
					+ testCaseID
					+ "')  like UPPER( '%'||TESTCASEKEY1||'%'||TESTCASEKEY2||'%'||TESTCASEKEY3||'%'||TESTCASEKEY4||'%' )";
			// log.debug("Query :::::" + query);
			preparedStatement = con.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			dbValidatedFieldsMap = new LinkedHashMap<Integer, String>();

			while (resultSet.next()) {
				String valueToReplace = resultSet.getString("VALUE_TO_REPLACE");
				int orderSequence = resultSet.getInt("ORDER_SEQUENCE");

				if (!valueToReplace.trim().isEmpty()) {
					dbValidatedFieldsMap.put(orderSequence, valueToReplace);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			DBUtility.closeDBResourcePstmt(resultSet, preparedStatement);
		}
		return dbValidatedFieldsMap;
	}

	// For field Map
	public static Map getExtraColumns(Map<Integer, String> map1) {

		try {
			map1.remove(219);
			map1.remove(220);
			map1.remove(221);
			map1.remove(222);
			map1.remove(223);
			map1.remove(224);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map1;
	}

	// for Section map
	public static LinkedHashMap getExtraColumnsForSectionMap(
			Map<String, Integer> map1, String formatName, String sheet_name) {

		LinkedHashMap<String, Integer> cellDataMapCopy1 = new LinkedHashMap<String, Integer>();
		try {
			int mapSize = map1.size() - 6;
			int size = 0;

			for (Entry<String, Integer> entry : map1.entrySet()) {
				cellDataMapCopy1.put(entry.getKey(), entry.getValue());
				size++;
				if (size == mapSize) {
					break;
				}
			}
			return cellDataMapCopy1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cellDataMapCopy1;
	}

	// for Record map
	public static Map getExtraColumnsForRecordMap(Map<Integer, Map> map1) {

		try {

			for (Entry<Integer, Map> entry : map1.entrySet()) {
				Map innerMap = entry.getValue();
				innerMap.remove(219);
				innerMap.remove(220);
				innerMap.remove(221);
				innerMap.remove(222);
				innerMap.remove(223);
				innerMap.remove(224);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return map1;

	}

}
