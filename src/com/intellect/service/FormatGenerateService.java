/*********************************************************************************************************/
/* Copyright @  2016 Intellect Design Arena Ltd. All rights reserved                    				 */
/*                                                                                      				 */
/*********************************************************************************************************/
/*  Application  : 	Intellect Payment Engine	   														 */
/*  Module Name  :	Generating Files for Testing														 */
/* 	  Author			     |        Date   			|	  Version     |   Description   	         */
/*********************************************************************************************************/
/* 	  Surendra Reddy.M 		 |		20:05:2016			|	   1.0	      |   New file    						
/*	  Upendar Ch			 |		13:07:2017	        |      1.1		  |							
 * 	  Shruti Gupta           |      24:07:2017			|	   1.2		  |								
 * 	  Gokaran Tiwari		 |	    07:02:2018			|	   1.3        | Multiple logical files in single 
 * 																		  | physical file for CMO103 format
 * 																		  | Also fixed 57D issue in CMO103 
 * 																		  | and ANSI Format issue fixed
 * 																		  | trailer and header concatenating 
 * 																		  | in same line
 *	Shruti Gupta			|	20:03:2018				|		1.4		  | Code changes for Suite_name 	
 */
/*********************************************************************************************************/
/*																										 */
/* 	Description  :  Service of Generating all Formats like getting property values using FormatConstants */
/*  				 class, Calling DAO class methods  and generating files,                             */
/*        																								 */
/*********************************************************************************************************/

package com.intellect.service;

import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.intellect.dao.ConvertFileISOPain001;
import com.intellect.dao.ConvertFileISOPain001_CNR_EMT;
import com.intellect.dao.ConvertFileISOPain008;
import com.intellect.dao.ConvertFileISOPain008_CNR_EMT;
import com.intellect.dao.ConvertFileInteracXml;
import com.intellect.dao.ConvertIsoRemitXml;
//jo added here
import com.intellect.dao.ConvertFileCAMT;
import com.intellect.dao.FormatGeneration;
import com.intellect.dao.IwReadExcel;
import com.intellect.dao.MessageTracking;
import com.intellect.dao.ReadTemplate;
import com.intellect.db.DBUtility;
import com.intellect.pojo.TestToolFileNamePOJO;
import com.intellect.property.FormatConstants;

public class FormatGenerateService {

	private static org.apache.log4j.Logger log = Logger
			.getLogger(FormatGenerateService.class);

	// FG_1.1 (S)
	public static String getSequenceNumber(Connection con, String formatName)
			throws Exception {
		// 1.2 starts
		String formatName1 = formatName.replace(" ", "") + "_SEQ.NEXTVAL";
		String generateSeq = null;

		if (formatName.equalsIgnoreCase(FormatConstants.nachaIat94)) {
			generateSeq = "SELECT LPAD(MOD(FLOOR( " + formatName1
					+ "/(60*60)),24),2,'0') || LPAD(MOD(FLOOR( " + formatName1
					+ "/(60)),60),2,'0') || LPAD(MOD( " + formatName1
					+ ",60),2,'0') MI FROM DUAL";
			
		//	log.debug("generateSeq if block :::"+generateSeq);
		} else {
			generateSeq = "SELECT  " + formatName1 + "  FROM DUAL";
	//		log.debug("generateSeq else block :::"+generateSeq);
		}

		Statement stmt = null;
		ResultSet rs = null;
		String sequencNum = null;
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(generateSeq);
			if (rs.next()) {
				sequencNum = rs.getString(1);
			}
			if (formatName.equalsIgnoreCase(FormatConstants.cibc1464)
					|| formatName.equalsIgnoreCase(FormatConstants.cibc80Byte)
					|| formatName.equalsIgnoreCase(FormatConstants.cpa005)) {

				if (sequencNum.length() == 1) {
					sequencNum = "000" + sequencNum;
				} else if (sequencNum.length() == 2) {
					sequencNum = "00" + sequencNum;
				} else if (sequencNum.length() == 3) {
					sequencNum = "0" + sequencNum;
				}

			} else {
				if (sequencNum.length() == 1) {
					sequencNum = "00000" + sequencNum;
				} else if (sequencNum.length() == 2) {
					sequencNum = "0000" + sequencNum;
				} else if (sequencNum.length() == 3) {
					sequencNum = "000" + sequencNum;
				} else if (sequencNum.length() == 4) {
					sequencNum = "00" + sequencNum;
				} else if (sequencNum.length() == 5) {
					sequencNum = "0" + sequencNum;
				}

			}
		} catch (SQLException e) {
			log.fatal("SQL Exception ", e);
			throw (e);
		} finally {
			DBUtility.closeResultSet(rs);
			DBUtility.closeStatement(stmt);
		}
	//	log.debug("sequencNum ::"+sequencNum +"format name :::"+formatName);
		return sequencNum;
	}

	// FG_1.1 (E)

	public static String getSeqNumber(Connection con, String formatName)
			throws Exception {
		// 1.2 starts
		String generateSeq = null;

		generateSeq = "SELECT  MULTILOGICALFILES_SEQ.nextval  FROM DUAL";

		Statement stmt = null;
		ResultSet rs = null;
		String sequencNum = null;
		try {
			if (formatName.equalsIgnoreCase(FormatConstants.cibc1464)
					|| formatName.equalsIgnoreCase(FormatConstants.cibc80Byte)
					|| formatName.equalsIgnoreCase(FormatConstants.cpa005)) {
				stmt = con.createStatement();
				rs = stmt.executeQuery(generateSeq);
				if (rs.next()) {
					sequencNum = rs.getString(1);
				}

				if (sequencNum.length() == 1) {
					sequencNum = "000" + sequencNum;
				} else if (sequencNum.length() == 2) {
					sequencNum = "00" + sequencNum;
				} else if (sequencNum.length() == 3) {
					sequencNum = "0" + sequencNum;
				}

			}
		} catch (SQLException e) {
			log.fatal("SQL Exception ", e);
			throw (e);
		} finally {
			DBUtility.closeResultSet(rs);
			DBUtility.closeStatement(stmt);
		}
		System.out.println("sequencNum for message id ::" + sequencNum);
		return sequencNum;
	}

	// To process of generating files or creating files with specific filename
	// convention
	@SuppressWarnings("unchecked")
	public int generateFilesService(String inputUploadedPath,
			List<String> selectedItems, String moduleSelected, char[] variants,String suiteName)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("generateFilesService method in FormatsGenerateService class");
		}
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
				.format(Calendar.getInstance().getTime());
		String dbTimeStamp = timeStamp.substring(0, 4) + "-"
				+ timeStamp.substring(4, 6) + "-" + timeStamp.substring(6, 8)
				+ "T" + timeStamp.substring(8, 10) + ":"
				+ timeStamp.substring(10, 12) + ":" + timeStamp.substring(12);

		int resultNum = 0, no_Sheets = 0, recordsStartrow = 0, fieldsRow = 0;
		String folderName = "", preChannel = "", channel = "";
		Connection connection = null;
		ReadTemplate readTemplate = new ReadTemplate();

		Map<Integer, String> templateFieldsData = null;

		Map<Integer, Map> templateRecordsData = null;

		Map<String, Integer> templateSectionNamesData = null;
		

		int tmpRecRow;
		int tmpRecRow1 = 0;
		try {
			// Reading Property file values
			String outputFilePath = FormatConstants.outputFile;
			//String sheetName[] = new String[9];//jo changed here
			String sheetName[] = new String[10];
			
			sheetName[0] = FormatConstants.tmpSheetName;
			sheetName[1] = FormatConstants.swiftSheetName;
			sheetName[2] = FormatConstants.ansiSheetName;
			sheetName[3] = FormatConstants.pain1SheetName;
			sheetName[4] = FormatConstants.pain8SheetName;
			sheetName[5] = FormatConstants.InteracSheetName;
			sheetName[6] = FormatConstants.IsoRemitSheetName;
			sheetName[7] = FormatConstants.PAIN1_REMIT_SHEET_NAME;
			sheetName[8] = FormatConstants.PAIN8_REMIT_SHEET_NAME;
			//jo added here
			sheetName[9] = FormatConstants.CAMT_SHEET_NAME;

			String fileSheetName = FormatConstants.folderNameSheetName;
			String fieldsRowNo = FormatConstants.fieldsRowNo;
			int fieldsRow_No = Integer.parseInt(fieldsRowNo);
			String ansiFieldsRowNo = FormatConstants.ansiFieldsRowNo;
			int ansiFieldsRow_No = Integer.parseInt(ansiFieldsRowNo);
			String db_tableName = FormatConstants.dbTableName;
			String db_table_file = FormatConstants.fileNamesDbName;
			String records_started_row = FormatConstants.recordsStartRow;
			String swift_records_started_row = FormatConstants.swiftRecordsStartRow;
			String ansi_records_started_row = FormatConstants.ansiRecordsStartRow;
			int records_started_row_no = Integer.parseInt(records_started_row);
			int swift_records_started_row_no = Integer
					.parseInt(swift_records_started_row);
			int ansi_records_started_row_no = Integer
					.parseInt(ansi_records_started_row);
			String formatNeededColumnNo = FormatConstants.formatColumn;
			int formatNeededColumn_No = Integer.parseInt(formatNeededColumnNo);
			String sectionRowNo = FormatConstants.sectionRow;
			int secRow_No = Integer.parseInt(sectionRowNo);
			String swift_applied_rule_pos = FormatConstants.swiftAppliedRulePosition;
			int swift_applied_rule_pos_no = Integer
					.parseInt(swift_applied_rule_pos);
			String native_applied_rule_pos = FormatConstants.nativeAppliedRulePosition;
			int native_applied_rule_pos_no = Integer
					.parseInt(native_applied_rule_pos);
			String ansi_applied_rule_pos = FormatConstants.ansiAppliedRulePosition;
			int ansi_applied_rule_pos_no = Integer
					.parseInt(ansi_applied_rule_pos);

			connection = DBUtility.openConnection();
			if (connection == null) {
			}
			if (connection == null) {
				return -11;//jo changed here was 10
			}
			if (log.isDebugEnabled()) {
				log.debug("Connection=>" + connection);
			}
			boolean tem1 = true;
			boolean tem2 = false;
			readTemplate = new ReadTemplate();
			int tmpcount = 0;
			IwReadExcel objIwReadExcel = new IwReadExcel();

			folderName = readTemplate.getFolderName(inputUploadedPath,
					fileSheetName);
			if (folderName.isEmpty()) {
				if (log.isDebugEnabled()) {
					log.debug("return value -10");// jo changed was -9
				}
				return -10;// jo changed was -9
			}
			//1.4 start
			ArrayList<TestToolFileNamePOJO> testToolData=new ArrayList<TestToolFileNamePOJO>();
			
			for (int sheetNo = 0; sheetNo < sheetName.length; sheetNo++) {
				if (log.isDebugEnabled()) {
					log.debug("checking the sheet in for loop");
				}
				String sheet_Name = sheetName[sheetNo];
				if ((sheet_Name.equalsIgnoreCase(sheetName[0]) && selectedItems.contains(sheet_Name))
						|| (sheet_Name.equalsIgnoreCase(sheetName[0]) && selectedItems
								.contains(FormatConstants.ALL))) {
					no_Sheets++;
					tem1 = true;
					tem2 = true;
					tmpcount = tmpcount + 1;
					if (log.isDebugEnabled()) {
						log.debug("sheetName Match with native formats");
					}
					recordsStartrow = records_started_row_no;
					fieldsRow = fieldsRow_No;
				} else if ((sheet_Name.equalsIgnoreCase(sheetName[1]) && selectedItems
						.contains(sheet_Name))
						|| (sheet_Name.equalsIgnoreCase(sheetName[1]) && selectedItems
								.contains(FormatConstants.ALL))) {
					no_Sheets++;
					tem1 = true;
					tem2 = true;
					tmpcount = tmpcount + 1;
					if (log.isDebugEnabled()) {
						log.debug("Sheetname match with Swift");
					}
					recordsStartrow = swift_records_started_row_no;
					fieldsRow = fieldsRow_No;
				} else if ((sheet_Name.equalsIgnoreCase(sheetName[2]) && selectedItems
						.contains(sheet_Name))
						|| (sheet_Name.equalsIgnoreCase(sheetName[2]) && selectedItems
								.contains(FormatConstants.ALL))) {
					no_Sheets++;
					recordsStartrow = ansi_records_started_row_no;
					fieldsRow = ansiFieldsRow_No;
					if (log.isDebugEnabled()) {
						log.debug("sheetName match with Ansi");
					}

					tem1 = true;
					tem2 = true;
					tmpcount = tmpcount + 1;
				} else if ((sheet_Name.equalsIgnoreCase(sheetName[3]) && selectedItems
						.contains(sheet_Name))
						|| (sheet_Name.equalsIgnoreCase(sheetName[3]) && selectedItems
								.contains(FormatConstants.ALL))) {
					no_Sheets++;
					LinkedHashMap<Integer, Vector<String>> map1 = (LinkedHashMap<Integer, Vector<String>>) objIwReadExcel
							.ReadData(inputUploadedPath, sheetName[3],
									moduleSelected);
					tem1 = false;
					tmpcount = tmpcount + 1;
					if (log.isDebugEnabled()) {
						log.debug("Sheetname match with pain001");
					}
					ConvertFileISOPain001 iso001 = new ConvertFileISOPain001();
					File destFolderISO = new File(outputFilePath + folderName
							+ "_" + timeStamp + "/" + FormatConstants.ISOPAIN001 /*+"/" + "ISO PAIN001"*/);
					//if (!destFolderISO.exists()) {
						destFolderISO.mkdirs();
					//}
						
					int countISOPain = iso001.main(
							inputUploadedPath, destFolderISO, sheetName[3],
							map1, connection, db_table_file, timeStamp,
							dbTimeStamp, moduleSelected, variants,testToolData);// FG_1.1
					if (countISOPain == 1) {
						log.debug("Pain 001 Format Generation Completed");
					}
				} else if ((sheet_Name.equalsIgnoreCase(sheetName[4]) && selectedItems
						.contains(sheet_Name))
						|| (sheet_Name.equalsIgnoreCase(sheetName[4]) && selectedItems
								.contains(FormatConstants.ALL))) {
					no_Sheets++;
					LinkedHashMap<Integer, Vector<String>> map2 = (LinkedHashMap<Integer, Vector<String>>) objIwReadExcel
							.ReadData(inputUploadedPath, sheetName[4],
									moduleSelected);
					tem1 = false;
					tmpcount = tmpcount + 1;
					if (log.isDebugEnabled()) {
						log.debug("SheetName match with pain008");
					}
					ConvertFileISOPain008 iso008 = new ConvertFileISOPain008();
					File destFolderISO = new File(outputFilePath + folderName
							+ "_" + timeStamp + "/" + FormatConstants.ISOPAIN008 /*+ "ISO PAIN008"*/);
					destFolderISO.mkdirs();
					int countISOPain = iso008.main(inputUploadedPath,
							destFolderISO, sheetName[4], map2, connection,
							db_table_file, timeStamp, dbTimeStamp,
							moduleSelected, variants,testToolData);// FG_1.1
					if (countISOPain == 1) {
						log.debug("Pain 008 Format Generation Completed");
					}
				}
				// intrac changes
				else if ((sheet_Name.equalsIgnoreCase(sheetName[5]) && selectedItems
						.contains(sheet_Name))
						|| (sheet_Name.equalsIgnoreCase(sheetName[5]) && selectedItems
								.contains(FormatConstants.ALL))) {
					no_Sheets++;
					LinkedHashMap<Integer, Vector<String>> map2 = (LinkedHashMap<Integer, Vector<String>>) objIwReadExcel
							.ReadData(inputUploadedPath, sheetName[5],
									moduleSelected);
					tem1 = false;
					tmpcount = tmpcount + 1;
					if (log.isDebugEnabled()) {
						log.debug("SheetName match with Interac");
					}
					ConvertFileInteracXml interac = new ConvertFileInteracXml();
					File destFolderISO = new File(outputFilePath + folderName
							+ "_" + timeStamp + "/" + FormatConstants.Interacxml /*+ "Interac xml"*/);
					destFolderISO.mkdirs();
					int countISOPain = interac.main(inputUploadedPath,
							destFolderISO, sheetName[5], map2, connection,
							db_table_file, timeStamp, dbTimeStamp,
							moduleSelected, variants,testToolData);
					if (countISOPain == 1) {
						log.debug("interac Format Generation Completed");
					}
				}
				// end intrac changes
				// remit changes
				else if ((sheet_Name.equalsIgnoreCase(sheetName[6]) && selectedItems
						.contains(sheet_Name))
						|| (sheet_Name.equalsIgnoreCase(sheetName[6]) && selectedItems
								.contains(FormatConstants.ALL))) {
					no_Sheets++;
					LinkedHashMap<Integer, Vector<String>> map2 = (LinkedHashMap<Integer, Vector<String>>) objIwReadExcel
							.ReadData(inputUploadedPath, sheetName[6],
									moduleSelected);
					tem1 = false;
					tmpcount = tmpcount + 1;
					if (log.isDebugEnabled()) {
						log.debug("SheetName match with remit");
					}
					ConvertIsoRemitXml interac = new ConvertIsoRemitXml();
					File destFolderISO = new File(outputFilePath + folderName
							+ "_" + timeStamp + "/" + FormatConstants.ISO_REMITxml /*+ "ISO_REMIT xml"*/);
					destFolderISO.mkdirs();
					int countISOPain = interac.main(inputUploadedPath,
							destFolderISO, sheetName[6], map2, connection,
							db_table_file, timeStamp, dbTimeStamp,
							moduleSelected, variants,testToolData);
					if (countISOPain == 1) {
						log.debug("remit Format Generation Completed");
					}
				}
				// end remit changes
				else if ((sheet_Name.equalsIgnoreCase(sheetName[7]) && selectedItems
						.contains(sheet_Name))
						|| (sheet_Name.equalsIgnoreCase(sheetName[7]) && selectedItems
								.contains(FormatConstants.ALL))) {
					no_Sheets++;
					LinkedHashMap<Integer, Vector<String>> map2 = (LinkedHashMap<Integer, Vector<String>>) objIwReadExcel.ReadData(inputUploadedPath, sheetName[7],moduleSelected);
					tem1 = false;
					tmpcount = tmpcount + 1;
					if (log.isDebugEnabled()) {
						log.debug("SheetName match with pain 1 remit");
					}
					//Jo-below is  fucntion  called which needs change
					ConvertFileISOPain001_CNR_EMT convertFileISOPain001_CNR_EMT = new ConvertFileISOPain001_CNR_EMT();
					File destFolderISO = new File(outputFilePath + folderName
							+ "_" + timeStamp + "/" + FormatConstants.ISOPAIN001_Remit /*+ "ISO PAIN001_Remit"*/);
					destFolderISO.mkdirs();
					int countISOPain = convertFileISOPain001_CNR_EMT.main(inputUploadedPath, destFolderISO, sheetName[7],map2, connection, db_table_file, timeStamp,dbTimeStamp, moduleSelected, variants,testToolData);// FG_1.1
					if (countISOPain == 1) {
						log.debug("pain 1 remit Format Generation Completed");
					}
				} 
				//JO ADDED HERE FOR CAMT
				
				else if ((sheet_Name.equalsIgnoreCase(sheetName[9]) && selectedItems
						.contains(sheet_Name))
						|| (sheet_Name.equalsIgnoreCase(sheetName[9]) && selectedItems
								.contains(FormatConstants.ALL))) {
					no_Sheets++;
					LinkedHashMap<Integer, Vector<String>> map2 = (LinkedHashMap<Integer, Vector<String>>) objIwReadExcel.ReadData(inputUploadedPath, sheetName[9],moduleSelected);
					tem1 = false;
					tmpcount = tmpcount + 1;
					if (log.isDebugEnabled()) {
						log.debug("SheetName match with CAMT");
					}
					//Jo-below is  fucntion  called which needs change
					ConvertFileCAMT convertCAMT = new ConvertFileCAMT();
					File destFolderISO = new File(outputFilePath + folderName
							+ "_" + timeStamp + "/" + FormatConstants.CAMT) ;
					destFolderISO.mkdirs();
					int countCAMT = convertCAMT.main(inputUploadedPath, destFolderISO, sheetName[9],map2, connection, db_table_file, timeStamp,dbTimeStamp, moduleSelected, variants,testToolData);
					if (countCAMT == 1) {
						log.debug("CAMT Format Generation Completed");
					}
				}
				
				//CAMT CHANGES END HERE
				
				else if ((sheet_Name.equalsIgnoreCase(sheetName[8]) && selectedItems
						.contains(sheet_Name))
						|| (sheet_Name.equalsIgnoreCase(sheetName[8]) && selectedItems
								.contains(FormatConstants.ALL))) {
					no_Sheets++;
					LinkedHashMap<Integer, Vector<String>> map2 = (LinkedHashMap<Integer, Vector<String>>) objIwReadExcel
							.ReadData(inputUploadedPath, sheetName[8],
									moduleSelected);
					tem1 = false;
					tmpcount = tmpcount + 1;
					if (log.isDebugEnabled()) {
						log.debug("SheetName match with pain 8 remit");
					}
					ConvertFileISOPain008_CNR_EMT convertFileISOPain008_CNR_EMT = new ConvertFileISOPain008_CNR_EMT();
					File destFolderISO = new File(outputFilePath + folderName
							+ "_" + timeStamp + "/" + FormatConstants.ISOPAIN008_Remit /*+ "ISO PAIN008_Remit"*/);
					destFolderISO.mkdirs();
					int countISOPain = convertFileISOPain008_CNR_EMT.main(
							inputUploadedPath, destFolderISO, sheetName[8],
							map2, connection, db_table_file, timeStamp,
							dbTimeStamp, moduleSelected, variants,testToolData);// FG_1.1
					if (countISOPain == 1) {
						log.debug("pain 8 remit Format Generation Completed");
					}
				}else if(!selectedItems.contains(sheet_Name) && !selectedItems.contains(FormatConstants.ALL)){
					continue;
				}
				if (tem1 && tem2) {
					Map[] inputData = readTemplate.readDataFromTemplate(
							inputUploadedPath, // Path where output file
												// generated
							sheetName[sheetNo],// sheet name
							fieldsRow, // ANSI-2,SWIFT,NATIVE-1,PAIN-0
							formatNeededColumn_No, // 2
							recordsStartrow, // Differnt for each format
							secRow_No); // 0
					if (inputData == null) {
						if (log.isDebugEnabled()) {
							log.debug("returning value is:-7");
						}
						return -7;
					}
					if (inputData[0] == null) {
						if (log.isDebugEnabled()) {
							log.debug("returning value is:-5");
						}
						return -5;
					}
					if (inputData[1] == null) {
						if (log.isDebugEnabled()) {
							log.debug("returning value is:-3");
						}
						return -3;
					}
					if (inputData[2] == null) {
						if (log.isDebugEnabled()) {
							log.debug("returning value is:-6");
						}
						return -6;
					}
					if (log.isDebugEnabled()) {
						log.debug("Excel (template) reading is completed");
					}
					templateFieldsData = inputData[0];
					templateRecordsData = inputData[1];
					templateSectionNamesData = inputData[2];

					File generationFiles = null;
					FileWriter generationfile_writer = null;
					FormatGeneration foramtsGeneration = null;
					int tempCount = 0;
					Set<String> formatNamesSet = readTemplate
							.getFormatsNameList(connection, db_tableName);
					String tempFormatename = templateRecordsData.get(0)
							.get(formatNeededColumn_No).toString().trim();
					boolean temp_statusToWrite = false;
					String fileName = "", oldFormat = "";
					int transactionNo = 0, transaction = 0, mtTransactionNo = 0;
					if (templateRecordsData.size() == 0) {
						if (log.isDebugEnabled()) {
							log.debug("templateRecordsData size is ZERO");
							log.debug("returning value is:-3");
						}
						return -3;
					} else {
						// FG_1.1 (S)

						List recordWiseList = new ArrayList();
						List recordWiseTagList = new ArrayList();
						Map tagsRecordWise = new HashMap();
						String tempFormatName1SWIFT = null;
						List allRecList = new ArrayList();
						List allTagList = new ArrayList();
						int multiMsg = 1;
						List formatNameList = new ArrayList();
						List fileList = new ArrayList();
						List fileNameList = new ArrayList();
						String fileName1 = null;
						List insertDBParamList = new ArrayList();
						List randomNumList1 = new ArrayList();
						HashMap loopData = new HashMap();
						String[] loop1 = null;
						String formatName = null;
						int flag = 0;
						if (tempFormatename.equals("X820")) {
							flag = getModularRow(tempFormatename);
							// log.debug("Flaggggggg   if   ::::"+flag
							// +" "+tempFormatename);
						} else if (sheet_Name
								.equalsIgnoreCase("Native Formats")) {
							flag = getModularRow(templateRecordsData.get(1)
									.get(2).toString());
							// log.debug("Flaggggggg  else if   ::::"+flag +" "
							// +sheet_Name);
						} else {
							flag = getModularRow(templateRecordsData.get(0)
									.get(2).toString());
							// log.debug("Flagggg else  :::"+flag);
						}
						
						int continuesCMO103ROWNO=-1; // 1.3
						int continuesANSIROWNO=-1;   // 1.3
						boolean iasflag = false;     // 1.3
						
						TestToolFileNamePOJO fileNamePojo = null;
						//1.4 end
						for (tmpRecRow = 0; tmpRecRow < templateRecordsData
								.size(); tmpRecRow++) {
							tmpRecRow1 = tmpRecRow;
							Map insertDBParamMap = new HashMap();
							if (templateRecordsData.get(tmpRecRow) != null&& (moduleSelected.equals("N") || (moduleSelected.equals("Y") && templateRecordsData.get(tmpRecRow).get(flag)	.toString().trim().equals("Y")))) {
								foramtsGeneration = new FormatGeneration();
								readTemplate = new ReadTemplate();
								String resultRecord = null;
								int formatNameCount = 0;
								boolean temp_status = false;
								Iterator<String> formatName_iterator = formatNamesSet
										.iterator();
								String tempFormatName1 = "";
								while (formatName_iterator.hasNext()) {
									formatName = formatName_iterator.next();
									if (formatName == null
											|| formatName.isEmpty()) {
										log.debug("formatName is null in DB.");
										return -4;
									} else {
										if (templateRecordsData.get(tmpRecRow) != null) {
											String replaceFormatName = templateRecordsData
													.get(tmpRecRow)
													.get(formatNeededColumn_No)
													.toString()
													.replace(" ", "").trim();

											if (formatName.replace(" ", "")
													.equalsIgnoreCase(
															replaceFormatName)
													|| (formatName
															.replace(" ", "")
															.equalsIgnoreCase(
																	tempFormatename
																			.replace(
																					" ",
																					"")
																			.trim()) && templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No)
															.toString().trim()
															.isEmpty())) {
												formatNameCount = formatNameCount + 1;
												tempFormatName1 = formatName;
												formatNameList.add(formatName);// FG_1.1
																				// (E)

												temp_status = true;
												break;
											} else {
												formatNameCount = formatNameCount + 1;

											}
										}
									}
								}
								/*1.3 changes starts*/
								if(tempFormatName1
										.equalsIgnoreCase(FormatConstants.cmo103)){
									for(int k=tmpRecRow+1;k<templateRecordsData.size();k++){
										if (templateRecordsData.get(k)
												.get(formatNeededColumn_No)
												.toString().trim().isEmpty()) {
											continuesCMO103ROWNO=k;
										}else{
											break;
										}
									}
								}else if(continuesCMO103ROWNO > -1){
									continuesCMO103ROWNO=-1;
								}
								if(tempFormatName1.equalsIgnoreCase(FormatConstants.x820)){
									if(continuesANSIROWNO<tmpRecRow){
										for(int k=tmpRecRow+1;k<templateRecordsData.size();k++){
											if (templateRecordsData.get(k)
													.get(formatNeededColumn_No)
													.toString().trim().isEmpty()) {
												continuesANSIROWNO=k;
												
											}else{
												break;
											}
										}
									}
								}
								/*1.3 changes ends*/
								if (temp_status && formatNameCount >= 1) {

									if (!templateRecordsData.get(tmpRecRow)
											.get(formatNeededColumn_No)
											.toString().trim().isEmpty()) {
										if (!templateRecordsData
												.get(tmpRecRow)
												.get(formatNeededColumn_No + 12)
												.toString().trim().isEmpty()) {
											transactionNo = 1;
										}
										if (!templateRecordsData
												.get(tmpRecRow)
												.get(formatNeededColumn_No + 11)
												.toString().trim().isEmpty()) {
											mtTransactionNo = 1;
										}
										if (tempFormatName1
												.equalsIgnoreCase(FormatConstants.cibc1464)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.nachaIat94)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.cibc80Byte)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.cpa005)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.nacha94)) {
											//
											resultRecord = foramtsGeneration
													.formatGeneration(
															formatNeededColumn_No,
															templateRecordsData
																	.get(tmpRecRow + 1),
															transactionNo,
															connection,
															templateRecordsData
																	.get(tmpRecRow),
															db_tableName,
															tempFormatName1,
															templateFieldsData,
															templateSectionNamesData);

										}
										if (tempFormatName1
												.equalsIgnoreCase(FormatConstants.mt101)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.mt104)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.cmo103)) {
											tempFormatName1SWIFT = tempFormatName1;
											HashMap temp = foramtsGeneration
													.mt101_104_cmo_103FormatGeneration(
															connection,
															templateRecordsData
																	.get(tmpRecRow),
															db_tableName,
															tempFormatName1,
															templateFieldsData,
															templateSectionNamesData,
															mtTransactionNo);
											resultRecord = (String) temp
													.get("GOODSTRING");
											
											//log.debug("resultRecord ::::"+resultRecord);
											
											ArrayList tags = (ArrayList) temp
													.get("TAGS");
											if (multiMsg != 1) {
												allRecList.add(recordWiseList);
												allTagList
														.add(recordWiseTagList);
												recordWiseList = new ArrayList();
												recordWiseTagList = new ArrayList();
											}
											recordWiseList.add(resultRecord);
											recordWiseTagList.add(tags);
											multiMsg++;
											// FG_1.1 (E)
										}
										if (tempFormatName1
												.equalsIgnoreCase(FormatConstants.pain001)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.pain008)) {

										}
										if (tempFormatName1
												.equalsIgnoreCase(FormatConstants.x820)) {

											resultRecord = foramtsGeneration
													.formatANSIGeneration(
															formatNeededColumn_No,
															connection,
															templateRecordsData
																	.get(tmpRecRow),
															db_tableName,
															tempFormatName1,
															templateFieldsData,
															templateSectionNamesData,iasflag);
											/*1.3 changes starts*/
											if(continuesANSIROWNO>tmpRecRow){
												iasflag=true;
											}else if(continuesANSIROWNO==tmpRecRow){
												iasflag=false;
											}
											/*1.3 changes ends*/
										}
									} else {
										if (templateRecordsData
												.get(tmpRecRow)
												.get(formatNeededColumn_No + 12)
												.toString().trim().isEmpty()) {
											transactionNo++;
										}
										if (templateRecordsData
												.get(tmpRecRow)
												.get(formatNeededColumn_No + 11)
												.toString().trim().isEmpty()) {
											mtTransactionNo++;
										}
										if (tempFormatName1
												.equalsIgnoreCase(FormatConstants.cibc1464)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.nachaIat94)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.cibc80Byte)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.cpa005)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.nacha94)) {

											resultRecord = foramtsGeneration
													.formatGeneration(
															formatNeededColumn_No,
															templateRecordsData
																	.get(tmpRecRow + 1),
															transactionNo,
															connection,
															templateRecordsData
																	.get(tmpRecRow),
															db_tableName,
															tempFormatName1,
															templateFieldsData,
															templateSectionNamesData);

										}
										if (tempFormatName1
												.equalsIgnoreCase(FormatConstants.mt101)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.mt104)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.cmo103)) {
											// FG_1.1 (S)
											tempFormatName1SWIFT = tempFormatName1;
											HashMap tmp = foramtsGeneration
													.mt101_104_cmo_103FormatGeneration(
															connection,
															templateRecordsData
																	.get(tmpRecRow),
															db_tableName,
															tempFormatName1,
															templateFieldsData,
															templateSectionNamesData,
															mtTransactionNo);
											resultRecord = (String) tmp
													.get("GOODSTRING");
											//log.debug("resultRecord ::::"+resultRecord);
											recordWiseList.add(resultRecord);
											recordWiseTagList
													.add((ArrayList) tmp
															.get("TAGS"));
											// FG_1.1 (E)
										}
										if (tempFormatName1
												.equalsIgnoreCase(FormatConstants.pain001)
												|| tempFormatName1
														.equalsIgnoreCase(FormatConstants.pain008)) {

										}
										if (tempFormatName1
												.equalsIgnoreCase(FormatConstants.x820)) {
											resultRecord = foramtsGeneration
													.formatANSIGeneration(
															formatNeededColumn_No,
															connection,
															templateRecordsData
																	.get(tmpRecRow),
															db_tableName,
															tempFormatName1,
															templateFieldsData,
															templateSectionNamesData,iasflag);
											/*1.3 changes starts*/
											if(continuesANSIROWNO>tmpRecRow){
												iasflag=true;
											}else if(continuesANSIROWNO==tmpRecRow){
												iasflag=false;
											}
											/*1.3 changes ends*/
										}
									}
								}
								if (!templateRecordsData.get(tmpRecRow)
										.get(formatNeededColumn_No).toString()
										.trim().isEmpty()) {

									temp_statusToWrite = false;
									if (resultRecord != null
											&& !resultRecord.trim().isEmpty()) {
										if (!templateRecordsData.get(tmpRecRow)
												.get(formatNeededColumn_No + 1)
												.toString().trim().isEmpty()) {
											if (templateRecordsData
													.get(tmpRecRow)
													.get(formatNeededColumn_No + 1)
													.toString()
													.trim()
													.equalsIgnoreCase(
															FormatConstants.ftsChannel)) {

												if (!preChannel
														.equalsIgnoreCase(templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim())) {
													oldFormat = "";
													preChannel = FormatConstants.ftsChannel;
												}
												preChannel = FormatConstants.ftsChannel;
												if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.cibc1464
																		.replace(
																				" ",
																				""))
														|| templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim()
																.replace(" ",
																		"")
																.equalsIgnoreCase(
																		FormatConstants.cpa005
																				.replace(
																						" ",
																						""))) {
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 12)
															.toString().trim()
															.isEmpty()) {
														transaction = 1;
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();
														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 2)
																		.toString()
																		.trim()
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 3)
																		.toString()
																		.trim()
																		.substring(
																				0,
																				templateRecordsData
																						.get(tmpRecRow)
																						.get(formatNeededColumn_No + 3)
																						.toString()
																						.trim()
																						.length() - 4)
																+ getSequenceNumber(
																		connection,
																		templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No)
																				.toString()
																				.trim())
																+ ".1464";
														
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}

													} else {
														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;

															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 2)
																			.toString()
																			.trim()
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 3)
																			.toString()
																			.trim()
																			.substring(
																					0,
																					templateRecordsData
																							.get(tmpRecRow)
																							.get(formatNeededColumn_No + 3)
																							.toString()
																							.trim()
																							.length() - 4)
																	+ getSequenceNumber(
																			connection,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No)
																					.toString()
																					.trim())
																	+ ".1464";
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.cibc80Byte
																		.replace(
																				" ",
																				""))) {
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 12)
															.toString().trim()
															.isEmpty()) {
														transaction = 1;
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();

														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 2)
																		.toString()
																		.trim()
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 3)
																		.toString()
																		.trim()
																		.substring(
																				0,
																				templateRecordsData
																						.get(tmpRecRow)
																						.get(formatNeededColumn_No + 3)
																						.toString()
																						.trim()
																						.length() - 4)
																+ getSequenceNumber(
																		connection,
																		templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No)
																				.toString()
																				.trim())
																+ ".0080";
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {

														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;
															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 2)
																			.toString()
																			.trim()
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 3)
																			.toString()
																			.trim()
																			.substring(
																					0,
																					templateRecordsData
																							.get(tmpRecRow)
																							.get(formatNeededColumn_No + 3)
																							.toString()
																							.trim()
																							.length() - 4)
																	+ getSequenceNumber(
																			connection,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No)
																					.toString()
																					.trim())
																	+ ".0080";
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.nachaIat94
																		.replace(
																				" ",
																				""))) {
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 12)
															.toString().trim()
															.isEmpty()) {
														transaction = 1;
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();

														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 2)
																		.toString()
																		.trim()
																+ "_"
																+ formatter
																		.format(date)
																+ "_"
																+ getSequenceNumber(
																		connection,
																		templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No)
																				.toString()
																				.trim())
																+ ".0094";
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {
														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;

															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 2)
																			.toString()
																			.trim()
																	+ "_"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 7)
																			.toString()
																			.trim()
																			.substring(
																					0,
																					templateRecordsData
																							.get(tmpRecRow)
																							.get(formatNeededColumn_No + 7)
																							.toString()
																							.trim()
																							.length() - 6)
																	+ getSequenceNumber(
																			connection,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No)
																					.toString()
																					.trim())
																	+ ".0094";
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.nacha94
																		.replace(
																				" ",
																				""))) {

													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 11)
															.toString().trim()
															.isEmpty()) {
														transaction = 1;
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();
														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 5)
																		.toString()
																		.trim()
																+ "_"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 8)
																		.toString()
																		.trim()
																+ "_"
																+ formatter
																		.format(date)
																+ "_111213"
																+ "_"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 6)
																		.toString()
																		.trim()
																		.substring(
																				0,
																				templateRecordsData
																						.get(tmpRecRow)
																						.get(formatNeededColumn_No + 6)
																						.toString()
																						.trim()
																						.length() - 6)
																+ getSequenceNumber(
																		connection,
																		templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No)
																				.toString()
																				.trim())
																+ ".E0094."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 9)
																		.toString()
																		.trim()
																+ ".dat";
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {

														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;

															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 5)
																			.toString()
																			.trim()
																	+ "_"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 8)
																			.toString()
																			.trim()
																	+ "_"
																	+ formatter
																			.format(date)
																	+ "_111213"
																	+ "_"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 6)
																			.toString()
																			.trim()
																			.substring(
																					0,
																					templateRecordsData
																							.get(tmpRecRow)
																							.get(formatNeededColumn_No + 6)
																							.toString()
																							.trim()
																							.length() - 6)
																	+ getSequenceNumber(
																			connection,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No)
																					.toString()
																					.trim())
																	+ ".E0094."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 9)
																			.toString()
																			.trim()
																	+ ".dat";
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.mt101
																		.replace(
																				" ",
																				""))) {

													String tempRandomNum = getSequenceNumber(
															connection,
															templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No)
																	.toString()
																	.trim());
													randomNumList1
															.add(tempRandomNum);

													transaction = 1;

													fileName = templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 1)
															.toString().trim()
															+ "/"
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 5)
																	.toString()
																	.trim()
															+ "_"
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 8)
																	.toString()
																	.trim()
															+ "_"
															+ formatter
																	.format(date)
															+ "_111213"
															+ "_"
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 6)
																	.toString()
																	.trim()
																	.substring(
																			0,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No + 6)
																					.toString()
																					.trim()
																					.length() - 6)
															+ tempRandomNum
															+ "."
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No)
																	.toString()
																	.trim()
																	.replace(
																			" ",
																			"")
															+ "."
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 9)
																	.toString()
																	.trim()
															+ ".dat";
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 10)
															.toString()
															.isEmpty()) {
														fileName = fileName
																+ "."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 10)
																		.toString();
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.mt104
																		.replace(
																				" ",
																				""))) {
													String tempRandomNum = getSequenceNumber(
															connection,
															templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No)
																	.toString()
																	.trim());
													randomNumList1
															.add(tempRandomNum);

													transaction = 1;
													fileName = templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 1)
															.toString().trim()
															+ "/"
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 5)
																	.toString()
																	.trim()
															+ "_"
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 8)
																	.toString()
																	.trim()
															+ "_"
															+ formatter
																	.format(date)
															+ "_111213"
															+ "_"
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 6)
																	.toString()
																	.trim()
																	.substring(
																			0,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No + 6)
																					.toString()
																					.trim()
																					.length() - 6)
															+ tempRandomNum
															+ "."
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No)
																	.toString()
																	.trim()
																	.replace(
																			" ",
																			"")
															+ "."
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 9)
																	.toString()
																	.trim()
															+ ".dat";
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 10)
															.toString()
															.isEmpty()) {
														fileName = fileName
																+ "."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 10)
																		.toString();
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.cmo103)) {
													String tempRandomNum = getSequenceNumber(
															connection,
															templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No)
																	.toString()
																	.trim());
													randomNumList1
															.add(tempRandomNum);

													transaction = 1;
													fileName = templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 1)
															.toString().trim()
															+ "/"
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 5)
																	.toString()
																	.trim()
															+ "_"
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 8)
																	.toString()
																	.trim()
															+ "_"
															+ formatter
																	.format(date)
															+ "_111213"
															+ "_"
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 6)
																	.toString()
																	.trim()
																	.substring(
																			0,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No + 6)
																					.toString()
																					.trim()
																					.length() - 6)
															+ tempRandomNum
															+ "."
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No)
																	.toString()
																	.trim()
																	.replace(
																			" ",
																			"")
															+ "."
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 9)
																	.toString()
																	.trim()
															+ ".dat";
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 10)
															.toString()
															.isEmpty()) {
														fileName = fileName
																+ "."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 10)
																		.toString();
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.x820
																		.replace(
																				" ",
																				""))) {

													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 11)
															.toString().trim()
															.isEmpty()) {
														transaction = 1;
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();
														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 5)
																		.toString()
																		.trim()
																+ "_"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 8)
																		.toString()
																		.trim()
																+ "_"
																+ formatter
																		.format(date)
																+ "_111213"
																+ "_"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 6)
																		.toString()
																		.trim()
																		.substring(
																				0,
																				templateRecordsData
																						.get(tmpRecRow)
																						.get(formatNeededColumn_No + 6)
																						.toString()
																						.trim()
																						.length() - 6)
																+ getSequenceNumber(
																		connection,
																		templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No)
																				.toString()
																				.trim())
																+ ".X12820v4010."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 9)
																		.toString()
																		.trim()
																+ ".dat";
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {
														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;
															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 5)
																			.toString()
																			.trim()
																	+ "_"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 8)
																			.toString()
																			.trim()
																	+ "_"
																	+ formatter
																			.format(date)
																	+ "_111213"
																	+ "_"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 6)
																			.toString()
																			.trim()
																			.substring(
																					0,
																					templateRecordsData
																							.get(tmpRecRow)
																							.get(formatNeededColumn_No + 6)
																							.toString()
																							.trim()
																							.length() - 6)
																	+ getSequenceNumber(
																			connection,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No)
																					.toString()
																					.trim())
																	+ ".X12820v4010."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 9)
																			.toString()
																			.trim()
																	+ ".dat";
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												}

											} else if (templateRecordsData
													.get(tmpRecRow)
													.get(formatNeededColumn_No + 1)
													.toString()
													.equalsIgnoreCase(
															FormatConstants.scaChannel)) {

												if (!preChannel
														.equalsIgnoreCase(templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim())) {
													oldFormat = "";
													preChannel = FormatConstants.scaChannel;
												}
												preChannel = FormatConstants.scaChannel;
												if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.cibc80Byte
																		.replace(
																				" ",
																				""))) {
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 11)
															.toString().trim()
															.isEmpty()) {
														transaction = 1;
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();

														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 4)
																		.toString()
																		.trim()
																+ "."
																+ formatter1
																		.format(date)
																+ "111213"
																+ "."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 8)
																		.toString()
																		.trim()
																		.substring(
																				0,
																				templateRecordsData
																						.get(tmpRecRow)
																						.get(formatNeededColumn_No + 8)
																						.toString()
																						.trim()
																						.length() - 4)
																+ getSequenceNumber(
																		connection,
																		templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No)
																				.toString()
																				.trim())
																+ ".0080."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 9)
																		.toString()
																		.trim()
																+ ".dat";
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {
														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;

															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 4)
																			.toString()
																			.trim()
																	+ "."
																	+ formatter1
																			.format(date)
																	+ "111213"
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 8)
																			.toString()
																			.trim()
																			.substring(
																					0,
																					templateRecordsData
																							.get(tmpRecRow)
																							.get(formatNeededColumn_No + 8)
																							.toString()
																							.trim()
																							.length() - 4)
																	+ getSequenceNumber(
																			connection,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No)
																					.toString()
																					.trim())
																	+ ".0080."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 9)
																			.toString()
																			.trim()
																	+ ".dat";
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.cibc1464
																		.replace(
																				" ",
																				""))
														|| templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim()
																.replace(" ",
																		"")
																.equalsIgnoreCase(
																		FormatConstants.cpa005
																				.replace(
																						" ",
																						""))) {
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 12)
															.toString().trim()
															.isEmpty()) {
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();
														transaction = 1;

														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 4)
																		.toString()
																		.trim()
																+ "."
																+ formatter1
																		.format(date)
																+ "111213"
																+ "."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 8)
																		.toString()
																		.trim()
																		.substring(
																				0,
																				templateRecordsData
																						.get(tmpRecRow)
																						.get(formatNeededColumn_No + 8)
																						.toString()
																						.trim()
																						.length() - 4)
																+ getSequenceNumber(
																		connection,
																		templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No)
																				.toString()
																				.trim())
																+ ".1464."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 9)
																		.toString()
																		.trim()
																+ ".dat";
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {

														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;
															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 4)
																			.toString()
																			.trim()
																	+ "."
																	+ formatter1
																			.format(date)
																	+ "111213"
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 8)
																			.toString()
																			.trim()
																			.substring(
																					0,
																					templateRecordsData
																							.get(tmpRecRow)
																							.get(formatNeededColumn_No + 8)
																							.toString()
																							.trim()
																							.length() - 4)
																	+ getSequenceNumber(
																			connection,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No)
																					.toString()
																					.trim())
																	+ ".1464."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 9)
																			.toString()
																			.trim()
																	+ ".dat";
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.nacha94
																		.replace(
																				" ",
																				""))) {
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 11)
															.toString().trim()
															.isEmpty()) {
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();
														transaction = 1;
														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 4)
																		.toString()
																		.trim()
																+ "."
																+ formatter1
																		.format(date)
																+ "111213"
																+ "."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 8)
																		.toString()
																		.trim()
																		.substring(
																				0,
																				templateRecordsData
																						.get(tmpRecRow)
																						.get(formatNeededColumn_No + 8)
																						.toString()
																						.trim()
																						.length() - 6)
																+ getSequenceNumber(
																		connection,
																		templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No)
																				.toString()
																				.trim())
																+ ".E0094."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 9)
																		.toString()
																		.trim()
																+ ".dat";
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {

														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;
															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 4)
																			.toString()
																			.trim()
																	+ "."
																	+ formatter1
																			.format(date)
																	+ "111213"
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 8)
																			.toString()
																			.trim()
																			.substring(
																					0,
																					templateRecordsData
																							.get(tmpRecRow)
																							.get(formatNeededColumn_No + 8)
																							.toString()
																							.trim()
																							.length() - 6)
																	+ getSequenceNumber(
																			connection,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No)
																					.toString()
																					.trim())
																	+ ".E0094."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 9)
																			.toString()
																			.trim()
																	+ ".dat";
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.nachaIat94
																		.replace(
																				" ",
																				""))) {
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 11)
															.toString().trim()
															.isEmpty()) {
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();
														transaction = 1;
														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 4)
																		.toString()
																		.trim()
																+ "."
																+ formatter1
																		.format(date)
																+ "111213"
																+ "."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 8)
																		.toString()
																		.trim()
																		.substring(
																				0,
																				templateRecordsData
																						.get(tmpRecRow)
																						.get(formatNeededColumn_No + 8)
																						.toString()
																						.trim()
																						.length() - 6)
																+ getSequenceNumber(
																		connection,
																		templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No)
																				.toString()
																				.trim())
																+ ".I0094."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 9)
																		.toString()
																		.trim()
																+ ".dat";
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {

														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;
															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 4)
																			.toString()
																			.trim()
																	+ "."
																	+ formatter1
																			.format(date)
																	+ "111213"
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 8)
																			.toString()
																			.trim()
																			.substring(
																					0,
																					templateRecordsData
																							.get(tmpRecRow)
																							.get(formatNeededColumn_No + 8)
																							.toString()
																							.trim()
																							.length() - 6)
																	+ getSequenceNumber(
																			connection,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No)
																					.toString()
																					.trim())
																	+ ".I0094."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 9)
																			.toString()
																			.trim()
																	+ ".dat";
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.mt101
																		.replace(
																				" ",
																				""))
														|| templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim()
																.replace(" ",
																		"")
																.equalsIgnoreCase(
																		FormatConstants.mt104
																				.replace(
																						" ",
																						""))
														|| templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim()
																.replace(" ",
																		"")
																.equalsIgnoreCase(
																		FormatConstants.cmo103)) {
													String tempRandomNum = getSequenceNumber(
															connection,
															templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No)
																	.toString()
																	.trim());
													randomNumList1
															.add(tempRandomNum);

													transaction = 1;
													fileName = templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 1)
															.toString().trim()
															+ "/"
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 4)
																	.toString()
																	.trim()
															+ "."
															+ formatter1
																	.format(date)
															+ "111213"
															+ "."
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 8)
																	.toString()
																	.trim()
																	.substring(
																			0,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No + 8)
																					.toString()
																					.trim()
																					.length() - 6)
															+ tempRandomNum
															+ "."
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No)
																	.toString()
																	.trim()
																	.replace(
																			" ",
																			"")
															+ "."
															+ templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 9)
																	.toString()
																	.trim()
															+ ".dat";
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 10)
															.toString()
															.isEmpty()) {
														fileName = fileName
																+ "."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 10)
																		.toString();
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.x820
																		.replace(
																				" ",
																				""))) {
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 11)
															.toString().trim()
															.isEmpty()) {
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();
														transaction = 1;
														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 4)
																		.toString()
																		.trim()
																+ "."
																+ formatter1
																		.format(date)
																+ "111213"
																+ "."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 8)
																		.toString()
																		.trim()
																		.substring(
																				0,
																				templateRecordsData
																						.get(tmpRecRow)
																						.get(formatNeededColumn_No + 8)
																						.toString()
																						.trim()
																						.length() - 6)
																+ getSequenceNumber(
																		connection,
																		templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No)
																				.toString()
																				.trim())
																+ ".X12820v4010."
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 9)
																		.toString()
																		.trim()
																+ ".dat";
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {

														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;
															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 4)
																			.toString()
																			.trim()
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 7)
																			.toString()
																			.trim()
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 8)
																			.toString()
																			.trim()
																			.substring(
																					0,
																					templateRecordsData
																							.get(tmpRecRow)
																							.get(formatNeededColumn_No + 8)
																							.toString()
																							.trim()
																							.length() - 6)
																	+ getSequenceNumber(
																			connection,
																			templateRecordsData
																					.get(tmpRecRow)
																					.get(formatNeededColumn_No)
																					.toString()
																					.trim())
																	+ ".X12820v4010."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 9)
																			.toString()
																			.trim()
																	+ ".dat";
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}

												}

												if (log.isDebugEnabled()) {
													log.debug("Channel is SCA");
													log.debug("FileName is:=>"
															+ fileName);
												}
											} else if (templateRecordsData
													.get(tmpRecRow)
													.get(formatNeededColumn_No + 1)
													.toString()
													.equalsIgnoreCase(
															FormatConstants.fttChannel)) {
												if (!preChannel
														.equalsIgnoreCase(templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim())) {
													oldFormat = "";
													preChannel = FormatConstants.fttChannel;
												}
												preChannel = FormatConstants.fttChannel;
												if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.cibc1464
																		.replace(
																				" ",
																				""))) {
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 12)
															.toString().trim()
															.isEmpty()) {
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();
														transaction = 1;
														/*fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 2)
																		.toString()
																		.trim()
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 3)
																		.toString()
																		.trim()
																+ ".1464";
																*/
														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 2)
																		.toString()
																		.trim()
																+ getSequenceNumber(
																		connection,
																		templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No)
																				.toString()
																				.trim())
																+ ".1464";																
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {

														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;
															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 4)
																			.toString()
																			.trim()
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 7)
																			.toString()
																			.trim()
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 8)
																			.toString()
																			.trim()
																	+ ".1464."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 9)
																			.toString()
																			.trim();
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.nachaIat94
																		.replace(
																				" ",
																				""))) {
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 11)
															.toString().trim()
															.isEmpty()) {
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();
														transaction = 1;
														//here
														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 2)
																		.toString()
																		.trim()
																+ "_"
																+timeStamp
																/*+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 7)
																		.toString()
																		.trim()*/
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No + 3)
																		.toString()
																		.trim()
																+ ".0094";
														
														System.out.println("here after 2800"+fileName);
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {

														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;
															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 2)
																			.toString()
																			.trim()
																	+ "_"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 7)
																			.toString()
																			.trim()
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 3)
																			.toString()
																			.trim()
																	+ ".0094";
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												}

												if (log.isDebugEnabled()) {
													log.debug("Channel is FTT");
													log.debug("FileName is:=>"
															+ fileName);
												}
											} else if (templateRecordsData
													.get(tmpRecRow)
													.get(formatNeededColumn_No + 1)
													.toString()
													.equalsIgnoreCase(
															FormatConstants.ndmChannel)) {
												if (!preChannel
														.equalsIgnoreCase(templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim())) {
													oldFormat = "";
													preChannel = FormatConstants.ndmChannel;
												}
												preChannel = FormatConstants.ndmChannel;
												if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.cibc80Byte
																		.replace(
																				" ",
																				""))) {
													log.debug("If format is CIBC80 Byte");
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 12)
															.toString().trim()
															.isEmpty()) {
														transaction = 1;
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();

														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ "NDM_"
																//+ formatter2.format(date)
																+formatter2.format(date).substring(0,8)+"_"
																+getNDMUniqueNo(connection)
																+".0080";
														System.out.println("fileName ::"+fileName);
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {
														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;
															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 4)
																			.toString()
																			.trim()
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 7)
																			.toString()
																			.trim()
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 8)
																			.toString()
																			.trim()
																	+ ".0080."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 9)
																			.toString()
																			.trim();
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.cibc1464
																		.replace(
																				" ",
																				""))
														|| templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim()
																.replace(" ",
																		"")
																.equalsIgnoreCase(
																		FormatConstants.cpa005
																				.replace(
																						" ",
																						""))) {
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 12)
															.toString().trim()
															.isEmpty()) {
														transaction = 1;
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();
													//	transaction = 1;
														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ "NDM_"
																+formatter2.format(date).substring(0,8)+"_"+
																getNDMUniqueNo(connection)
																//+ formatter2.format(date) 
																 +".1464";
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {

														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;
															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 4)
																			.toString()
																			.trim()
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 7)
																			.toString()
																			.trim()
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 8)
																			.toString()
																			.trim()
																	+ ".1464."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 9)
																			.toString()
																			.trim();
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												} else if (templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString()
														.trim()
														.replace(" ", "")
														.equalsIgnoreCase(
																FormatConstants.nachaIat94
																		.replace(
																				" ",
																				""))) {
													if (!templateRecordsData
															.get(tmpRecRow)
															.get(formatNeededColumn_No + 11)
															.toString().trim()
															.isEmpty()) {
														transaction = 1;
														oldFormat = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim();
														
														fileName = templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim()
																+ "/"
																+ "NDM_"+ formatter2.format(date).substring(0,8) +"_"
																+getNDMUniqueNo(connection)
															//	+ formatter2.format(date)
																+ ".0094";
														if (!templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 10)
																.toString()
																.isEmpty()) {
															fileName = fileName
																	+ "."
																	+ templateRecordsData
																			.get(tmpRecRow)
																			.get(formatNeededColumn_No + 10)
																			.toString();
														}
													} else {

														if (oldFormat
																.equalsIgnoreCase(templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim())) {
															transaction = -1;
														} else {
															transaction = 0;
															fileName = templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 1)
																	.toString()
																	.trim()
																	+ "/"
																	+ "NDM_" +formatter2.format(date).substring(0,8) +"_"
																	+getNDMUniqueNo(connection)
																	//+ formatter2.format(date)
																	+ ".0094";
															if (!templateRecordsData
																	.get(tmpRecRow)
																	.get(formatNeededColumn_No + 10)
																	.toString()
																	.isEmpty()) {
																fileName = fileName
																		+ "."
																		+ templateRecordsData
																				.get(tmpRecRow)
																				.get(formatNeededColumn_No + 10)
																				.toString();
															}
														}
													}
												}

												if (log.isDebugEnabled()) {
													log.debug("Channel is NDM");
													log.debug("FileName is:=>"
															+ fileName);
												}
											} else {
												if (log.isDebugEnabled()) {
													log.debug("Channel is either FTS or SCA or FTT or NDM only pls check in your excel file");
												}
											}
										} else {
											if (log.isDebugEnabled()) {
												log.debug("Channel is Empty in your Excel file pls Check once");
											}
										}

										if (fileName.contains("/")) {
											channel = fileName.substring(0, 3);
											fileName = fileName.replace(channel
													+ "/", "");

										}
										String formatName1 = templateRecordsData
												.get(tmpRecRow)
												.get(formatNeededColumn_No)
												.toString().trim();
										int increase = getModularRow(formatName1);
										String message_id = fetchMessageId(
												formatName1, tmpRecRow,
												formatNeededColumn_No,
												templateRecordsData);// 1.1
																		// Message
																		// id
																		// Configuration
																		// in
																		// table
										
										//1.4 start
										fileNamePojo = new TestToolFileNamePOJO();
										fileNamePojo.setTestCaseid(templateRecordsData.get(tmpRecRow).get(0).toString().trim());
										fileNamePojo.setFormatName(templateRecordsData.get(tmpRecRow).get(formatNeededColumn_No).toString().trim());
										fileNamePojo.setChannel(templateRecordsData.get(tmpRecRow).get(formatNeededColumn_No+1).toString().trim());
										fileNamePojo.setLsi(templateRecordsData.get(tmpRecRow).get(formatNeededColumn_No+2).toString().trim());
										fileNamePojo.setClientRefNum(message_id);
										fileNamePojo.setBicCode(templateRecordsData.get(tmpRecRow).get(formatNeededColumn_No+4).toString().trim());
										fileNamePojo.setBbdId(templateRecordsData.get(tmpRecRow).get(formatNeededColumn_No+5).toString().trim());
										fileNamePojo.setCustRef(templateRecordsData.get(tmpRecRow).get(formatNeededColumn_No+6).toString().trim());
										fileNamePojo.setUniqueId(templateRecordsData.get(tmpRecRow).get(formatNeededColumn_No+8).toString().trim());
										fileNamePojo.setFileName(fileName);
										fileNamePojo.setTimeStamp(dbTimeStamp);
										fileNamePojo.setAppliedRules(templateRecordsData.get(tmpRecRow).get(increase - 2).toString().trim());
										fileNamePojo.setErrorCodes(templateRecordsData.get(tmpRecRow).get(increase -1).toString().trim());
										fileNamePojo.setModifiedField(templateRecordsData.get(tmpRecRow).get(increase +1).toString().trim());
										fileNamePojo.setFolderName(folderName);
										fileNamePojo.setSuiteName(suiteName);  
										testToolData.add(fileNamePojo);
										//1.4 end
										
										insertDBParamMap.put(
												1,
												templateRecordsData
														.get(tmpRecRow).get(0)
														.toString().trim());
										insertDBParamMap
												.put(2,
														templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim());
										insertDBParamMap
												.put(3,
														templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 1)
																.toString()
																.trim());
										insertDBParamMap
												.put(4,
														templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 2)
																.toString()
																.trim());
										insertDBParamMap
												.put(5,
														templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 3)
																.toString()
																.trim());
										insertDBParamMap
												.put(6,
														templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 4)
																.toString()
																.trim());
										insertDBParamMap
												.put(7,
														templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 5)
																.toString()
																.trim());
										insertDBParamMap
												.put(8,
														templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 6)
																.toString()
																.trim());
										insertDBParamMap
												.put(9,
														templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No + 7)
																.toString()
																.trim());
										insertDBParamMap.put(10, fileName);
										insertDBParamList.add(insertDBParamMap);
										generationFiles = new File(
												outputFilePath
														+ "/"
														+ folderName
														+ "_"
														+ timeStamp
														+ "/"
														+ templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim() + "/"
														+ channel);
										String filePath = generationFiles
												.toString();
										if (!generationFiles.exists()) {
											generationFiles.mkdirs();
										}
										generationFiles = new File(
												outputFilePath
														+ "/"
														+ folderName
														+ "_"
														+ timeStamp
														+ "/"
														+ templateRecordsData
																.get(tmpRecRow)
																.get(formatNeededColumn_No)
																.toString()
																.trim() + "/"
														+ channel + "/"
														+ fileName);

										loop1 = new String[3];
										loop1[0] = fileName;
										loop1[1] = filePath;
										loop1[2] = dbTimeStamp;

										loopData.put(tmpRecRow, loop1);

										if (generationFiles.exists()) {

											if (log.isDebugEnabled())
												log.debug("Duplicated File is occured::::"
														+ generationFiles
																.toString());
											if (transaction == 1) {
												generationFiles.delete();
												generationFiles = new File(
														outputFilePath
																+ "/"
																+ folderName
																+ "_"
																+ timeStamp
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim()
																+ "/" + channel
																+ "/"
																+ fileName);
												generationfile_writer = new FileWriter(
														generationFiles,
														temp_statusToWrite);
											} else if (transaction == 0) {

												generationFiles = new File(
														outputFilePath
																+ "/"
																+ folderName
																+ "_"
																+ timeStamp
																+ "/"
																+ templateRecordsData
																		.get(tmpRecRow)
																		.get(formatNeededColumn_No)
																		.toString()
																		.trim()
																+ "/" + channel
																+ "/"
																+ fileName);
												generationfile_writer = new FileWriter(
														generationFiles,
														temp_statusToWrite);
												log.debug("Inside else if block when txn is 0::temp_statusToWrite :::"
														+ temp_statusToWrite);
											} else {
												generationfile_writer
														.write("\n");
											}
										} else {
											generationfile_writer = new FileWriter(
													generationFiles,
													temp_statusToWrite);
										}

										fileName1 = outputFilePath
												+ "/"
												+ folderName
												+ "_"
												+ timeStamp
												+ "/"
												+ templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString().trim()
												+ "/" + channel + "/"
												+ fileName;

										fileList.add(outputFilePath
												+ "/"
												+ folderName
												+ "_"
												+ timeStamp
												+ "/"
												+ templateRecordsData
														.get(tmpRecRow)
														.get(formatNeededColumn_No)
														.toString().trim()
												+ "/" + channel + "/"
												+ fileName);
										fileNameList.add(fileName);

										generationfile_writer
												.write(resultRecord);

										generationfile_writer.flush();
										temp_statusToWrite = true;
										tempCount = tempCount + 1;
										if (log.isDebugEnabled()) {
											log.debug("File is Generated :"
													+ generationFiles
															.toString());
										}

									} else {
										if (log.isDebugEnabled()) {
											log.debug("resultRecord holds null value for that return value is -8");
										}
										return -8;
									}
									tempFormatename = templateRecordsData
											.get(tmpRecRow)
											.get(formatNeededColumn_No)
											.toString().trim();

								} else {
									if (formatName
											.equalsIgnoreCase(FormatConstants.cibc1464)
											|| formatName
													.equalsIgnoreCase(FormatConstants.cibc80Byte)
											|| formatName
													.equalsIgnoreCase(FormatConstants.cpa005)) {
										log.debug("Inside if condition ::::");
										resultRecord = fetchSequence(
												connection, resultRecord,
												formatName);
										log.debug("resultRecord ::"
												+ resultRecord);
									}
									transaction = 0;
									if (resultRecord != null
											&& !resultRecord.trim().isEmpty()) {
										// 1.3
										if(continuesCMO103ROWNO>=tmpRecRow){
											resultRecord="\n"+resultRecord;
										}
										generationfile_writer
												.write(resultRecord);
										generationfile_writer.flush();
									}
								}

								if (tmpRecRow == templateRecordsData.size() - 1) {
									allRecList.add(recordWiseList);
									allTagList.add(recordWiseTagList);

								}
							}
						}
						// FG_1.1 (S)

						
						for (int i = 0; i < allRecList.size(); i++) {
							String fileToWrite = (String) fileList.get(i);
							List recordWiseList1 = (ArrayList) allRecList
									.get(i);
							List recordWiseTagList1 = (ArrayList) allTagList
									.get(i);
							List badResultString = foramtsGeneration
									.modifyRecordWiseTags(recordWiseList1,
											recordWiseTagList1, variants);

							Map insertDBParm = (HashMap) insertDBParamList
									.get(i);
							String ruleType = null;
							String tagApplied = null;
							String transactionNumber = null;
							String errorCode = null;
							for (int k = 0; k < badResultString.size(); k++) {
								MessageTracking msT = (MessageTracking) badResultString
										.get(k);
								String resultRecord = msT.getResultString();

								ruleType = msT.getRuleType();

								if ("M".equals(ruleType)) {
									ruleType = "TAG_MISSING";
									errorCode = "TAG_MISSING_ERRORCODE";
								} else if ("D".equals(ruleType)) {
									ruleType = "DUPLICATE_TAG";
									errorCode = "DUPLICATE_TAG_ERRORCODE";
								} else if ("I".equals(ruleType)) {
									ruleType = "INVALID_TAG";
									errorCode = "INVALID_TAG_ERRORCODE";
								} else if ("O".equals(ruleType)) {
									ruleType = "TAG_ORDER_CHANGE";
									errorCode = "TAG_ORDER_CHANGE_ERRORCODE";
								}
								tagApplied = msT.getTagApplied();
								transactionNumber = msT.getTransactionNumber();
								// log.debug("transactionNumber :::"+transactionNumber);
								String seqNum = getSequenceNumber(connection,
										formatName);
								
								/*readTemplate
										.insertFileInDB(
												(String) insertDBParm.get(1),
												(String) insertDBParm.get(2),
												(String) insertDBParm.get(3),
												(String) insertDBParm.get(4),
												(String) insertDBParm.get(5),
												(String) insertDBParm.get(6),
												(String) insertDBParm.get(7),
												(String) insertDBParm.get(8),
												(String) insertDBParm.get(9),
												((String) insertDBParm.get(10))
														.replaceAll(
																(String) randomNumList1
																		.get(i),
																seqNum),
												ruleType, errorCode,
												tagApplied, db_table_file,
												connection, dbTimeStamp);*/
								generationFiles = new File(
										fileToWrite.replaceAll(
												randomNumList1.get(i) + "",
												seqNum));
								generationfile_writer = new FileWriter(
										generationFiles, true);
								generationfile_writer.write(resultRecord);
								generationfile_writer.flush();
							}
						}

						// FG_1.1 (E)
					}
				}
				if ((selectedItems.contains(FormatConstants.ALL) && tmpcount == 10) 
						|| (!selectedItems.contains(FormatConstants.ALL) && tmpcount==selectedItems.size())) {
					resultNum = 1;
					break;
				}
			}
			readTemplate = new ReadTemplate();
			//1.4 start
			
		//	System.out.println("testToolData before if block ::"+testToolData);
			if(!testToolData.isEmpty()){
				int size = readTemplate.fetchDataDetails(suiteName,connection);
				//	System.out.println("size ::"+size);
				if (size != 0 ) {

					readTemplate.insertIntoAuditTable(suiteName,connection);
					readTemplate.deleteDataFromDB(suiteName,connection);
				} 

				for(TestToolFileNamePOJO str : testToolData){
					//	System.out.println("Insert into test_tool_file_mame ");

					readTemplate.insertFileInDB(str.getTestCaseid(), str.getFormatName(), str.getChannel(),
							str.getLsi(), str.getClientRefNum(), str.getBicCode(), str.getBbdId(), str.getCustRef(),
							str.getUniqueId(), str.getFileName(), str.getAppliedRules(), str.getErrorCodes(), str.getModifiedField(),
							db_table_file, suiteName, connection, dbTimeStamp);
				}
			}
			
			
			//1.4 end 
		} catch (Exception e) {
			if (log.isDebugEnabled())
				log.error("Exception while Generating the files in Service class");
			log.fatal("Exception ==", e);
			// Throw exception
			throw (e);
		} finally {
			DBUtility.closeConnection(connection);
		}
		return resultNum;
	}

	public int getModularRow(String formatName) {

		int increase = 0;

		if (FormatConstants.cmo103.equals(formatName)
				|| FormatConstants.mt101.equals(formatName)
				|| FormatConstants.mt104.equals(formatName)) {
			increase = Integer.parseInt(FormatConstants.swiftModularPosition);
		} else if (FormatConstants.cibc1464.equals(formatName)
				|| FormatConstants.cibc80Byte.equals(formatName)
				|| FormatConstants.nachaIat94.equals(formatName)
				|| FormatConstants.cpa005.equals(formatName)
				|| FormatConstants.nacha94.equals(formatName)) {
			increase = Integer.parseInt(FormatConstants.nativeModularPosition);
		} else if (FormatConstants.x820.equals(formatName)) {
			increase = Integer.parseInt(FormatConstants.ansiModularPosition);
		}

		return increase;

	}

	// 1.1 Message id Configuration in table [s]
	public String fetchMessageId(String formatName, int tmpRecRow,
			int formatNeededColumn_No, Map<Integer, Map> templateRecordsData) {

		String message_id = "";

		if (null != formatName && !formatName.isEmpty()) {
			if ((FormatConstants.mt101.equalsIgnoreCase(formatName))
					|| (FormatConstants.mt104.equalsIgnoreCase(formatName))) {

				message_id = templateRecordsData.get(tmpRecRow)
						.get(formatNeededColumn_No + 29).toString();

			} else if (FormatConstants.nachaIat94.equalsIgnoreCase(formatName)
					|| FormatConstants.nacha94.equals(formatName)) {
				message_id = templateRecordsData.get(tmpRecRow)
						.get(formatNeededColumn_No + 29).toString();

			} else if (FormatConstants.cibc1464.equalsIgnoreCase(formatName)
					|| FormatConstants.cpa005.equals(formatName)
					|| FormatConstants.cibc80Byte.equalsIgnoreCase(formatName)) {

				message_id = templateRecordsData.get(tmpRecRow)
						.get(formatNeededColumn_No + 19).toString();

			} else if ((FormatConstants.cmo103.equalsIgnoreCase(formatName))) {

				message_id = templateRecordsData.get(tmpRecRow)
						.get(formatNeededColumn_No + 63).toString();
			} else if ((FormatConstants.x820.equalsIgnoreCase(formatName))) {

				message_id = templateRecordsData.get(tmpRecRow)
						.get(formatNeededColumn_No + 30).toString();
			}

			return message_id;
		}
		return message_id;
	}

	// 1.1 Message id Configuration in table [E]

	public String fetchSequence(Connection con, String resultString,
			String formatName) throws Exception {
		String seqNum = "";

		try {
			if(con== null){
				con = DBUtility.openConnection();
			}
			seqNum = getSeqNumber(con, formatName);
			if (resultString.startsWith("1")
					&& formatName.equalsIgnoreCase(FormatConstants.cibc80Byte)) {
				resultString = resultString.replace(
						resultString.substring(29, 33), seqNum);
			} else if (resultString.startsWith("A")
					&& (formatName.equalsIgnoreCase(FormatConstants.cibc1464) || formatName
							.equalsIgnoreCase(FormatConstants.cpa005))) {
				resultString = resultString.replace(
						resultString.substring(20, 24), seqNum);
			}

		} catch (Exception e) {
			throw (e);
		}
		return resultString;

	}
	
	// FG_1.1 (S)
		public static String getNDMUniqueNo(Connection con)
				throws Exception {
			// 1.2 starts
			String seq = "NDM_TIME_FORMAT_SEQ.NEXTVAL";
			String generateSeq = null;

				generateSeq = "SELECT LPAD(MOD(FLOOR( " + seq
						+ "/(60*60)),24),2,'0') || LPAD(MOD(FLOOR( " + seq
						+ "/(60)),60),2,'0') || LPAD(MOD( " + seq
						+ ",60),2,'0') MI FROM DUAL";
				
			//	log.debug("generateSeq if block :::"+generateSeq);
			

			Statement stmt = null;
			ResultSet rs = null;
			String sequencNum = null;
			try {
				stmt = con.createStatement();
				rs = stmt.executeQuery(generateSeq);
				if (rs.next()) {
					sequencNum = rs.getString(1);
				}
				
			} catch (SQLException e) {
				log.fatal("SQL Exception ", e);
				throw (e);
			} finally {
				DBUtility.closeResultSet(rs);
				DBUtility.closeStatement(stmt);
			}
			return sequencNum;
		}

		// FG_1.1 (E)
	
}