/********************************************************************************************************/
/* Copyright @  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/*				 									 													*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : AllFormatsGenerationController.java                                 					*/
/*                                                                                      				*/
/********************************************************************************************************/
/* 				 Author					          |        	  Date   			|	  Version           */
/********************************************************************************************************/
/*         	Surendra Reddy.M                              20:05:2016					1.0                    
 *          preetam sanjore              	   |	      07:04:2017			|		1.1	            
 *          Preetam sanjore						|		  18:07:2017			|		1.2
 *          Shruti Gupta						|		  30:01:2018			|		1.3(Code changes for unique message id  
 *          																			of multiple logical files )
 *			Gokaran Tiwari						|		  12:02:2018			|       1.4 Issue fix for ANSI file end 
 *																						unique number matching with header unique number
 *			Gokaran Tiwari						|         13:02:2018			|		1.5 Issue fix dynamic file creation number
 *			Shruti Gupta						|		  14.03.2018			|	    1.6 Code changes for character sequence

 /********************************************************************************************************/
/* 																										*/
/* 	Description  : Main Controller 	
 * version id       																	*/
/*        			 																					*/
/********************************************************************************************************/

package com.intellect.service;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.log4j.Logger;

import com.intellect.db.DBUtility;
import com.intellect.property.JulianDate;
import com.intellect.property.FormatConstants;

public class UpdateFormatXlsService {
	static ResultSet rs = null;
	static Statement statement = null;
	private static org.apache.log4j.Logger log = Logger
			.getLogger(UpdateFormatXlsService.class);

	public static void updatePain1Format(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		Connection connection = DBUtility.openConnection();
		int count = workbook.getSheetAt(1).getLastRowNum();
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;

		String[] Msg_id = { "", "" };
		for (int i = 6; i <= count; i++) {
			Row row = sheet.getRow(i);

			if (flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {

				flag = 0;
			}
			if (FormatConstants.pain001.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString())) 
			{
				cell = row.getCell(17);
			
				Msg_id = getData(connection, FormatConstants.pain001);
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				cell.setCellValue(Msg_id[0]);

				cell = row.getCell(42, Row.CREATE_NULL_AS_BLANK);

				
				// if(!cell.toString().isEmpty()){
				// cell.setCellValue(Msg_id[1]);
				// }

				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				// insertData(FormatConstants.pain001, MSG_ID, TXN_REF_NUM, connection);
				flag = 1;
			}

			else if (null != row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString()
					&& flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {

				cell = row.getCell(42, Row.CREATE_NULL_AS_BLANK);

				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				// if(!cell.getStringCellValue().isEmpty()){

				// cell.setCellValue(Msg_id[1]);
				// }
				TXN_REF_NUM = Msg_id[1];
				// insertData(FormatConstants.pain001, MSG_ID, TXN_REF_NUM, connection);
			}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();
	}
//Jo-This method is where changes are needed
	public static void updatePain1_RMTCHKFormat(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		Connection connection = DBUtility.openConnection();
		int count = workbook.getSheetAt(2).getLastRowNum();
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		String chqnum = null;
		String[] CHQ_NUM = { "", "" };// 1.2 cheq number dynamic enrichment
		String[] Msg_id = { "", "" };
		for (int i = 6; i <= count; i++) {
			Row row = sheet.getRow(i);
			if (flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				flag = 0;
			}
			if (FormatConstants.pain001.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString())) {
				cell = row.getCell(17);

				Msg_id = getData(connection, FormatConstants.pain001);
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				cell.setCellValue(Msg_id[0]);

				cell = row.getCell(42, Row.CREATE_NULL_AS_BLANK);
				if (!cell.toString().isEmpty()) {
					// cell.setCellValue(Msg_id[1]);
				}// 1.2 [s]
				CHQ_NUM = getData(connection, "Pain001v03_CHQNumber");
				CHQ_NUM[0] = getFileCreationNumCPA(CHQ_NUM[0]);
				//Jo- Change this reference for cheque no to be displayed correctly
				cell = row.getCell(60, Row.CREATE_NULL_AS_BLANK);
				if (!cell.getStringCellValue().isEmpty()) {
					cell.setCellValue(CHQ_NUM[0]);
				}
				chqnum = CHQ_NUM[0];
				TXN_REF_NUM = Msg_id[1];
				/*
				 * if(!cell.getStringCellValue().isEmpty()){
				 * insertData("Pain001v03_CHQNumber", chqnum, TXN_REF_NUM,
				 * connection); }
				 */// 1.2 [E]
					// //System.out.println("updatePain1_RMTCHKFormat flag "+flag);
				MSG_ID = Msg_id[0];
				// insertData(FormatConstants.pain001, MSG_ID, TXN_REF_NUM, connection);
				flag = 1;
			}

			else if (null != row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString()
					&& flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {

				cell = row.getCell(42, Row.CREATE_NULL_AS_BLANK);

				// if(!cell.getStringCellValue().isEmpty()){
				// cell.setCellValue(Msg_id[1]);
				// }
				CHQ_NUM = getData(connection, "Pain001v03_CHQNumber");// 1.2 [s]
				CHQ_NUM[0] = getFileCreationNumCPA(CHQ_NUM[0]);
				cell = row.getCell(60, Row.CREATE_NULL_AS_BLANK);
				if (!cell.getStringCellValue().isEmpty()) {
					cell.setCellValue(CHQ_NUM[0]);
				}

				chqnum = CHQ_NUM[0];
				// if(!cell.getStringCellValue().isEmpty()){
				// insertData("Pain001v03_CHQNumber", chqnum, TXN_REF_NUM,
				// connection);
				// }//1.2 [e]

				TXN_REF_NUM = Msg_id[1];
				// insertData(FormatConstants.pain001, MSG_ID, TXN_REF_NUM, connection);
			}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();
	}
	//Jo added new method for CAMT format
	
	public static void updateCAMTFormat(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		Connection connection = DBUtility.openConnection();
		int count = workbook.getSheetAt(9).getLastRowNum();
		//CHECKING THE VALUE
		// count =12;
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		String chqnum = null;
		String[] CHQ_NUM = { "", "" };// 1.2 cheq number dynamic enrichment
		String[] Msg_id = { "", "" };
		
		//
		for (int i = 6; i <= count; i++) {
			Row row = sheet.getRow(i);

			if (flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {

				flag = 0;
			}
			if (FormatConstants.CAMT.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString())) 
			{
				cell = row.getCell(17);
			
				Msg_id = getData(connection, FormatConstants.CAMT);
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				/*cell.setCellValue(Msg_id[0]);

				cell = row.getCell(42, Row.CREATE_NULL_AS_BLANK);*/

				
				

				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
			
				flag = 1;
			}

			else if (null != row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString()
					&& flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {

			//	cell = row.getCell(42, Row.CREATE_NULL_AS_BLANK);

				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				
				TXN_REF_NUM = Msg_id[1];
				
			}
		}
		//
		
		
/*		for (int i = 6; i <= count; i++) {
			Row row = sheet.getRow(i);
			if (flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				flag = 0;
			}
			if (FormatConstants.CAMT.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString())) {
				cell = row.getCell(17);

				Msg_id = getData(connection, FormatConstants.CAMT);
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				cell.setCellValue(Msg_id[0]);

			}

			
		}*/
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();
	}

	
	//CAMT changes end here
	public static void updatePain8eFormat(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		// int count=workbook.getSheetAt(2).getLastRowNum();
		int count = workbook.getSheetAt(3).getLastRowNum();// 1.1 sheet sequence
															// related changes
															// as per 1.6
															// version
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		Connection connection = DBUtility.openConnection();
		String[] Msg_id = { "", "" };
		for (int i = 6; i <= count; i++) {
			Row row = sheet.getRow(i);
			if (null != row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
					&& flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				flag = 0;
			}
			if (FormatConstants.pain008.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString())) {
				cell = row.getCell(17);
				Msg_id = getData(connection, FormatConstants.pain008);
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				cell.setCellValue(Msg_id[0]);

				cell = row.getCell(42, Row.CREATE_NULL_AS_BLANK);
				// if(!cell.getStringCellValue().isEmpty()){
				// cell.setCellValue(Msg_id[1]);
				// }

				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				// insertData(FormatConstants.pain008, MSG_ID, TXN_REF_NUM, connection);
				flag = 1;
			}

			else if (flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				cell = row.getCell(42, Row.CREATE_NULL_AS_BLANK);

				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				// if(!cell.getStringCellValue().isEmpty()){
				// cell.setCellValue(Msg_id[1]);
				// }

				TXN_REF_NUM = Msg_id[1];
				// insertData(FormatConstants.pain008, MSG_ID, TXN_REF_NUM, connection);
			}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();
	}

	public static void updatePain8_RmtFormat(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		int count = workbook.getSheetAt(4).getLastRowNum();// 1.1 sheet sequence
															// related changes
															// as per 1.6
															// version
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		Connection connection = DBUtility.openConnection();
		String[] Msg_id = { "", "" };
		for (int i = 6; i <= count; i++) {
			Row row = sheet.getRow(i);

			if (null != row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
					&& flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				flag = 0;
			}
			if (FormatConstants.pain008.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString())) {
				cell = row.getCell(17);
				Msg_id = getData(connection, FormatConstants.pain008);
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);

				cell.setCellValue(Msg_id[0]);

				cell = row.getCell(42, Row.CREATE_NULL_AS_BLANK);
				if (!cell.getStringCellValue().isEmpty()) {
					// cell.setCellValue(Msg_id[1]);
				}

				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				// insertData(FormatConstants.pain008, MSG_ID, TXN_REF_NUM, connection);
				flag = 1;
			}

			else if (flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				cell = row.getCell(42, Row.CREATE_NULL_AS_BLANK);

				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				// if(!cell.getStringCellValue().isEmpty()){
				// cell.setCellValue(Msg_id[1]);
				// }

				TXN_REF_NUM = Msg_id[1];
				// insertData(FormatConstants.pain008, MSG_ID, TXN_REF_NUM, connection);
			}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();
	}

	public static void updateISORemitFormat(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		int count = workbook.getSheetAt(8).getLastRowNum();// 1.1 sheet sequence
															// related changes
															// as per 1.6
															// version
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		Connection connection = DBUtility.openConnection();

		String[] Msg_id = { "", "" };
		for (int i = 6; i <= count; i++) {
			Row row = sheet.getRow(i);

			if (null != row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
					&& flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				flag = 0;
			}
			if ("remt001v02".equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString())) {
				cell = row.getCell(17);
				Msg_id = getData(connection, "remt001v02");
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);

				cell.setCellValue(Msg_id[0]);

				// if(!cell.getStringCellValue().isEmpty()){
				// cell.setCellValue(Msg_id[1]);
				// }

				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				// insertData("remt001v02", MSG_ID, TXN_REF_NUM, connection);
				flag = 1;
			}

			else if (flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				// cell = row.getCell(42,Row.CREATE_NULL_AS_BLANK);

				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				// if(!cell.getStringCellValue().isEmpty()){
				// cell.setCellValue(Msg_id[1]);
				// }

				TXN_REF_NUM = Msg_id[1];
				// insertData("remt001v02", MSG_ID, TXN_REF_NUM, connection);
			}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		// //System.out.println("path r "+inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();
	}

	public static void updateEMTXMLFormat(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		int count = workbook.getSheetAt(7).getLastRowNum();// 1.1 sheet sequence
															// related changes
															// as per 1.6
															// version
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		Connection connection = DBUtility.openConnection();
		String[] Msg_id = { "", "" };
		for (int i = 6; i <= count; i++) {
			Row row = sheet.getRow(i);
			if (null != row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
					&& flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				// //System.out.println("break loop");
				flag = 0;
			}
			if ("EMTXML".equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString())) {
				cell = row.getCell(17);
				Msg_id = getData(connection, "EMTXML");
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				cell.setCellValue(Msg_id[0]);

				// cell = row.getCell(42,Row.CREATE_NULL_AS_BLANK);
				// if(!cell.getStringCellValue().isEmpty()){
				// cell.setCellValue(Msg_id[1]);
				// }

				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				insertData("EMTXML", MSG_ID, TXN_REF_NUM, connection);
				flag = 1;
			}

			else if (flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				// cell = row.getCell(42,Row.CREATE_NULL_AS_BLANK);

				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				// if(!cell.getStringCellValue().isEmpty()){
				// cell.setCellValue(Msg_id[1]);
				// }

				TXN_REF_NUM = Msg_id[1];
				insertData("EMTXML", MSG_ID, TXN_REF_NUM, connection);
			}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();
	}

	public static void updateAnsiFormat(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		int count = workbook.getSheetAt(6).getLastRowNum(); // 1.1 sheet
															// sequence related
															// changes as per
															// 1.6 version
		int flag = 0;
		Boolean chqflag = false;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		String chqnum = null;
		String[] CHQ_NUM = { "", "" };// 1.2
		Connection connection = DBUtility.openConnection();
		String[] Msg_id = { "", "" };
		for (int i = 10; i <= count; i++) {
			//log.debug("Inside for loop :::"+(i+1) +"count :::"+count );
			Row row = sheet.getRow(i);
			//log.debug("flag :::"+flag);
			if (flag == 1 && !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				//log.debug("format name :::"+row.getCell(2));
				flag = 0;
				//log.debug("flag :::"+flag);
			}
			//log.debug("flag :::"+flag);
			if ("X820".equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString())) {
				//log.debug("Format name inside if block :::"+row.getCell(2));
				cell = row.getCell(32);
				//log.debug("Cell value at 32 position :::"+cell);
				Msg_id = getData(connection, "X820");
				
				resetData(connection, "X820", Msg_id[0]);// 1.1
				//log.debug("Msg_id :::"+Msg_id[0]);
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);

				cell.setCellValue(Msg_id[0]);
				//log.debug("Cell value after set value :::"+cell);
				// cell = row.getCell(42,Row.CREATE_NULL_AS_BLANK);//batch level
				// unique id
				if (!cell.toString().isEmpty()) {
					// cell.setCellValue(Msg_id[1]);
					// row.getCell(143,Row.CREATE_NULL_AS_BLANK).setCellValue(Msg_id[1]);
					if ((i + 1) <= count) {
						//log.debug("i+1::::::"+(i+1) +" ::count ::"+count);
						if (("X820".equals(sheet.getRow(i + 1)
								.getCell(2, Row.CREATE_NULL_AS_BLANK)
								.toString()))) {
							
							
							// file trailer set
							// row.getCell(151,Row.CREATE_NULL_AS_BLANK).setCellValue(Msg_id[0]);
							row.getCell(160, Row.CREATE_NULL_AS_BLANK).setCellValue(Msg_id[0]);

						}
					} else if ((i + 1) > count) {
						//log.debug("In else if block :::(i+1) ::"+(i+1) + "count ::"+count);
						// row.getCell(151,Row.CREATE_NULL_AS_BLANK).setCellValue(Msg_id[0]);
						//log.debug("trailer value ::::"+row.getCell(160));
						row.getCell(160, Row.CREATE_NULL_AS_BLANK)
								.setCellValue(Msg_id[0]);
					}
				} // 1.2 [S]
				CHQ_NUM = getData(connection, "X820_CHQNumber");
				CHQ_NUM[0] = getFileCreationNumCPA(CHQ_NUM[0]);
				cell = row.getCell(77, Row.CREATE_NULL_AS_BLANK);
				if (!cell.getStringCellValue().isEmpty()) {
					chqflag = isNumeric(cell.getStringCellValue());
					if (chqflag) {
						cell.setCellValue(CHQ_NUM[0]);
					}

				}

				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				chqnum = CHQ_NUM[0];
				if (!cell.toString().isEmpty()) {
					if (chqflag) {
						// insertData("X820_CHQNumber",chqnum , TXN_REF_NUM,
						// connection);
					}
					chqflag = false;
				}// 1.2 [E]
					// //System.out.println("X820-- MSG_ID::"+MSG_ID+" TXN_REF_NUM::: "+TXN_REF_NUM);
					// row.getCell(70,Row.CREATE_NULL_AS_BLANK).setCellValue(Msg_id[1]);//
					// txn level changes
					// insertData("X820", MSG_ID, TXN_REF_NUM, connection);
				flag = 1;
			} else if (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
					.isEmpty()
					&& !(row.getCell(32, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				//log.debug("Inside else if block :::"+row.getCell(2)+": row.getCell(32):" +row.getCell(32));
				
				cell = row.getCell(32);
				//log.debug("cell value at 32 position :::"+cell);
				Msg_id = getData(connection, "X820");
				resetData(connection, "X820", Msg_id[0]);// 1.1
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				//log.debug("msg[0] :" +Msg_id[0]);
				cell.setCellValue(Msg_id[0]);
				//log.debug("cell value set at 32 position :::"+cell);
				if (!cell.toString().isEmpty()) {
					if ((i + 1) <= count) {
						//log.debug("i+1 :::"+(i+1));
						if ("X820".equals(sheet.getRow(i + 1)
								.getCell(2, Row.CREATE_NULL_AS_BLANK)
								.toString())) {
							
							// file trailer set
							row.getCell(160, Row.CREATE_NULL_AS_BLANK)
									.setCellValue(Msg_id[0]);
						}
					} else if ((i + 1) > count) {
						row.getCell(160, Row.CREATE_NULL_AS_BLANK)
								.setCellValue(Msg_id[0]);
					}
				} // 1.2 [S]
				CHQ_NUM = getData(connection, "X820_CHQNumber");
				CHQ_NUM[0] = getFileCreationNumCPA(CHQ_NUM[0]);
				cell = row.getCell(77, Row.CREATE_NULL_AS_BLANK);
				if (!cell.getStringCellValue().isEmpty()) {
					chqflag = isNumeric(cell.getStringCellValue());
					if (chqflag) {
						cell.setCellValue(CHQ_NUM[0]);
					}

				}

				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				chqnum = CHQ_NUM[0];
				if (!cell.toString().isEmpty()) {
					if (chqflag) {
						// insertData("X820_CHQNumber",chqnum , TXN_REF_NUM,
						// connection);
					}
					chqflag = false;
				}// 1.3 [E]

				flag = 1;

			} else if (flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				// cell = row.getCell(42,Row.CREATE_NULL_AS_BLANK);

				// Msg_id[1]=getCrossRefNumCPA(Msg_id[1]);
				// 1.4
				cell = (cell == null || cell.toString().isEmpty()) ? row.getCell(160) : cell;
				if (cell != null && !cell.toString().isEmpty()) {
					// cell.setCellValue(Msg_id[1]);
					// row.getCell(143,Row.CREATE_NULL_AS_BLANK).setCellValue(Msg_id[1]);
					if ((i + 1) <= count) {
						if (("X820".equals(sheet.getRow(i + 1)
								.getCell(2, Row.CREATE_NULL_AS_BLANK)
								.toString()))
								|| !(row.getCell(160, Row.CREATE_NULL_AS_BLANK)
										.toString().isEmpty())) {
							// file trailer set
							// row.getCell(151,Row.CREATE_NULL_AS_BLANK).setCellValue(Msg_id[0]);
							row.getCell(160, Row.CREATE_NULL_AS_BLANK)
									.setCellValue(Msg_id[0]);
							
						}
					} else if ((i + 1) > count) {
						// row.getCell(151,Row.CREATE_NULL_AS_BLANK).setCellValue(Msg_id[0]);
						row.getCell(160, Row.CREATE_NULL_AS_BLANK)
								.setCellValue(Msg_id[0]);
					}
				}// 1.2 [S]
				CHQ_NUM = getData(connection, "X820_CHQNumber");
				CHQ_NUM[0] = getFileCreationNumCPA(CHQ_NUM[0]);
				cell = row.getCell(77, Row.CREATE_NULL_AS_BLANK);

				if (!cell.getStringCellValue().isEmpty()) {
					chqflag = isNumeric(cell.getStringCellValue());
					if (chqflag) {
						cell.setCellValue(CHQ_NUM[0]);
					}
				}
				// row.getCell(70,Row.CREATE_NULL_AS_BLANK).setCellValue(Msg_id[1]);//
				// txn level changes
				chqnum = CHQ_NUM[0];
				TXN_REF_NUM = Msg_id[1];

				if (!cell.toString().isEmpty()) {
					if (chqflag) {
						// insertData("X820_CHQNumber",chqnum , TXN_REF_NUM,
						// connection);
						chqflag = false;
					}
				}// 1.2 [E]
					// insertData("X820", MSG_ID, TXN_REF_NUM, connection);
					// Code changes for multiple logical files 1.3 (S)
			}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();

	}

	public static void updateCPA005Format(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		Connection connection = DBUtility.openConnection();
		int count = workbook.getSheetAt(0).getLastRowNum();
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;

		String[] Msg_id = { "", "" };
		for (int i = 20; i <= count; i++) {
			Row row = sheet.getRow(i);
			if (flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				// System.out.println("updateCPA005Format break loop");
				flag = 0;
			}
			if (FormatConstants.cpa005.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString())) {
				cell = row.getCell(21);

				Msg_id = getData(connection, FormatConstants.cpa005);
				resetData(connection, FormatConstants.cpa005, Msg_id[0]);// 1.1
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);// 1.1
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				cell.setCellValue(Msg_id[0]);
				row.getCell(20).setCellValue(JulianDate.convertToJulian());
				cell = row.getCell(102, Row.CREATE_NULL_AS_BLANK);
				if (!cell.getStringCellValue().isEmpty()) {
					cell.setCellValue(Msg_id[1]);
				}
				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				// insertData("CPA 005", MSG_ID, TXN_REF_NUM, connection);
				flag = 1;
			}

			else if (flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				cell = row.getCell(102, Row.CREATE_NULL_AS_BLANK);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				// if(!cell.getStringCellValue().isEmpty()){
				// cell.setCellValue(Msg_id[1]);
				// }

				TXN_REF_NUM = Msg_id[1];
				// insertData("CPA 005", MSG_ID, TXN_REF_NUM, connection);
			}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		workbook.write(outputStream);

		outputStream.close();
		if (connection != null)
			connection.close();

	}

	public static void updateCIBC80Format(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		int count = workbook.getSheetAt(0).getLastRowNum();
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		Connection connection = DBUtility.openConnection();
		String[] Msg_id = { "", "" };
		for (int i = 20; i <= count; i++) {
			Row row = sheet.getRow(i);
			if (flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				flag = 0;
			}
			if (FormatConstants.cibc80Byte.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString())) {
				// System.out.println("row.getCell(21)::"+row.getCell(21));
				cell = row.getCell(21);

				Msg_id = getData(connection, FormatConstants.cibc80Byte);// 1.1
				// //log.debug("Msg_id :: "+Msg_id);
				resetData(connection, FormatConstants.cibc80Byte, Msg_id[0]);// 1.1
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				cell.setCellValue(Msg_id[0]);
				row.getCell(20).setCellValue(JulianDate.convertToJulian());
				cell = row.getCell(102, Row.CREATE_NULL_AS_BLANK);
				// //log.debug("Msg_id[0] :::"+Msg_id[0]
				// +"  Msg_id[1]  "+Msg_id[1]
				// +"row.getCell(20)  ::::"+row.getCell(20) );
				if (!cell.getStringCellValue().isEmpty()) {
					cell.setCellValue(Msg_id[1]);
				}

				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				// insertData("CIBC 80 BYTE", MSG_ID, TXN_REF_NUM, connection);
				flag = 1;
			}

			else if (flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				cell = row.getCell(102, Row.CREATE_NULL_AS_BLANK);
				// System.out.println("CIBC80 ABHI "+Msg_id[1]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				// if(!cell.getStringCellValue().isEmpty()){
				// System.out.println(cell.getStringCellValue());
				// cell.setCellValue(Msg_id[1]);
				// }
				TXN_REF_NUM = Msg_id[1];
				// insertData("CIBC 80 BYTE", MSG_ID, TXN_REF_NUM, connection);

				// System.out.println("flag 1 CIBC 80 BYTE Msg_id[0] ::::"+MSG_ID);
				// System.out.println("flag 1 CIBC 80 BYTE Msg_id[1]::::"+Msg_id[1]);
			}

		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		// System.out.println("path r "+inputUploadedPath);
		workbook.write(outputStream);
		outputStream.close();
		if (connection != null)
			connection.close();

	}

	public static void updateCIBC1464Format(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		int count = workbook.getSheetAt(0).getLastRowNum();
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		Connection connection = DBUtility.openConnection();
		// System.out.println("count "+count);
		String[] Msg_id = { "", "" };
		for (int i = 20; i <= count; i++) {
			Row row = sheet.getRow(i);

			if (flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				// //System.out.println("break loop");
				flag = 0;
			}
			// 1.5
			String formatName=row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString();
			String fileCreationNo=row.getCell(21, Row.CREATE_NULL_AS_BLANK).toString();
			if (FormatConstants.cibc1464.equals(formatName) 
					|| (formatName.isEmpty() && flag==1 && !fileCreationNo.isEmpty())) {
				cell = row.getCell(21);

				Msg_id = getData(connection, FormatConstants.cibc1464);
				resetData(connection, FormatConstants.cibc1464, Msg_id[0]);// 1.1
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);// 1.1
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				row.getCell(20).setCellValue(JulianDate.convertToJulian());
				cell.setCellValue(Msg_id[0]);
				cell = row.getCell(102, Row.CREATE_NULL_AS_BLANK);

				// if(!cell.getStringCellValue().isEmpty()){
				// System.out.println("transaction ref num:::::::"+cell.getStringCellValue());
				// cell.setCellValue(Msg_id[1]);
				// }
				// System.out.println(" flag 0 CIBC 1464 ::::"+cell.getStringCellValue());
				// System.out.println("flag 0 CIBC 1464 Msg_id[0] ::::"+MSG_ID);
				// System.out.println("flag 0 CIBC 1464 Msg_id[1]::::"+Msg_id[1]);

				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				// insertData("CIBC 1464", MSG_ID, TXN_REF_NUM, connection);
				flag = 1;
			}

			else if (flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				cell = row.getCell(102, Row.CREATE_NULL_AS_BLANK);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				// if(!cell.getStringCellValue().isEmpty()){
				// cell.setCellValue(Msg_id[1]);
				// System.out.println(" flag 1 CIBC 1464 ::::"+cell.getStringCellValue());
				// }

				// System.out.println("flag 1 CIBC 1464 Msg_id[0] ::::"+MSG_ID);
				// System.out.println("flag 1 CIBC 1464 Msg_id[1]::::"+Msg_id[1]);
				TXN_REF_NUM = Msg_id[1];
				// insertData("CIBC 1464", MSG_ID, TXN_REF_NUM, connection);
			}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		// System.out.println("path r "+inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();
	}

	public static void updateNACHA_IATFormat(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		// //log.debug("Inside  updateNACHA_IATFormat  :::::");
		int count = workbook.getSheetAt(0).getLastRowNum();
		//log.debug("count :::" + count);

		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		Connection connection = DBUtility.openConnection();
		// System.out.println("count "+count);
		String[] Msg_id = { "", "" };
		String charSeq = null;
		for (int i = 20; i <= count; i++) {
			Row row = sheet.getRow(i);

			if (flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				flag = 0;
			}
			if (FormatConstants.nachaIat94.equals(row.getCell(2,
					Row.CREATE_NULL_AS_BLANK).toString())) {
				cell = row.getCell(31);
				Msg_id = getData(connection, FormatConstants.nachaIat94);

				// 1.6 start
				charSeq = fetchRowsNachaCharSeq(connection);
				row.getCell(32).setCellValue(charSeq);
				resetData(connection, FormatConstants.nachaIat94, Msg_id[0]);// 1.1
				if (Integer.parseInt(Msg_id[0]) == 1439) {

					updateSequence(connection,charSeq);
				}
				// 1.6 end

				int seqVal = Integer.parseInt(Msg_id[0]);
				int hours=seqVal/60;
				int minutes=seqVal%60;
				String str = String.valueOf((hours > 9 ? hours : String.valueOf("0")
						+ String.valueOf(hours)))
						+ (minutes > 9 ? minutes : String.valueOf("0")
								+ String.valueOf(minutes));
				
				//Msg_id[0] = getFileCreationNumNativeCPA(Msg_id[0]);// 1.1
				Msg_id[0]=str;
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				cell.setCellValue(Msg_id[0]);

				row.getCell(20).setCellValue(JulianDate.convertDate());
				cell = row.getCell(102, Row.CREATE_NULL_AS_BLANK);
				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				flag = 1;
			}

			else if (flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				cell = row.getCell(102, Row.CREATE_NULL_AS_BLANK);

				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);

				TXN_REF_NUM = Msg_id[1];

			}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();
	}

	public static void updateNACHAFormat(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		int count = workbook.getSheetAt(0).getLastRowNum();
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		String charSeq = null;
		Connection connection = DBUtility.openConnection();
		// System.out.println("count "+count);
		String[] Msg_id = { "", "" };
		for (int i = 20; i <= count; i++) {
			Row row = sheet.getRow(i);
			if (flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				flag = 0;
			}
			if (FormatConstants.nacha94.equals(row.getCell(2,
					Row.CREATE_NULL_AS_BLANK).toString())) {
				cell = row.getCell(31);
				Msg_id = getData(connection, FormatConstants.nacha94);
				log.debug("Msg_id[0] pass in resetData::::" + Msg_id[0]);
				System.out.println("Msg_id[0] pass in resetData::::"
						+ Msg_id[0]);

				// 1.6 start
				charSeq =fetchRowsNachaCharSeq(connection);
				row.getCell(32).setCellValue(charSeq);
				// resetData(connection, FormatConstants.nachaIat94,
				// Msg_id[0]);// 1.1
				resetData(connection, FormatConstants.nacha94, Msg_id[0]); // changes for Format name

				if (Integer.parseInt(Msg_id[0]) == 1439) {

					updateSequence(connection , charSeq);
				}
				// 1.6 end
				int seqVal = Integer.parseInt(Msg_id[0]);
				int hours=seqVal/60;
				int minutes=seqVal%60;
				String str = String.valueOf((hours > 9 ? hours : String.valueOf("0")
						+ String.valueOf(hours)))
						+ (minutes > 9 ? minutes : String.valueOf("0")
								+ String.valueOf(minutes));
				
				//Msg_id[0] = getFileCreationNumNativeCPA(Msg_id[0]);// 1.1
				Msg_id[0]=str;

				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);

				log.debug("Msg_id[0] from getFileCreationNumNativeCPA::::"
						+ Msg_id[0]);
				row.getCell(20).setCellValue(JulianDate.convertDate());
				cell.setCellValue(Msg_id[0]);
				cell = row.getCell(102, Row.CREATE_NULL_AS_BLANK);
				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				flag = 1;
			}

			else if (flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				cell = row.getCell(102, Row.CREATE_NULL_AS_BLANK);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);

				TXN_REF_NUM = Msg_id[1];
			}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();
	}

	// 1.6 start
	public static String updateSequence(Connection con, String charSeq) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("updateSequence method start in UpdateFormatXLSService");
		}

		String query = "";
		PreparedStatement pStmt = null;
		try {
			char value = charSeq.charAt(0);
			System.out.println("value ::" + value);
			int asciiVal = (int) value;
			System.out.println("asciiVal ::" + asciiVal);
			charSeq = charSeq.replace((char) asciiVal, ((char) (asciiVal + 1)));
			query = "Update nacha_seq_data set current_char = ?  where DATEVAL = ?";
			System.out.println("query ::" + query);
			pStmt = con.prepareStatement(query);
			pStmt.setString(1, charSeq);
			pStmt.setDate(2, getCurrentDate());
			pStmt.executeUpdate();
		} catch (Exception e) {
			log.error("Error occured while updating nacha char sequence in table", e);
			throw e;
		}
		System.out.println("charSeq ::" + charSeq);
		return charSeq;

	}

	public static String fetchRowsNachaCharSeq(Connection con) throws Exception {
		String char_seq = null;

		String query = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		try {
			query = "Select * from nacha_seq_data where DATEVAL = ?";
			psmt = con.prepareStatement(query);
			psmt.setDate(1, getCurrentDate());
			rs = psmt.executeQuery();
			if (!rs.next()) {
				System.out.println("No record found in table ");
				char_seq=insertNachaSeqRecord(con);
				System.out.println("Record inserted sucessfully");
			}else{
				char_seq = rs.getString("CURRENT_CHAR");
			}
		} catch (Exception e) {
			log.error("Error occured while fetching nacha char sequence from table", e);
			throw e;
		}

		return char_seq;
	}

	public static String insertNachaSeqRecord(Connection con) throws Exception {
		String insertQuery = null;
		PreparedStatement ps = null;
		String charSeq="A";
		try {
			insertQuery = "Insert into nacha_seq_data values" + "(?,?)";
			ps = con.prepareStatement(insertQuery);
			ps.setDate(1, getCurrentDate());
			ps.setString(2, charSeq);
			ps.executeUpdate();
		} catch (Exception e) {
			log.error("Error occured while inserting charsequence in nacha_seq_data !!!!", e);
			throw e;
		}
		return charSeq;
	}

	private static java.sql.Date getCurrentDate() {
		java.util.Date today = new java.util.Date();
		return new java.sql.Date(today.getTime());
	}

	// 1.6 end

	public static void updateMT101Format(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		// int count=workbook.getSheetAt(3).getLastRowNum();
		int count = workbook.getSheetAt(5).getLastRowNum(); // 1.1 sheet
															// sequence related
															// changes as per
															// 1.6 version
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		Connection connection = DBUtility.openConnection();
		// //System.out.println("count "+count);
		String[] Msg_id = { "", "" };
		for (int i = 14; i <= count; i++) {
			Row row = sheet.getRow(i);
			// ////System.out.println("asddf  "+row.getCell(2,Row.CREATE_NULL_AS_BLANK).toString());
			// ////System.out.println("line 89 "+row.getCell(2).getStringCellValue());
			if (flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				// //System.out.println("break loop");
				flag = 0;
			}
			if (FormatConstants.mt101.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString())) {
				cell = row.getCell(31);
				// ////System.out.println(cell.getNumericCellValue());

				Msg_id = getData(connection, FormatConstants.mt101);
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				// if(!cell.getStringCellValue().isEmpty()){
				// //System.out.println("MT 101:::: "+cell.getStringCellValue());
				cell.setCellValue(Msg_id[0]);
				// //System.out.println("MT 101 after setting value2 "
				// +" "+i+" "+cell.getStringCellValue());
				// }
				cell = row.getCell(66, Row.CREATE_NULL_AS_BLANK);

				// if(!cell.getStringCellValue().isEmpty()){
				// //System.out.println(cell.getStringCellValue());
				// cell.setCellValue(Msg_id[1]);
				// }
				// ////System.out.println("flag 0 MT 101 ::::"+cell.getStringCellValue());
				// //System.out.println("flag 0 MT 101 Msg_id[0] ::::"+MSG_ID);
				// //System.out.println("flag 0 MT 101 TXN_REF_NUM::::"+Msg_id[1]);

				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				// insertData("MT 101", MSG_ID, TXN_REF_NUM, connection);
				flag = 1;
			}

			else if (flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				cell = row.getCell(66, Row.CREATE_NULL_AS_BLANK);

				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				if (!cell.getStringCellValue().isEmpty()) {
					// //System.out.println("else MT 101 :::::"+cell.getStringCellValue());
					// cell.setCellValue(Msg_id[1]);
				}
				// //System.out.println("flag 1 MT 101 Msg_id[0] ::::"+MSG_ID);
				// //System.out.println("flag 1 MT 101 TXN_REF_NUM::::"+Msg_id[1]);
				TXN_REF_NUM = Msg_id[1];
				// insertData("MT 101", MSG_ID, TXN_REF_NUM, connection);
			}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		// //System.out.println("path r "+inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();
	}

	public static void updateMT104Format(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		int count = workbook.getSheetAt(5).getLastRowNum();
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		Connection connection = DBUtility.openConnection();
		String[] Msg_id = { "", "" };
		for (int i = 14; i <= count; i++) {
			Row row = sheet.getRow(i);
			if (flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				flag = 0;
			}
			if (FormatConstants.mt104.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString())) {
				cell = row.getCell(31);
				log.debug("Msg_id for MT104 from sheet ::"+cell);
				Msg_id = getData(connection, FormatConstants.mt104);
				
				log.debug("Msg_id for MT104 ::"+Msg_id);
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);

				cell.setCellValue(Msg_id[0]);
				log.debug("Msg_id for MT104 after update ::"+cell);
				cell = row.getCell(66, Row.CREATE_NULL_AS_BLANK);
				// //System.out.println(cell.getStringCellValue());
				// if(!cell.getStringCellValue().isEmpty()){
				// cell.setCellValue(Msg_id[1]);
				// //System.out.println("flag 0 MT 104 ::::"+cell.getStringCellValue());
				// }

				// //System.out.println("flag 0 MT 104 Msg_id[0] ::::"+MSG_ID);
				// //System.out.println("flag 0 MT 104 TXN_REF_NUM::::"+Msg_id[1]);
				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				// insertData("MT 104", MSG_ID, TXN_REF_NUM, connection);
				flag = 1;
			}

			else if (flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				cell = row.getCell(66, Row.CREATE_NULL_AS_BLANK);

				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				// if(!cell.getStringCellValue().isEmpty()){
				// //System.out.println("else MT104 :::"+cell.getStringCellValue());
				// //cell.setCellValue(Msg_id[1]);
				// }

				// //System.out.println("flag 1 MT 104 Msg_id[0] ::::"+MSG_ID);
				// //System.out.println("flag 1 MT 104 TXN_REF_NUM::::"+Msg_id[1]);
				TXN_REF_NUM = Msg_id[1];
				// insertData("MT 104", MSG_ID, TXN_REF_NUM, connection);
			}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		// //System.out.println("path r "+inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();
	}

	public static void updateCMO103Format(Sheet sheet, Workbook workbook,
			String inputUploadedPath) throws Exception {
		// int count=workbook.getSheetAt(3).getLastRowNum();
		int count = workbook.getSheetAt(5).getLastRowNum();// 1.1 sheet sequence
															// related changes
															// as per 1.6
															// version
		int flag = 0;
		Cell cell = null;
		String MSG_ID = null;
		String TXN_REF_NUM = null;
		Connection connection = DBUtility.openConnection();
		// //System.out.println("count "+count);
		String[] Msg_id = { "", "" };
		for (int i = 14; i <= count; i++) {
			Row row = sheet.getRow(i);
			if (flag == 1
					&& !(row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				// //System.out.println("break loop");
				flag = 0;
			}
			if (FormatConstants.cmo103.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString())) {
				cell = row.getCell(65);
				Msg_id = getData(connection, FormatConstants.cmo103);
				Msg_id[0] = getFileCreationNumCPA(Msg_id[0]);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);

				cell.setCellValue(Msg_id[0]);

				cell = row.getCell(65, Row.CREATE_NULL_AS_BLANK);

				if (!cell.getStringCellValue().isEmpty()) {
					cell.setCellValue(Msg_id[1]);
					// //System.out.println("CMO 103 line 704"+cell.getStringCellValue());
				}

				// //System.out.println("flag 0 CMO 103 Msg_id[0] ::::"+MSG_ID);
				// //System.out.println("flag 0 CMO 103 TXN_REF_NUM::::"+Msg_id[1]);
				MSG_ID = Msg_id[0];
				TXN_REF_NUM = Msg_id[1];
				// insertData("CMO 103", MSG_ID, TXN_REF_NUM, connection);
				flag = 1;
			}

			else if (flag == 1
					&& (row.getCell(2, Row.CREATE_NULL_AS_BLANK).toString()
							.isEmpty())) {
				cell = row.getCell(66, Row.CREATE_NULL_AS_BLANK);
				Msg_id[1] = getCrossRefNumCPA(Msg_id[1]);
				if (!cell.getStringCellValue().isEmpty()) {
					// //System.out.println("CMO 103 line 713"+cell.getStringCellValue());
					cell.setCellValue(Msg_id[1]);
					// //System.out.println("flag 1 CMO 103 ::::"+cell.getStringCellValue());
				}
				TXN_REF_NUM = Msg_id[1];
				}
		}
		FileOutputStream outputStream = new FileOutputStream(inputUploadedPath);
		workbook.write(outputStream);
		if (connection != null)
			connection.close();
		outputStream.close();

	}

	public static void insertData(String FORMAT_NAME, String MSG_ID,
			String TXN_REF_NUM, Connection connection) throws SQLException {
		if (null != MSG_ID && null != TXN_REF_NUM) { // 1.2
			String query = "insert into TEST_TOOL_PAIN1_MSG_ID values ('"
					+ FORMAT_NAME + "','" + MSG_ID + "','" + TXN_REF_NUM + "')";
			try {
				statement = connection.createStatement();

				statement.executeUpdate(query);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new SQLException(e);
			} finally {
				statement.close();
			}
		} else {
			}
	}

	public static String[] getData(Connection connection, String formateName)
			throws SQLException {
		// //log.debug("formateName::::"+formateName);
		String query = null;
		if (formateName.equalsIgnoreCase(FormatConstants.cibc1464)
				|| formateName.equalsIgnoreCase(FormatConstants.cibc80Byte)
				|| formateName.equalsIgnoreCase(FormatConstants.cpa005)) {
			query = "SELECT SEQ_FGT_NATIVE_MESSAGEID.NEXTVAL  MSG_ID,  TXN_REF_NUM TXN_REF_NUM FROM FGT_MESSAGEID_DEF WHERE FORMAT_NAME ='"
					+ formateName + "'";
		} else if(formateName.equalsIgnoreCase(FormatConstants.nachaIat94)
				|| formateName.equalsIgnoreCase(FormatConstants.nacha94)){
			query = "SELECT NACHA_TIME_FORMAT_SEQ.NEXTVAL  MSG_ID,  TXN_REF_NUM TXN_REF_NUM FROM FGT_MESSAGEID_DEF WHERE FORMAT_NAME ='"
					+ formateName + "'";
			} else {

			query = "SELECT '1'||LPAD(SUBSTR(POWER(10,MSGID_LENGTH-1)+SEQ_FGT_MESSAGEID.NEXTVAL,-1*(MSGID_LENGTH-1)), MSGID_LENGTH-1,'0') MSG_ID,  TXN_REF_NUM TXN_REF_NUM FROM FGT_MESSAGEID_DEF WHERE FORMAT_NAME ='"
					+ formateName + "'";
			
		}
		String Msg_id[] = { "", "" };
		try {
			if (connection == null) {
				
			}
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			if (rs.next()) {
				Msg_id[0] = rs.getString("MSG_ID");
				Msg_id[1] = rs.getString("TXN_REF_NUM");
				// System.out.println("getData()::::TXN_REF_NUM"+rs.getString("TXN_REF_NUM"));
			}
			} catch (SQLException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			statement.close();
			rs.close();
		}
		log.debug("Message id :::"+Msg_id);
		return Msg_id;
	}

	// 1.1 starts
	public static String getFileCreationNumCPA(String str) {
		int a = 1;
		String str3;
		Integer b = Integer.parseInt(str.substring(str.length() - 4,
				str.length()));
		b = b + 1;
		str3 = b.toString();
		while (str3.length() != 4)
			str3 = "0" + str3;

		String newStr = str.substring(0, str.length() - 4) + str3;
		return newStr;
	}

	// 1.1 ends
	public static String getCrossRefNumCPA(String str) {

		int a = 1;
		String str3;
		Integer b = Integer.parseInt(str.substring(str.length() - 4,
				str.length()));
		b = b + 1;
		str3 = b.toString();
		while (str3.length() != 4)
			str3 = "0" + str3;
		String newStr = str.substring(0, str.length() - 4) + str3;
		return newStr;
	}

	// 1.1
	public static String getFileCreationNumNativeCPA(String str) {
		String str_msgid1 = null, str_msgid2 = null, generatedstr = null;
		int msgid2 = Integer.parseInt(str.substring(2));
		int msgid1 = Integer.parseInt(str.substring(0, 2));
		String str3;
		if (msgid1 >= 23 && msgid2 >= 59) {
			msgid1 = (msgid1 % 23);
		} else if (msgid2 == 59) {
			msgid1++;
		}

		if (msgid2 >= 59) {
			msgid2 = (msgid2 % 59);
		} else {
			msgid2++;
		}

		str = String.valueOf((msgid1 > 9 ? msgid1 : String.valueOf("0")
				+ String.valueOf(msgid1)))
				+ (msgid2 > 9 ? msgid2 : String.valueOf("0")
						+ String.valueOf(msgid2));

		return str;
	}

	public static void resetData(Connection connection, String formateName,
			String message_id) throws SQLException {
		int msgid = 0;
		PreparedStatement preparedStmt = null;
		msgid = Integer.parseInt(message_id);
		if (msgid >= 2359) {
			String query = "DELETE FROM TEST_TOOL_PAIN1_MSG_ID WHERE FORMAT_NAME= '"
					+ formateName + "'";
			try {
				if (connection == null) {
					// //System.out.println("connection null conn");

				}
				preparedStmt = connection.prepareStatement(query);
				preparedStmt.execute();

			} catch (SQLException e) {
				e.printStackTrace();
				throw new SQLException(e);
			} finally {
				preparedStmt.close();

			}
			} else {
			}

	}

	// 1.1 ends
	// 1.2 [s]
	public static boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;

		}
		return true;
	}
	// 1.2 [e]

}
