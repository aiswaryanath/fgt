/********************************************************************************************************/
/* Copyright @  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/*				 									 													*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : UpdateFormatXls.java        		                                 					*/
/*                                                                                      				*/
/********************************************************************************************************/
/* 				 Author					          |        	  Date   			|	  Version           */
/********************************************************************************************************/
/*         	Surendra Reddy.M 	             	  |			20:05:2016			|		1.0	    
 * 			Preetam Sanjore 					  |			07:07:2017			|		1.1			    
/*			Shruti Gupta						  |         19:02:2018			|		1.2 Removed dynamic cust_ref_no(tag 20) for CMO103
/********************************************************************************************************/
/* 																										*/
/* 	Description  : Main Controller 																		*/
/*        			 																					*/
/********************************************************************************************************/

package com.intellect.service;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.log4j.Logger;
import com.intellect.db.DBUtility;
import com.intellect.property.FormatConstants;

//import com.polaris.payments.common.util.PT_DatabaseUtil;

public class UpdateFormatXls {
	private static org.apache.log4j.Logger log = Logger
			.getLogger(UpdateFormatXls.class);

	ResultSet rs = null;
	Statement statement = null;
	Sheet sheet = null;

	public int updateFilesXls(String inputUploadedPath, String selectedItem)
			throws Exception {
		Connection connection = null;
		// //System.out.println("inside updateFilesXls get conn");
		connection = DBUtility.openConnection();
		// connection = PT_DatabaseUtil.mGetConnection(); //uncomment to work on
		// server
		if (connection == null) {
			// System.out.println("connection problem");

		}
		String excelFilePath = inputUploadedPath;
		FileInputStream inputStream = new FileInputStream(excelFilePath);
		Workbook workbook = WorkbookFactory.create(inputStream);

		if (FormatConstants.tmpSheetName.equalsIgnoreCase(selectedItem)) {
			sheet = workbook.getSheetAt(0);
			// System.out.println("inside updateFilesXls calling CPA");
			updateNativeFormat(sheet, workbook, inputUploadedPath, connection);

		}
		if (FormatConstants.pain1SheetName.equalsIgnoreCase(selectedItem)) {
			sheet = workbook.getSheetAt(1);
			// System.out.println("inside updateFilesXls calling pain1");
			UpdateFormatXlsService.updatePain1Format(sheet, workbook,
					inputUploadedPath);
		}
		if (FormatConstants.PAIN1_REMIT_SHEET_NAME.equalsIgnoreCase(selectedItem)) {
			sheet = workbook.getSheetAt(2); // 1.1 sheet sequence related
											// changes as per 1.6 version
			// System.out.println("inside updateFilesXls  calling pain001_remit");
			UpdateFormatXlsService.updatePain1_RMTCHKFormat(sheet, workbook,
					inputUploadedPath);
		}
		if (FormatConstants.pain8SheetName.equalsIgnoreCase(selectedItem)) {
			sheet = workbook.getSheetAt(3);// 1.1 sheet sequence related changes
											// as per 1.6 version
			// System.out.println("inside updateFilesXls calling pain8");
			UpdateFormatXlsService.updatePain8eFormat(sheet, workbook,
					inputUploadedPath);
		}
		if (FormatConstants.swiftSheetName.equalsIgnoreCase(selectedItem)) {
			sheet = workbook.getSheetAt(5);// 1.1 sheet sequence related changes
											// as per 1.6 version
			// System.out.println("inside updateFilesXls calling SWIFT");
			updatesWIFTFormat(sheet, workbook, inputUploadedPath, connection);
		}
		if (FormatConstants.ansiSheetName.equalsIgnoreCase(selectedItem)) {
			sheet = workbook.getSheetAt(6); // 1.1 sheet sequence related
											// changes as per 1.6 version
			// System.out.println("inside updateFilesXls  calling ANSI");
			UpdateFormatXlsService.updateAnsiFormat(sheet, workbook,
					inputUploadedPath);
		}
		if (FormatConstants.PAIN8_REMIT_SHEET_NAME.equalsIgnoreCase(selectedItem)) {
			sheet = workbook.getSheetAt(4); // 1.1 sheet sequence related
											// changes as per 1.6 version
			// System.out.println("inside updateFilesXls  calling pain008_remit");
			UpdateFormatXlsService.updatePain8_RmtFormat(sheet, workbook,
					inputUploadedPath);
		}
		if (FormatConstants.InteracSheetName.equalsIgnoreCase(selectedItem)) {
			sheet = workbook.getSheetAt(7); // 1.1 sheet sequence related
											// changes as per 1.6 version
			// System.out.println("inside updateFilesXls  calling interac.......");
			UpdateFormatXlsService.updateEMTXMLFormat(sheet, workbook,
					inputUploadedPath);
		}
		if (FormatConstants.IsoRemitSheetName.equalsIgnoreCase(selectedItem)) {
			sheet = workbook.getSheetAt(8); // 1.1 sheet sequence related
											// changes as per 1.6 version
			// System.out.println("inside updateFilesXls  calling isoREMIT......");
			UpdateFormatXlsService.updateISORemitFormat(sheet, workbook,
					inputUploadedPath);
		}
		if (FormatConstants.CAMT_SHEET_NAME.equalsIgnoreCase(selectedItem)) {
			sheet = workbook.getSheetAt(9); // 1.1 sheet sequence related
											// changes as per 1.6 version
			// System.out.println("inside updateFilesXls  calling isoREMIT......");
			UpdateFormatXlsService.updateCAMTFormat(sheet, workbook,
					inputUploadedPath);
		}

		if (FormatConstants.ALL.equalsIgnoreCase(selectedItem)) {
			// System.out.println("inside updateFilesXls selectedItem ::::"+selectedItem);
			// System.out.println("calling ALL FORMATS");
			sheet = workbook.getSheetAt(0);
			updateNativeFormat(sheet, workbook, inputUploadedPath, connection);
			sheet = workbook.getSheetAt(1);
			UpdateFormatXlsService.updatePain1Format(sheet, workbook,
					inputUploadedPath);
			sheet = workbook.getSheetAt(2);
			UpdateFormatXlsService.updatePain1_RMTCHKFormat(sheet, workbook,
					inputUploadedPath);
			sheet = workbook.getSheetAt(3);// 1.1 sheet sequence related changes
											// as per 1.6 version
			UpdateFormatXlsService.updatePain8eFormat(sheet, workbook,
					inputUploadedPath);
			sheet = workbook.getSheetAt(4);// 1.1 sheet sequence related changes
											// as per 1.6 version
			UpdateFormatXlsService.updatePain8_RmtFormat(sheet, workbook,
					inputUploadedPath);
			sheet = workbook.getSheetAt(5);// 1.1 sheet sequence related changes
											// as per 1.6 version
			updatesWIFTFormat(sheet, workbook, inputUploadedPath, connection);
			sheet = workbook.getSheetAt(6);// 1.1 sheet sequence related changes
											// as per 1.6 version
			UpdateFormatXlsService.updateAnsiFormat(sheet, workbook,
					inputUploadedPath);
			sheet = workbook.getSheetAt(7);// 1.1 sheet sequence related changes
											// as per 1.6 version
			UpdateFormatXlsService.updateEMTXMLFormat(sheet, workbook,
					inputUploadedPath);
		sheet = workbook.getSheetAt(8);// 1.1 sheet sequence related changes
											// as per 1.6 version
		UpdateFormatXlsService.updateISORemitFormat(sheet, workbook,
				inputUploadedPath);
		
			sheet=workbook.getSheetAt(9);
			UpdateFormatXlsService.updateCAMTFormat(sheet, workbook,
					inputUploadedPath);
			
					
		}

		inputStream.close();
		if (connection != null) {
			// System.out.println("connection created by abhi");
			connection.close();
			// UpdateFormatXlsService.getData(connection);
		}
		// System.out.println("leaving updateFilesXls get conn");
		return 0;

	}

	public void updateNativeFormat(Sheet sheet, Workbook workbook,
			String inputUploadedPath, Connection connection) throws Exception {
		int count = workbook.getSheetAt(0).getLastRowNum();
		int cibc80 = 0, cpa005 = 0, cibc1464 = 0, nachaIat = 0, nacha = 0;
		for (int i = 20; i <= count; i++) {
			Row row = sheet.getRow(i);
			if (FormatConstants.cpa005.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString()) && cpa005 == 0) {
				cpa005 = 1;
				UpdateFormatXlsService.updateCPA005Format(sheet, workbook,
						inputUploadedPath);
			}
			if (FormatConstants.cibc80Byte.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString()) && cibc80 == 0) {
				cibc80 = 1;
				UpdateFormatXlsService.updateCIBC80Format(sheet, workbook,
						inputUploadedPath);
			}
			if (FormatConstants.cibc1464.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString()) && cibc1464 == 0) {
				cibc1464 = 1;
				UpdateFormatXlsService.updateCIBC1464Format(sheet, workbook,
						inputUploadedPath);
			}
			if (FormatConstants.nachaIat94.equals(row.getCell(2,
					Row.CREATE_NULL_AS_BLANK).toString())
					&& nachaIat == 0) {
				nachaIat = 1;
				UpdateFormatXlsService.updateNACHA_IATFormat(sheet, workbook,
						inputUploadedPath);
			}
			if (FormatConstants.nacha94.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString()) && nacha == 0) {
				nacha = 1;
				UpdateFormatXlsService.updateNACHAFormat(sheet, workbook,
						inputUploadedPath);
			}
		}
	}

	public void updatesWIFTFormat(Sheet sheet, Workbook workbook,
			String inputUploadedPath, Connection connection) throws Exception {
		// int count=workbook.getSheetAt(3).getLastRowNum();
		int count = workbook.getSheetAt(5).getLastRowNum();
		int cmo103 = 0, mt101 = 0, mt104 = 0;
		for (int i = 14; i <= count; i++) {
			Row row = sheet.getRow(i);
			if (FormatConstants.cmo103.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString()) && cmo103 == 0) {
				cmo103 = 1;
				// System.out.println("updatesWIFTFormat :::::updateCMO103Format() ");
				//1.2
				/*UpdateFormatXlsService.updateCMO103Format(sheet, workbook,
						inputUploadedPath);*/

			}
			if (FormatConstants.mt101.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString()) && mt101 == 0) {
				mt101 = 1;
				// System.out.println("updatesWIFTFormat :::::updateMT101Format() ");
				UpdateFormatXlsService.updateMT101Format(sheet, workbook,
						inputUploadedPath);
			}
			if (FormatConstants.mt104.equals(row.getCell(2, Row.CREATE_NULL_AS_BLANK)
					.toString()) && mt104 == 0) {
				mt104 = 1;
				// System.out.println("updatesWIFTFormat :::::updateMT104Format() ");
				UpdateFormatXlsService.updateMT104Format(sheet, workbook,
						inputUploadedPath);
			}

		}
	}
}
