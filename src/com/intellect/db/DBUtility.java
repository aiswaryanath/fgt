/********************************************************************************************************/
/* Copyright �  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/* 													 													*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : DBUtility.java                                           					        */
/*                                                                                      				*/
/********************************************************************************************************/
/*  			 Author					         |        	  Date   			|	  Version           */
/********************************************************************************************************/
/* 	         Veera Kumar Reddy.V 		         |			20:05:2016			|		1.0	            */
/********************************************************************************************************/
/* 																									    */
/* 	Description  : To open DBConnection and Close Resources.                       						*/
/*        																								*/
/********************************************************************************************************/

package com.intellect.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.intellect.property.FormatConstants;

public class DBUtility {

	private static org.apache.log4j.Logger log = Logger
			.getLogger(DBUtility.class);

	public DBUtility() {

	}
	/*
	// To open Connection
	public static Connection openConnection(){
		log.debug("Inside getConnection():");
		Connection connection = null;
		try {
			// DataBase Connection using JDBC Connection.
			Class.forName(FormatConstants.dbDriver);
			if (connection == null)
				connection = DriverManager
						.getConnection(FormatConstants.dbUrl, FormatConstants.dbUser, FormatConstants.dbPwd);
			
			//
			//DataBase Connection using Connection pool.
			  javax.naming.InitialContext intialContext = null;
			  javax.sql.DataSource dataSource = null; 
			  intialContext = new javax.naming.InitialContext(); 
			  dataSource =(javax.sql.DataSource) intialContext.lookup("paymentspool");
			  if(dataSource!=null){
				  if(connection==null) 
					  connection =dataSource.getConnection();
			  }
			  else{
				  log.debug("Failed to lookup datasource."); 
			  }
			 
			//
			if (log.isDebugEnabled()) {
				log.debug("Connection:=>" + connection);
				log.debug("openConnection method in DBUtility class");
			}
		} catch (SQLException e) {
			if (log.isDebugEnabled()) {
				log.error("SQLException while creating Connection object");
			}
			e.printStackTrace();
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.error("Exception while creating Connection object");
			}
			e.printStackTrace();
		}
		return connection;
	}
	*/
	
	//For testing 
	// To open Connection
	public static Connection openConnection(){
	//	log.debug("Inside getConnection():");
		//System.out.println("Inside getConnection():");
		Connection connection = null;
		try {
			// DataBase Connection using JDBC Connection.
			//System.out.println("FormatConstants.dbDriver "+FormatConstants.dbDriver +" "+FormatConstants.dbUrl);
			//System.out.println("FormatConstants.dbUser "+ FormatConstants.dbUser +" "+FormatConstants.dbPwd);
			Class.forName(FormatConstants.dbDriver);
			if (connection == null)
				connection = DriverManager
						.getConnection(FormatConstants.dbUrl, FormatConstants.dbUser, FormatConstants.dbPwd);
			
			
			System.out.println("inside db uttility Connection:=>" + connection);
//		
				log.debug("Connection:=>" + connection);
				log.debug("openConnection method in DBUtility class");
//			}
		} catch (SQLException e) {
			if (log.isDebugEnabled()) {
				log.error("SQLException while creating Connection object");
			}
			e.printStackTrace();
			} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.error("Exception while creating Connection object");
			}
			e.printStackTrace();
		}
		return connection;
	}

	// To close the connection
	public static void closeConnection(Connection conn) {
	//	log.debug("Inside closeConnection()");
		try {
			if (conn != null) {
				conn.close();
				conn = null;
			}

		} catch (SQLException e) {
			log.debug("SQLException while closeConnection() :", e);

		}
	}

	// To close the ResultSet
	public static void closeResultSet(ResultSet rs) {
	//	log.debug("Inside closeResultSet()");
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}

		} catch (SQLException e) {
			log.debug("SQLException while closeResultSet() :", e);

		}
	}

	// To close the PreparedStatement object
	public static void closePreparedStatement(PreparedStatement pstmt) {
	//	log.debug("Inside closePreparedStatement()");
		try {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}

		} catch (SQLException e) {
			log.debug("SQLException while closePreparedStatement() :", e);

		}
	}

	// To close the Statement object
	public static void closeStatement(Statement stmt) {
	//	log.debug("Inside closeStatement()");
		try {
			if (stmt != null) {
				stmt.close();
				stmt = null;
			}

		} catch (SQLException e) {
			log.debug("SQLException while closeStatement() :", e);

		}
	}

	// To close the DBResource
	public static void closeDBResource(ResultSet rs, Statement pstmt,
			Connection conn) {
	//	log.debug("Inside closeDBResource()");
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}

			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}

			if (conn != null) {
				conn.close();
				conn = null;
			}

		} catch (SQLException e) {
			log.debug("SQLException while closeDBResource() :", e);

		}
	}

	// To close the DBResource
	public static void closeDBResource(ResultSet rs, Statement pstmt) {
	//	log.debug("Inside closeDBResource()");
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}

			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}

		} catch (SQLException e) {
			log.debug("SQLException while closeDBResource() :", e);

		}
	}

	// To close the DBResource
	public static void closeDBResourcePstmt(ResultSet rs,
			PreparedStatement pstmt) {
	//	log.debug("Inside closeDBResourcePstmt()");
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}

			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}

		} catch (SQLException e) {
			log.debug("SQLException while closeDBResourcePstmt() :", e);

		}
	}
	
	public static void main(String[] args) throws Exception{
		openConnection();
	}

}
