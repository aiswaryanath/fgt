/********************************************************************************************************/
/* Copyright �  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/* 								 																		*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : ConvertFileISOPain001.java                                          					*/
/*                                                                                      				*/
/********************************************************************************************************/
/*  			  Author					    |        	  Date   			|	  Version           */
/********************************************************************************************************/
/*         Veera Kumar Reddy.V		            |			20:05:2016			|		1.0	            */
/*		   Sapna Jain							|			18:07:2017			|		1.1
 /* 	   Preetam Sanjore						|			20:07:2017			|		1.2	
 /* 	   Preetam Sanjore						|			20:07:2017			|		1.3	
 /* 	   Gokaran Tiwari						|			22:03:2018			|		1.4 latest suite change
 /* 																				in main table , old move to audit

 /********************************************************************************************************/
/* 											  														    */
/* Description   : For generation of PAIN 001 format 													*/
/*        																								*/
/********************************************************************************************************/

package com.intellect.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.EmptyStackException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Namespace;

import org.apache.log4j.Logger;

import com.intellect.pojo.TestToolFileNamePOJO;
import com.intellect.property.FormatConstants;
import com.intellect.service.FormatGenerateService;
import com.intellect.service.UpdateFormatXlsService;
import com.sun.jmx.snmp.Timestamp;

public class ConvertFileCAMT {

	private static org.apache.log4j.Logger log = Logger
			.getLogger(ConvertFileCAMT.class);

	int m = 7, p = 7;
	int batchrowCount = 7;
	LinkedHashMap<Integer, Vector<String>> map = null;
	LinkedHashMap<Integer, Vector> map1 = null;
	ReadTemplate readTemplate = new ReadTemplate();
	int filesCrtdCnt = 0;


	// BAD_FILES_CHANGES
	private String invalidAppender = "001";

	public int main(String source, File target, String fileTypeSelected,
			LinkedHashMap<Integer, Vector<String>> map1, Connection connection,
			String db_table_file, String timeStamp, String dbTimeStamp,
			String moduleSelected, char[] variants, List<TestToolFileNamePOJO> testToolData) throws Exception {// BAD_FILES_CHANGES
		System.out.println("in camt class here");
		ConvertFileCAMT xmlWriter = new ConvertFileCAMT();
		filesCrtdCnt = xmlWriter.rootXMLFile(source, target, fileTypeSelected,map1, connection, db_table_file, timeStamp, dbTimeStamp,moduleSelected, variants,testToolData);// BAD_FILES_CHANGES

		return filesCrtdCnt;
	}

	public int rootXMLFile(String source, File target, String fileTypeSelected,
			LinkedHashMap map1, Connection connection, String db_table_file,
			String timeStamp, String dbTimeStamp, String moduleSelected,
			char[] variants,List<TestToolFileNamePOJO> pojoList) throws Exception {// BAD_FILES_CHANGES
		//Jo-here is where changes need to be made
		IwReadExcel objIwReadExcel = new IwReadExcel();
		map = (LinkedHashMap<Integer, Vector<String>>) objIwReadExcel.ReadData(
				source, fileTypeSelected, moduleSelected);
		// Change 1.4
		int rowCount = 7, colCount = 17;
		ArrayList<Integer> stockList = new ArrayList<Integer>();
		int count = 0, k = 0, fileCount = 0;
		for (int j = 0; j < map.size(); j++) {
			if (!map.get(rowCount).get(colCount).toString().isEmpty()) {
				stockList.add(count + 1);
				fileCount++;
				count = 0;
				k++;

			} else
				count++;
			rowCount++;
		}

		stockList.add(count + 1);
		int arr[] = new int[stockList.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = stockList.get(i).intValue();
		}
		ArrayList allRecList = new ArrayList();
		for (int i = 0, j2 = 1; i < fileCount; i++, j2++) {
			ArrayList rowCol = writeXML(arr[j2], target, connection,
					db_table_file, timeStamp, dbTimeStamp, null, -1, -1,pojoList);// Change 1.4
			allRecList.add(rowCol);
		}
		log.debug("ALL good files generated");

		// BAD_FILES_CHANGES STARTS
		String[] ruleNames = null;
		boolean oFind = false;
		for (int i = 0; i < variants.length; i++) {
			if (variants[i] == 'O') {
				oFind = true;
			}
		}
		if (oFind) {
			ruleNames = new String[variants.length - 1];
		} else {
			ruleNames = new String[variants.length];
		}

		for (int i = 0; i < variants.length; i++) {
			if (variants[i] == 'O')
				continue;
			else
				ruleNames[i] = variants[i] + "";
		}

		int lastRowNo = 0;
		int startNoForEachMessage = 0;
		// iterating over no of messages
		for (int allRec = 0; allRec < allRecList.size(); allRec++) {
			if (lastRowNo == 0) {
				startNoForEachMessage = 7 + lastRowNo;
			} else {
				startNoForEachMessage = 1 + lastRowNo;

			}
			p = startNoForEachMessage;
			batchrowCount = startNoForEachMessage;
			m = startNoForEachMessage;
			ArrayList rowCol = (ArrayList) allRecList.get(allRec);
			for (int rowColNo = 0; rowColNo < rowCol.size(); rowColNo++) {
				String s = (String) rowCol.get(rowColNo);

				String[] strArray = s.split("~");
				lastRowNo = Integer.valueOf(strArray[0]);
				int ruleColNo = Integer.valueOf(strArray[1]);

				for (int x = 0; x < ruleNames.length; x++) {

					p = startNoForEachMessage;
					batchrowCount = startNoForEachMessage;
					m = startNoForEachMessage;

					writeXML(arr[allRec + 1], target, connection,
							db_table_file, timeStamp, dbTimeStamp,
							ruleNames[x], lastRowNo, ruleColNo,pojoList);// Change 1.4

				}
			}
		}
		
		return fileCount;

	}

	public ArrayList writeXML(int batchCount, File target,
			Connection connection, String db_table_file, String timeStamp,
			String dbTimeStamp, String ruleName, int ruleRowNo, int ruleColNo,List<TestToolFileNamePOJO> pojoList)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("writeXML method starts in ConvertCAMT class: batchCount: "
					+ batchCount
					+ "ruleName: "
					+ ruleName
					+ " ruleRowNo: "
					+ ruleRowNo + " ruleColNo: " + ruleColNo + " p: " + p);
		}
		XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMdd");
		String XMLPath = target + "/";

		XMLPath = XMLPath.replaceAll(".xml", m + ".xml");
		String fileName = null, dbfileName = null;
		String camt_applied_rule_pos = FormatConstants.CAMTAppliedRulePosition;
		int camt_applied_rule_pos_no = Integer
				.parseInt(camt_applied_rule_pos);
		// BAD_FILES_CHANGES STARTS
		ArrayList<String> rowCol = null;
		// BAD_FILES_CHANGES ENDS
		System.out.println("map  :::" + map);
		if (map.get(p).get(2) != null
				&& map.get(p).get(2).toString().length() > 0) {
			String temp = FormatGenerateService.getSequenceNumber(connection,
					map.get(p).get(2).toString());

			if (map.get(p).get(3) != null
					&& map.get(p).get(3).toString().replace(" ", "")
							.equalsIgnoreCase("FTS")) {

				File filefolder = new File(XMLPath + "/FTS");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();

				fileName = XMLPath
						+ "/"
						+ map.get(p).get(7).toString()
						+ "_"
						+ map.get(p).get(10).toString()
						+ "_"
						+ formatter.format(date)
						+ "_111213_"
						+ map.get(p)
								.get(8)
								.toString()
								.replace(".0", "")
								.substring(
										0,
										map.get(p).get(8).toString().length() - 6)
						+ temp + "." + map.get(p).get(2).toString() + "."
						+ map.get(p).get(11).toString() + ".xml";

				// log.debug("fileName:" + fileName);
				// BAD_FILES_CHANGES ENDS
				if (map.get(p).get(12) != null
						&& !map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(7).toString()
							+ "_"
							+ map.get(p).get(10).toString()
							+ "_"
							+ formatter.format(date)
							+ "_111213_"
							+ map.get(p)
									.get(8)
									.toString()
									.replace(".0", "")
									.substring(
											0,
											map.get(p).get(8).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();
				} else {
					dbfileName = map.get(p).get(7).toString()
							+ "_"
							+ map.get(p).get(10).toString()
							+ "_"
							+ formatter.format(date)
							+ "_111213_"
							+ map.get(p)
									.get(8)
									.toString()
									.replace(".0", "")
									.substring(
											0,
											map.get(p).get(8).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + ".xml";
				}

			} else if (map.get(p).get(3).toString().replace(" ", "")
					.equalsIgnoreCase("SCA")) {

				File filefolder = new File(XMLPath + "/SCA");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();

				fileName = XMLPath
						+ "/"
						+ map.get(p).get(6).toString()
						+ "."
						+ formatter1.format(date)
						+ "111213"
						+ "."
						+ map.get(p)
								.get(10)
								.toString()
								.substring(
										0,
										map.get(p).get(10).toString().length() - 6)
						+ temp + "."
						+ map.get(p).get(2).toString().replace(".0", "") + "."
						+ map.get(p).get(11).toString() + ".xml";
				// log.debug("fileName:" + fileName);
				// BAD_FILES_CHANGES ENDS
				if (!map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();
				} else {
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";
				}
			} else if (map.get(p).get(3).toString().replace(" ", "")
					.equalsIgnoreCase("FTT")) {

				File filefolder = new File(XMLPath + "/FTT");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();
				// BAD_FILES_CHANGES STARTS
				if (ruleName != null) {
					fileName = XMLPath + "/" + map.get(p).get(7).toString()
							+ "_" + map.get(p).get(10).toString() + "_"
							+ map.get(p).get(9).toString() + "_"
							+ map.get(p).get(8).toString().replace(".0", "")
							+ "." + map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + "_" + ruleName
							+ String.valueOf(ruleRowNo)
							+ String.valueOf(ruleColNo) + ".xml";

				} else {
					fileName = XMLPath + "/" + map.get(p).get(6).toString()
							+ "." + map.get(p).get(9).toString() + "."
							+ map.get(p).get(10).toString() + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";
				}
				// log.debug("fileName:" + fileName);
				// BAD_FILES_CHANGES ENDS
				if (!map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();
				} else {
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";
				}
			}
			// BAD_FILES_CHANGES
			StringBuffer strModifiedField = new StringBuffer();
			if (ruleName != null && !"".equals(ruleName)) {
				strModifiedField.append(ruleRowNo).append("~")
						.append(ruleColNo);
			}
			String errorCode = null;
			String ruleType = null;
			if ("M".equals(ruleName)) {
				ruleType = "TAG_MISSING";
				errorCode = "TAG_MISSING_ERRORCODE";
				// errorCode =
				// "Invalid Tag Found Invalid Tag Found PHERC_1002 Mandatory Section Group Missing";
			} else if ("D".equals(ruleName)) {
				ruleType = "DUPLICATE_TAG";
				errorCode = "DUPLICATE_TAG_ERRORCODE";
				// errorCode = "Invalid Tag Found Invalid Tag Found";
			} else if ("I".equals(ruleName)) {
				ruleType = "INVALID_TAG";
				errorCode = "INVALID_TAG_ERRORCODE";
				// errorCode =
				// "Invalid Tag Found Invalid Tag Found PHERC_1002 Mandatory Section Group Missing";
			} else if ("O".equals(ruleName)) {
				ruleType = "TAG_ORDER_CHANGE";
				errorCode = "TAG_ORDER_CHANGE_ERRORCODE";
				// errorCode =
				// "Invalid Tag Found Invalid Tag Found PHERC_1002 Mandatory Section Group Missing";
			}
			// Change 1.4 starts
			if (ruleName != null) {
				TestToolFileNamePOJO pojoObj = new TestToolFileNamePOJO();
				pojoObj.setTestCaseid(map.get(p).get(0).toString());
				pojoObj.setFormatName(map.get(p).get(2).toString());
				pojoObj.setChannel(map.get(p).get(3).toString());
				pojoObj.setLsi(map.get(p).get(4).toString());
				pojoObj.setClientRefNum(map.get(p).get(17).toString());
				pojoObj.setBicCode(map.get(p).get(6).toString());
				pojoObj.setBbdId(map.get(p).get(7).toString());
				pojoObj.setCustRef(map.get(p).get(8).toString());
				pojoObj.setUniqueId(map.get(p).get(10).toString());
				pojoObj.setFileName(dbfileName);
				pojoObj.setAppliedRules(ruleType);
				pojoObj.setAppliedRules(errorCode);
				pojoObj.setModifiedField(strModifiedField.toString());
				pojoObj.setTimeStamp(dbTimeStamp);
				pojoList.add(pojoObj);
				
			} else {
				TestToolFileNamePOJO pojoObj = new TestToolFileNamePOJO();
				pojoObj.setTestCaseid(map.get(p).get(0).toString());
				pojoObj.setFormatName(map.get(p).get(2).toString());
				pojoObj.setChannel(map.get(p).get(3).toString());
				pojoObj.setLsi(map.get(p).get(4).toString());
				pojoObj.setClientRefNum(map.get(p).get(17).toString());
				pojoObj.setBicCode(map.get(p).get(6).toString());
				pojoObj.setBbdId(map.get(p).get(7).toString());
				pojoObj.setCustRef(map.get(p).get(8).toString());
				pojoObj.setUniqueId(map.get(p).get(10).toString());
				pojoObj.setFileName(dbfileName);
				pojoObj.setAppliedRules(map.get(p).get(camt_applied_rule_pos_no).toString());
				pojoObj.setAppliedRules(map.get(p).get(camt_applied_rule_pos_no + 1).toString());
				pojoObj.setModifiedField(map.get(p).get(camt_applied_rule_pos_no + 3).toString());
				pojoObj.setTimeStamp(dbTimeStamp);
				pojoList.add(pojoObj);
			}
			// Change 1.4 ends
		} else {

			if (log.isDebugEnabled()) {
				log.debug("FormatName is empty");
			}
		}
		m++;
		String rootElement = "CstmrPmtCxlReq";

		try {
			XMLEventWriter xmlEventWriter = xmlOutputFactory
					.createXMLEventWriter(new FileOutputStream(fileName),
							"UTF-8");
			XMLEventFactory eventFactory = XMLEventFactory.newInstance();
			XMLEvent end = eventFactory.createDTD(System
					.getProperty("line.separator"));
			XMLEvent tab = eventFactory.createDTD("\t");

			ArrayList<Namespace> ns = new ArrayList<Namespace>();
			ArrayList<Attribute> atts = new ArrayList<Attribute>();
			atts.add(eventFactory
					.createAttribute(
							"xsi:schemaLocation",
							"urn:iso:std:iso:20022:tech:xsd:pain.001.001.03 file:///C:/Users/IBM_ADMIN/Box Sync/My Files/PSH/pain.001.001.03.xsd"));
			atts.add(eventFactory.createAttribute("xmlns",
					"urn:iso:std:iso:20022:tech:xsd:camt.055.001.07"));
			atts.add(eventFactory.createAttribute("xmlns:xsi",
					"http://www.w3.org/2001/XMLSchema-instance"));
			
			

			StartElement tempDocument = eventFactory.createStartElement("", "",
					"Document", atts.iterator(), ns.iterator());
			// 1.1 ends
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			StartElement configStartElement = eventFactory.createStartElement(
					"", "", rootElement);
			xmlEventWriter.add(tab);
			xmlEventWriter.add(end);//Jo-Added this 
			xmlEventWriter.add(configStartElement);
			xmlEventWriter.add(end);
			EndElement EndDocument = null;
			Map<String, String> childNodes = null;
			Set<String> elementNodes = null;
			tempDocument = eventFactory.createStartElement("", "", "Assgnmt");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			Map<String, String> elementsMap = new LinkedHashMap<String, String>();
			String[] Msg_id = { "", "" };// messageid changes
			// BAD_FILES_CHANGES STARTS
			if (ruleName == null) {
				rowCol = new ArrayList();
			}
			// BAD_FILES_CHANGES ENDS

			// Write the element nodes

			// <Assgnmt> start

			// Id
			if (!map.get(p).get(17).toString().isEmpty()) {
				
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				
			    
				String val="CAMT"+batchCount+timestamp.getDateTime();
				System.out.println(val);
				
				//elementsMap.put("Id", (String) map.get(p).get(17));
				
				elementsMap.put("Id", val);
				// BAD_FILES_CHANGES STARTS
				
				
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + 17);
				
				else {
					
					
					Msg_id = UpdateFormatXlsService.getData(connection,
							FormatConstants.CAMT);// messageid changes
					elementsMap.put("Id", Msg_id[0]);//Jo-Incorrect msg id is getting displayed .check why
					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "Id", 17, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS

			}

			elementNodes = elementsMap.keySet();
			for (String key : elementNodes) {

				createNode(xmlEventWriter, key, elementsMap.get(key), 3);
			}
			elementsMap = new LinkedHashMap<String, String>();

			// Assgnr
			if (!map.get(p).get(18).toString().isEmpty() ||!map.get(p).get(19).toString().isEmpty()) {
				tab = eventFactory.createDTD("\t");
				tempDocument = eventFactory.createStartElement("", "",
						"Assgnr");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				
				tempDocument = eventFactory.createStartElement("", "",
						"Pty");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				
				if ((!map.get(p).get(18).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(18).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(18));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 18,
								xmlEventWriter);
						// BAD_FILES_CHANGES ENDS
					}

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}
				
				
				//additional changes here
				if (!map.get(p).get(19).toString().isEmpty()) {
					tempDocument = eventFactory
							.createStartElement("", "", "Id");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					tempDocument = eventFactory.createStartElement("", "",
							"OrgId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 7);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					childNodes.put("Id", (String) map.get(p).get(19));

					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(19));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 19, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 8);
					}

					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 7);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory
							.createEndElement("", "", "OrgId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory.createEndElement("", "", "Id");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				
				EndDocument = eventFactory.createEndElement("", "", "Pty");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				
				EndDocument = eventFactory.createEndElement("", "", "Assgnr");
				tabMarked(xmlEventWriter, 3);

				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			
			//Assgne
			if (!map.get(p).get(20).toString().isEmpty()) {
			tab = eventFactory.createDTD("\t");
			tempDocument = eventFactory.createStartElement("", "",
					"Assgne");
			tabMarked(xmlEventWriter, 3);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			
				childNodes = new LinkedHashMap<String, String>();

				tempDocument = eventFactory.createStartElement("", "",
						"Agt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				tempDocument = eventFactory.createStartElement("", "",
						"FinInstnId");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();

				childNodes.put("BICFI", (String) map.get(p).get(20));

				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(20));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p,
							ruleName, childNodes, "BICFI", 19, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS

				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 6);
				}

				EndDocument = eventFactory.createEndElement("", "", "FinInstnId");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

			

				EndDocument = eventFactory.createEndElement("", "", "Agt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			
	
			EndDocument = eventFactory.createEndElement("", "", "Assgne");
			tabMarked(xmlEventWriter, 3);

			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
		
			//end assgne
			
			//crtd time
			
			if (!map.get(p).get(21).toString().isEmpty()) {
			
		  elementsMap.put("CreDtTm", (String) map.get(p).get(21));
		
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(21));
				else {

					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "CreDtTm", 21, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = elementsMap.keySet();
				for (String key : elementNodes) {

					createNode(xmlEventWriter, key, elementsMap.get(key), 3);
				}
			}
			
			//end of crtd time
			
			
			EndDocument = eventFactory.createEndElement("", "", "Assgnmt");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
	
			}
			//end of assgnmt
			
			//start of ctrl data
			
			tempDocument = eventFactory.createStartElement("", "", "CtrlData");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(tempDocument);

			xmlEventWriter.add(end);
			childNodes = new LinkedHashMap<String, String>();

			tab = eventFactory.createDTD("\t");

		//	NbOfTxs
			if (!map.get(p).get(22).toString().isEmpty()) {
				childNodes.put("NbOfTxs", map.get(p).get(22).toString());

				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(22));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "NbOfTxs", 22, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			// CtrlSum
			if (!map.get(p).get(23).toString().isEmpty()) {
				childNodes.put("CtrlSum", map.get(p).get(23).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(23));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "CtrlSum", 23, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			

		
			elementNodes = childNodes.keySet();

			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, childNodes.get(key), 3);
			}

			EndDocument = eventFactory.createEndElement("", "", "CtrlData");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
		
			//end of ctrl data
		
			//batch loop here
			
			
			int colCount = 24;

			ArrayList<Integer> stockList = new ArrayList<Integer>();

			int count = 0, k = 0, fileCount = 0;
			for (int j = 0; j < batchCount; j++) {
				if (!map.get(batchrowCount).get(colCount).toString().isEmpty()) {
					stockList.add(count + 1);
					fileCount++;
					count = 0;
					k++;
				} else
					count++;
				batchrowCount++;
			}
			stockList.add(count + 1);
			int arr[] = new int[stockList.size()];
			for (int i2 = 0; i2 < arr.length; i2++) {
				arr[i2] = stockList.get(i2).intValue();
			}
			for (int j2 = 1; j2 <= k; j2++) {
			}
			// int batchCount2=0;
			for (int i2 = 0, k2 = 1; i2 < fileCount; i2++, k2++) {
				cancellationInfo(tempDocument, EndDocument, xmlEventWriter,
						eventFactory, end, childNodes, elementNodes, tab, p,
						arr[k2], rowCol, ruleName, ruleRowNo, ruleColNo);

			}
			
			//batch loop end here
			tabMarked(xmlEventWriter, 1);
			xmlEventWriter.add(eventFactory.createEndElement("", "",
					rootElement));

			xmlEventWriter.add(end);

			xmlEventWriter.add(eventFactory.createEndDocument());
			eventFactory.createEndDocument();

	//	EndDocument = eventFactory.createEndElement("", "", "Document");
		//xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

			String rp = xmlEventWriter.toString();
			rp = rp.replaceAll(
					"</Document xmlns=urn:iso:std:iso:20022:tech:xsd:pain.001.001.03 xmlns:xsi=http://www.w3.org/2001/XMLSchema-instance>",
					"</Document>");

			xmlEventWriter.close();

		} catch (EmptyStackException eex) {
			log.fatal("EmptyStackException: ", eex);
			throw new EmptyStackException();
		} catch (FileNotFoundException e) {
			log.fatal("FileNotFoundException: ", e);
			e.printStackTrace();
			throw new FileNotFoundException();

		} catch (XMLStreamException ex) {
			log.fatal("XMLStreamException: ", ex);
			ex.printStackTrace();
			throw new XMLStreamException();
		}
		return rowCol;

	}

	// BAD_FILES_CHANGES
	private Map applyRule(int ruleRowNo, int ruleColNo, int p2,
			String ruleName, Map<String, String> elementsMap, String key,
			int actualColNo, XMLEventWriter xmlEventWriter)
			throws XMLStreamException {
		if (log.isDebugEnabled())
			log.debug("in applyRule ruleRowNo: " + ruleRowNo + " ruleColNo: "
					+ ruleColNo + "p: " + p2 + " ruleName: " + ruleName
					+ " elementsMap: " + elementsMap + " key: " + key
					+ " actualColNo: " + actualColNo);
		if (p2 == ruleRowNo && ruleColNo == actualColNo) {
			if ("D".equals(ruleName)) {
				createNode(xmlEventWriter, key, elementsMap.get(key), 3);

			} else if ("M".equals(ruleName)) {
				elementsMap.remove(key);

			} else if ("I".equals(ruleName)) {
				elementsMap.put(key + invalidAppender,
						(String) map.get(p).get(actualColNo));
				elementsMap.remove(key);

			}
		}
		if (log.isDebugEnabled())
			log.debug("Leaving applyRule: elementsMap " + elementsMap);
		return elementsMap;

	}

	public void cancellationInfo(StartElement tempDocument, EndElement EndDocument,
			XMLEventWriter xmlEventWriter, XMLEventFactory eventFactory,
			XMLEvent end, Map<String, String> childNodes,
			Set<String> elementNodes, XMLEvent tab, int tempP, int batchCount,
			ArrayList rowCol, String ruleName, int ruleRowNo, int ruleColNo)
			throws XMLStreamException {
		if (log.isDebugEnabled()) {
			log.debug("paymentInfo method starts in ConvertCAMT class");
		}
		// start <Undrlyg>

		try {
			
			
			
			tempDocument = eventFactory.createStartElement("", "", "Undrlyg");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			 
			
			tempDocument = eventFactory.createStartElement("", "", "OrgnlPmtInfAndCxl");
			tabMarked(xmlEventWriter, 3);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
		
			childNodes = new LinkedHashMap<String, String>();
			tab = eventFactory.createDTD("\t");
			
			
			// OrgnlPmtInfId
			if (!map.get(p).get(24).toString().isEmpty()) {
				
			
				
				childNodes.put("OrgnlPmtInfId", map.get(p).get(24).toString());

				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(24));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "OrgnlPmtInfId", 24, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
        
			elementNodes = childNodes.keySet();

			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, childNodes.get(key), 4);
			}
			
			
			
			
			for (int transC = 0; transC < batchCount; transC++) {
				createTransaction(tempDocument, EndDocument,
						xmlEventWriter, eventFactory, end, childNodes,
						elementNodes, tab, p, batchCount, rowCol, ruleName,
						ruleRowNo, ruleColNo);
				p++;
				while (p < (map.size() + 7) && map.get(p).get(25).isEmpty()) {
					p++;
					transC++;
					if (transC == batchCount)
						break;
				}
				
			}
			
			EndDocument = eventFactory.createEndElement("", "", "OrgnlPmtInfAndCxl");
			tabMarked(xmlEventWriter, 3);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
			
			
			EndDocument = eventFactory.createEndElement("", "", "Undrlyg");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
		
		
		} catch (XMLStreamException e) {
			e.printStackTrace();
			throw new XMLStreamException();
		}

	}

	private static void createNode(XMLEventWriter eventWriter, String element,
			String value, int tab2) throws XMLStreamException {

		XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();
		XMLEvent end = xmlEventFactory.createDTD("\n");
		XMLEvent tab = xmlEventFactory.createDTD("\t");
		// Create Start node
		StartElement sElement = xmlEventFactory.createStartElement("", "",
				element);
		for (int i = 0; i < tab2; i++)
			eventWriter.add(tab);
		eventWriter.add(sElement);
		// Create Content
		Characters characters = xmlEventFactory.createCharacters(value);
		eventWriter.add(characters);
		// Create End node
		EndElement eElement = xmlEventFactory.createEndElement("", "", element);
		eventWriter.add(eElement);
		eventWriter.add(end);

	}

	public void createTransaction(StartElement tempDocument,
			EndElement EndDocument, XMLEventWriter xmlEventWriter,
			XMLEventFactory eventFactory, XMLEvent end,
			Map<String, String> childNodes, Set<String> elementNodes,
			XMLEvent tab, int p, int batchCount, ArrayList rowCol,
			String ruleName, int ruleRowNo, int ruleColNo)
			throws XMLStreamException {

		try {
			for (int i = 25; i <= 34; i++) {
				
				if ((!map.get(p).get(i).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"TxInf");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					break;
				}
			}
				
				if ((!map.get(p).get(25).toString().isEmpty())
						|| (!map.get(p).get(26).toString().isEmpty())
						|| (!map.get(p).get(27).toString().isEmpty())
						|| (!map.get(p).get(28).toString().isEmpty()))
					 {
				
				
				
			
			childNodes = new LinkedHashMap<String, String>();

			if ((!map.get(p).get(25).toString().isEmpty())) {
				
				childNodes.put("CxlId", map.get(p).get(25).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(25));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p,
							ruleName, childNodes, "CxlId", 25,
							xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			
			
			elementNodes = childNodes.keySet();
			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, childNodes.get(key), 5);
			}

			
			}
				
				if ((!map.get(p).get(26).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();
					tempDocument = eventFactory.createStartElement("", "",
							"Case");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					
					
					childNodes.put("Id", map.get(p).get(26).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(26));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 26,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				
				
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 6);
				}
				}
				
				
				if ((!map.get(p).get(28).toString().isEmpty())) {
					
					tempDocument = eventFactory.createStartElement("", "",
							"Cretr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					
					tempDocument = eventFactory.createStartElement("", "",
							"Pty");
					tabMarked(xmlEventWriter, 7);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					
					
					childNodes = new LinkedHashMap<String, String>();

					childNodes.put("Nm", (String) map.get(p).get(27));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(27));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 27, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 8);
					}
				
					
				tempDocument = eventFactory.createStartElement("", "",
						"Id");
				tabMarked(xmlEventWriter, 8);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
					
					tempDocument = eventFactory.createStartElement("", "",
							"OrgId");
					tabMarked(xmlEventWriter, 9);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					
					
					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 10);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					
					
					childNodes = new LinkedHashMap<String, String>();
					childNodes.put("Id", map.get(p).get(28).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(28));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 28,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 11);
				}
				
				
				EndDocument = eventFactory.createEndElement("", "", "Othr");
				tabMarked(xmlEventWriter, 10);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				
				
				EndDocument = eventFactory.createEndElement("", "", "OrgId");
				tabMarked(xmlEventWriter, 9);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
		
				
				EndDocument = eventFactory.createEndElement("", "", "Id");
				tabMarked(xmlEventWriter, 8);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				
				EndDocument = eventFactory.createEndElement("", "", "Pty");
				tabMarked(xmlEventWriter, 7);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				
				EndDocument = eventFactory.createEndElement("", "", "Cretr");
				tabMarked(xmlEventWriter, 6);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				
				
				EndDocument = eventFactory.createEndElement("", "", "Case");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			  
			//new
				childNodes = new LinkedHashMap<String, String>();
				if ((!map.get(p).get(29).toString().isEmpty())) {
					
					
					childNodes.put("OrgnlInstrId", map.get(p).get(29).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(29));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "OrgnlInstrId", 29,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				
				}
				
				if ((!map.get(p).get(30).toString().isEmpty())) {
					
					childNodes.put("OrgnlEndToEndId", map.get(p).get(30).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(30));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "OrgnlEndToEndId", 30,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				
				}
				
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}
				
				XMLEvent xmlDocs;
				Characters characters;
				
				if ((!map.get(p).get(31).toString().isEmpty())) {
					
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
					{
						tempDocument = eventFactory.createStartElement("", "",
								"OrgnlInstdAmt");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlDocs = eventFactory.createAttribute("Ccy", map.get(p)
								.get(31).toString());
						xmlEventWriter.add(xmlDocs);

						characters = eventFactory.createCharacters(map.get(p)
								.get(32).toString());
						xmlEventWriter.add(characters);
						EndDocument = eventFactory.createEndElement("", "",
								"OrgnlInstdAmt");
						EndDocument.asEndElement();

						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(31));
						
						
						
					}
					else {

						// childNodes =
						// applyRule(ruleRowNo,ruleColNo,p,ruleName,childNodes,"Prtry",47);
						if (p == ruleRowNo && ruleColNo == 31) {
							if ("D".equals(ruleName)) {
								tempDocument = eventFactory.createStartElement(
										"", "", "OrgnlInstdAmt");
								tabMarked(xmlEventWriter, 5);
								xmlEventWriter.add(tempDocument);
								xmlDocs = eventFactory.createAttribute("Ccy",
										map.get(p).get(31).toString());
								xmlEventWriter.add(xmlDocs);
								characters = eventFactory.createCharacters(map
										.get(p).get(32).toString());
								xmlEventWriter.add(characters);

								EndDocument = eventFactory.createEndElement("",
										"", "OrgnlInstdAmt");
								EndDocument.asEndElement();

								xmlEventWriter.add(EndDocument);
								xmlEventWriter.add(end);

								tempDocument = eventFactory.createStartElement(
										"", "", "OrgnlInstdAmt");
								tabMarked(xmlEventWriter, 5);
								xmlEventWriter.add(tempDocument);
								xmlDocs = eventFactory.createAttribute("Ccy",
										map.get(p).get(31).toString());
								xmlEventWriter.add(xmlDocs);
								characters = eventFactory.createCharacters(map
										.get(p).get(32).toString());
								xmlEventWriter.add(characters);

								EndDocument = eventFactory.createEndElement("",
										"", "OrgnlInstdAmt");
								EndDocument.asEndElement();

								xmlEventWriter.add(EndDocument);
								xmlEventWriter.add(end);

							} else if ("M".equals(ruleName)) {

							} else if ("I".equals(ruleName)) {

								tempDocument = eventFactory.createStartElement(
										"", "", "Amt" + invalidAppender);
								tabMarked(xmlEventWriter, 5);
								xmlEventWriter.add(tempDocument);
								xmlDocs = eventFactory.createAttribute("Ccy",
										map.get(p).get(31).toString());
								xmlEventWriter.add(xmlDocs);

								characters = eventFactory.createCharacters(map
										.get(p).get(32).toString());
								xmlEventWriter.add(characters);
								EndDocument = eventFactory.createEndElement("",
										"", "OrgnlInstdAmt" + invalidAppender);
								EndDocument.asEndElement();

								xmlEventWriter.add(EndDocument);
								xmlEventWriter.add(end);

							}
						} else {
							tempDocument = eventFactory.createStartElement("",
									"", "OrgnlInstdAmt");
							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							xmlDocs = eventFactory.createAttribute("Ccy", map
									.get(p).get(31).toString());
							xmlEventWriter.add(xmlDocs);
							characters = eventFactory.createCharacters(map
									.get(p).get(32).toString());
							xmlEventWriter.add(characters);

							EndDocument = eventFactory.createEndElement("", "",
									"OrgnlInstdAmt");
							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

						}
						// pending
					}

				
				}
				
				
				if ((!map.get(p).get(33).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"OrgnlReqdExctnDt");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();
					tab = eventFactory.createDTD("\t");
					
						childNodes.put("Dt", map.get(p).get(33).toString()); // change
																					// 0803
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~" + String.valueOf(33));// change
																						// 0803
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Dt", 33,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key, childNodes.get(key), 6);
						}
						
						EndDocument = eventFactory.createEndElement("", "", "OrgnlReqdExctnDt");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
						
				}
				
				if ((!map.get(p).get(34).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"CxlRsnInf");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();
					tab = eventFactory.createDTD("\t");
					
						childNodes.put("AddtlInf", map.get(p).get(34).toString()); // change
																					// 0803
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~" + String.valueOf(34));// change
																						// 0803
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "AddtlInf", 34,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key, childNodes.get(key), 5);
						}
						
						EndDocument = eventFactory.createEndElement("", "", "CxlRsnInf");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
						
				}
			
				
					 
					 
					 
					 }
				for (int i = 25; i <= 34; i++) {//was int i = 46; i <= 143; i++

					if ((!map.get(p).get(i).toString().isEmpty())) {
						EndDocument = eventFactory.createEndElement("", "",
								"TxInf");
						tabMarked(xmlEventWriter, 4);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
						break;
					}
				}
				
				
			
			}
			

			
		 catch (XMLStreamException e) {
			e.printStackTrace();
			throw new XMLStreamException();
		}
		if (log.isDebugEnabled()) {
			log.debug("createTransactionNode method ends in CAMT class");
		}
	}


		public void tabMarked(XMLEventWriter xmlEventWriter, int count)
			throws XMLStreamException {
		XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();
		XMLEvent tab = xmlEventFactory.createDTD("\t");

		try {
			for (int i = 0; i < count; i++)
				xmlEventWriter.add(tab);
		} catch (XMLStreamException e) {
			e.printStackTrace();
			throw new XMLStreamException();
		}
	}
}