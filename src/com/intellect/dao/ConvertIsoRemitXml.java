/********************************************************************************************************/
/* Copyright �  2017 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/* 								 																		*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : ConvertFileInteracXml.java                                          					*/
/*                                                                                      				*/
/********************************************************************************************************/
/*  	   Author					    |        	  Date   			|	  Version                   */
/********************************************************************************************************/
/* 	         Abhishek Tiwari	        	|			02-06-2017			|		1.0	            */
/*		   Sapna Jain						|			18:07:2017			|   1.1
 /* 		  	 Preetam Sanjore			|			20:07:2017			|		1.2	
 * 			Shruti Gupta					|			29:08:2017			|		1.3
 * 			Preetam Sanjore					|			05:09:2017			|		1.4
 /* 	   Gokaran Tiwari					|			22:03:2018			|		1.5 latest suite change
 /* 																				in main table , old move to audit
*/
/********************************************************************************************************/
/* 											  														    */
/* Description   : For generation of Interac Xml file format 											*/
/*        																								*/
/********************************************************************************************************/

package com.intellect.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EmptyStackException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.log4j.Logger;

import com.intellect.pojo.TestToolFileNamePOJO;
import com.intellect.property.FormatConstants;
import com.intellect.service.FormatGenerateService;
import com.intellect.service.UpdateFormatXlsService;

public class ConvertIsoRemitXml {

	private static org.apache.log4j.Logger log = Logger
			.getLogger(ConvertIsoRemitXml.class);

	int m = 7, p = 7;
	int batchrowCount = 7;
	LinkedHashMap<Integer, Vector<String>> map = null;
	LinkedHashMap<Integer, Vector> map1 = null;
	ReadTemplate readTemplate = new ReadTemplate();
	int filesCrtdCnt = 0;

	// BAD_FILES_CHANGES
	private String invalidAppender = "001";

	public int main(String source, File target, String fileTypeSelected,
			LinkedHashMap<Integer, Vector<String>> map1, Connection connection,
			String db_table_file, String timeStamp, String dbTimeStamp,
			String moduleSelected, char[] variants,List<TestToolFileNamePOJO> testToolData) throws Exception {

		ConvertIsoRemitXml xmlWriter = new ConvertIsoRemitXml();
		try {
			filesCrtdCnt = xmlWriter.rootXMLFile(source, target,
					fileTypeSelected, map1, connection, db_table_file,
					timeStamp, dbTimeStamp, moduleSelected, variants,testToolData);
		} catch (Exception e) {
			log.fatal("Exception: ", e);
			throw new Exception(e);
		}

		return filesCrtdCnt;
	}

	public int rootXMLFile(String source, File target, String fileTypeSelected,
			LinkedHashMap map1, Connection connection, String db_table_file,
			String timeStamp, String dbTimeStamp, String moduleSelected,
			char[] variants, List<TestToolFileNamePOJO> pojoList) throws Exception {

		IwReadExcel objIwReadExcel = new IwReadExcel();

		map = (LinkedHashMap<Integer, Vector<String>>) objIwReadExcel.ReadData(
				source, fileTypeSelected, moduleSelected);
		// Change 1.5
		int rowCount = 7, colCount = 17;
		ArrayList<Integer> stockList = new ArrayList<Integer>();
		int count = 0, k = 0, fileCount = 0;
		for (int j = 0; j < map.size(); j++) {
			if (!map.get(rowCount).get(colCount).toString().isEmpty()) {
				stockList.add(count + 1);
				fileCount++;
				count = 0;
				k++;

			} else
				count++;
			rowCount++;
		}
		
		stockList.add(count + 1);
		int arr[] = new int[stockList.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = stockList.get(i).intValue();
		}
		ArrayList allRecList = new ArrayList();
		for (int i = 0, j2 = 1; i < fileCount; i++, j2++) {
			ArrayList rowCol = writeXML(arr[j2], target, connection,
					db_table_file, timeStamp, dbTimeStamp, null, -1, -1,pojoList);// Change 1.5
			allRecList.add(rowCol);
			for (int z = 0; z < rowCol.size(); z++) {
				
			}
		}
		log.debug("ALL good files generated");
		// BAD_FILES_CHANGES STARTS
		String[] ruleNames = null;
		boolean oFind = false;
		for (int i = 0; i < variants.length; i++) {
			if (variants[i] == 'O') {
				oFind = true;
			}
		}
		if (oFind) {
			ruleNames = new String[variants.length - 1];
		} else {
			ruleNames = new String[variants.length];
		}

		for (int i = 0; i < variants.length; i++) {
			if (variants[i] == 'O')
				continue;
			else
				ruleNames[i] = variants[i] + "";
		}

		int lastRowNo = 0;
		int startNoForEachMessage = 0;
		// iterating over no of messages
		for (int allRec = 0; allRec < allRecList.size(); allRec++) {
			if (lastRowNo == 0) {
				startNoForEachMessage = 7 + lastRowNo;
			} else {
				startNoForEachMessage = 1 + lastRowNo;

			}
			p = startNoForEachMessage;
			batchrowCount = startNoForEachMessage;
			m = startNoForEachMessage;
			ArrayList rowCol = (ArrayList) allRecList.get(allRec);
			for (int rowColNo = 0; rowColNo < rowCol.size(); rowColNo++) {
				String s = (String) rowCol.get(rowColNo);

				String[] strArray = s.split("~");
				lastRowNo = Integer.valueOf(strArray[0]);
				int ruleColNo = Integer.valueOf(strArray[1]);

				for (int x = 0; x < ruleNames.length; x++) {

					p = startNoForEachMessage;
					batchrowCount = startNoForEachMessage;
					m = startNoForEachMessage;

					// for (int i = 0, j2 = 1; i < fileCount; i++, j2++) {
					writeXML(arr[allRec + 1], target, connection,
							db_table_file, timeStamp, dbTimeStamp,
							ruleNames[x], lastRowNo, ruleColNo,pojoList);// Change 1.5
					// }

				}
			}
		}
		
		return fileCount;

	}

	public ArrayList writeXML(int batchCount, File target,
			Connection connection, String db_table_file, String timeStamp,
			String dbTimeStamp, String ruleName, int ruleRowNo, int ruleColNo,List<TestToolFileNamePOJO> pojoList)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("writeXML method starts in ConvertFileISOPain001 class: batchCount: "
					+ batchCount
					+ "ruleName: "
					+ ruleName
					+ " ruleRowNo: "
					+ ruleRowNo + " ruleColNo: " + ruleColNo + " p: " + p);
		}
		XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
		String XMLPath = target + "/";
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMdd");

		// XMLPath = XMLPath.replaceAll(".xml", m + ".xml");
		String fileName = null, dbfileName = null;
		// BAD_FILES_CHANGES STARTS
		ArrayList<String> rowCol = null;
		// BAD_FILES_CHANGES ENDS

		String isoremit_applied_rule_pos = FormatConstants.isoremitAppliedRulePosition;
		int isoremit_applied_rule_pos_no = Integer
				.parseInt(isoremit_applied_rule_pos);

		if (map.get(p).get(2) != null
				&& map.get(p).get(2).toString().length() > 0) {

			String temp = FormatGenerateService.getSequenceNumber(connection,
					map.get(p).get(2).toString());

			if (map.get(p).get(3) != null
					&& map.get(p).get(3).toString().replace(" ", "")
							.equalsIgnoreCase("FTS")) {

				File filefolder = new File(XMLPath + "/FTS");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();

				// BAD_FILES_CHANGES STARTS
				fileName = XMLPath
						+ "/"
						+ map.get(p).get(7).toString()
						+ "_"
						+ map.get(p).get(10).toString()
						+ "_"
						+ formatter.format(date)
						+ "_111213_"
						+ map.get(p)
								.get(8)
								.toString()
								.replace(".0", "")
								.substring(
										0,
										map.get(p).get(8).toString().length() - 6)
						+ temp + "." + map.get(p).get(2).toString() + "."
						+ map.get(p).get(11).toString() + ".xml";
				// BAD_FILES_CHANGES ENDS
				if (map.get(p).get(12) != null
						&& !map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(7).toString()
							+ "_"
							+ map.get(p).get(10).toString()
							+ "_"
							+ formatter.format(date)
							+ "_111213_"
							+ map.get(p)
									.get(8)
									.toString()
									.replace(".0", "")
									.substring(
											0,
											map.get(p).get(8).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();

				} else {
					dbfileName = map.get(p).get(7).toString()
							+ "_"
							+ map.get(p).get(10).toString()
							+ "_"
							+ formatter.format(date)
							+ "_111213_"
							+ map.get(p)
									.get(8)
									.toString()
									.replace(".0", "")
									.substring(
											0,
											map.get(p).get(8).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + ".xml";
				}

			} else if (map.get(p).get(3).toString().replace(" ", "")
					.equalsIgnoreCase("SCA")) {

				File filefolder = new File(XMLPath + "/SCA");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();

				fileName = XMLPath
						+ "/"
						+ map.get(p).get(6).toString()
						+ "."
						+ formatter1.format(date)
						+ "111213."
						+ map.get(p)
								.get(10)
								.toString()
								.substring(
										0,
										map.get(p).get(10).toString().length() - 6)
						+ temp + "."
						+ map.get(p).get(2).toString().replace(".0", "") + "."
						+ map.get(p).get(11).toString() + ".xml";
				if (!map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();

					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();

				} else {
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";

				}
			} else if (map.get(p).get(3).toString().replace(" ", "")
					.equalsIgnoreCase("FTT")) {

				File filefolder = new File(XMLPath + "/FTT");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();
				// BAD_FILES_CHANGES STARTS
				if (ruleName != null) {
					fileName = XMLPath + "/" + map.get(p).get(7).toString()
							+ "_" + map.get(p).get(10).toString() + "_"
							+ map.get(p).get(9).toString() + "_"
							+ map.get(p).get(8).toString().replace(".0", "")
							+ "." + map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + "_" + ruleName
							+ String.valueOf(ruleRowNo)
							+ String.valueOf(ruleColNo) + ".xml";

				} else {
					fileName = XMLPath + "/" + map.get(p).get(6).toString()
							+ "." + map.get(p).get(9).toString() + "."
							+ map.get(p).get(10).toString() + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";
				}
				// BAD_FILES_CHANGES ENDS
				if (!map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(6).toString() + "."
							+ map.get(p).get(9).toString() + "."
							+ map.get(p).get(10).toString() + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();

				} else {
					dbfileName = map.get(p).get(6).toString() + "."
							+ map.get(p).get(9).toString() + "."
							+ map.get(p).get(10).toString() + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";

				}
			}

			// BAD_FILES_CHANGES (S)
			StringBuffer strModifiedField = new StringBuffer();
			if (ruleName != null && !"".equals(ruleName)) {
				strModifiedField.append(ruleRowNo).append("~")
						.append(ruleColNo);
			}
			String errorCode = null;
			String ruleType = null;
			if ("M".equals(ruleName)) {
				ruleType = "TAG_MISSING";
				errorCode = "TAG_MISSING_ERRORCODE";
				// errorCode =
				// "Invalid Tag Found Invalid Tag Found PHERC_1002 Mandatory Section Group Missing";
			} else if ("D".equals(ruleName)) {
				ruleType = "DUPLICATE_TAG";
				errorCode = "DUPLICATE_TAG_ERRORCODE";
				// errorCode = "Invalid Tag Found Invalid Tag Found";
			} else if ("I".equals(ruleName)) {
				ruleType = "INVALID_TAG";
				errorCode = "INVALID_TAG_ERRORCODE";
				// errorCode =
				// "Invalid Tag Found Invalid Tag Found PHERC_1002 Mandatory Section Group Missing";
			} else if ("O".equals(ruleName)) {
				ruleType = "TAG_ORDER_CHANGE";
				errorCode = "TAG_ORDER_CHANGE_ERRORCODE";
				// errorCode =
				// "Invalid Tag Found Invalid Tag Found PHERC_1002 Mandatory Section Group Missing";
			} // BAD_FILES_CHANGES (E)

			String pain001_applied_rule_pos = FormatConstants.pain001AppliedRulePosition;
			int pain001_applied_rule_pos_no = Integer
					.parseInt(pain001_applied_rule_pos);

			// log.debug("dbfileName:::: "+dbfileName);
			// Change 1.5 starts
			if (ruleName != null && !"".equals(ruleName)) {
				TestToolFileNamePOJO pojoObj = new TestToolFileNamePOJO();
				pojoObj.setTestCaseid(map.get(p).get(0).toString());
				pojoObj.setFormatName(map.get(p).get(2).toString());
				pojoObj.setChannel(map.get(p).get(3).toString());
				pojoObj.setLsi(map.get(p).get(4).toString());
				pojoObj.setClientRefNum(map.get(p).get(17).toString());
				pojoObj.setBicCode(map.get(p).get(6).toString());
				pojoObj.setBbdId(map.get(p).get(7).toString());
				pojoObj.setCustRef(map.get(p).get(8).toString());
				pojoObj.setUniqueId(map.get(p).get(10).toString());
				pojoObj.setFileName(dbfileName);
				pojoObj.setAppliedRules(ruleType);
				pojoObj.setAppliedRules(errorCode);
				pojoObj.setModifiedField(strModifiedField.toString());
				pojoObj.setTimeStamp(dbTimeStamp);
				pojoList.add(pojoObj);
			} else {
				TestToolFileNamePOJO pojoObj = new TestToolFileNamePOJO();
				pojoObj.setTestCaseid(map.get(p).get(0).toString());
				pojoObj.setFormatName(map.get(p).get(2).toString());
				pojoObj.setChannel(map.get(p).get(3).toString());
				pojoObj.setLsi(map.get(p).get(4).toString());
				pojoObj.setClientRefNum(map.get(p).get(17).toString());
				pojoObj.setBicCode(map.get(p).get(6).toString());
				pojoObj.setBbdId(map.get(p).get(7).toString());
				pojoObj.setCustRef(map.get(p).get(8).toString());
				pojoObj.setUniqueId(map.get(p).get(10).toString());
				pojoObj.setFileName(dbfileName);
				pojoObj.setAppliedRules(map.get(p).get(isoremit_applied_rule_pos_no).toString());
				pojoObj.setAppliedRules(map.get(p).get(isoremit_applied_rule_pos_no + 1).toString());
				pojoObj.setModifiedField(map.get(p).get(isoremit_applied_rule_pos_no + 3).toString());
				pojoObj.setTimeStamp(dbTimeStamp);
				pojoList.add(pojoObj);
			}
			// Change 1.5 ends
		} else {

			if (log.isDebugEnabled()) {
				log.debug("FormatName is empty");
			}
		}
		m++;
		String rootElement = "RmtAdvc";

		try {
			XMLEventWriter xmlEventWriter = xmlOutputFactory
					.createXMLEventWriter(new FileOutputStream(fileName),
							"UTF-8");
			// System.out.println("file name -- "+fileName);
			XMLEventFactory eventFactory = XMLEventFactory.newInstance();
			XMLEvent end = eventFactory.createDTD(System
					.getProperty("line.separator"));
			XMLEvent tab = eventFactory.createDTD("\t");
			// 1.3 start
			/*
			 * StartDocument startDocument = eventFactory.createStartDocument();
			 * xmlEventWriter.add(startDocument); xmlEventWriter.add(end);
			 * StartElement tempDocument = eventFactory.createStartElement("",
			 * "", "Document");
			 */
			ArrayList<Namespace> ns = new ArrayList<Namespace>();

			ArrayList<Attribute> atts = new ArrayList<Attribute>();
			// atts.add(eventFactory.createAttribute("Document",""));
			atts.add(eventFactory
					.createAttribute(
							"xsi:schemaLocation",
							"urn:iso:std:iso:20022:tech:xsd:remt.001.001.02 file:///C:/Users/IBM_ADMIN/Box%20Sync/My%20Files/PSH/remt.001.001.02.xsd"));
			atts.add(eventFactory.createAttribute("xmlns",
					"urn:iso:std:iso:20022:tech:xsd:remt.001.001.02"));
			atts.add(eventFactory.createAttribute("xmlns:xsi",
					"http://www.w3.org/2001/XMLSchema-instance"));

			StartElement tempDocument = eventFactory.createStartElement("", "",
					"Document", atts.iterator(), ns.iterator());
			// 1.1 ends
			xmlEventWriter.add(tempDocument);
			/*
			 * XMLEvent xmlDocs2 = eventFactory.createAttribute("xmlns",
			 * "urn:iso:std:iso:20022:tech:xsd:pain.001.001.03"); xmlDocs2 =
			 * eventFactory.createNamespace("xsi",
			 * "http://www.w3.org/2001/XMLSchema-instance");
			 */
			// 1.3 end
			xmlEventWriter.add(end);

			StartElement configStartElement = eventFactory.createStartElement(
					"", "", rootElement);
			xmlEventWriter.add(tab);
			xmlEventWriter.add(configStartElement);
			xmlEventWriter.add(end);
			EndElement EndDocument = null;
			Map<String, String> childNodes = null;
			Set<String> elementNodes = null;
			tempDocument = eventFactory.createStartElement("", "", "GrpHdr");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			Map<String, String> elementsMap = new LinkedHashMap<String, String>();
			String[] Msg_id = { "", "" };// messageid changes
			// BAD_FILES_CHANGES STARTS
			if (ruleName == null) {
				rowCol = new ArrayList();
			}
			// BAD_FILES_CHANGES ENDS

			// Write the element nodes

			// <GrpHdr> start

			// MsgId
			if (!map.get(p).get(17).toString().isEmpty()) {
				elementsMap.put("MsgId", (String) map.get(p).get(17));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(17));
				else {
					Msg_id = UpdateFormatXlsService.getData(connection,
							"remt001v02");// messageid changes
					elementsMap.put("MsgId", Msg_id[0]);
					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "MsgId", 17, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			// CreDtTm
			// elementsMap = new LinkedHashMap<String, String>();
			if (!map.get(p).get(19).toString().isEmpty()) {
				elementsMap.put("CreDtTm", (String) map.get(p).get(19));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(19));
				else {

					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "CreDtTm", 19, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}

			elementNodes = elementsMap.keySet();
			for (String key : elementNodes) {

				createNode(xmlEventWriter, key, elementsMap.get(key), 3);
			}
			// Authstn
			if (!map.get(p).get(18).toString().isEmpty()) {
				tab = eventFactory.createDTD("\t");
				tempDocument = eventFactory.createStartElement("", "",
						"Authstn");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if ((!map.get(p).get(18).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Prtry", map.get(p).get(18).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(18));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Prtry", 18,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}
				EndDocument = eventFactory.createEndElement("", "", "Authstn");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			elementsMap = new LinkedHashMap<String, String>();

			if (!map.get(p).get(20).toString().isEmpty()
					|| !map.get(p).get(21).toString().isEmpty()) {

				tempDocument = eventFactory.createStartElement("", "",
						"InitgPty");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				if (!map.get(p).get(20).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					childNodes.put("Nm", (String) map.get(p).get(20));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(20));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 20, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 4);
					}
				}
				if (!map.get(p).get(21).toString().isEmpty()) {
					tempDocument = eventFactory
							.createStartElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					tempDocument = eventFactory.createStartElement("", "",
							"OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					childNodes.put("Id", (String) map.get(p).get(21));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(21));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 21, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}

					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory
							.createEndElement("", "", "OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory.createEndElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "InitgPty");
				tabMarked(xmlEventWriter, 3);

				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			EndDocument = eventFactory.createEndElement("", "", "GrpHdr");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

			int colCount = 22;// Remittance Identification column

			ArrayList<Integer> stockList = new ArrayList<Integer>();

			int count = 0, k = 0, fileCount = 0;
			for (int j = 0; j < batchCount; j++) {
				if (!map.get(batchrowCount).get(colCount).toString().isEmpty()) {
					stockList.add(count + 1);
					fileCount++;
					count = 0;
					k++;
				} else
					count++;
				batchrowCount++;
			}
			stockList.add(count + 1);
			int arr[] = new int[stockList.size()];
			for (int i2 = 0; i2 < arr.length; i2++) {
				arr[i2] = stockList.get(i2).intValue();
			}
			for (int j2 = 1; j2 <= k; j2++) {
			}
			// int batchCount2=0;
			// System.out.println("batch count "+fileCount);
			for (int i2 = 0, k2 = 1; i2 < fileCount; i2++, k2++) {
				paymentInfo(tempDocument, EndDocument, xmlEventWriter,
						eventFactory, end, childNodes, elementNodes, tab, p,
						arr[k2], rowCol, ruleName, ruleRowNo, ruleColNo);
				// System.out.println("payment p"+fileCount);

				p++;// p++;
				while (p < (map.size() + 7)
						&& map.get(p).get(22).toString().isEmpty()) {
					p++;

				}
			}
			// System.out.println("out loop payment p"+p);

			tabMarked(xmlEventWriter, 1);
			xmlEventWriter.add(eventFactory.createEndElement("", "",
					rootElement));

			xmlEventWriter.add(end);

			xmlEventWriter.add(eventFactory.createEndDocument());
			eventFactory.createEndDocument();
			// Changes done as getting exception
			// EndDocument = eventFactory.createEndElement("", "", "Document");
			// xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

			String rp = xmlEventWriter.toString();
			rp = rp.replaceAll(
					"</Document xmlns=urn:iso:std:iso:20022:tech:xsd:pain.001.001.03 xmlns:xsi=http://www.w3.org/2001/XMLSchema-instance>",
					"</Document>");

			xmlEventWriter.close();

		} catch (EmptyStackException exx) {
			log.fatal("EmptyStackException: ", exx);
			throw new EmptyStackException();
		} catch (FileNotFoundException e) {
			log.fatal("FileNotFoundException: ", e);
			throw new FileNotFoundException();
			// e.printStackTrace();
		} catch (XMLStreamException ex) {
			log.fatal("XMLStreamException: ", ex);
			throw new XMLStreamException();
		}
		return rowCol;

	}

	// BAD_FILES_CHANGES
	private Map applyRule(int ruleRowNo, int ruleColNo, int p2,
			String ruleName, Map<String, String> elementsMap, String key,
			int actualColNo, XMLEventWriter xmlEventWriter) throws Exception {
		if (log.isDebugEnabled())
			log.debug("in applyRule ruleRowNo: " + ruleRowNo + " ruleColNo: "
					+ ruleColNo + "p: " + p2 + " ruleName: " + ruleName
					+ " elementsMap: " + elementsMap + " key: " + key
					+ " actualColNo: " + actualColNo);
		if (p2 == ruleRowNo && ruleColNo == actualColNo) {
			if ("D".equals(ruleName)) {
				// elementsMap.put(key, (String) map.get(p).get(actualColNo));
				createNode(xmlEventWriter, key, elementsMap.get(key), 3);

			} else if ("M".equals(ruleName)) {
				elementsMap.remove(key);

			} else if ("I".equals(ruleName)) {
				elementsMap.put(key + invalidAppender,
						(String) map.get(p).get(actualColNo));
				elementsMap.remove(key);

			}
		}
		if (log.isDebugEnabled())
			log.debug("Leaving applyRule: elementsMap " + elementsMap);
		return elementsMap;

	}

	public void paymentInfo(StartElement tempDocument, EndElement EndDocument,
			XMLEventWriter xmlEventWriter, XMLEventFactory eventFactory,
			XMLEvent end, Map<String, String> childNodes,
			Set<String> elementNodes, XMLEvent tab, int tempP, int batchCount,
			ArrayList rowCol, String ruleName, int ruleRowNo, int ruleColNo)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("paymentInfo method starts in ConvertFileISOPain001 class");
		}
		// start <PmtInf>
		tempDocument = eventFactory.createStartElement("", "", "RmtInf");
		try {
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(tempDocument);

			xmlEventWriter.add(end);
			childNodes = new LinkedHashMap<String, String>();

			tab = eventFactory.createDTD("\t");

			// PmtInfId
			if (!map.get(p).get(22).toString().isEmpty()) {
				childNodes.put("RmtId", map.get(p).get(22).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(22));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "RmtId", 22, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				}// PmtMtd
			else {
				}
			if (!map.get(p).get(23).toString().isEmpty()) {
				childNodes.put("Ustrd", map.get(p).get(23).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(23));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Ustrd", 23, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			elementNodes = childNodes.keySet();
			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, childNodes.get(key), 3);
			}

			// strd
			// check start
			int p2 = p + 1;
			int count = 1;
			int p3 = 0;

			// System.out.println("map size::::::: "+map.size()+" "+p);
			int flag = 0;
			int newp = p;
			while (p2 < (map.size() + 7)) {
				if (map.get(p2).get(22).toString().isEmpty()) {
					// System.out.println("RmtInf is empty:::: "+map.get(p2).get(22).toString());
					if (map.get(p2).get(24).toString().isEmpty()
							&& (!map.get(p2).get(25).toString().isEmpty()
									|| !map.get(p2).get(26).toString()
											.isEmpty() || !map.get(p2).get(27)
									.toString().isEmpty())) {

						count++;
						flag = 1;
					} else if (flag == 0
							&& !map.get(p2).get(24).toString().isEmpty()) {
						for (int i = 0; i < count; i++) {
							strdEntry(tempDocument, EndDocument,
									xmlEventWriter, eventFactory, end,
									childNodes, elementNodes, tab, newp,
									batchCount, i, count, rowCol, ruleName,
									ruleRowNo, ruleColNo);

							newp++;
						}
					} else if (flag == 1
							&& !map.get(p2).get(24).toString().isEmpty()) {
						for (int i = 0; i < count; i++) {
							strdEntry(tempDocument, EndDocument,
									xmlEventWriter, eventFactory, end,
									childNodes, elementNodes, tab, newp,
									batchCount, i, count, rowCol, ruleName,
									ruleRowNo, ruleColNo);

							newp++;
						}
						flag = 0;
						count = 1;
					}
				} else {
					for (int i = 0; i < count; i++) {
						strdEntry(tempDocument, EndDocument, xmlEventWriter,
								eventFactory, end, childNodes, elementNodes,
								tab, newp, batchCount, i, count, rowCol,
								ruleName, ruleRowNo, ruleColNo);

						newp++;
					}
					flag = 2;
					break;
				}
				p2 = p2 + 1;
			}
			if (flag != 2)
				for (int i = 0; i < count; i++) {
					strdEntry(tempDocument, EndDocument, xmlEventWriter,
							eventFactory, end, childNodes, elementNodes, tab,
							newp, batchCount, i, count, rowCol, ruleName,
							ruleRowNo, ruleColNo);

					newp++;
				}

			// start OrglPmtInf
			if ((!map.get(p).get(28).toString().isEmpty())
					|| (!map.get(p).get(29).toString().isEmpty())
					|| (!map.get(p).get(30).toString().isEmpty())) {

				tempDocument = eventFactory.createStartElement("", "",
						"OrgnlPmtInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				tempDocument = eventFactory.createStartElement("", "", "Refs");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("PmtInfId", map.get(p).get(28).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(28));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "PmtInfId", 28, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				childNodes.put("InstrId", map.get(p).get(29).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(29));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "InstrId", 29, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				childNodes.put("EndToEndId", map.get(p).get(30).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(30));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "EndToEndId", 30, xmlEventWriter);
				}
				if (!map.get(p).get(31).toString().isEmpty()) {
					// log.debug("TxId ::"+map.get(p).get(31).toString());
					// BAD_FILES_CHANGES ENDS
					childNodes.put("TxId", map.get(p).get(31).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(31));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "TxId", 31,
								xmlEventWriter);
					}
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}

				EndDocument = eventFactory.createEndElement("", "", "Refs");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				// PmtTpInf

				if ((!map.get(p).get(32).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"PmtTpInf");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					tempDocument = eventFactory.createStartElement("", "",
							"LclInstrm");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Prtry", map.get(p).get(32).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(32));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Prtry", 32,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"LclInstrm");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					// Category Purpose Code Proprietary
					if ((!map.get(p).get(33).toString().isEmpty())) {
						tempDocument = eventFactory.createStartElement("", "",
								"SvcLvl");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Prtry", map.get(p).get(33).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(33));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Prtry", 33,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}

						EndDocument = eventFactory.createEndElement("", "",
								"SvcLvl");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}

					// 1.4 starts //added CtgyPurp Category Purpose Code
					// Proprietary
					if ((!map.get(p).get(59).toString().isEmpty())) {
						tempDocument = eventFactory.createStartElement("", "",
								"CtgyPurp");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Prtry", map.get(p).get(59).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(59));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Prtry", 59,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}

						EndDocument = eventFactory.createEndElement("", "",
								"CtgyPurp");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}
					// 1.4 ends

					EndDocument = eventFactory.createEndElement("", "",
							"PmtTpInf");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				// Amt

				XMLEvent xmlDocs;
				Characters characters;
				if (!map.get(p).get(34).toString().isEmpty())

				{
					tempDocument = eventFactory.createStartElement("", "",
							"Amt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					// BAD_FILES_CHANGES STARTS
					if (rowCol != null) {

						tempDocument = eventFactory.createStartElement("", "",
								"InstdAmt");

						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlDocs = eventFactory.createAttribute("Ccy", map
								.get(p).get(35).toString());
						xmlEventWriter.add(xmlDocs);

						characters = eventFactory.createCharacters(map.get(p)
								.get(34).toString());
						xmlEventWriter.add(characters);

						EndDocument = eventFactory.createEndElement("", "",
								"InstdAmt");

						EndDocument.asEndElement();

						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);

						rowCol.add(String.valueOf(p) + "~" + String.valueOf(34));
					} else {

						// childNodes =
						// applyRule(ruleRowNo,ruleColNo,p,ruleName,childNodes,"InstdAmt",47);
						if (p == ruleRowNo && ruleColNo == 34) {
							if ("D".equals(ruleName)) {
								tempDocument = eventFactory.createStartElement(
										"", "", "InstdAmt");

								tabMarked(xmlEventWriter, 5);
								xmlEventWriter.add(tempDocument);
								xmlDocs = eventFactory.createAttribute("Ccy",
										map.get(p).get(35).toString());
								xmlEventWriter.add(xmlDocs);

								characters = eventFactory.createCharacters(map
										.get(p).get(34).toString());
								xmlEventWriter.add(characters);

								EndDocument = eventFactory.createEndElement("",
										"", "InstdAmt");

								EndDocument.asEndElement();

								xmlEventWriter.add(EndDocument);
								xmlEventWriter.add(end);

								tempDocument = eventFactory.createStartElement(
										"", "", "InstdAmt");

								tabMarked(xmlEventWriter, 5);
								xmlEventWriter.add(tempDocument);
								xmlDocs = eventFactory.createAttribute("Ccy",
										map.get(p).get(35).toString());
								xmlEventWriter.add(xmlDocs);

								characters = eventFactory.createCharacters(map
										.get(p).get(34).toString());
								xmlEventWriter.add(characters);

								EndDocument = eventFactory.createEndElement("",
										"", "InstdAmt");

								EndDocument.asEndElement();

								xmlEventWriter.add(EndDocument);
								xmlEventWriter.add(end);

							} else if ("M".equals(ruleName)) {

							} else if ("I".equals(ruleName)) {

								tempDocument = eventFactory.createStartElement(
										"", "", "InstdAmt" + invalidAppender);

								tabMarked(xmlEventWriter, 5);
								xmlEventWriter.add(tempDocument);
								xmlDocs = eventFactory.createAttribute("Ccy",
										map.get(p).get(35).toString());
								xmlEventWriter.add(xmlDocs);

								characters = eventFactory.createCharacters(map
										.get(p).get(34).toString());
								xmlEventWriter.add(characters);

								EndDocument = eventFactory.createEndElement("",
										"", "InstdAmt" + invalidAppender);

								EndDocument.asEndElement();

								xmlEventWriter.add(EndDocument);
								xmlEventWriter.add(end);

							}
						} else {
							tempDocument = eventFactory.createStartElement("",
									"", "InstdAmt");

							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							xmlDocs = eventFactory.createAttribute("Ccy", map
									.get(p).get(35).toString());
							xmlEventWriter.add(xmlDocs);

							characters = eventFactory.createCharacters(map
									.get(p).get(34).toString());
							xmlEventWriter.add(characters);

							EndDocument = eventFactory.createEndElement("", "",
									"InstdAmt");

							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

						}
						// pending
					}
					// BAD_FILES_CHANGES ENDS

					EndDocument = eventFactory.createEndElement("", "", "Amt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				// end Amt
				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("ReqdExctnDt", map.get(p).get(36).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(36));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "ReqdExctnDt", 36, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 6);
				}

				// Dbtr
				// <Dbtr>
				if ((!map.get(p).get(37).toString().isEmpty())
						|| (!map.get(p).get(38).toString().isEmpty())
						|| (!map.get(p).get(39).toString().isEmpty())
						|| (!map.get(p).get(40).toString().isEmpty())
						|| (!map.get(p).get(41).toString().isEmpty())
						|| (!map.get(p).get(42).toString().isEmpty())

						|| (!map.get(p).get(43).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"Dbtr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					if ((!map.get(p).get(37).toString().isEmpty())) {
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Nm", map.get(p).get(37).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(37));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Nm", 37,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 5);
						}
					}
					if ((!map.get(p).get(38).toString().isEmpty())
							|| (!map.get(p).get(39).toString().isEmpty())
							|| (!map.get(p).get(40).toString().isEmpty())
							|| (!map.get(p).get(41).toString().isEmpty())
							|| (!map.get(p).get(42).toString().isEmpty())
							|| (!map.get(p).get(43).toString().isEmpty())) {

						tempDocument = eventFactory.createStartElement("", "",
								"PstlAdr");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();
						tab = eventFactory.createDTD("\t");
						if (!map.get(p).get(38).toString().isEmpty()) {
							childNodes.put("StrtNm", map.get(p).get(38)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(38));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "StrtNm", 38,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}

						if (!map.get(p).get(39).toString().isEmpty()) {
							childNodes.put("BldgNb", map.get(p).get(39)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(39));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "BldgNb", 39,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						
						if (!map.get(p).get(40).toString().isEmpty()) {
							childNodes.put("PstCd", map.get(p).get(40)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(40));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "PstCd", 40,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(41).toString().isEmpty()) {
							childNodes.put("TwnNm", map.get(p).get(41)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(41));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "TwnNm", 41,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(42).toString().isEmpty()) {
							childNodes.put("CtrySubDvsn", map.get(p).get(42)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(42));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "CtrySubDvsn",
										42, xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(43).toString().isEmpty()) {
							childNodes.put("Ctry", map.get(p).get(43)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(43));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "Ctry", 43,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
						EndDocument = eventFactory.createEndElement("", "",
								"PstlAdr");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}
					if ((!map.get(p).get(44).toString().isEmpty())
							|| (!map.get(p).get(45).toString().isEmpty())) {

						tempDocument = eventFactory.createStartElement("", "",
								"CtctDtls");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();
						tab = eventFactory.createDTD("\t");
						if (!map.get(p).get(44).toString().isEmpty()) {
							childNodes.put("FaxNb", map.get(p).get(44)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(44));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "FaxNb", 44,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}

						if (!map.get(p).get(45).toString().isEmpty()) {
							childNodes.put("EmailAdr", map.get(p).get(45)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(45));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "EmailAdr", 45,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
						EndDocument = eventFactory.createEndElement("", "",
								"CtctDtls");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}

					EndDocument = eventFactory.createEndElement("", "", "Dbtr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				// end Dbtr

				// 1.4 starts
				// DbtrAcct
				if (!map.get(p).get(56).toString().isEmpty()) {
					tempDocument = eventFactory.createStartElement("", "",
							"DbtrAcct");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					tempDocument = eventFactory
							.createStartElement("", "", "Id");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Id", map.get(p).get(56).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(56));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 56, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}
					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					// end else part
					EndDocument = eventFactory.createEndElement("", "", "Id");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
					EndDocument = eventFactory.createEndElement("", "",
							"DbtrAcct");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				// DbtrAgt
				if ((!map.get(p).get(57).toString().isEmpty())
						|| (!map.get(p).get(58).toString().isEmpty())
						|| (!map.get(p).get(60).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"DbtrAgt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					tempDocument = eventFactory.createStartElement("", "",
							"FinInstnId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					if ((!map.get(p).get(60).toString().isEmpty())) {

						tempDocument = eventFactory.createStartElement("", "",
								"PstlAdr");
						tabMarked(xmlEventWriter, 6);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						/*
						 * if (!map.get(p).get(71).toString().isEmpty()){
						 * childNodes.put("StrtNm",
						 * map.get(p).get(71).toString()); //BAD_FILES_CHANGES
						 * STARTS if(rowCol!=null)
						 * rowCol.add(String.valueOf(p)+"~"+String.valueOf(71));
						 * else{
						 * 
						 * childNodes =
						 * applyRule(ruleRowNo,ruleColNo,p,ruleName,
						 * childNodes,"StrtNm",71, xmlEventWriter); }
						 * //BAD_FILES_CHANGES ENDS } if
						 * (!map.get(p).get(73).toString().isEmpty()){
						 * childNodes.put("PstCd",
						 * map.get(p).get(73).toString()); //BAD_FILES_CHANGES
						 * STARTS if(rowCol!=null)
						 * rowCol.add(String.valueOf(p)+"~"+String.valueOf(73));
						 * else{
						 * 
						 * childNodes =
						 * applyRule(ruleRowNo,ruleColNo,p,ruleName,
						 * childNodes,"PstCd",73, xmlEventWriter); }
						 * //BAD_FILES_CHANGES ENDS } if
						 * (!map.get(p).get(74).toString().isEmpty()){
						 * childNodes.put("TwnNm",
						 * map.get(p).get(74).toString()); //BAD_FILES_CHANGES
						 * STARTS if(rowCol!=null)
						 * rowCol.add(String.valueOf(p)+"~"+String.valueOf(74));
						 * else{
						 * 
						 * childNodes =
						 * applyRule(ruleRowNo,ruleColNo,p,ruleName,
						 * childNodes,"TwnNm",74, xmlEventWriter); }
						 * //BAD_FILES_CHANGES ENDS } if
						 * (!map.get(p).get(75).toString().isEmpty()){
						 * childNodes.put("CtrySubDvsn", map.get(p).get(75)
						 * .toString()); //BAD_FILES_CHANGES STARTS
						 * if(rowCol!=null)
						 * rowCol.add(String.valueOf(p)+"~"+String.valueOf(75));
						 * else{
						 * 
						 * childNodes =
						 * applyRule(ruleRowNo,ruleColNo,p,ruleName,
						 * childNodes,"CtrySubDvsn",75, xmlEventWriter); }
						 * //BAD_FILES_CHANGES ENDS }
						 */
						if (!map.get(p).get(60).toString().isEmpty()) {
							childNodes.put("Ctry", map.get(p).get(60)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(60));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "Ctry", 60,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 7);
						}
						EndDocument = eventFactory.createEndElement("", "",
								"PstlAdr");
						tabMarked(xmlEventWriter, 6);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}
					if ((!map.get(p).get(57).toString().isEmpty())) {
						tempDocument = eventFactory.createStartElement("", "",
								"Othr");
						tabMarked(xmlEventWriter, 6);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Id", map.get(p).get(57).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(57));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Id", 57,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS

						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}

						EndDocument = eventFactory.createEndElement("", "",
								"Othr");
						tabMarked(xmlEventWriter, 6);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"FinInstnId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					if ((!map.get(p).get(58).toString().isEmpty())) {
						tempDocument = eventFactory.createStartElement("", "",
								"BrnchId");
						tabMarked(xmlEventWriter, 6);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Id", map.get(p).get(58).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(58));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Id", 58,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}

						EndDocument = eventFactory.createEndElement("", "",
								"BrnchId");
						tabMarked(xmlEventWriter, 6);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"DbtrAgt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				// 1.4 ends

				// start Cdtr
				if ((!map.get(p).get(46).toString().isEmpty())
						|| (!map.get(p).get(47).toString().isEmpty())
						|| (!map.get(p).get(48).toString().isEmpty())
						|| (!map.get(p).get(49).toString().isEmpty())
						|| (!map.get(p).get(50).toString().isEmpty())
						|| (!map.get(p).get(51).toString().isEmpty())
						|| (!map.get(p).get(52).toString().isEmpty())) {

					tempDocument = eventFactory.createStartElement("", "",
							"Cdtr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					if ((!map.get(p).get(46).toString().isEmpty())) {
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Nm", map.get(p).get(46).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(46));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Nm", 46,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 5);
						}
					}
					if ((!map.get(p).get(47).toString().isEmpty())
							|| (!map.get(p).get(48).toString().isEmpty())
							|| (!map.get(p).get(49).toString().isEmpty())
							|| (!map.get(p).get(50).toString().isEmpty())
							|| (!map.get(p).get(51).toString().isEmpty())
							|| (!map.get(p).get(52).toString().isEmpty())) {

						tempDocument = eventFactory.createStartElement("", "",
								"PstlAdr");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();
						tab = eventFactory.createDTD("\t");
						if (!map.get(p).get(47).toString().isEmpty()) {
							childNodes.put("StrtNm", map.get(p).get(47)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(47));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "StrtNm", 47,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}

						if (!map.get(p).get(48).toString().isEmpty()) {
							childNodes.put("BldgNb", map.get(p).get(48)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(48));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "BldgNb", 48,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						
						if (!map.get(p).get(49).toString().isEmpty()) {
							childNodes.put("PstCd", map.get(p).get(49)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(49));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "PstCd", 49,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(50).toString().isEmpty()) {
							childNodes.put("TwnNm", map.get(p).get(50)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(50));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "TwnNm", 50,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(51).toString().isEmpty()) {
							childNodes.put("CtrySubDvsn", map.get(p).get(51)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(51));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "CtrySubDvsn",
										51, xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(52).toString().isEmpty()) {
							childNodes.put("Ctry", map.get(p).get(52)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(52));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "Ctry", 52,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
						EndDocument = eventFactory.createEndElement("", "",
								"PstlAdr");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}
					if ((!map.get(p).get(53).toString().isEmpty())
							|| (!map.get(p).get(54).toString().isEmpty())) {

						tempDocument = eventFactory.createStartElement("", "",
								"CtctDtls");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();
						tab = eventFactory.createDTD("\t");
						if (!map.get(p).get(53).toString().isEmpty()) {
							childNodes.put("FaxNb", map.get(p).get(53)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(53));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "FaxNb", 53,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}

						if (!map.get(p).get(54).toString().isEmpty()) {
							childNodes.put("EmailAdr", map.get(p).get(54)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(54));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "EmailAdr", 54,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
						EndDocument = eventFactory.createEndElement("", "",
								"CtctDtls");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}

					EndDocument = eventFactory.createEndElement("", "", "Cdtr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				// end Cdtr

				// CdtrAcct
				if (!map.get(p).get(55).toString().isEmpty()) {
					tempDocument = eventFactory.createStartElement("", "",
							"CdtrAcct");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					tempDocument = eventFactory
							.createStartElement("", "", "Id");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Id", map.get(p).get(55).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(55));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 55, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}
					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					// end else part
					EndDocument = eventFactory.createEndElement("", "", "Id");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
					EndDocument = eventFactory.createEndElement("", "",
							"CdtrAcct");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				// end CdtrAcct

				EndDocument = eventFactory.createEndElement("", "",
						"OrgnlPmtInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// p++;
			// end OrglPmtInf
			EndDocument = eventFactory.createEndElement("", "", "RmtInf");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
			// end </PmtInf>

		} catch (XMLStreamException e) {
			e.printStackTrace();
			throw new XMLStreamException(e);
		}

	}

	private static void createNode(XMLEventWriter eventWriter, String element,
			String value, int tab2) throws XMLStreamException {

		XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();
		XMLEvent end = xmlEventFactory.createDTD("\n");
		XMLEvent tab = xmlEventFactory.createDTD("\t");
		// Create Start node
		StartElement sElement = xmlEventFactory.createStartElement("", "",
				element);
		for (int i = 0; i < tab2; i++)
			eventWriter.add(tab);
		eventWriter.add(sElement);
		// Create Content
		Characters characters = xmlEventFactory.createCharacters(value);
		eventWriter.add(characters);
		// Create End node
		EndElement eElement = xmlEventFactory.createEndElement("", "", element);
		eventWriter.add(eElement);
		eventWriter.add(end);

	}

	public void tabMarked(XMLEventWriter xmlEventWriter, int count) {
		XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();
		// XMLEvent end = xmlEventFactory.createDTD("\n");
		XMLEvent tab = xmlEventFactory.createDTD("\t");

		try {
			for (int i = 0; i < count; i++)
				xmlEventWriter.add(tab);
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}

	public void strdEntry(StartElement tempDocument, EndElement EndDocument,
			XMLEventWriter xmlEventWriter, XMLEventFactory eventFactory,
			XMLEvent end, Map<String, String> childNodes,
			Set<String> elementNodes, XMLEvent tab, int p, int batchCount,
			int i, int count, ArrayList rowCol, String ruleName, int ruleRowNo,
			int ruleColNo) throws Exception {
		if (!map.get(p).get(24).toString().isEmpty()
				&& map.get(p).get(24).toString().equals("Strd")) {
			tempDocument = eventFactory.createStartElement("", "", "Strd");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);

		}
		if (!map.get(p).get(25).toString().isEmpty()
				|| !map.get(p).get(26).toString().isEmpty()) {
			tempDocument = eventFactory
					.createStartElement("", "", "RfrdDocInf");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			if ((!map.get(p).get(25).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "", "Tp");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				tempDocument = eventFactory.createStartElement("", "",
						"CdOrPrtry");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Prtry", map.get(p).get(25).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(25));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Prtry", 25, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}

				EndDocument = eventFactory
						.createEndElement("", "", "CdOrPrtry");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				EndDocument = eventFactory.createEndElement("", "", "Tp");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			} // fix for strd issue

			if ((!map.get(p).get(26).toString().isEmpty())) {
				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Nb", map.get(p).get(26).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(26));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Nb", 26, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}
			}
			if ((!map.get(p).get(27).toString().isEmpty())) {
				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("RltdDt", map.get(p).get(27).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(27));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "RltdDt", 27, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}
			}
			EndDocument = eventFactory.createEndElement("", "", "RfrdDocInf");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

		}
		// if(map.get(p).get(24).toString().isEmpty() &&
		// map.get(p).get(24).toString().equals("Strd")){
		if (!map.get(p).get(24).toString().isEmpty() && i == count - 1
				&& map.get(p).get(24).toString().equals("Strd")) {
			EndDocument = eventFactory.createEndElement("", "", "Strd");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
		} else if (i == count - 1) {
			EndDocument = eventFactory.createEndElement("", "", "Strd");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
		}

	}

}