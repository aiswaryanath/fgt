/* Copyright �  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/* 								 																		*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : MessageTracking.java                                           					*/
/*                                                                                      				*/
/********************************************************************************************************/
/*  		      Author				        |        	  Date   			|	  Version           */
/********************************************************************************************************/
/*			Upendar Ch							|			18:07:2017			|
 /********************************************************************************************************/
/* 																										*/
/* Description   : DAO class to hold ruleType,Tag,TransactionNumber and result string        		     */
/*        																								*/
/********************************************************************************************************/
package com.intellect.dao;

public class MessageTracking {
	private String resultString;
	private String ruleType;
	private String tagApplied;
	private String transactionNumber;

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public String getResultString() {
		return resultString;
	}

	public void setResultString(String resultString) {
		this.resultString = resultString;
	}

	public String getRuleType() {
		return ruleType;
	}

	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	public String getTagApplied() {
		return tagApplied;
	}

	public void setTagApplied(String tagApplied) {
		this.tagApplied = tagApplied;
	}
}
