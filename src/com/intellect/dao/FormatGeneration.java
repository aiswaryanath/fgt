/********************************************************************************************************/
/* Copyright �  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/* 								 																		*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : AllFormatsGeneration.java                                           					*/
/*                                                                                      				*/
/********************************************************************************************************/
/*  		      Author				        |        	  Date   			|	  Version           */
/********************************************************************************************************/
/*         	Surendra Reddy.M 		            |			20:05:2016			|		1.0	            */
/*         	Abhishek Tiwari 		            |			09:03:2017			|		1.1	            */
/*			Upendar Ch							|			18:07:2017			|       1.2
 *  		Shruti Gupta                        |           27:07:2017			|       1.3
 *  		Gokaran Tiwari						|			08:02:2018			|       1.4 removed class 
 *  																			| 		level static variable because
 *  																			| 		it was maintaining previous value.
 *  		Gokaran Tiwari						|			14:02:2018			| 		1.5 Length padding issue for CIBC 1464(NDM)
 */
/********************************************************************************************************/
/* 																										*/
/* Description   : For generation of 9 Formats ( i.e CIBC 1464, CIBC 80 BYTE, NACHA IAT 94 BYTE, CPA005 */
/*                                                   NACHA 94, MT 101, MT104, CMO103, ANSI(X820) )      */
/*        																								*/
/********************************************************************************************************/

package com.intellect.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.intellect.property.FormatConstants;
import com.intellect.service.UpdateFieldValues;
import com.intellect.service.UpdateFormatXlsService;

public class FormatGeneration {

	private static org.apache.log4j.Logger log = Logger
			.getLogger(FormatGeneration.class);
	
	//public  static boolean iasflag = false; // 1.4

	public static String elementSeperator;
	public static String segmentSeperator;

	// To generate result string for particular file ( CIBC 1464, CIBC 80 BYTE,
	// CPA 005,NACHA 94 & NACHA IAT 94 BYTE Formats )
	public String formatGeneration(int formatNeededColumn,
			Map nextRowTemplateRecord, int transactionNo,
			Connection connection, Map templateRecordsData, String tableName,
			String formatName, Map<Integer, String> templateFieldsData,
			Map<String, Integer> sectionTagsMap) throws Exception {

		ReadTemplate readTemplate = new ReadTemplate();
		String resultString = "";
		String sectionNames[] = null;
		Map<String, Integer> sectionMap = null;
		try {
			sectionMap = readTemplate.getSectionsMapFromDB(connection,
					tableName, formatName);
			sectionNames = new String[sectionMap.size()];
			for (int section = 0; section < sectionNames.length; section++) {
				for (Map.Entry<String, Integer> entry : sectionMap.entrySet()) {
					String key = entry.getKey();

					int value = entry.getValue();
					if (section == value) {
						sectionNames[section] = key;
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);

		}
		Map[] ifMap = new Map[sectionNames.length];

		for (int sectionName = 0; sectionName < sectionNames.length; sectionName++) {
			try {
				int indexStarted = readTemplate.getOfSectiontagIndex(
						connection, tableName, formatName,
						sectionNames[sectionName], sectionTagsMap);

				ifMap[sectionName] = readTemplate.getIndexOfInputFile(
						readTemplate.getFieldsFromDB(connection, tableName,
								formatName, sectionNames[sectionName],
								sectionTagsMap), templateFieldsData,
						indexStarted);

			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception(e);
			}

			String tempResultStr = formatLineDetails(templateRecordsData,
					ifMap[sectionName]);

			if (!tempResultStr.trim().isEmpty()) {
				if (sectionName == 0) {
					resultString = resultString + tempResultStr;
				} else {
					if (formatName
							.toString()
							.trim()
							.replace(" ", "")
							.equalsIgnoreCase(
									FormatConstants.cibc1464)
							|| formatName
									.toString()
									.trim()
									.replace(" ", "")
									.equalsIgnoreCase(
											FormatConstants.cpa005)) {

						if (nextRowTemplateRecord != null) {
							if (sectionName + 1 == sectionNames.length) {
								String range = "";
								String resStr = "";
								if (transactionNo % 6 == 1
										&& transactionNo == 1) {
									range = "1200";
									resStr = getWithSpaces(range);
									resultString = resultString + resStr;
								}
								if (transactionNo % 6 == 1
										&& transactionNo >= 7) {
									range = "1200";
									resStr = getWithSpaces(range);
									resultString = resultString + resStr;
								}
								if (transactionNo % 6 == 2) {
									range = "960";
									resStr = getWithSpaces(range);
									resultString = resultString + resStr;
								}
								if (transactionNo % 6 == 3) {
									range = "720";
									resStr = getWithSpaces(range);
									resultString = resultString + resStr;
								}
								if (transactionNo % 6 == 4) {
									range = "480";
									resStr = getWithSpaces(range);
									resultString = resultString + resStr;
								}
								if (transactionNo % 6 == 5) {
									range = "240";
									resStr = getWithSpaces(range);
									resultString = resultString + resStr;
								}
								resultString = resultString + "\n"
										+ tempResultStr;

							} else {
								if (transactionNo % 6 == 1
										&& transactionNo != 1) {
									resultString = resultString + "\n"
											+ tempResultStr;
								} else {
									if (sectionName == 1 && transactionNo == 1) {
										resultString = resultString + "\n"
												+ tempResultStr;
									} else {
										String newTmpRes = tempResultStr
												.substring(24);
										resultString = resultString + newTmpRes;
									}
								}
							}
						} else {
							if (sectionName + 1 == sectionNames.length) {
								String range = "";
								String resStr = "";
								if (transactionNo % 6 == 1
										&& transactionNo == 1) {
									range = "1200";
									resStr = getWithSpaces(range);
									resultString = resultString + resStr;
								}
								if (transactionNo % 6 == 1
										&& transactionNo >= 7) {
									range = "1200";
									resStr = getWithSpaces(range);
									resultString = resultString + resStr;
								}
								if (transactionNo % 6 == 2) {
									range = "960";
									resStr = getWithSpaces(range);
									resultString = resultString + resStr;
								}
								if (transactionNo % 6 == 3) {
									range = "720";
									resStr = getWithSpaces(range);
									resultString = resultString + resStr;
								}
								if (transactionNo % 6 == 4) {
									range = "480";
									resStr = getWithSpaces(range);
									resultString = resultString + resStr;
								}
								if (transactionNo % 6 == 5) {
									range = "240";
									resStr = getWithSpaces(range);
									resultString = resultString + resStr;
								}
								resultString = resultString + "\n"
										+ tempResultStr;
							} else {
								if (transactionNo % 6 == 1
										&& transactionNo != 1) {
									resultString = resultString + "\n"
											+ tempResultStr;

								} else {
									if (sectionName == 1 && transactionNo == 1) {
										resultString = resultString + "\n"
												+ tempResultStr;
									} else {
										String newTmpRes = tempResultStr
												.substring(24);
										resultString = resultString + newTmpRes;
									}
								}
							}
						}
					} else {
						resultString = resultString + "\n" + tempResultStr;
					}
				}
			}
		}
		String[] str2 = resultString.split("\n");
		resultString = "";
		for (int i = 0; i < str2.length; i++) {
			if (str2[i].startsWith("C") || str2[i].startsWith("D")) {
				// 1.5
				Integer dif=1464-str2[i].length();
				if(dif<0){
					str2[i]=str2[i].substring(0, str2[i].length()+dif);
				}else{
					str2[i] = str2[i] + getWithSpaces(dif.toString());
				}
			}

			/**
			 * Added by Apurv Pandey on 19.01.2018
			 * 
			 * For keeping the length of each line in Native formats fixed to
			 * the expected length.
			 * 
			 * Added till //// is encountered along with "if blocks"
			 */
			if (str2[i].startsWith("Z") && formatName.endsWith(FormatConstants.cibc1464)) {// //entire
																				// block
				int s = 0;
				s = 1464 - str2[i].length();
				String ss = "" + s;
				str2[i] = str2[i] + getWithSpaces(ss) + "\n";
			}

			if (str2[i].startsWith("9") && formatName.equals(FormatConstants.cibc80Byte)) {// //entire
																				// block
				int s = 0;
				s = 80 - str2[i].length();
				String ss = "" + s;
				str2[i] = str2[i] + getWithSpaces(ss) + "\n";
			}

			if (str2[i].startsWith("Z") && formatName.equals(FormatConstants.cpa005)) {// //entire
																			// block
				int s = 0;
				s = 1464 - str2[i].length();
				String ss = "" + s;
				str2[i] = str2[i] + getWithSpaces(ss) + "\n";
			}

			if (str2[i].startsWith("9") && formatName.equals(FormatConstants.nacha94)) {// //entire
																			// block
				int s = 0;
				s = 94 - str2[i].length();
				String ss = "" + s;
				str2[i] = str2[i] + getWithSpaces(ss) + "\n";
			}

			if (str2[i].startsWith("9")
					&& formatName.equals(FormatConstants.nachaIat94)) {// //entire
																// block
				int s = 0;
				s = 94 - str2[i].length();
				String ss = "" + s;
				str2[i] = str2[i] + getWithSpaces(ss) + "\n";

			}
			if (i < str2.length - 1)

				resultString += str2[i] + "\n";
			else
				resultString += str2[i];

		}
		return resultString;
	}

	// To Generate result string for each Transaction ( MT101, MT104 & CMO103 )
	// FG_1.1 (S)
	public HashMap mt101_104_cmo_103FormatGeneration(Connection connection,
			Map templateRecordsData, String tableName, String formatName,
			Map<Integer, String> templateFieldsData,
			Map<String, Integer> sectionTagsMap, int transactionNo)
			throws Exception {
		
		if (log.isDebugEnabled()) {
			log.debug("Inside mt101_104_cmo_103FormatGeneration method");
		}
		
		/*log.debug("templateFieldsData::"+templateFieldsData);
		log.debug("templateRecordsData::"+templateRecordsData);
		log.debug("sectionTagsMap::"+sectionTagsMap);*/
		int count = 0;
		ReadTemplate readTemplate = new ReadTemplate();
		String resultString = "";
		String sectionNames[] = null;
		Map<String, Integer> sectionMap = null;
		try {
			sectionMap = readTemplate.getSectionsMapFromDB(connection,
					tableName, formatName);
		//	log.debug("sectionMap :::"+sectionMap);
			sectionNames = new String[sectionMap.size()];
			for (Map.Entry<String, Integer> entry : sectionMap.entrySet()) {
				sectionNames[entry.getValue()] = entry.getKey();
			}
			/*for (int section = 0; section < sectionNames.length; section++) {
				for (Map.Entry<String, Integer> entry : sectionMap.entrySet()) {
					String key = entry.getKey();
					int value = entry.getValue();
					if (section == value) {
						sectionNames[section] = key;
						break;
					}
				}
			}*/
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		Map<String, String>[] ifMap = new Map[sectionNames.length];
	//	log.debug("ifMap ::"+ifMap);
		List tagList = new ArrayList();// FG_1.1
		for (int i = 0; i < sectionNames.length; i++) {
			try {
				int indexStarted = readTemplate.getOfSectiontagIndex(
						connection, tableName, formatName,
						sectionNames[i], sectionTagsMap);
				ifMap[i] = readTemplate.getIndexOfInputFileMTCMO(
						readTemplate.getFieldsFromDB(connection, tableName,
								formatName, sectionNames[i],
								sectionTagsMap), templateFieldsData,
						indexStarted, connection, formatName, tableName,
						templateRecordsData);
				
				/*log.debug("ifMap[sectionName] ::"+ifMap[sectionName]);
				log.debug("sectionNames[sectionName] ::"+sectionNames[sectionName]);
				log.debug("sectionTagsMap ::"+sectionTagsMap);*/
				
				
				

			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception(e);
			}
			String newFormat =(String) templateRecordsData.get(2);
			//log.debug("formatName :::"+formatName);
			Set<String> keys = ifMap[i].keySet();
			String temp = (String) templateRecordsData.get(13);
		//	log.debug("temp ::"+temp);
			for (String key : keys) {
				String mtCmoTag = key;
				String mtCmoValue = ifMap[i].get(key);
				if (!mtCmoValue.isEmpty() && !mtCmoValue.equalsIgnoreCase("")
						&& mtCmoValue != null) {
					if (mtCmoTag.equalsIgnoreCase("1")) {
						count = 1;
						resultString = resultString + "{" + mtCmoTag + ":"
								+ mtCmoValue + "}\n";
						tagList.add(mtCmoTag + ":" + mtCmoValue + "~"
								+ transactionNo + "~" + mtCmoTag);// FG_1.1
					} else if (mtCmoTag.equalsIgnoreCase("2")) {
						count = 2;
						resultString = resultString + "{" + mtCmoTag + ":"
								+ mtCmoValue + "}\n";
						tagList.add(mtCmoTag + ":" + mtCmoValue + "~"
								+ transactionNo + "~" + mtCmoTag);// FG_1.1
					} else if (mtCmoTag.equalsIgnoreCase("3")) {
						count = 3;
						resultString = resultString + "{" + mtCmoTag + ":"
								+ mtCmoValue + "}\n" + "{4:";
						tagList.add(mtCmoTag + ":" + mtCmoValue + "~"
								+ transactionNo + "~" + mtCmoTag);// FG_1.1

					} else if (mtCmoTag.equalsIgnoreCase("5")) {
						resultString = resultString + "\n-}\n{" + mtCmoTag
								+ ":" + mtCmoValue + "}";
						tagList.add(mtCmoTag + ":" + mtCmoValue + "~"
								+ transactionNo + "~" + mtCmoTag);// FG_1.1
					} else {

						if (count != 3) {
							count = 3;
							if (transactionNo == 1)
								resultString = resultString + "{4:";
						}
						resultString = resultString + "\n:" + mtCmoTag + ":"
								+ mtCmoValue;
					//	log.debug("resultString :::"+resultString );
						tagList.add(mtCmoTag + ":" + mtCmoValue + "~"
								+ transactionNo + "~" + mtCmoTag);// FG_1.1
					//	log.debug("tagList :::"+tagList );

					}
				}
			}

		}

		// FG_1.1 (S)
		HashMap tmp = new HashMap();
		tmp.put("GOODSTRING", resultString);
		tmp.put("TAGS", tagList);
	//	log.debug("tmp :::"+tmp );
		return tmp;
		// FG_1.1 (E)
	}

	// To generate Line
	private String formatLineDetails(Map tmpRecordMap, Map sectionMapData) {
		String resultStr = "";
		Map<Integer, ArrayList> dbSectionMapData = (Map<Integer, ArrayList>) sectionMapData;
		if (dbSectionMapData.size() == 0 || dbSectionMapData == null
				|| tmpRecordMap.size() == 0 || tmpRecordMap == null) {
		} else {

			for (int dbSection = 0; dbSection < dbSectionMapData.size(); dbSection++) {
				if (dbSection == 0) {
					ArrayList arrayList = dbSectionMapData.get(0);

				}

				if (tmpRecordMap.get(dbSectionMapData.get(dbSection).get(0)) != null
						&& !tmpRecordMap
								.get(dbSectionMapData.get(dbSection).get(0))
								.toString().trim().isEmpty()) {
					int range = Integer.parseInt(dbSectionMapData
							.get(dbSection).get(1).toString());
					if (range != 1200) {
						resultStr = resultStr
								+ getDataValueWithSpaces1(
										tmpRecordMap.get(
												dbSectionMapData.get(dbSection)
														.get(0)).toString(),
										dbSectionMapData.get(dbSection).get(1)
												.toString());
					}
				} else {
					int range = Integer.parseInt(dbSectionMapData
							.get(dbSection).get(1).toString());
					if (range != 1200) {
						resultStr = resultStr
								+ getWithSpaces(dbSectionMapData.get(dbSection)
										.get(1).toString());
					}
				}
			}
		}

		return resultStr;
	}

	// To generate ANSI Formats
	private String formatLineANSIDetails(Connection connection,
			String tableName, String formatName, String elementSeperator,
			Map tmpRecordMap, Map sectionMapData) throws SQLException {

		String resultStr = "";
		int count = 0;
		Map<Integer, ArrayList> dbSectionMapData = (Map<Integer, ArrayList>) sectionMapData;
		if (dbSectionMapData.size() == 0 || dbSectionMapData == null
				|| tmpRecordMap.size() == 0 || tmpRecordMap == null) {
		} else {
			for (int dbSection = 0; dbSection < dbSectionMapData.size(); dbSection++) {

				if (tmpRecordMap.get(dbSectionMapData.get(dbSection).get(0))
						.toString().isEmpty()) {
					count++;
				}

			}
			if (count != dbSectionMapData.size()) {
				for (int dbSection = 0; dbSection < dbSectionMapData.size(); dbSection++) {
					if (!tmpRecordMap
							.get(dbSectionMapData.get(dbSection).get(0))
							.toString().isEmpty()) {
						if (dbSection + 1 == dbSectionMapData.size())
							resultStr = resultStr
									+ tmpRecordMap.get(
											dbSectionMapData.get(dbSection)
													.get(0)).toString();
						else
							resultStr = resultStr
									+ tmpRecordMap.get(
											dbSectionMapData.get(dbSection)
													.get(0)).toString()
									+ elementSeperator;

					} else {
						if (dbSection + 1 != dbSectionMapData.size())

							resultStr = resultStr + elementSeperator;
					}
				}

			}
		}
		return resultStr;
	}

	// Generate Data with Spaces ( Compare the data length and exact length )
	public String getWithSpaces(String range) {

		String space = "";
		int len = Integer.parseInt(range);

		for (int filler = 0; filler < len; filler++) {
			space = space + " ";
		}

		return space;
	}

	// To get the data with space
	public String getDataValueWithSpaces1(String strValue, String range) {

		String spaceWithData = "";
		int len = Integer.parseInt(range);
		if (strValue.length() < len) {
			spaceWithData = strValue;
			for (int curStrLen = strValue.length(); curStrLen < len; curStrLen++) {
				spaceWithData = spaceWithData + " ";
			}
		} else {
			spaceWithData = strValue;
		}
		return spaceWithData;
	}

	// To generate Line for ANSI Formats
	public String formatANSIGeneration(int formatNeededColumn_No,
			Connection connection, Map templateRecordsData, String tableName,
			String formatName, Map<Integer, String> templateFieldsData,
			Map<String, Integer> sectionTagsMap, boolean iasflag) throws Exception {
		
		ReadTemplate readTemplate = new ReadTemplate();
		String resultString = "";
		if (!templateRecordsData.get(formatNeededColumn_No + 15).toString()
				.isEmpty())
			elementSeperator = (String) templateRecordsData
					.get(formatNeededColumn_No + 15);
		if (!templateRecordsData.get(formatNeededColumn_No + 16).toString()
				.isEmpty())
			segmentSeperator = (String) templateRecordsData
					.get(formatNeededColumn_No + 16);

		String sectionNames[] = null;
		Map<String, Integer> sectionMap = null;
		try {
			sectionMap = readTemplate.getSectionsMapFromDB(connection,
					tableName, formatName);
			sectionNames = new String[sectionMap.size()];

			for (int section = 0; section < sectionNames.length; section++) {
				for (Map.Entry<String, Integer> entry : sectionMap.entrySet()) {
					String key = entry.getKey();
					int value = entry.getValue();
					if (section == value) {
						sectionNames[section] = key;
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		Map[] ifMap = new Map[sectionNames.length];

		// iasflag = false;
		for (int sectionName = 0; sectionName < sectionNames.length; sectionName++) {

			try {
				int indexStarted = readTemplate.getOfSectiontagIndex(
						connection, tableName, formatName,
						sectionNames[sectionName], sectionTagsMap);
				ifMap[sectionName] = readTemplate.getIndexOfInputFile(
						readTemplate.getFieldsFromDB(connection, tableName,
								formatName, sectionNames[sectionName],
								sectionTagsMap), templateFieldsData,
						indexStarted);
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception(e);
			}
			String tempResuStr = formatLineANSIDetails(connection, tableName,
					formatName, elementSeperator, templateRecordsData,
					ifMap[sectionName]);
			if (sectionName == 0) {
				if (!tempResuStr.trim().isEmpty())
				
				if (iasflag) {
						
						resultString = "\n" +resultString + tempResuStr
								+ segmentSeperator;
					} else {

						resultString = resultString + tempResuStr
								+ segmentSeperator;
					}
				
				
			} else if (!tempResuStr.trim().isEmpty()) {
				resultString = resultString + "\n" + tempResuStr
						+ segmentSeperator;
			}

			iasflag = true;

			
		}
		
		return resultString;
	}

	// FG_1.1 (S)
	// Generates list of bad strings for SWIFT .
	public ArrayList modifyRecordWiseTags(List recordWiseList,
			List recordWiseTagList, char[] variants) throws Exception {
		char[] ruleNames = variants;
		ArrayList<MessageTracking> returnString = new ArrayList();
		try {
			for (int x = 0; x < ruleNames.length; x++) {

				for (int rec = 0; rec < recordWiseList.size(); rec++) {
					String resultString = (String) recordWiseList.get(rec);
					ArrayList tags = (ArrayList) recordWiseTagList.get(rec);
					for (int j = 0; j < tags.size(); j++) {
						String uuid = "";
						String mtCmoTag = (String) tags.get(j);
						String[] strArray = mtCmoTag.split("~");
						mtCmoTag = strArray[0];
						MessageTracking msT = new MessageTracking();
						String onlyTag = strArray[2];
						msT.setTagApplied(onlyTag);
						msT.setTransactionNumber("" + (rec + 1) + "");
						int transactionNo = Integer.parseInt(strArray[1]);
						String nextTag = "";
						if (tags.size() == 1) {
							nextTag = (String) tags.get(0);
							String[] strNextTagArray = nextTag.split("~");
							nextTag = strNextTagArray[0];
						} else if (j < tags.size() - 1) {
							nextTag = (String) tags.get(j + 1);
							String[] strNextTagArray = nextTag.split("~");
							nextTag = strNextTagArray[0];
						} else {
							nextTag = (String) tags.get(j - 1);
							String[] strNextTagArray = nextTag.split("~");
							nextTag = strNextTagArray[0];
						}
						String pattern = ".*"
								+ mtCmoTag.replace("{", "\\{").replace("}",
										"\\}") + ".*";
						Pattern r = Pattern.compile(pattern);
						Matcher m = r.matcher(resultString);
						String temp = resultString;
						uuid = getUniqueUiid(mtCmoTag);
						if (mtCmoTag.contains("\n")) {
							mtCmoTag = mtCmoTag.replace("\n", uuid);
							temp = temp.replace("\n", uuid);
						}

						switch (ruleNames[x]) {
						case 'D':
							msT.setRuleType("D");
							if (mtCmoTag.contains(uuid)) {
								String tempGroup = null;
								if (temp.contains(":" + mtCmoTag))
									tempGroup = temp.replace(":" + mtCmoTag,
											":" + mtCmoTag + "\n" + ":"
													+ mtCmoTag);
								else
									tempGroup = temp.replace(mtCmoTag, mtCmoTag
											+ "\n" + mtCmoTag);
								temp = temp.replace(temp, tempGroup);
								temp = temp.replace(uuid, "\n");

							} else if (m.find()) {
								temp = temp.replace(m.group(0), m.group(0)
										+ "\n" + m.group(0));
							}
							break;
						case 'M':
							msT.setRuleType("M");

							// 1.3 Tag missing changes
							String temp1 = temp + "\n";
							if (mtCmoTag.contains(uuid)) {
								String tempGroup = null;

								// 1.3 Tag missing changes
								String mtCmoTg1 = ":" + mtCmoTag + uuid;
								String mtCmoTg2 = mtCmoTag + uuid;

								if (temp.contains(":" + mtCmoTag))
									// 1.3 Tag missing changes
									tempGroup = temp.replace(mtCmoTg1, "");

								else
									// 1.3 Tag missing changes
									tempGroup = temp.replace(mtCmoTg2, "");
								temp = temp.replace(temp, tempGroup);
								temp = temp.replace(uuid, "\n");
							} else if (m.find()) {
								// 1.3 Tag missing changes
								temp = temp1.replace(m.group(0) + "\n", "");

							}

							break;
						case 'I':
							msT.setRuleType("I");
							if (mtCmoTag.contains(uuid)) {
								String tempGroup = temp.replace(onlyTag + ":",
										onlyTag + "001:");
								temp = temp.replace(temp, tempGroup);
								temp = temp.replace(uuid, "\n");
							} else if (m.find()) {
								String group1 = m.group(0);
								group1 = group1.replace(onlyTag + ":", onlyTag
										+ "001:");
								temp = temp.replace(m.group(0), group1);
							}
							break;
						case 'O':
							msT.setRuleType("O");
							String nextPattern = ".*"
									+ nextTag.replace("{", "\\{").replace("}",
											"\\}") + ".*";
							Pattern r1 = Pattern.compile(nextPattern);
							Matcher m1 = r1.matcher(resultString);

							if (mtCmoTag.contains(uuid)
									|| nextTag.contains("\n")) {
								if (nextTag.contains("\n")) {
									nextTag = nextTag.replace("\n", uuid);
								}

								String str = mtCmoTag;
								String str1 = nextTag;
								String uuid1 = getUniqueUiid(mtCmoTag);
								String str2 = str1 + uuid1;

								temp = temp.replace("\n", uuid);
								temp = temp.replace(str1, str2);
								temp = temp.replace(str, str1);
								temp = temp.replace(str2, str);
								temp = temp.replace(uuid, "\n");

							} else if (m.find() && m1.find()) {

								String str = m.group(0);
								String str1 = m1.group(0);
								String uuid1 = getUniqueUiid(mtCmoTag);
								String str2 = str1 + uuid1;

								temp = temp.replace(str1, str2);
								temp = temp.replace(str, str1);
								temp = temp.replace(str2, str);
							}
							break;

						default:

							break;

						}
						String strToAdd = "";
						for (int recordNo = 0; recordNo < recordWiseList.size(); recordNo++) {
							if (recordNo == transactionNo - 1) {
								strToAdd = strToAdd + temp;
							} else {
								String strToAddOther = (String) recordWiseList
										.get(recordNo);
								strToAdd = strToAdd + strToAddOther;
							}
						}

						msT.setResultString(strToAdd);
						returnString.add(msT);
					}

				}
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
		return returnString;

	}

	private String getUniqueUiid(String mtCmoTag) {
		// TODO Auto-generated method stub
		String uuid = UUID.randomUUID().toString();
		if (mtCmoTag.contains(uuid)) {
			uuid = getUniqueUiid(mtCmoTag);
		}
		return uuid;
	}

}
// FG_1.1 (E)

