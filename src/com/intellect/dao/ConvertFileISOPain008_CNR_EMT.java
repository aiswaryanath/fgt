/********************************************************************************************************/
/* Copyright �  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/* 								 																		*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : ConvertFileISOPain008.java                                          					*/
/*                                                                                      				*/
/********************************************************************************************************/
/*                Author			        |        	  Date   			|	  Version           */
/********************************************************************************************************/
/* 	         Abhishek Tiwari	        	|			02-06-2017			|		1.0	            */
/*			 Sapna Jain						|			18-07-2017			|		1.1
 /* 		 Preetam Sanjore				|			20:07:2017			|		1.2	
 /* 		 Preetam Sanjore				|			20:07:2017			|		1.3	
 /* 	     Shruti Gupta					|			29:08:2017			|		1.3
 /* 	     Gokaran Tiwari					|			22:03:2018			|		1.4 latest suite change
 /* 																				in main table , old move to audit
/********************************************************************************************************/
/* 											  														    */
/* Description   : For generation of PAIN 008 format 													*/
/*        																								*/
/********************************************************************************************************/
package com.intellect.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EmptyStackException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.log4j.Logger;

import com.intellect.pojo.TestToolFileNamePOJO;
import com.intellect.property.FormatConstants;
import com.intellect.service.FormatGenerateService;
import com.intellect.service.UpdateFormatXlsService;

public class ConvertFileISOPain008_CNR_EMT {

	private static org.apache.log4j.Logger log = Logger
			.getLogger(ConvertFileISOPain008_CNR_EMT.class);

	int m = 7, p = 7;
	int batchrowCount = 7;
	LinkedHashMap<Integer, Vector<String>> map = null;
	LinkedHashMap<Integer, Vector<String>> map2 = null;
	ReadTemplate readTemplate = new ReadTemplate();
	int filesCrtdCnt = 0;

	// BAD_FILES_CHANGES
	private String invalidAppender = "001";

	public int main(String source, File target, String fileTypeSelected,
			LinkedHashMap<Integer, Vector<String>> map2, Connection connection,
			String db_table_file, String timeStamp, String dbTimeStamp,
			String moduleSelected, char[] variants, 
			List<TestToolFileNamePOJO> testToolData) throws Exception {

		ConvertFileISOPain008_CNR_EMT xmlWriter = new ConvertFileISOPain008_CNR_EMT();
		filesCrtdCnt = xmlWriter.rootXMLFile(source, target, fileTypeSelected,
				map2, connection, db_table_file, timeStamp, dbTimeStamp,
				moduleSelected, variants,testToolData);
		return filesCrtdCnt;
	}

	public int rootXMLFile(String source, File target, String fileTypeSelected,
			LinkedHashMap<Integer, Vector<String>> map2, Connection connection,
			String db_table_file, String timeStamp, String dbTimeStamp,
			String moduleSelected, char[] variants,List<TestToolFileNamePOJO> pojoList) throws Exception {

		IwReadExcel objIwReadExcel = new IwReadExcel();
		map = (LinkedHashMap<Integer, Vector<String>>) objIwReadExcel.ReadData(
				source, fileTypeSelected, moduleSelected);
		// Change 1.4
		int rowCount = 7, colCount = 17;
		ArrayList<Integer> stockList = new ArrayList<Integer>();
		int count = 0, k = 0, fileCount = 0;
		for (int j = 0; j < map.size(); j++) {
			if (!map.get(rowCount).get(colCount).toString().isEmpty()) {
				stockList.add(count + 1);
				fileCount++;
				count = 0;
				k++;
			} else
				count++;
			rowCount++;
		}
		stockList.add(count + 1);
		int arr[] = new int[stockList.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = stockList.get(i).intValue();
		}
		ArrayList allRecList = new ArrayList();
		for (int i = 0, j2 = 1; i < fileCount; i++, j2++) {
			ArrayList rowCol = writeXML(arr[j2], target, connection,
					db_table_file, timeStamp, dbTimeStamp, null, -1, -1,pojoList);// Change 1.4
			allRecList.add(rowCol);
		}
		log.debug("ALL good files generated");

		// BAD_FILES_CHANGES STARTS

		String[] ruleNames = null;
		boolean oFind = false;
		for (int i = 0; i < variants.length; i++) {
			if (variants[i] == 'O') {
				oFind = true;
			}
		}
		if (oFind) {
			ruleNames = new String[variants.length - 1];
		} else {
			ruleNames = new String[variants.length];
		}

		for (int i = 0; i < variants.length; i++) {
			if (variants[i] == 'O')
				continue;
			else
				ruleNames[i] = variants[i] + "";
		}

		int lastRowNo = 0;
		int startNoForEachMessage = 0;
		// iterating over no of messages
		for (int allRec = 0; allRec < allRecList.size(); allRec++) {
			if (lastRowNo == 0) {
				startNoForEachMessage = 7 + lastRowNo;
			} else {
				startNoForEachMessage = 1 + lastRowNo;

			}
			p = startNoForEachMessage;
			batchrowCount = startNoForEachMessage;
			m = startNoForEachMessage;
			ArrayList rowCol = (ArrayList) allRecList.get(allRec);
			for (int rowColNo = 0; rowColNo < rowCol.size(); rowColNo++) {
				String s = (String) rowCol.get(rowColNo);

				String[] strArray = s.split("~");
				lastRowNo = Integer.valueOf(strArray[0]);
				int ruleColNo = Integer.valueOf(strArray[1]);

				for (int x = 0; x < ruleNames.length; x++) {

					p = startNoForEachMessage;
					batchrowCount = startNoForEachMessage;
					m = startNoForEachMessage;

					// for (int i = 0, j2 = 1; i < fileCount; i++, j2++) {
					writeXML(arr[allRec + 1], target, connection,
							db_table_file, timeStamp, dbTimeStamp,
							ruleNames[x], lastRowNo, ruleColNo, pojoList);// Change 1.4
					// }

				}
			}
		}
		
		return fileCount;
	}

	public ArrayList writeXML(int batchCount, File target,
			Connection connection, String db_table_file, String timeStamp,
			String dbTimeStamp, String ruleName, int ruleRowNo, int ruleColNo,List<TestToolFileNamePOJO> pojoList)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("writeXML method starts in ConvertFileISOPain008_CNR_EMT class: batchCount: "
					+ batchCount
					+ "ruleName: "
					+ ruleName
					+ " ruleRowNo: "
					+ ruleRowNo + " ruleColNo: " + ruleColNo + " p: " + p);
		}
		XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMdd");
		String XMLPath = target + "/";
		XMLPath = XMLPath.replaceAll(".xml", m + ".xml");
		String fileName = null, dbfileName = null;
		// BAD_FILES_CHANGES STARTS
		ArrayList<String> rowCol = null;
		// BAD_FILES_CHANGES ENDS

		String pain8remit_applied_rule_pos = FormatConstants.pain8remitAppliedRulePosition;
		int pain8remit_applied_rule_pos_no = Integer
				.parseInt(pain8remit_applied_rule_pos);
		log.debug("pain8remit_applied_rule_pos_no:::"
				+ pain8remit_applied_rule_pos_no);
		log.debug("map.get(p)::" + map.get(p));
		// log.debug("map.get(p).get(2)::"+map.get(p).get(2));
		log.debug("map.get(p).get(2).toString().length()::"
				+ map.get(p).get(2).toString().length());
		// log.debug("temp::"+temp);
		if (map.get(p) != null && map.get(p).get(2) != null
				&& map.get(p).get(2).toString().length() > 0) {
			String temp = FormatGenerateService.getSequenceNumber(connection,
					map.get(p).get(2).toString());
			log.debug("temp::" + temp);
			if (map.get(p).get(3) != null
					&& map.get(p).get(3).toString().replace(" ", "")
							.equalsIgnoreCase("FTS")) {
				File filefolder = new File(XMLPath + "/FTS");
				if (!filefolder.exists())
					filefolder.mkdirs();
				XMLPath = filefolder.toString();
				fileName = XMLPath
						+ "/"
						+ map.get(p).get(7).toString()
						+ "_"
						+ map.get(p).get(10).toString()
						+ "_"
						+ formatter.format(date)
						+ "_111213"
						+ "_"
						+ map.get(p)
								.get(8)
								.toString()
								.replace(".0", "")
								.substring(
										0,
										map.get(p).get(8).toString().length() - 6)
						+ temp + "." + map.get(p).get(2).toString() + "."
						+ map.get(p).get(11).toString() + ".xml";
				System.out.println("remit "+fileName);
				log.debug("map::" + map);
				log.debug("fileName:" + fileName);
				// BAD_FILES_CHANGES ENDS
				if (map.get(p) != null && map.get(p).get(12) != null
						&& !map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(7).toString()
							+ "_"
							+ map.get(p).get(10).toString()
							+ "_"
							+ formatter.format(date)
							+ "_111213"
							+ "_"
							+ map.get(p)
									.get(8)
									.toString()
									.replace(".0", "")
									.substring(
											0,
											map.get(p).get(8).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();
					log.debug("map::" + map);
				} else {
					dbfileName = map.get(p).get(7).toString()
							+ "_"
							+ map.get(p).get(10).toString()
							+ "_"
							+ formatter.format(date)
							+ "_111213"
							+ "_"
							+ map.get(p)
									.get(8)
									.toString()
									.replace(".0", "")
									.substring(
											0,
											map.get(p).get(8).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + ".xml";
					log.debug("map::" + map);
				}

			} else if (map.get(p) != null
					&& map.get(p).get(3).toString().replace(" ", "")
							.equalsIgnoreCase("SCA")) {

				File filefolder = new File(XMLPath + "/SCA");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();

				fileName = XMLPath
						+ "/"
						+ map.get(p).get(6).toString()
						+ "."
						+ formatter1.format(date)
						+ "111213"
						+ "."
						+ map.get(p)
								.get(10)
								.toString()
								.substring(
										0,
										map.get(p).get(10).toString().length() - 6)
						+ temp + "."
						+ map.get(p).get(2).toString().replace(".0", "") + "."
						+ map.get(p).get(11).toString() + ".xml";
				log.debug("map::" + map);
				log.debug("fileName:" + fileName);
				// BAD_FILES_CHANGES ENDS
				if (map.get(p) != null
						&& !map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();
					log.debug("map::" + map);
				} else {
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";
					log.debug("map::" + map);
				}
			}
			// BAD_FILES_CHANGES
			StringBuffer strModifiedField = new StringBuffer();
			if (ruleName != null && !"".equals(ruleName)) {
				strModifiedField.append(ruleRowNo).append("~")
						.append(ruleColNo);
			}
			String errorCode = null;
			String ruleType = null;
			if ("M".equals(ruleName)) {
				ruleType = "TAG_MISSING";
				errorCode = "TAG_MISSING_ERRORCODE";
			} else if ("D".equals(ruleName)) {
				ruleType = "DUPLICATE_TAG";
				errorCode = "DUPLICATE_TAG_ERRORCODE";
			} else if ("I".equals(ruleName)) {
				ruleType = "INVALID_TAG";
				errorCode = "INVALID_TAG_ERRORCODE";
			} else if ("O".equals(ruleName)) {
				ruleType = "TAG_ORDER_CHANGE";
				errorCode = "TAG_ORDER_CHANGE_ERRORCODE";
			}
			// Change 1.4 starts
			if (ruleName != null && !"".equals(ruleName)) {
				TestToolFileNamePOJO pojoObj = new TestToolFileNamePOJO();
				pojoObj.setTestCaseid(map.get(p).get(0).toString());
				pojoObj.setFormatName(map.get(p).get(2).toString());
				pojoObj.setChannel(map.get(p).get(3).toString());
				pojoObj.setLsi(map.get(p).get(4).toString());
				pojoObj.setClientRefNum(map.get(p).get(17).toString());
				pojoObj.setBicCode(map.get(p).get(6).toString());
				pojoObj.setBbdId(map.get(p).get(7).toString());
				pojoObj.setCustRef(map.get(p).get(8).toString());
				pojoObj.setUniqueId(map.get(p).get(10).toString());
				pojoObj.setFileName(dbfileName);
				pojoObj.setAppliedRules(ruleType);
				pojoObj.setAppliedRules(errorCode);
				pojoObj.setModifiedField(strModifiedField.toString());
				pojoObj.setTimeStamp(dbTimeStamp);
				pojoList.add(pojoObj);
			} else {
				TestToolFileNamePOJO pojoObj = new TestToolFileNamePOJO();
				pojoObj.setTestCaseid(map.get(p).get(0).toString());
				pojoObj.setFormatName(map.get(p).get(2).toString());
				pojoObj.setChannel(map.get(p).get(3).toString());
				pojoObj.setLsi(map.get(p).get(4).toString());
				pojoObj.setClientRefNum(map.get(p).get(17).toString());
				pojoObj.setBicCode(map.get(p).get(6).toString());
				pojoObj.setBbdId(map.get(p).get(7).toString());
				pojoObj.setCustRef(map.get(p).get(8).toString());
				pojoObj.setUniqueId(map.get(p).get(10).toString());
				pojoObj.setFileName(dbfileName);
				pojoObj.setAppliedRules(map.get(p).get(pain8remit_applied_rule_pos_no).toString());
				pojoObj.setAppliedRules(map.get(p).get(pain8remit_applied_rule_pos_no + 1).toString());
				pojoObj.setModifiedField(map.get(p).get(pain8remit_applied_rule_pos_no + 3).toString());
				pojoObj.setTimeStamp(dbTimeStamp);
				pojoList.add(pojoObj);
			}
			// Change 1.4 ends
		} else {

			if (log.isDebugEnabled()) {
				log.debug("FormatName is empty");
			}
		}
		m++;
		String rootElement = "CstmrDrctDbtInitn";
		try {
			XMLEventWriter xmlEventWriter = xmlOutputFactory
					.createXMLEventWriter(new FileOutputStream(fileName),
							"UTF-8");
			XMLEventFactory eventFactory = XMLEventFactory.newInstance();
			XMLEvent end = eventFactory.createDTD("\r\n");
			XMLEvent tab = eventFactory.createDTD("\t");

			ArrayList<Namespace> ns = new ArrayList<Namespace>();

			ArrayList<Attribute> atts = new ArrayList<Attribute>();
			// atts.add(eventFactory.createAttribute("Document",""));
			// 1.3 start
			atts.add(eventFactory
					.createAttribute(
							"xsi:schemaLocation",
							"urn:iso:std:iso:20022:tech:xsd:pain.008.001.02 file:///C:/Users/IBM_ADMIN/Box%20Sync/My%20Files/PSH/pain.008.001.02.xsd"));
			atts.add(eventFactory.createAttribute("xmlns",
					"urn:iso:std:iso:20022:tech:xsd:pain.008.001.02"));
			atts.add(eventFactory.createAttribute("xmlns:xsi",
					"http://www.w3.org/2001/XMLSchema-instance"));

			StartElement tempDocument = eventFactory.createStartElement("", "",
					"Document", atts.iterator(), ns.iterator());

			// 1.1 ends
			xmlEventWriter.add(tempDocument);
			// XMLEvent xmlDocs2 = eventFactory.createAttribute("xmlns",
			// "urn:iso:std:iso:20022:tech:xsd:pain.001.001.03");
			// xmlEventWriter.add(xmlDocs2);
			// xmlDocs2 = eventFactory.createNamespace("xsi",
			// "http://www.w3.org/2001/XMLSchema-instance");
			// xmlEventWriter.add(xmlDocs2);
			// 1.3 end
			xmlEventWriter.add(end);
			StartElement configStartElement = eventFactory.createStartElement(
					"", "", rootElement);
			xmlEventWriter.add(tab);
			xmlEventWriter.add(configStartElement);
			xmlEventWriter.add(end);
			EndElement EndDocument = null;
			Map<String, String> childNodes = null;
			Set<String> elementNodes = null;

			tempDocument = eventFactory.createStartElement("", "", "GrpHdr");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			Map<String, String> elementsMap = new LinkedHashMap<String, String>();
			String[] Msg_id = { "", "" };// messageid changes
			// BAD_FILES_CHANGES STARTS
			if (ruleName == null) {
				rowCol = new ArrayList();
			}
			// BAD_FILES_CHANGES ENDS

			// start GrpHdr
			if (!map.get(p).get(17).toString().isEmpty()) {
				elementsMap.put("MsgId", (String) map.get(p).get(17));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(17));
				else {
					Msg_id = UpdateFormatXlsService.getData(connection,
							FormatConstants.pain008);// messageid changes
					elementsMap.put("MsgId", Msg_id[0]);
					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "MsgId", 17, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS

			}
			// CreDtTm
			if (!map.get(p).get(19).toString().isEmpty()) {
				elementsMap.put("CreDtTm", (String) map.get(p).get(19));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(19));
				else {

					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "CreDtTm", 19, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS

			}
			elementNodes = elementsMap.keySet();
			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, elementsMap.get(key), 3);
			}
			elementsMap = new LinkedHashMap<String, String>();

			// Authstn
			if (!map.get(p).get(19).toString().isEmpty()) {
				tab = eventFactory.createDTD("\t");
				tempDocument = eventFactory.createStartElement("", "",
						"Authstn");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if ((!map.get(p).get(18).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Prtry", map.get(p).get(18).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(18));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Prtry", 18,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}
				EndDocument = eventFactory.createEndElement("", "", "Authstn");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

			}

			// NbOfTxs
			if (!map.get(p).get(20).toString().isEmpty()) {
				elementsMap.put("NbOfTxs", (String) map.get(p).get(20));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(20));
				else {

					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "NbOfTxs", 20, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}

			// CtrlSum
			if (!map.get(p).get(21).toString().isEmpty()) {
				elementsMap.put("CtrlSum", (String) map.get(p).get(21));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(21));
				else {

					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "CtrlSum", 21, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}

			elementNodes = elementsMap.keySet();
			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, elementsMap.get(key), 3);
			}
			if (!map.get(p).get(22).toString().isEmpty()
					|| !map.get(p).get(22).toString().isEmpty()) {
				tempDocument = eventFactory.createStartElement("", "",
						"InitgPty");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if (!map.get(p).get(22).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					// XMLEvent tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", (String) map.get(p).get(22));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(22));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 22, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 4);
					}
				}

				if (!map.get(p).get(23).toString().isEmpty()) {
					tempDocument = eventFactory
							.createStartElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					tempDocument = eventFactory.createStartElement("", "",
							"OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					childNodes.put("Id", (String) map.get(p).get(23));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(23));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 23, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}

					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory
							.createEndElement("", "", "OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory.createEndElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "InitgPty");
				tabMarked(xmlEventWriter, 3);

				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			EndDocument = eventFactory.createEndElement("", "", "GrpHdr");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

			// <GrpHdr> end
			int colCount = 24;
			ArrayList<Integer> stockList = new ArrayList<Integer>();
			int count = 0, k = 0, fileCount = 0;

			for (int j = 0; j < batchCount; j++) {
				if (!map.get(batchrowCount).get(colCount).toString().isEmpty()) {
					// arr[k]=count+1;
					stockList.add(count + 1);
					fileCount++;
					count = 0;
					k++;
				} else
					count++;
				batchrowCount++;
			}
			stockList.add(count + 1);
			int arr[] = new int[stockList.size()];
			for (int i2 = 0; i2 < arr.length; i2++) {
				arr[i2] = stockList.get(i2).intValue();
			}

			for (int i2 = 0, k2 = 1; i2 < fileCount; i2++, k2++)
				paymentInfo(tempDocument, EndDocument, xmlEventWriter,
						eventFactory, end, childNodes, elementNodes, tab, p,
						arr[k2], rowCol, ruleName, ruleRowNo, ruleColNo);

			tabMarked(xmlEventWriter, 1);
			xmlEventWriter.add(eventFactory.createEndElement("", "",
					rootElement));
			xmlEventWriter.add(end);
			xmlEventWriter.add(eventFactory.createEndDocument());
			eventFactory.createEndDocument();
//			EndDocument = eventFactory.createEndElement("", "", "Document");
//			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
			String rp = xmlEventWriter.toString();
			rp = rp.replaceAll(
					"</Document xmlns=urn:iso:std:iso:20022:tech:xsd:pain.001.001.03 xmlns:xsi=http://www.w3.org/2001/XMLSchema-instance>",
					"</Document>");

			xmlEventWriter.close();

		} catch (EmptyStackException exx) {
			log.fatal("Empty stack Trace Exception: ", exx);
			exx.printStackTrace();
			throw new EmptyStackException();

		} catch (FileNotFoundException e) {

			e.printStackTrace();
			throw new FileNotFoundException();

		} catch (XMLStreamException ex) {
			ex.printStackTrace();
			throw new XMLStreamException();
		}
		return rowCol;

	}

	// BAD_FILES_CHANGES
	private Map applyRule(int ruleRowNo, int ruleColNo, int p2,
			String ruleName, Map<String, String> elementsMap, String key,
			int actualColNo, XMLEventWriter xmlEventWriter)
			throws XMLStreamException {
		if (log.isDebugEnabled())
			log.debug("in applyRule ruleRowNo: " + ruleRowNo + " ruleColNo: "
					+ ruleColNo + "p: " + p2 + " ruleName: " + ruleName
					+ " elementsMap: " + elementsMap + " key: " + key
					+ " actualColNo: " + actualColNo);
		if (p2 == ruleRowNo && ruleColNo == actualColNo) {
			if ("D".equals(ruleName)) {
				// elementsMap.put(key, (String) map.get(p).get(actualColNo));
				createNode(xmlEventWriter, key, elementsMap.get(key), 3);

			} else if ("M".equals(ruleName)) {
				elementsMap.remove(key);

			} else if ("I".equals(ruleName)) {
				elementsMap.put(key + invalidAppender,
						(String) map.get(p).get(actualColNo));
				elementsMap.remove(key);

			}
		}
		if (log.isDebugEnabled())
			log.debug("Leaving applyRule: elementsMap " + elementsMap);
		return elementsMap;

	}

	public void paymentInfo(StartElement tempDocument, EndElement EndDocument,
			XMLEventWriter xmlEventWriter, XMLEventFactory eventFactory,
			XMLEvent end, Map<String, String> childNodes,
			Set<String> elementNodes, XMLEvent tab, int tempP, int batchCount,
			ArrayList rowCol, String ruleName, int ruleRowNo, int ruleColNo)
			throws XMLStreamException {

		if (log.isDebugEnabled()) {
			log.debug("paymentInfo method starts in ConvertFileISOPain008 class");
		}
		// start <PmtInf>
		tempDocument = eventFactory.createStartElement("", "", "PmtInf");
		try {
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			childNodes = new LinkedHashMap<String, String>();
			tab = eventFactory.createDTD("\t");
			if (!map.get(p).get(24).toString().isEmpty()) {
				childNodes.put("PmtInfId", map.get(p).get(24).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(24));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "PmtInfId", 24, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			if (!map.get(p).get(25).toString().isEmpty()) {
				childNodes.put("PmtMtd", map.get(p).get(25).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(25));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "PmtMtd", 25, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			if (!map.get(p).get(26).toString().isEmpty()) {
				childNodes.put("NbOfTxs", (String) map.get(p).get(26));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(26));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "NbOfTxs", 26, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS

			}
			if (!map.get(p).get(27).toString().isEmpty()) {
				childNodes.put("CtrlSum", (String) map.get(p).get(27));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(27));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "CtrlSum", 27, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS

			}
			elementNodes = childNodes.keySet();
			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, childNodes.get(key), 3);
			}

			// newly added
			if (!map.get(p).get(28).toString().isEmpty()
					|| !map.get(p).get(29).toString().isEmpty()
					|| !map.get(p).get(30).toString().isEmpty()
					|| !map.get(p).get(31).toString().isEmpty()) {
				tempDocument = eventFactory.createStartElement("", "",
						"PmtTpInf");
				tabMarked(xmlEventWriter, 6);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				tempDocument = eventFactory.createStartElement("", "",
						"LclInstrm");
				tabMarked(xmlEventWriter, 6);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();

				// XMLEvent tab = eventFactory.createDTD("\t");
				if (!map.get(p).get(28).toString().isEmpty()) {
					childNodes.put("Cd", (String) map.get(p).get(28));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(28));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Cd", 28, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				} else {
					childNodes.put("Prtry", (String) map.get(p).get(29));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(29));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Prtry", 29,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 7);
				}
				EndDocument = eventFactory
						.createEndElement("", "", "LclInstrm");
				tabMarked(xmlEventWriter, 6);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				tempDocument = eventFactory.createStartElement("", "",
						"CtgyPurp");
				tabMarked(xmlEventWriter, 6);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();

				// XMLEvent tab = eventFactory.createDTD("\t");
				if (!map.get(p).get(30).toString().isEmpty()) {
					childNodes.put("Cd", (String) map.get(p).get(30));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(30));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Cd", 30, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				} else {
					childNodes.put("Prtry", (String) map.get(p).get(31));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(31));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Prtry", 31,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 7);
				}
				EndDocument = eventFactory.createEndElement("", "", "CtgyPurp");
				tabMarked(xmlEventWriter, 6);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				EndDocument = eventFactory.createEndElement("", "", "PmtTpInf");
				tabMarked(xmlEventWriter, 6);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// end newly added
			// Requested Execution Date
			if (!map.get(p).get(32).toString().isEmpty()) {
				childNodes = new LinkedHashMap<String, String>();
				childNodes.put("ReqdColltnDt", map.get(p).get(32).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(32));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "ReqdColltnDt", 32, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			elementNodes = childNodes.keySet();

			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, childNodes.get(key), 3);
			}

			// Cdtr
			if ((!map.get(p).get(33).toString().isEmpty())
					|| (!map.get(p).get(34).toString().isEmpty())
					|| (!map.get(p).get(35).toString().isEmpty())
					|| (!map.get(p).get(36).toString().isEmpty())
					|| (!map.get(p).get(37).toString().isEmpty())
					|| (!map.get(p).get(38).toString().isEmpty())
					|| (!map.get(p).get(39).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "", "Cdtr");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				// if (!map.get(p).get(29).toString().isEmpty())
				// {//<PmtInf><Cdtr><Nm></Nm><PmtInf> is mandatory changes
				if (!map.get(p).get(33).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(33).toString());// <PmtInf><Cdtr><Nm></Nm><PmtInf>
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(33));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 33, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 4);
					}
				}
				// }
				if ((!map.get(p).get(34).toString().isEmpty())
						|| (!map.get(p).get(35).toString().isEmpty())
						|| (!map.get(p).get(36).toString().isEmpty())
						|| (!map.get(p).get(37).toString().isEmpty())
						|| (!map.get(p).get(38).toString().isEmpty())
						|| (!map.get(p).get(39).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();
					tab = eventFactory.createDTD("\t");
					if ((!map.get(p).get(34).toString().isEmpty())) {
						childNodes.put("StrtNm", map.get(p).get(34).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(34));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "StrtNm", 34,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					/*
					 * if ((!map.get(p).get(24).toString().isEmpty()))
					 * childNodes.put("BldgNb", map.get(p).get(24).toString());
					 */
					if ((!map.get(p).get(36).toString().isEmpty())) {
						childNodes.put("PstCd", map.get(p).get(36).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(36));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "PstCd", 36,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if ((!map.get(p).get(37).toString().isEmpty())) {
						childNodes.put("TwnNm", map.get(p).get(37).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(37));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "TwnNm", 37,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if ((!map.get(p).get(38).toString().isEmpty())) {
						childNodes.put("CtrySubDvsn", map.get(p).get(38)
								.toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(38));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "CtrySubDvsn", 38,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if ((!map.get(p).get(39).toString().isEmpty())) {
						childNodes.put("Ctry", map.get(p).get(39).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(39));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Ctry", 39,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				if (!map.get(p).get(40).toString().isEmpty()) {
					tempDocument = eventFactory
							.createStartElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					tempDocument = eventFactory.createStartElement("", "",
							"OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					// XMLEvent tab = eventFactory.createDTD("\t");
					childNodes.put("Id", (String) map.get(p).get(40));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(40));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 40, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}

					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
					EndDocument = eventFactory
							.createEndElement("", "", "OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory.createEndElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "Cdtr");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// CdtrAcct
			if (!map.get(p).get(41).toString().isEmpty()) {
				tempDocument = eventFactory.createStartElement("", "",
						"CdtrAcct");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				tempDocument = eventFactory.createStartElement("", "", "Id");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				tempDocument = eventFactory.createStartElement("", "", "Othr");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Id", map.get(p).get(41).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(41));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Id", 41, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 6);
				}
				EndDocument = eventFactory.createEndElement("", "", "Othr");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				EndDocument = eventFactory.createEndElement("", "", "Id");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				EndDocument = eventFactory.createEndElement("", "", "CdtrAcct");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// CdtrAgt
			// issue fix for field level modification on bad files
			if ((!map.get(p).get(42).toString().isEmpty())
					|| (!map.get(p).get(43).toString().isEmpty())
					|| (!map.get(p).get(44).toString().isEmpty())
					|| (!map.get(p).get(45).toString().isEmpty())
					|| (!map.get(p).get(46).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"CdtrAgt");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				// new data
				if ((!map.get(p).get(42).toString().isEmpty())
						|| (!map.get(p).get(43).toString().isEmpty())
						|| (!map.get(p).get(44).toString().isEmpty())
						|| (!map.get(p).get(45).toString().isEmpty())
						|| (!map.get(p).get(46).toString().isEmpty())) {

					tempDocument = eventFactory.createStartElement("", "",
							"FinInstnId");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					// IF part
					if ((!map.get(p).get(42).toString().isEmpty())
							|| (!map.get(p).get(43).toString().isEmpty())) {
						tempDocument = eventFactory.createStartElement("", "",
								"ClrSysMmbId");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						tempDocument = eventFactory.createStartElement("", "",
								"ClrSysId");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);

						if (!map.get(p).get(42).toString().isEmpty()) {
							childNodes = new LinkedHashMap<String, String>();

							tab = eventFactory.createDTD("\t");

							childNodes.put("Cd", map.get(p).get(42).toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(42));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "Cd", 42,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
							elementNodes = childNodes.keySet();
							for (String key : elementNodes) {
								createNode(xmlEventWriter, key,
										childNodes.get(key), 5);
							}
						}

						EndDocument = eventFactory.createEndElement("", "",
								"ClrSysId");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
						if (!map.get(p).get(43).toString().isEmpty()) {
							childNodes = new LinkedHashMap<String, String>();

							tab = eventFactory.createDTD("\t");
							childNodes.put("MmbId", map.get(p).get(43)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(43));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "MmbId", 43,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
							elementNodes = childNodes.keySet();
							for (String key : elementNodes) {
								createNode(xmlEventWriter, key,
										childNodes.get(key), 5);
							}
						}
						EndDocument = eventFactory.createEndElement("", "",
								"ClrSysMmbId");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);

					}
					tab = eventFactory.createDTD("\t");

					if (!map.get(p).get(44).toString().isEmpty()) {
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Nm", map.get(p).get(44).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(44));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Nm", 44,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 5);
						}
					}
					if (!map.get(p).get(45).toString().isEmpty()) {
						tempDocument = eventFactory.createStartElement("", "",
								"PstlAdr");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						if ((!map.get(p).get(45).toString().isEmpty())) {
							childNodes.put("Ctry", map.get(p).get(45)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(45));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "Ctry", 45,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}

						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}

						EndDocument = eventFactory.createEndElement("", "",
								"PstlAdr");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);

					}
					if (!map.get(p).get(46).toString().isEmpty()) {
						tempDocument = eventFactory.createStartElement("", "",
								"Othr");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						if ((!map.get(p).get(46).toString().isEmpty())) {
							childNodes.put("Id", map.get(p).get(46).toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(46));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "Id", 46,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}

						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}

						EndDocument = eventFactory.createEndElement("", "",
								"Othr");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);

					}

					EndDocument = eventFactory.createEndElement("", "",
							"FinInstnId");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				// BrnchId

				if (!map.get(p).get(47).toString().isEmpty()) {
					tempDocument = eventFactory.createStartElement("", "",
							"BrnchId");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					if ((!map.get(p).get(47).toString().isEmpty())) {
						childNodes.put("Id", map.get(p).get(47).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(47));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Id", 47,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"BrnchId");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				EndDocument = eventFactory.createEndElement("", "", "CdtrAgt");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			for (int transC = 0; transC < batchCount; transC++) {
				createTransactionNode(tempDocument, EndDocument,
						xmlEventWriter, eventFactory, end, childNodes,
						elementNodes, tab, p, batchCount, rowCol, ruleName,
						ruleRowNo, ruleColNo);
				p++;
				// new added
				// System.out.println(" abhi  "+p+" "+ map.size()+7);
				while (p < (map.size() + 7) && map.get(p).get(41).isEmpty()) {
					if (!map.get(p).get(48).isEmpty()) {
						createTransactionNode(tempDocument, EndDocument,
								xmlEventWriter, eventFactory, end, childNodes,
								elementNodes, tab, p, batchCount, rowCol,
								ruleName, ruleRowNo, ruleColNo);// fixed for not
																// able to
																// generate
																// multitxn
																// files.
						p++;
						transC++;
						if (transC == batchCount)
							break;
						// System.out.println(" nagma "+p);

					}
				}
				// new added end
			}

			EndDocument = eventFactory.createEndElement("", "", "PmtInf");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
			// end </PmtInf>

		}

		catch (XMLStreamException e) {
			e.printStackTrace();
			throw new XMLStreamException();
		}

	}

	private static void createNode(XMLEventWriter eventWriter, String element,
			String value, int tab2) throws XMLStreamException {

		XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();
		XMLEvent end = xmlEventFactory.createDTD("\n");
		XMLEvent tab = xmlEventFactory.createDTD("\t");
		// Create Start node
		StartElement sElement = xmlEventFactory.createStartElement("", "",
				element);
		for (int i = 0; i < tab2; i++)
			eventWriter.add(tab);
		eventWriter.add(sElement);
		// Create Content
		Characters characters = xmlEventFactory.createCharacters(value);
		eventWriter.add(characters);
		// Create End node
		EndElement eElement = xmlEventFactory.createEndElement("", "", element);
		eventWriter.add(eElement);
		eventWriter.add(end);
	}

	public void createTransactionNode(StartElement tempDocument,
			EndElement EndDocument, XMLEventWriter xmlEventWriter,
			XMLEventFactory eventFactory, XMLEvent end,
			Map<String, String> childNodes, Set<String> elementNodes,
			XMLEvent tab, int p, int batchCount, ArrayList rowCol,
			String ruleName, int ruleRowNo, int ruleColNo)
			throws XMLStreamException, EmptyStackException {

		// start DrctDbtTxInf

		try {
			for (int i = 41; i <= 90; i++) {
				if ((!map.get(p).get(i).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"DrctDbtTxInf");
					tabMarked(xmlEventWriter, 3);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					break;
				}
			}

			if ((!map.get(p).get(48).toString().isEmpty())
					|| (!map.get(p).get(49).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "", "PmtId");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();
				tab = eventFactory.createDTD("\t");
				if ((!map.get(p).get(48).toString().isEmpty())) {
					childNodes.put("InstrId", map.get(p).get(48).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(48));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "InstrId", 48,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}
				if ((!map.get(p).get(49).toString().isEmpty())) {
					childNodes.put("EndToEndId", map.get(p).get(49).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(49));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "EndToEndId", 49,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}

				EndDocument = eventFactory.createEndElement("", "", "PmtId");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// PmtTpInf

			if ((!map.get(p).get(50).toString().isEmpty())
					|| (!map.get(p).get(51).toString().isEmpty())
					|| (!map.get(p).get(52).toString().isEmpty())
					|| (!map.get(p).get(53).toString().isEmpty())
					|| (!map.get(p).get(54).toString().isEmpty())
					|| (!map.get(p).get(55).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"PmtTpInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if ((!map.get(p).get(50).toString().isEmpty())
						|| (!map.get(p).get(51).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"SvcLvl");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					if ((!map.get(p).get(50).toString().isEmpty())) {
						childNodes.put("Cd", map.get(p).get(50).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(50));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Cd", 50,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
					}
					childNodes = new LinkedHashMap<String, String>();
					if ((!map.get(p).get(51).toString().isEmpty())) {
						childNodes.put("Prtry", map.get(p).get(51).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(51));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Prtry", 51,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
					}
					EndDocument = eventFactory.createEndElement("", "",
							"SvcLvl");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				tempDocument = eventFactory.createStartElement("", "",
						"LclInstrm");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				if ((!map.get(p).get(52).toString().isEmpty())) {
					childNodes.put("Cd", map.get(p).get(52).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(52));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Cd", 52, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}
				childNodes = new LinkedHashMap<String, String>();
				if ((!map.get(p).get(53).toString().isEmpty())) {
					childNodes.put("Prtry", map.get(p).get(53).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(53));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Prtry", 53,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}
				EndDocument = eventFactory
						.createEndElement("", "", "LclInstrm");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				// Category Purpose Code Proprietary
				if ((!map.get(p).get(54).toString().isEmpty())
						|| (!map.get(p).get(55).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"CtgyPurp");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					if ((!map.get(p).get(54).toString().isEmpty())) {
						childNodes.put("Cd", map.get(p).get(54).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(54));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Cd", 54,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
					}
					childNodes = new LinkedHashMap<String, String>();
					if ((!map.get(p).get(55).toString().isEmpty())) {
						childNodes.put("Prtry", map.get(p).get(55).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(55));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Prtry", 55,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
					}
					EndDocument = eventFactory.createEndElement("", "",
							"CtgyPurp");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "PmtTpInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// Amt

			XMLEvent xmlDocs;
			Characters characters;
			if (!map.get(p).get(56).toString().isEmpty()
					|| !map.get(p).get(57).toString().isEmpty())

			{

				// BAD_FILES_CHANGES STARTS
				if (rowCol != null) {

					tempDocument = eventFactory.createStartElement("", "",
							"InstdAmt");

					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlDocs = eventFactory.createAttribute("Ccy", map.get(p)
							.get(56).toString());
					xmlEventWriter.add(xmlDocs);

					characters = eventFactory.createCharacters(map.get(p)
							.get(57).toString());
					xmlEventWriter.add(characters);

					EndDocument = eventFactory.createEndElement("", "",
							"InstdAmt");

					EndDocument.asEndElement();

					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					rowCol.add(String.valueOf(p) + "~" + String.valueOf(57));
				} else {

					// childNodes =
					// applyRule(ruleRowNo,ruleColNo,p,ruleName,childNodes,"InstdAmt",47);
					if (p == ruleRowNo && ruleColNo == 57) {
						if ("D".equals(ruleName)) {
							tempDocument = eventFactory.createStartElement("",
									"", "InstdAmt");

							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							xmlDocs = eventFactory.createAttribute("Ccy", map
									.get(p).get(56).toString());
							xmlEventWriter.add(xmlDocs);

							characters = eventFactory.createCharacters(map
									.get(p).get(57).toString());
							xmlEventWriter.add(characters);

							EndDocument = eventFactory.createEndElement("", "",
									"InstdAmt");

							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

							tempDocument = eventFactory.createStartElement("",
									"", "InstdAmt");

							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							xmlDocs = eventFactory.createAttribute("Ccy", map
									.get(p).get(56).toString());
							xmlEventWriter.add(xmlDocs);

							characters = eventFactory.createCharacters(map
									.get(p).get(57).toString());
							xmlEventWriter.add(characters);

							EndDocument = eventFactory.createEndElement("", "",
									"InstdAmt");

							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

						} else if ("M".equals(ruleName)) {

						} else if ("I".equals(ruleName)) {
							tempDocument = eventFactory.createStartElement("",
									"", "InstdAmt" + invalidAppender);
							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							xmlDocs = eventFactory.createAttribute("Ccy", map
									.get(p).get(56).toString());
							xmlEventWriter.add(xmlDocs);

							characters = eventFactory.createCharacters(map
									.get(p).get(57).toString());
							xmlEventWriter.add(characters);
							EndDocument = eventFactory.createEndElement("", "",
									"InstdAmt" + invalidAppender);
							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

						}
					} else {
						tempDocument = eventFactory.createStartElement("", "",
								"InstdAmt");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlDocs = eventFactory.createAttribute("Ccy", map
								.get(p).get(56).toString());
						xmlEventWriter.add(xmlDocs);

						characters = eventFactory.createCharacters(map.get(p)
								.get(57).toString());
						xmlEventWriter.add(characters);
						EndDocument = eventFactory.createEndElement("", "",
								"InstdAmt");
						EndDocument.asEndElement();

						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);

					}
					// pending
				}
				// BAD_FILES_CHANGES ENDS

			}

			// DbtrAgt
			if ((!map.get(p).get(77).toString().isEmpty())
					|| (!map.get(p).get(78).toString().isEmpty())
					|| (!map.get(p).get(79).toString().isEmpty())
					|| (!map.get(p).get(80).toString().isEmpty())
					|| (!map.get(p).get(81).toString().isEmpty())
					|| (!map.get(p).get(82).toString().isEmpty())
					|| (!map.get(p).get(83).toString().isEmpty())
					|| (!map.get(p).get(84).toString().isEmpty())
					|| (!map.get(p).get(85).toString().isEmpty())
					|| (!map.get(p).get(86).toString().isEmpty())
					|| (!map.get(p).get(87).toString().isEmpty())
					|| (!map.get(p).get(88).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"DbtrAgt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				tempDocument = eventFactory.createStartElement("", "",
						"FinInstnId");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				/*
				 * Column 77 changes/
				 */

				if ((!map.get(p).get(77).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("BIC", map.get(p).get(77).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(77));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "BIC", 77, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}

				// end else part

				if ((!map.get(p).get(80).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(80).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(80));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 80, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}

				if ((!map.get(p).get(81).toString().isEmpty())
						|| (!map.get(p).get(82).toString().isEmpty())
						|| (!map.get(p).get(83).toString().isEmpty())
						|| (!map.get(p).get(84).toString().isEmpty())
						|| (!map.get(p).get(85).toString().isEmpty())
						|| (!map.get(p).get(86).toString().isEmpty())) {

					tempDocument = eventFactory.createStartElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					if (!map.get(p).get(81).toString().isEmpty()) {
						childNodes.put("StrtNm", map.get(p).get(81).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(81));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "StrtNm", 81,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}

					if (!map.get(p).get(83).toString().isEmpty()) {
						childNodes.put("PstCd", map.get(p).get(83).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(83));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "PstCd", 83,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(84).toString().isEmpty()) {
						childNodes.put("TwnNm", map.get(p).get(84).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(84));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "TwnNm", 84,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(85).toString().isEmpty()) {
						childNodes.put("CtrySubDvsn", map.get(p).get(85)
								.toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(85));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "CtrySubDvsn", 85,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(86).toString().isEmpty()) {
						childNodes.put("Ctry", map.get(p).get(86).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(86));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Ctry", 86,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				if ((!map.get(p).get(78).toString().isEmpty())
						|| (!map.get(p).get(79).toString().isEmpty())) {

					tempDocument = eventFactory.createStartElement("", "",
							"ClrSysMmbId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					if ((!map.get(p).get(79).toString().isEmpty())) {
						childNodes = new LinkedHashMap<String, String>();
						tempDocument = eventFactory.createStartElement("", "",
								"ClrSysId");
						tabMarked(xmlEventWriter, 6);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						if (!map.get(p).get(79).toString().isEmpty()) {
							childNodes.put("Cd", map.get(p).get(79).toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(79));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "Cd", 79,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
							elementNodes = childNodes.keySet();
							for (String key : elementNodes) {
								createNode(xmlEventWriter, key,
										childNodes.get(key), 7);
							}
						}
						EndDocument = eventFactory.createEndElement("", "",
								"ClrSysMmbId");
						tabMarked(xmlEventWriter, 6);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					if (!map.get(p).get(78).toString().isEmpty()) {
						childNodes.put("MmbId", map.get(p).get(78).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(78));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "MmbId", 78,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 7);
						}
					}
					EndDocument = eventFactory.createEndElement("", "",
							"ClrSysMmbId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				if ((!map.get(p).get(87).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Id", map.get(p).get(87).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(87));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 87, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "",
						"FinInstnId");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				if ((!map.get(p).get(88).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"BrnchId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Id", map.get(p).get(88).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(88));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 88, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"BrnchId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "DbtrAgt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// <Dbtr>
			if ((!map.get(p).get(89).toString().isEmpty())
					|| (!map.get(p).get(90).toString().isEmpty())
					|| (!map.get(p).get(91).toString().isEmpty())
					|| (!map.get(p).get(92).toString().isEmpty())
					|| (!map.get(p).get(93).toString().isEmpty())
					|| (!map.get(p).get(94).toString().isEmpty())
					|| (!map.get(p).get(95).toString().isEmpty())) {

				tempDocument = eventFactory.createStartElement("", "", "Dbtr");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if ((!map.get(p).get(89).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(89).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(89));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 89, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}
				if ((!map.get(p).get(90).toString().isEmpty())
						|| (!map.get(p).get(91).toString().isEmpty())
						|| (!map.get(p).get(92).toString().isEmpty())
						|| (!map.get(p).get(93).toString().isEmpty())
						|| (!map.get(p).get(94).toString().isEmpty())
						|| (!map.get(p).get(95).toString().isEmpty())) {

					tempDocument = eventFactory.createStartElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();
					tab = eventFactory.createDTD("\t");
					if (!map.get(p).get(90).toString().isEmpty()) {
						childNodes.put("StrtNm", map.get(p).get(90).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(90));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "StrtNm", 90,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}

					if (!map.get(p).get(92).toString().isEmpty()) {
						childNodes.put("PstCd", map.get(p).get(92).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(92));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "PstCd", 92,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(93).toString().isEmpty()) {
						childNodes.put("TwnNm", map.get(p).get(93).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(93));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "TwnNm", 93,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(94).toString().isEmpty()) {
						childNodes.put("CtrySubDvsn", map.get(p).get(94)
								.toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(94));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "CtrySubDvsn", 94,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(95).toString().isEmpty()) {
						childNodes.put("Ctry", map.get(p).get(95).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(95));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Ctry", 95,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				EndDocument = eventFactory.createEndElement("", "", "Dbtr");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				// cdtr account

				// if part
				if (!map.get(p).get(96).toString().isEmpty()) {
					tempDocument = eventFactory.createStartElement("", "",
							"DbtrAcct");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					tempDocument = eventFactory
							.createStartElement("", "", "Id");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Id", map.get(p).get(96).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(96));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 96, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}
					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

				}
				// end else part
				EndDocument = eventFactory.createEndElement("", "", "Id");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				// ACH Transaction Code

				if ((!map.get(p).get(98).toString().isEmpty())) {
					tempDocument = eventFactory
							.createStartElement("", "", "Tp");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					// new column added in excel as tag Cd by pritam
					if ((!map.get(p).get(115).toString().isEmpty())) {
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Cd", map.get(p).get(115).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(115));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Cd", 115,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
					}
					if ((!map.get(p).get(98).toString().isEmpty())) {
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Prtry", map.get(p).get(98).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(98));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Prtry", 98,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
					}

					EndDocument = eventFactory.createEndElement("", "", "Tp");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "DbtrAcct");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// new column modification InstrForCdtrAgt ,Cd added by preetam
			if ((!map.get(p).get(116).toString().isEmpty())) {

				// BAD_FILES_CHANGES STARTS
				if (rowCol != null) {

					tempDocument = eventFactory.createStartElement("", "",
							"InstrForCdtrAgt");

					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					characters = eventFactory.createCharacters(map.get(p)
							.get(116).toString());
					xmlEventWriter.add(characters);

					EndDocument = eventFactory.createEndElement("", "",
							"InstrForCdtrAgt");

					EndDocument.asEndElement();

					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(116));
				} else {

					// childNodes =
					// applyRule(ruleRowNo,ruleColNo,p,ruleName,childNodes,"InstdAmt",47);
					if (p == ruleRowNo && ruleColNo == 116) {
						if ("D".equals(ruleName)) {
							tempDocument = eventFactory.createStartElement("",
									"", "InstrForCdtrAgt");

							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							characters = eventFactory.createCharacters(map
									.get(p).get(116).toString());
							xmlEventWriter.add(characters);

							EndDocument = eventFactory.createEndElement("", "",
									"InstrForCdtrAgt");

							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

							tempDocument = eventFactory.createStartElement("",
									"", "InstrForCdtrAgt");

							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							characters = eventFactory.createCharacters(map
									.get(p).get(116).toString());
							xmlEventWriter.add(characters);

							EndDocument = eventFactory.createEndElement("", "",
									"InstrForCdtrAgt");

							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

						} else if ("M".equals(ruleName)) {

						} else if ("I".equals(ruleName)) {
							tempDocument = eventFactory.createStartElement("",
									"", "InstrForCdtrAgt" + invalidAppender);

							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							characters = eventFactory.createCharacters(map
									.get(p).get(116).toString());
							xmlEventWriter.add(characters);

							EndDocument = eventFactory.createEndElement("", "",
									"InstrForCdtrAgt" + invalidAppender);

							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

						}
					} else {
						tempDocument = eventFactory.createStartElement("", "",
								"InstrForCdtrAgt");

						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						characters = eventFactory.createCharacters(map.get(p)
								.get(116).toString());
						xmlEventWriter.add(characters);

						EndDocument = eventFactory.createEndElement("", "",
								"InstrForCdtrAgt");

						EndDocument.asEndElement();

						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}
					// pending
				}
				// BAD_FILES_CHANGES ENDS
			}
			// new column modification InstrForCdtrAgt added by preetam

			// Transaction Purpose Code

			if (!map.get(p).get(99).toString().isEmpty()
					|| !map.get(p).get(117).toString().isEmpty()) {
				tempDocument = eventFactory.createStartElement("", "", "Purp");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if (!map.get(p).get(117).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Cd", map.get(p).get(117).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(117));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Cd", 117, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}
				if (!map.get(p).get(99).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Prtry", map.get(p).get(99).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(99));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Prtry", 99,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}
				EndDocument = eventFactory.createEndElement("", "", "Purp");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

			}
			// check start
			if ((!map.get(p).get(100).toString().isEmpty())
					|| (!map.get(p).get(101).toString().isEmpty())
					|| (!map.get(p).get(102).toString().isEmpty())
					|| (!map.get(p).get(103).toString().isEmpty())
					|| (!map.get(p).get(104).toString().isEmpty())
					|| (!map.get(p).get(105).toString().isEmpty())
					|| (!map.get(p).get(106).toString().isEmpty())
					|| (!map.get(p).get(107).toString().isEmpty())
					|| (!map.get(p).get(108).toString().isEmpty())
					|| (!map.get(p).get(109).toString().isEmpty())) {

				tempDocument = eventFactory.createStartElement("", "",
						"RltdRmtInf");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				childNodes = new LinkedHashMap<String, String>();
				tab = eventFactory.createDTD("\t");
				if (!map.get(p).get(100).toString().isEmpty()) {
					childNodes.put("RmtId", map.get(p).get(100).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(100));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "RmtId", 100,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}

				if (!map.get(p).get(101).toString().isEmpty()) {
					childNodes
							.put("RmtLctnMtd", map.get(p).get(101).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(101));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "RmtLctnMtd", 101,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}
				if (!map.get(p).get(102).toString().isEmpty()) {
					childNodes.put("RmtLctnElctrncAdr", map.get(p).get(102)
							.toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(102));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "RmtLctnElctrncAdr", 102,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}

				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 6);
				}

				if ((!map.get(p).get(103).toString().isEmpty())
						|| (!map.get(p).get(104).toString().isEmpty())
						|| (!map.get(p).get(105).toString().isEmpty())
						|| (!map.get(p).get(106).toString().isEmpty())
						|| (!map.get(p).get(107).toString().isEmpty())
						|| (!map.get(p).get(108).toString().isEmpty())
						|| (!map.get(p).get(109).toString().isEmpty())) {

					tempDocument = eventFactory.createStartElement("", "",
							"RmtLctnPstlAdr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(103).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(103));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 103, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}

					if ((!map.get(p).get(104).toString().isEmpty())
							|| (!map.get(p).get(105).toString().isEmpty())
							|| (!map.get(p).get(106).toString().isEmpty())
							|| (!map.get(p).get(107).toString().isEmpty())
							|| (!map.get(p).get(108).toString().isEmpty())
							|| (!map.get(p).get(109).toString().isEmpty())) {

						tempDocument = eventFactory.createStartElement("", "",
								"Adr");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();
						tab = eventFactory.createDTD("\t");
						if (!map.get(p).get(104).toString().isEmpty()) {
							childNodes.put("StrtNm", map.get(p).get(104)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(104));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "StrtNm", 104,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}

						if (!map.get(p).get(105).toString().isEmpty()) {
							childNodes.put("BldgNb", map.get(p).get(105)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(105));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "BldgNb", 105,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						// System.out.println("map.get(p)" + map.get(p));

						if (!map.get(p).get(106).toString().isEmpty()) {
							childNodes.put("PstCd", map.get(p).get(106)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(106));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "PstCd", 106,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(107).toString().isEmpty()) {
							childNodes.put("TwnNm", map.get(p).get(107)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(107));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "TwnNm", 107,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(108).toString().isEmpty()) {
							childNodes.put("CtrySubDvsn", map.get(p).get(108)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(108));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "CtrySubDvsn",
										108, xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(109).toString().isEmpty()) {
							childNodes.put("Ctry", map.get(p).get(109)
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(109));
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "Ctry", 109,
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
						EndDocument = eventFactory.createEndElement("", "",
								"Adr");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"RmtLctnPstlAdr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				EndDocument = eventFactory.createEndElement("", "",
						"RltdRmtInf");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// end check
			// Remittance Information

			if (!map.get(p).get(110).toString().isEmpty()
					|| !map.get(p).get(111).toString().isEmpty()) {

				tempDocument = eventFactory
						.createStartElement("", "", "RmtInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				if (!map.get(p).get(110).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Ustrd", map.get(p).get(110).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(110));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Ustrd", 110,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}

				// check start

				System.out.println("PS: " + p);
				if (!map.get(p).get(111).toString().isEmpty()) {
					int p2 = p;
					List<String> strd = new ArrayList<String>();
					List<String> cd = new ArrayList<String>();
					List<String> nb = new ArrayList<String>();
					List<String> dt = new ArrayList<String>();
					String currentInstrId = map.get(p).get(48).toString();
					while (p2 < (map.size() + 7)) {
						String tempInstrId = map.get(p2).get(48).toString();
						if (tempInstrId.isEmpty()
								|| tempInstrId.equals(currentInstrId)) {
							strd.add(map.get(p2).get(111).toString());
							cd.add(map.get(p2).get(112).toString());
							nb.add(map.get(p2).get(113).toString());
							dt.add(map.get(p2).get(114).toString());
						} else {
							break;
						}

						p2++;
					}
					strdEntry(tempDocument, EndDocument, xmlEventWriter,
							eventFactory, end, childNodes, elementNodes, tab,
							strd, cd, nb, dt, rowCol, ruleName, ruleRowNo,
							ruleColNo);

				}

				EndDocument = eventFactory.createEndElement("", "", "RmtInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// DrctDbtTxInf

			for (int i = 41; i <= 114; i++) {
				if ((!map.get(p).get(i).toString().isEmpty())) {
					EndDocument = eventFactory.createEndElement("", "",
							"DrctDbtTxInf");
					tabMarked(xmlEventWriter, 3);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
					break;
				}
			}

		} catch (XMLStreamException e) {
			e.printStackTrace();
			log.fatal("Exception: ", e);
			throw new XMLStreamException();
		}

	}

	public void strdEntry(StartElement tempDocument, EndElement EndDocument,
			XMLEventWriter xmlEventWriter, XMLEventFactory eventFactory,
			XMLEvent end, Map<String, String> childNodes,
			Set<String> elementNodes, XMLEvent tab, List<String> strd,
			List<String> cd, List<String> nb, List<String> dt,
			ArrayList rowCol, String ruleName, int ruleRowNo, int ruleColNo)
			throws XMLStreamException {
		for (int i = 0; i < strd.size(); i++) {
			if ((i == 0 && !strd.get(i).toString().isEmpty())
					|| strd.get(i).toString().isEmpty())// fix for strd grid
			{
				if (!strd.get(i).toString().isEmpty()) {

					tempDocument = eventFactory.createStartElement("", "",
							"Strd");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
				}
				// }

				tempDocument = eventFactory.createStartElement("", "",
						"RfrdDocInf");
				tabMarked(xmlEventWriter, 6);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if (!cd.get(i).toString().isEmpty()) {
					tempDocument = eventFactory
							.createStartElement("", "", "Tp");
					tabMarked(xmlEventWriter, 7);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					tempDocument = eventFactory.createStartElement("", "",
							"CdOrPrtry");
					tabMarked(xmlEventWriter, 8);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Prtry", cd.get(i).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(112));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Prtry", 112,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 9);
					}
					// }
					EndDocument = eventFactory.createEndElement("", "",
							"CdOrPrtry");
					tabMarked(xmlEventWriter, 8);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
					EndDocument = eventFactory.createEndElement("", "", "Tp");
					tabMarked(xmlEventWriter, 7);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

				}

				if (!nb.get(i).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nb", nb.get(i).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(113));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nb", 113, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}
				}

				if (!dt.get(i).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("RltdDt", dt.get(i).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(114));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "RltdDt", 114,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}
				}

				EndDocument = eventFactory.createEndElement("", "",
						"RfrdDocInf");
				tabMarked(xmlEventWriter, 6);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				// if(i == (strd.size() - 1) || !strd.get(i +
				// 1).toString().isEmpty())
				// {
				if (!strd.get(i).toString().isEmpty()) {
					EndDocument = eventFactory.createEndElement("", "", "Strd");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

				}
			}
		}
	}

	public void tabMarked(XMLEventWriter xmlEventWriter, int count)
			throws XMLStreamException {
		XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();
		XMLEvent tab = xmlEventFactory.createDTD("\t");

		try {
			for (int i = 0; i < count; i++)
				xmlEventWriter.add(tab);
		} catch (XMLStreamException e) {
			e.printStackTrace();
			throw new XMLStreamException();
		}
	}
}