/********************************************************************************************************/
/* Copyright �  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/* 								 																		*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : ConvertFileISOPain001.java                                          					*/
/*                                                                                      				*/
/********************************************************************************************************/
/*  			  Author					    |        	  Date   			|	  Version           */
/********************************************************************************************************/
/*         Veera Kumar Reddy.V		            |			20:05:2016			|		1.0	            */
/*		   Sapna Jain							|			18:07:2017			|		1.1
 /* 	   Preetam Sanjore						|			20:07:2017			|		1.2	
 /* 	   Preetam Sanjore						|			20:07:2017			|		1.3	
 /* 	   Gokaran Tiwari						|			22:03:2018			|		1.4 latest suite change
 /* 																				in main table , old move to audit

 /********************************************************************************************************/
/* 											  														    */
/* Description   : For generation of PAIN 001 format 													*/
/*        																								*/
/********************************************************************************************************/

package com.intellect.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.EmptyStackException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Namespace;

import org.apache.log4j.Logger;

import com.intellect.pojo.TestToolFileNamePOJO;
import com.intellect.property.FormatConstants;
import com.intellect.service.FormatGenerateService;
import com.intellect.service.UpdateFormatXlsService;

public class ConvertFileISOPain001_CNR_EMT {

	private static org.apache.log4j.Logger log = Logger
			.getLogger(ConvertFileISOPain001_CNR_EMT.class);

	int m = 7, p = 7;
	int batchrowCount = 7;
	LinkedHashMap<Integer, Vector<String>> map = null;
	LinkedHashMap<Integer, Vector> map1 = null;
	ReadTemplate readTemplate = new ReadTemplate();
	int filesCrtdCnt = 0;

	// BAD_FILES_CHANGES
	private String invalidAppender = "001";

	public int main(String source, File target, String fileTypeSelected,
			LinkedHashMap<Integer, Vector<String>> map1, Connection connection,
			String db_table_file, String timeStamp, String dbTimeStamp,
			String moduleSelected, char[] variants, List<TestToolFileNamePOJO> testToolData) throws Exception {// BAD_FILES_CHANGES

		ConvertFileISOPain001_CNR_EMT xmlWriter = new ConvertFileISOPain001_CNR_EMT();
		filesCrtdCnt = xmlWriter.rootXMLFile(source, target, fileTypeSelected,map1, connection, db_table_file, timeStamp, dbTimeStamp,moduleSelected, variants,testToolData);// BAD_FILES_CHANGES

		return filesCrtdCnt;
	}

	public int rootXMLFile(String source, File target, String fileTypeSelected,
			LinkedHashMap map1, Connection connection, String db_table_file,
			String timeStamp, String dbTimeStamp, String moduleSelected,
			char[] variants,List<TestToolFileNamePOJO> pojoList) throws Exception {// BAD_FILES_CHANGES
		//Jo-here is where changes need to be made
		IwReadExcel objIwReadExcel = new IwReadExcel();
		map = (LinkedHashMap<Integer, Vector<String>>) objIwReadExcel.ReadData(
				source, fileTypeSelected, moduleSelected);
		// Change 1.4
		int rowCount = 7, colCount = 17;
		ArrayList<Integer> stockList = new ArrayList<Integer>();
		int count = 0, k = 0, fileCount = 0;
		for (int j = 0; j < map.size(); j++) {
			if (!map.get(rowCount).get(colCount).toString().isEmpty()) {
				stockList.add(count + 1);
				fileCount++;
				count = 0;
				k++;

			} else
				count++;
			rowCount++;
		}

		stockList.add(count + 1);
		int arr[] = new int[stockList.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = stockList.get(i).intValue();
		}
		ArrayList allRecList = new ArrayList();
		for (int i = 0, j2 = 1; i < fileCount; i++, j2++) {
			ArrayList rowCol = writeXML(arr[j2], target, connection,
					db_table_file, timeStamp, dbTimeStamp, null, -1, -1,pojoList);// Change 1.4
			allRecList.add(rowCol);
		}
		log.debug("ALL good files generated");

		// BAD_FILES_CHANGES STARTS
		String[] ruleNames = null;
		boolean oFind = false;
		for (int i = 0; i < variants.length; i++) {
			if (variants[i] == 'O') {
				oFind = true;
			}
		}
		if (oFind) {
			ruleNames = new String[variants.length - 1];
		} else {
			ruleNames = new String[variants.length];
		}

		for (int i = 0; i < variants.length; i++) {
			if (variants[i] == 'O')
				continue;
			else
				ruleNames[i] = variants[i] + "";
		}

		int lastRowNo = 0;
		int startNoForEachMessage = 0;
		// iterating over no of messages
		for (int allRec = 0; allRec < allRecList.size(); allRec++) {
			if (lastRowNo == 0) {
				startNoForEachMessage = 7 + lastRowNo;
			} else {
				startNoForEachMessage = 1 + lastRowNo;

			}
			p = startNoForEachMessage;
			batchrowCount = startNoForEachMessage;
			m = startNoForEachMessage;
			ArrayList rowCol = (ArrayList) allRecList.get(allRec);
			for (int rowColNo = 0; rowColNo < rowCol.size(); rowColNo++) {
				String s = (String) rowCol.get(rowColNo);

				String[] strArray = s.split("~");
				lastRowNo = Integer.valueOf(strArray[0]);
				int ruleColNo = Integer.valueOf(strArray[1]);

				for (int x = 0; x < ruleNames.length; x++) {

					p = startNoForEachMessage;
					batchrowCount = startNoForEachMessage;
					m = startNoForEachMessage;

					writeXML(arr[allRec + 1], target, connection,
							db_table_file, timeStamp, dbTimeStamp,
							ruleNames[x], lastRowNo, ruleColNo,pojoList);// Change 1.4

				}
			}
		}
		
		return fileCount;

	}

	public ArrayList writeXML(int batchCount, File target,
			Connection connection, String db_table_file, String timeStamp,
			String dbTimeStamp, String ruleName, int ruleRowNo, int ruleColNo,List<TestToolFileNamePOJO> pojoList)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("writeXML method starts in ConvertFileISOPain001 class: batchCount: "
					+ batchCount
					+ "ruleName: "
					+ ruleName
					+ " ruleRowNo: "
					+ ruleRowNo + " ruleColNo: " + ruleColNo + " p: " + p);
		}
		XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMdd");
		String XMLPath = target + "/";

		XMLPath = XMLPath.replaceAll(".xml", m + ".xml");
		String fileName = null, dbfileName = null;
		String pain1remit_applied_rule_pos = FormatConstants.pain1remitAppliedRulePosition;
		int pain1remit_applied_rule_pos_no = Integer
				.parseInt(pain1remit_applied_rule_pos);
		// BAD_FILES_CHANGES STARTS
		ArrayList<String> rowCol = null;
		// BAD_FILES_CHANGES ENDS
		System.out.println("map  :::" + map);
		if (map.get(p).get(2) != null
				&& map.get(p).get(2).toString().length() > 0) {
			String temp = FormatGenerateService.getSequenceNumber(connection,
					map.get(p).get(2).toString());

			if (map.get(p).get(3) != null
					&& map.get(p).get(3).toString().replace(" ", "")
							.equalsIgnoreCase("FTS")) {

				File filefolder = new File(XMLPath + "/FTS");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();

				fileName = XMLPath
						+ "/"
						+ map.get(p).get(7).toString()
						+ "_"
						+ map.get(p).get(10).toString()
						+ "_"
						+ formatter.format(date)
						+ "_111213_"
						+ map.get(p)
								.get(8)
								.toString()
								.replace(".0", "")
								.substring(
										0,
										map.get(p).get(8).toString().length() - 6)
						+ temp + "." + map.get(p).get(2).toString() + "."
						+ map.get(p).get(11).toString() + ".xml";

				// log.debug("fileName:" + fileName);
				// BAD_FILES_CHANGES ENDS
				if (map.get(p).get(12) != null
						&& !map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(7).toString()
							+ "_"
							+ map.get(p).get(10).toString()
							+ "_"
							+ formatter.format(date)
							+ "_111213_"
							+ map.get(p)
									.get(8)
									.toString()
									.replace(".0", "")
									.substring(
											0,
											map.get(p).get(8).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();
				} else {
					dbfileName = map.get(p).get(7).toString()
							+ "_"
							+ map.get(p).get(10).toString()
							+ "_"
							+ formatter.format(date)
							+ "_111213_"
							+ map.get(p)
									.get(8)
									.toString()
									.replace(".0", "")
									.substring(
											0,
											map.get(p).get(8).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + ".xml";
				}

			} else if (map.get(p).get(3).toString().replace(" ", "")
					.equalsIgnoreCase("SCA")) {

				File filefolder = new File(XMLPath + "/SCA");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();

				fileName = XMLPath
						+ "/"
						+ map.get(p).get(6).toString()
						+ "."
						+ formatter1.format(date)
						+ "111213"
						+ "."
						+ map.get(p)
								.get(10)
								.toString()
								.substring(
										0,
										map.get(p).get(10).toString().length() - 6)
						+ temp + "."
						+ map.get(p).get(2).toString().replace(".0", "") + "."
						+ map.get(p).get(11).toString() + ".xml";
				// log.debug("fileName:" + fileName);
				// BAD_FILES_CHANGES ENDS
				if (!map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();
				} else {
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";
				}
			} else if (map.get(p).get(3).toString().replace(" ", "")
					.equalsIgnoreCase("FTT")) {

				File filefolder = new File(XMLPath + "/FTT");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();
				// BAD_FILES_CHANGES STARTS
				if (ruleName != null) {
					fileName = XMLPath + "/" + map.get(p).get(7).toString()
							+ "_" + map.get(p).get(10).toString() + "_"
							+ map.get(p).get(9).toString() + "_"
							+ map.get(p).get(8).toString().replace(".0", "")
							+ "." + map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + "_" + ruleName
							+ String.valueOf(ruleRowNo)
							+ String.valueOf(ruleColNo) + ".xml";
       
				} else {
					fileName = XMLPath + "/" + map.get(p).get(6).toString()
							/*+ "." + map.get(p).get(9).toString() + "."*/
							+"." +timeStamp+"." 
							+ map.get(p).get(10).toString() + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";
					System.out.println("j1 second"+fileName+"2");
				}
				// log.debug("fileName:" + fileName);
				// BAD_FILES_CHANGES ENDS
				if (!map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();
				} else {
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";
				}
			}
			// BAD_FILES_CHANGES
			StringBuffer strModifiedField = new StringBuffer();
			if (ruleName != null && !"".equals(ruleName)) {
				strModifiedField.append(ruleRowNo).append("~")
						.append(ruleColNo);
			}
			String errorCode = null;
			String ruleType = null;
			if ("M".equals(ruleName)) {
				ruleType = "TAG_MISSING";
				errorCode = "TAG_MISSING_ERRORCODE";
				// errorCode =
				// "Invalid Tag Found Invalid Tag Found PHERC_1002 Mandatory Section Group Missing";
			} else if ("D".equals(ruleName)) {
				ruleType = "DUPLICATE_TAG";
				errorCode = "DUPLICATE_TAG_ERRORCODE";
				// errorCode = "Invalid Tag Found Invalid Tag Found";
			} else if ("I".equals(ruleName)) {
				ruleType = "INVALID_TAG";
				errorCode = "INVALID_TAG_ERRORCODE";
				// errorCode =
				// "Invalid Tag Found Invalid Tag Found PHERC_1002 Mandatory Section Group Missing";
			} else if ("O".equals(ruleName)) {
				ruleType = "TAG_ORDER_CHANGE";
				errorCode = "TAG_ORDER_CHANGE_ERRORCODE";
				// errorCode =
				// "Invalid Tag Found Invalid Tag Found PHERC_1002 Mandatory Section Group Missing";
			}
			// Change 1.4 starts
			if (ruleName != null) {
				TestToolFileNamePOJO pojoObj = new TestToolFileNamePOJO();
				pojoObj.setTestCaseid(map.get(p).get(0).toString());
				pojoObj.setFormatName(map.get(p).get(2).toString());
				pojoObj.setChannel(map.get(p).get(3).toString());
				pojoObj.setLsi(map.get(p).get(4).toString());
				pojoObj.setClientRefNum(map.get(p).get(17).toString());
				pojoObj.setBicCode(map.get(p).get(6).toString());
				pojoObj.setBbdId(map.get(p).get(7).toString());
				pojoObj.setCustRef(map.get(p).get(8).toString());
				pojoObj.setUniqueId(map.get(p).get(10).toString());
				pojoObj.setFileName(dbfileName);
				pojoObj.setAppliedRules(ruleType);
				pojoObj.setAppliedRules(errorCode);
				pojoObj.setModifiedField(strModifiedField.toString());
				pojoObj.setTimeStamp(dbTimeStamp);
				pojoList.add(pojoObj);
				
			} else {
				TestToolFileNamePOJO pojoObj = new TestToolFileNamePOJO();
				pojoObj.setTestCaseid(map.get(p).get(0).toString());
				pojoObj.setFormatName(map.get(p).get(2).toString());
				pojoObj.setChannel(map.get(p).get(3).toString());
				pojoObj.setLsi(map.get(p).get(4).toString());
				pojoObj.setClientRefNum(map.get(p).get(17).toString());
				pojoObj.setBicCode(map.get(p).get(6).toString());
				pojoObj.setBbdId(map.get(p).get(7).toString());
				pojoObj.setCustRef(map.get(p).get(8).toString());
				pojoObj.setUniqueId(map.get(p).get(10).toString());
				pojoObj.setFileName(dbfileName);
				pojoObj.setAppliedRules(map.get(p).get(pain1remit_applied_rule_pos_no).toString());
				pojoObj.setAppliedRules(map.get(p).get(pain1remit_applied_rule_pos_no + 1).toString());
				pojoObj.setModifiedField(map.get(p).get(pain1remit_applied_rule_pos_no + 3).toString());
				pojoObj.setTimeStamp(dbTimeStamp);
				pojoList.add(pojoObj);
			}
			// Change 1.4 ends
		} else {

			if (log.isDebugEnabled()) {
				log.debug("FormatName is empty");
			}
		}
		m++;
		String rootElement = "CstmrCdtTrfInitn";

		try {
			XMLEventWriter xmlEventWriter = xmlOutputFactory
					.createXMLEventWriter(new FileOutputStream(fileName),
							"UTF-8");
			XMLEventFactory eventFactory = XMLEventFactory.newInstance();
			XMLEvent end = eventFactory.createDTD(System
					.getProperty("line.separator"));
			XMLEvent tab = eventFactory.createDTD("\t");

			ArrayList<Namespace> ns = new ArrayList<Namespace>();
			ArrayList<Attribute> atts = new ArrayList<Attribute>();
			atts.add(eventFactory
					.createAttribute(
							"xsi:schemaLocation",
							"urn:iso:std:iso:20022:tech:xsd:pain.001.001.03 file:///C:/Users/IBM_ADMIN/Box%20Sync/My%20Files/PSH/pain.001.001.03.xsd"));
			atts.add(eventFactory.createAttribute("xmlns",
					"urn:iso:std:iso:20022:tech:xsd:pain.001.001.03"));
			atts.add(eventFactory.createAttribute("xmlns:xsi",
					"http://www.w3.org/2001/XMLSchema-instance"));

			StartElement tempDocument = eventFactory.createStartElement("", "",
					"Document", atts.iterator(), ns.iterator());
			// 1.1 ends
			xmlEventWriter.add(tempDocument);

			StartElement configStartElement = eventFactory.createStartElement(
					"", "", rootElement);
			xmlEventWriter.add(tab);
			xmlEventWriter.add(end);//Jo-Added this 
			xmlEventWriter.add(configStartElement);
			xmlEventWriter.add(end);
			EndElement EndDocument = null;
			Map<String, String> childNodes = null;
			Set<String> elementNodes = null;
			tempDocument = eventFactory.createStartElement("", "", "GrpHdr");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			Map<String, String> elementsMap = new LinkedHashMap<String, String>();
			String[] Msg_id = { "", "" };// messageid changes
			// BAD_FILES_CHANGES STARTS
			if (ruleName == null) {
				rowCol = new ArrayList();
			}
			// BAD_FILES_CHANGES ENDS

			// Write the element nodes

			// <GrpHdr> start

			// MsgId
			if (!map.get(p).get(17).toString().isEmpty()) {
				elementsMap.put("MsgId", (String) map.get(p).get(17));//Jo-Incorrect msg id is getting displayed .check why
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(17));
				else {
					Msg_id = UpdateFormatXlsService.getData(connection,
							FormatConstants.pain001);// messageid changes
					elementsMap.put("MsgId", Msg_id[0]);//Jo-Incorrect msg id is getting displayed .check why
					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "MsgId", 17, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS

			}

			elementNodes = elementsMap.keySet();
			for (String key : elementNodes) {

				createNode(xmlEventWriter, key, elementsMap.get(key), 3);
			}
			elementsMap = new LinkedHashMap<String, String>();

			// Authstn
			if (!map.get(p).get(18).toString().isEmpty()) {
				tab = eventFactory.createDTD("\t");
				tempDocument = eventFactory.createStartElement("", "",
						"Authstn");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if ((!map.get(p).get(18).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Prtry", map.get(p).get(18).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(18));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Prtry", 18,
								xmlEventWriter);
						// BAD_FILES_CHANGES ENDS
					}

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}
				EndDocument = eventFactory.createEndElement("", "", "Authstn");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// CreDtTm
			if (!map.get(p).get(19).toString().isEmpty()) {
				System.out.println("craeted time"+(String) map.get(p).get(19));
		elementsMap.put("CreDtTm", (String) map.get(p).get(19));
			/*	ConvertFileISOPain001_CNR_EMT ob=new ConvertFileISOPain001_CNR_EMT();
				String _date=ob.getSimpleDateFormat();
				elementsMap.put("CreDtTm",_date);*/
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(19));
				else {

					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "CreDtTm", 19, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}

			// NbOfTxs
			if (!map.get(p).get(20).toString().isEmpty()) {
				elementsMap.put("NbOfTxs", (String) map.get(p).get(20));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(20));
				else {

					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "NbOfTxs", 20, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}

			// CtrlSum
			if (!map.get(p).get(21).toString().isEmpty()) {
				elementsMap.put("CtrlSum", (String) map.get(p).get(21));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(21));
				else {

					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "CtrlSum", 21, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}

			// InitgPty

			elementNodes = elementsMap.keySet();
			for (String key : elementNodes) {

				createNode(xmlEventWriter, key, elementsMap.get(key), 3);
			}

			if (!map.get(p).get(22).toString().isEmpty()
					|| !map.get(p).get(23).toString().isEmpty()) {

				tempDocument = eventFactory.createStartElement("", "",
						"InitgPty");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				if (!map.get(p).get(22).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					childNodes.put("Nm", (String) map.get(p).get(22));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(22));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 22, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 4);
					}
				}
				if (!map.get(p).get(23).toString().isEmpty()) {
					tempDocument = eventFactory
							.createStartElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					tempDocument = eventFactory.createStartElement("", "",
							"OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					childNodes.put("Id", (String) map.get(p).get(23));

					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(23));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 23, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}

					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory
							.createEndElement("", "", "OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory.createEndElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "InitgPty");
				tabMarked(xmlEventWriter, 3);

				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			EndDocument = eventFactory.createEndElement("", "", "GrpHdr");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

			int colCount = 24;

			ArrayList<Integer> stockList = new ArrayList<Integer>();

			int count = 0, k = 0, fileCount = 0;
			for (int j = 0; j < batchCount; j++) {
				if (!map.get(batchrowCount).get(colCount).toString().isEmpty()) {
					stockList.add(count + 1);
					fileCount++;
					count = 0;
					k++;
				} else
					count++;
				batchrowCount++;
			}
			stockList.add(count + 1);
			int arr[] = new int[stockList.size()];
			for (int i2 = 0; i2 < arr.length; i2++) {
				arr[i2] = stockList.get(i2).intValue();
			}
			for (int j2 = 1; j2 <= k; j2++) {
			}
			// int batchCount2=0;
			for (int i2 = 0, k2 = 1; i2 < fileCount; i2++, k2++) {
				paymentInfo(tempDocument, EndDocument, xmlEventWriter,
						eventFactory, end, childNodes, elementNodes, tab, p,
						arr[k2], rowCol, ruleName, ruleRowNo, ruleColNo);

			}

			tabMarked(xmlEventWriter, 1);
			xmlEventWriter.add(eventFactory.createEndElement("", "",
					rootElement));

			xmlEventWriter.add(end);

			xmlEventWriter.add(eventFactory.createEndDocument());
			eventFactory.createEndDocument();

//			EndDocument = eventFactory.createEndElement("", "", "Document");
//			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

			String rp = xmlEventWriter.toString();
			rp = rp.replaceAll(
					"</Document xmlns=urn:iso:std:iso:20022:tech:xsd:pain.001.001.03 xmlns:xsi=http://www.w3.org/2001/XMLSchema-instance>",
					"</Document>");

			xmlEventWriter.close();

		} catch (EmptyStackException eex) {
			log.fatal("EmptyStackException: ", eex);
			throw new EmptyStackException();
		} catch (FileNotFoundException e) {
			log.fatal("FileNotFoundException: ", e);
			e.printStackTrace();
			throw new FileNotFoundException();

		} catch (XMLStreamException ex) {
			log.fatal("XMLStreamException: ", ex);
			ex.printStackTrace();
			throw new XMLStreamException();
		}
		return rowCol;

	}

	// BAD_FILES_CHANGES
	private Map applyRule(int ruleRowNo, int ruleColNo, int p2,
			String ruleName, Map<String, String> elementsMap, String key,
			int actualColNo, XMLEventWriter xmlEventWriter)
			throws XMLStreamException {
		if (log.isDebugEnabled())
			log.debug("in applyRule ruleRowNo: " + ruleRowNo + " ruleColNo: "
					+ ruleColNo + "p: " + p2 + " ruleName: " + ruleName
					+ " elementsMap: " + elementsMap + " key: " + key
					+ " actualColNo: " + actualColNo);
		if (p2 == ruleRowNo && ruleColNo == actualColNo) {
			if ("D".equals(ruleName)) {
				createNode(xmlEventWriter, key, elementsMap.get(key), 3);

			} else if ("M".equals(ruleName)) {
				elementsMap.remove(key);

			} else if ("I".equals(ruleName)) {
				elementsMap.put(key + invalidAppender,
						(String) map.get(p).get(actualColNo));
				elementsMap.remove(key);

			}
		}
		if (log.isDebugEnabled())
			log.debug("Leaving applyRule: elementsMap " + elementsMap);
		return elementsMap;

	}

	public void paymentInfo(StartElement tempDocument, EndElement EndDocument,
			XMLEventWriter xmlEventWriter, XMLEventFactory eventFactory,
			XMLEvent end, Map<String, String> childNodes,
			Set<String> elementNodes, XMLEvent tab, int tempP, int batchCount,
			ArrayList rowCol, String ruleName, int ruleRowNo, int ruleColNo)
			throws XMLStreamException {
		if (log.isDebugEnabled()) {
			log.debug("paymentInfo method starts in ConvertFileISOPain001 class");
		}
		// start <PmtInf>

		try {
			tempDocument = eventFactory.createStartElement("", "", "PmtInf");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(tempDocument);

			xmlEventWriter.add(end);
			childNodes = new LinkedHashMap<String, String>();

			tab = eventFactory.createDTD("\t");

			// PmtInfId
			if (!map.get(p).get(24).toString().isEmpty()) {
				childNodes.put("PmtInfId", map.get(p).get(24).toString());

				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(24));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "PmtInfId", 24, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			// PmtMtd
			if (!map.get(p).get(25).toString().isEmpty()) {
				childNodes.put("PmtMtd", map.get(p).get(25).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(25));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "PmtMtd", 25, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			// NbOfTxs
			if (!map.get(p).get(26).toString().isEmpty()) {
				childNodes.put("NbOfTxs", (String) map.get(p).get(26));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(26));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "NbOfTxs", 26, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}

			// CtrlSum

			if (!map.get(p).get(27).toString().isEmpty()) {
				childNodes.put("CtrlSum", (String) map.get(p).get(27));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(27));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "CtrlSum", 27, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}

			elementNodes = childNodes.keySet();

			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, childNodes.get(key), 3);
			}

			// Requested Execution Date

			if ((!map.get(p).get(41).toString().isEmpty())
					|| (!map.get(p).get(42).toString().isEmpty())
					|| (!map.get(p).get(43).toString().isEmpty())
					|| (!map.get(p).get(44).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"PmtTpInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				// check
				childNodes = new LinkedHashMap<String, String>();
				if ((!map.get(p).get(41).toString().isEmpty())) {
					childNodes.put("InstrPrty", map.get(p).get(41).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(41));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "InstrPrty", 41,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}

				if ((!map.get(p).get(42).toString().isEmpty())
						|| (!map.get(p).get(43).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"LclInstrm");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();
					if ((!map.get(p).get(42).toString().isEmpty())) {
						tab = eventFactory.createDTD("\t");
						childNodes.put("Cd", map.get(p).get(42).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(42));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Cd", 42,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
					} else {
						tab = eventFactory.createDTD("\t");
						childNodes.put("Prtry", map.get(p).get(43).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(43));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Prtry", 43,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
					}

					EndDocument = eventFactory.createEndElement("", "",
							"LclInstrm");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					// Category Purpose Code Proprietary
					if ((!map.get(p).get(44).toString().isEmpty())) {
						tempDocument = eventFactory.createStartElement("", "",
								"CtgyPurp");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Prtry", map.get(p).get(44).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(44));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Prtry", 44,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}

						EndDocument = eventFactory.createEndElement("", "",
								"CtgyPurp");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"PmtTpInf");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
			}
			// Batch level changes end for PmtTpInf by preetam

			if (!map.get(p).get(28).toString().isEmpty()) {
				childNodes = new LinkedHashMap<String, String>();
				childNodes.put("ReqdExctnDt", map.get(p).get(28).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(28));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "ReqdExctnDt", 28, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();

				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 3);
				}
			}
			// Dbtr
			if ((!map.get(p).get(29).toString().isEmpty())
					|| (!map.get(p).get(30).toString().isEmpty())
					|| (!map.get(p).get(31).toString().isEmpty())
					|| (!map.get(p).get(32).toString().isEmpty())
					|| (!map.get(p).get(33).toString().isEmpty())
					|| (!map.get(p).get(34).toString().isEmpty())
					|| (!map.get(p).get(35).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "", "Dbtr");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if (!map.get(p).get(29).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(29).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(29));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 29, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 4);
					}
				}

				if ((!map.get(p).get(30).toString().isEmpty())
						|| (!map.get(p).get(31).toString().isEmpty())
						|| (!map.get(p).get(32).toString().isEmpty())
						|| (!map.get(p).get(33).toString().isEmpty())
						|| (!map.get(p).get(34).toString().isEmpty())
						|| (!map.get(p).get(35).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					if ((!map.get(p).get(30).toString().isEmpty())) {
						childNodes.put("StrtNm", map.get(p).get(30).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(30));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "StrtNm", 30,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if ((!map.get(p).get(31).toString().isEmpty())) {
						childNodes.put("BldgNb", map.get(p).get(31).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(31));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "BldgNb", 31,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if ((!map.get(p).get(32).toString().isEmpty())) {
						childNodes.put("PstCd", map.get(p).get(32).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(32));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "PstCd", 32,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if ((!map.get(p).get(33).toString().isEmpty())) {
						childNodes.put("TwnNm", map.get(p).get(33).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(33));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "TwnNm", 33,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if ((!map.get(p).get(34).toString().isEmpty())) {
						childNodes.put("CtrySubDvsn", map.get(p).get(34)
								.toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(34));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "CtrySubDvsn", 34,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if ((!map.get(p).get(35).toString().isEmpty())) {
						childNodes.put("Ctry", map.get(p).get(35).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(35));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Ctry", 35,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				if (!map.get(p).get(36).toString().isEmpty()) {
					tempDocument = eventFactory
							.createStartElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					tempDocument = eventFactory.createStartElement("", "",
							"OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					childNodes.put("Id", (String) map.get(p).get(36));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(36));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 36, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}

					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory
							.createEndElement("", "", "OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory.createEndElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "Dbtr");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// DbtrAcct
			if (!map.get(p).get(37).toString().isEmpty()) {
				tempDocument = eventFactory.createStartElement("", "",
						"DbtrAcct");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				tempDocument = eventFactory.createStartElement("", "", "Id");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				tempDocument = eventFactory.createStartElement("", "", "Othr");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Id", map.get(p).get(37).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(37));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Id", 37, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 6);
				}
				EndDocument = eventFactory.createEndElement("", "", "Othr");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				EndDocument = eventFactory.createEndElement("", "", "Id");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				EndDocument = eventFactory.createEndElement("", "", "DbtrAcct");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// DbtrAgt
			if ((!map.get(p).get(38).toString().isEmpty())
					|| (!map.get(p).get(39).toString().isEmpty())
					|| (!map.get(p).get(40).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"DbtrAgt");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				// new data
				if ((!map.get(p).get(38).toString().isEmpty())
						|| (!map.get(p).get(39).toString().isEmpty())
						|| (!map.get(p).get(40).toString().isEmpty())
						|| (!map.get(p).get(45).toString().isEmpty())) {

					tempDocument = eventFactory.createStartElement("", "",
							"FinInstnId");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					// IF part

					if (!map.get(p).get(38).toString().isEmpty()) {
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Nm", map.get(p).get(38).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(38));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Nm", 38,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 5);
						}
					}

					tempDocument = eventFactory.createStartElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					if ((!map.get(p).get(39).toString().isEmpty())) {
						childNodes.put("Ctry", map.get(p).get(39).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(39));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Ctry", 39,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

				}

				if ((!map.get(p).get(45).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap();

					tab = eventFactory.createDTD("\t");

					childNodes.put("Id", map.get(p).get(45).toString());

					if (rowCol != null) {
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(45));
					} else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 45, xmlEventWriter);
					}

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "", "Othr");

					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "",
						"FinInstnId");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// BrnchId
			if (!map.get(p).get(40).toString().isEmpty()) {
				tempDocument = eventFactory.createStartElement("", "",
						"BrnchId");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Id", map.get(p).get(40).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(40));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Id", 40, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}

				EndDocument = eventFactory.createEndElement("", "", "BrnchId");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			EndDocument = eventFactory.createEndElement("", "", "DbtrAgt");
			tabMarked(xmlEventWriter, 3);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

			for (int transC = 0; transC < batchCount; transC++) {
				createTransactionNode(tempDocument, EndDocument,
						xmlEventWriter, eventFactory, end, childNodes,
						elementNodes, tab, p, batchCount, rowCol, ruleName,
						ruleRowNo, ruleColNo);
				p++;
				while (p < (map.size() + 7) && map.get(p).get(46).isEmpty()) {
					p++;
					transC++;
					if (transC == batchCount)
						break;
				}
			}

			EndDocument = eventFactory.createEndElement("", "", "PmtInf");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
			// end </PmtInf>

		} catch (XMLStreamException e) {
			e.printStackTrace();
			throw new XMLStreamException();
		}

	}

	private static void createNode(XMLEventWriter eventWriter, String element,
			String value, int tab2) throws XMLStreamException {

		XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();
		XMLEvent end = xmlEventFactory.createDTD("\n");
		XMLEvent tab = xmlEventFactory.createDTD("\t");
		// Create Start node
		StartElement sElement = xmlEventFactory.createStartElement("", "",
				element);
		for (int i = 0; i < tab2; i++)
			eventWriter.add(tab);
		eventWriter.add(sElement);
		// Create Content
		Characters characters = xmlEventFactory.createCharacters(value);
		eventWriter.add(characters);
		// Create End node
		EndElement eElement = xmlEventFactory.createEndElement("", "", element);
		eventWriter.add(eElement);
		eventWriter.add(end);

	}

	public void createTransactionNode(StartElement tempDocument,
			EndElement EndDocument, XMLEventWriter xmlEventWriter,
			XMLEventFactory eventFactory, XMLEvent end,
			Map<String, String> childNodes, Set<String> elementNodes,
			XMLEvent tab, int p, int batchCount, ArrayList rowCol,
			String ruleName, int ruleRowNo, int ruleColNo)
			throws XMLStreamException {

		try {
			for (int i = 46; i <= 95; i++) {
				if ((!map.get(p).get(i).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"CdtTrfTxInf");
					tabMarked(xmlEventWriter, 3);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					break;
				}
			}
			// start CdtTrfTxInf

			if ((!map.get(p).get(46).toString().isEmpty())
					|| (!map.get(p).get(47).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "", "PmtId");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();
				tab = eventFactory.createDTD("\t");
				if ((!map.get(p).get(46).toString().isEmpty())) {
					childNodes.put("InstrId", map.get(p).get(46).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(46));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "InstrId", 46,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}
				if ((!map.get(p).get(47).toString().isEmpty())) {
					childNodes.put("EndToEndId", map.get(p).get(47).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(47));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "EndToEndId", 47,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}

				EndDocument = eventFactory.createEndElement("", "", "PmtId");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// PmtTpInf

			// fixed for issue 36 mentioned in Defect tracker of FGT
			if ((!map.get(p).get(48).toString().isEmpty())
					|| (!map.get(p).get(49).toString().isEmpty())
					|| (!map.get(p).get(146).toString().isEmpty())//was 143
					|| (!map.get(p).get(148).toString().isEmpty())//was 145
					|| (!map.get(p).get(149).toString().isEmpty())) {//was 146
				tempDocument = eventFactory.createStartElement("", "",
						"PmtTpInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				// check
				childNodes = new LinkedHashMap<String, String>();
				if ((!map.get(p).get(146).toString().isEmpty())) {//was 143
					childNodes.put("InstrPrty", map.get(p).get(146).toString());//was 143
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(146));//was 143
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "InstrPrty", 146,//was 143
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}
				// check
				if ((!map.get(p).get(48).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"SvcLvl");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");

					childNodes.put("Cd", map.get(p).get(48).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(48));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Cd", 48, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"SvcLvl");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				if ((!map.get(p).get(49).toString().isEmpty())
						|| !(map.get(p).get(148).toString().isEmpty())) {//was 145
					tempDocument = eventFactory.createStartElement("", "",
							"LclInstrm");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					if (!map.get(p).get(49).toString().isEmpty()) {
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Cd", map.get(p).get(49).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(49));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Cd", 49,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
					}
					if (!(map.get(p).get(148).toString().isEmpty())) {//was 145
						childNodes = new LinkedHashMap<String, String>();
						tab = eventFactory.createDTD("\t");
						childNodes.put("Prtry", map.get(p).get(148).toString());//was 145
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(148));//was 145
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Prtry", 148,//was 145
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
					}
					EndDocument = eventFactory.createEndElement("", "",
							"LclInstrm");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				// Category Purpose Code Proprietary
				if ((!map.get(p).get(50).toString().isEmpty())
						|| (!map.get(p).get(149).toString().isEmpty())) {//was 146
					tempDocument = eventFactory.createStartElement("", "",
							"CtgyPurp");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					if (!map.get(p).get(149).toString().isEmpty()) {//was 146
						childNodes = new LinkedHashMap<String, String>();
						tab = eventFactory.createDTD("\t");
						childNodes.put("Cd", map.get(p).get(149).toString());//was 146

						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(149));//was 146
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Cd", 149,//was 146
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
					}
					if ((!map.get(p).get(50).toString().isEmpty())) {
						childNodes = new LinkedHashMap<String, String>();
						tab = eventFactory.createDTD("\t");
						childNodes.put("Prtry", map.get(p).get(50).toString());

						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(50));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Prtry", 50,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
					}
					EndDocument = eventFactory.createEndElement("", "",
							"CtgyPurp");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "PmtTpInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

			}
			// Amout
			XMLEvent xmlDocs;
			Characters characters;
			if (!map.get(p).get(51).toString().isEmpty()) {

				tempDocument = eventFactory.createStartElement("", "", "Amt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				// BAD_FILES_CHANGES STARTS
				if (rowCol != null) {
					tempDocument = eventFactory.createStartElement("", "",
							"InstdAmt");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlDocs = eventFactory.createAttribute("Ccy", map.get(p)
							.get(51).toString());
					xmlEventWriter.add(xmlDocs);

					characters = eventFactory.createCharacters(map.get(p)
							.get(52).toString());
					xmlEventWriter.add(characters);
					EndDocument = eventFactory.createEndElement("", "",
							"InstdAmt");
					EndDocument.asEndElement();

					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(52));
				} else {

					// childNodes =
					// applyRule(ruleRowNo,ruleColNo,p,ruleName,childNodes,"InstdAmt",47);
					if (p == ruleRowNo && ruleColNo == 52) {
						if ("D".equals(ruleName)) {
							tempDocument = eventFactory.createStartElement("",
									"", "InstdAmt");
							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							xmlDocs = eventFactory.createAttribute("Ccy", map
									.get(p).get(51).toString());
							xmlEventWriter.add(xmlDocs);

							characters = eventFactory.createCharacters(map
									.get(p).get(52).toString());
							xmlEventWriter.add(characters);

							EndDocument = eventFactory.createEndElement("", "",
									"InstdAmt");
							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

							tempDocument = eventFactory.createStartElement("",
									"", "InstdAmt");
							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							xmlDocs = eventFactory.createAttribute("Ccy", map
									.get(p).get(51).toString());
							xmlEventWriter.add(xmlDocs);

							characters = eventFactory.createCharacters(map
									.get(p).get(52).toString());
							xmlEventWriter.add(characters);

							EndDocument = eventFactory.createEndElement("", "",
									"InstdAmt");
							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

						} else if ("M".equals(ruleName)) {

						} else if ("I".equals(ruleName)) {
							tempDocument = eventFactory.createStartElement("",
									"", "InstdAmt" + invalidAppender);
							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							xmlDocs = eventFactory.createAttribute("Ccy", map
									.get(p).get(51).toString());
							xmlEventWriter.add(xmlDocs);

							characters = eventFactory.createCharacters(map
									.get(p).get(52).toString());
							xmlEventWriter.add(characters);
							EndDocument = eventFactory.createEndElement("", "",
									"InstdAmt" + invalidAppender);
							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

						}
					} else {
						tempDocument = eventFactory.createStartElement("", "",
								"InstdAmt");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlDocs = eventFactory.createAttribute("Ccy", map
								.get(p).get(51).toString());
						xmlEventWriter.add(xmlDocs);

						characters = eventFactory.createCharacters(map.get(p)
								.get(52).toString());
						xmlEventWriter.add(characters);

						EndDocument = eventFactory.createEndElement("", "",
								"InstdAmt");
						EndDocument.asEndElement();

						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}
					// pending
				}
				// BAD_FILES_CHANGES ENDS

				EndDocument = eventFactory.createEndElement("", "", "Amt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

			} else {
				if (!map.get(p).get(53).toString().isEmpty()) {
					tempDocument = eventFactory.createStartElement("", "",
							"Amt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					tempDocument = eventFactory.createStartElement("", "",
							"EqvtAmt");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					// BAD_FILES_CHANGES STARTS
					if (rowCol != null) {
						tempDocument = eventFactory.createStartElement("", "",
								"Amt");
						tabMarked(xmlEventWriter, 6);
						xmlEventWriter.add(tempDocument);
						xmlDocs = eventFactory.createAttribute("Ccy", map
								.get(p).get(54).toString());
						xmlEventWriter.add(xmlDocs);
						characters = eventFactory.createCharacters(map.get(p)
								.get(53).toString());
						xmlEventWriter.add(characters);

						EndDocument = eventFactory.createEndElement("", "",
								"Amt");
						EndDocument.asEndElement();

						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);

						rowCol.add(String.valueOf(p) + "~" + String.valueOf(53));
					} else {

						// childNodes =
						// applyRule(ruleRowNo,ruleColNo,p,ruleName,childNodes,"Prtry",47);
						if (p == ruleRowNo && ruleColNo == 53) {
							if ("D".equals(ruleName)) {
								tempDocument = eventFactory.createStartElement(
										"", "", "Amt");
								tabMarked(xmlEventWriter, 6);
								xmlEventWriter.add(tempDocument);
								xmlDocs = eventFactory.createAttribute("Ccy",
										map.get(p).get(54).toString());
								xmlEventWriter.add(xmlDocs);
								characters = eventFactory.createCharacters(map
										.get(p).get(53).toString());
								xmlEventWriter.add(characters);

								EndDocument = eventFactory.createEndElement("",
										"", "Amt");
								EndDocument.asEndElement();

								xmlEventWriter.add(EndDocument);
								xmlEventWriter.add(end);

								tempDocument = eventFactory.createStartElement(
										"", "", "Amt");
								tabMarked(xmlEventWriter, 6);
								xmlEventWriter.add(tempDocument);
								xmlDocs = eventFactory.createAttribute("Ccy",
										map.get(p).get(54).toString());
								xmlEventWriter.add(xmlDocs);
								characters = eventFactory.createCharacters(map
										.get(p).get(53).toString());
								xmlEventWriter.add(characters);

								EndDocument = eventFactory.createEndElement("",
										"", "Amt");
								EndDocument.asEndElement();

								xmlEventWriter.add(EndDocument);
								xmlEventWriter.add(end);

							} else if ("M".equals(ruleName)) {

							} else if ("I".equals(ruleName)) {

								tempDocument = eventFactory.createStartElement(
										"", "", "Amt" + invalidAppender);
								tabMarked(xmlEventWriter, 6);
								xmlEventWriter.add(tempDocument);
								xmlDocs = eventFactory.createAttribute("Ccy",
										map.get(p).get(54).toString());
								xmlEventWriter.add(xmlDocs);

								characters = eventFactory.createCharacters(map
										.get(p).get(53).toString());
								xmlEventWriter.add(characters);
								EndDocument = eventFactory.createEndElement("",
										"", "Amt" + invalidAppender);
								EndDocument.asEndElement();

								xmlEventWriter.add(EndDocument);
								xmlEventWriter.add(end);

							}
						} else {
							tempDocument = eventFactory.createStartElement("",
									"", "Amt");
							tabMarked(xmlEventWriter, 6);
							xmlEventWriter.add(tempDocument);
							xmlDocs = eventFactory.createAttribute("Ccy", map
									.get(p).get(54).toString());
							xmlEventWriter.add(xmlDocs);
							characters = eventFactory.createCharacters(map
									.get(p).get(53).toString());
							xmlEventWriter.add(characters);

							EndDocument = eventFactory.createEndElement("", "",
									"Amt");
							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

						}
						// pending
					}
					// BAD_FILES_CHANGES ENDS

					childNodes = new LinkedHashMap<String, String>();
					tab = eventFactory.createDTD("\t");
					childNodes.put("CcyOfTrf", map.get(p).get(55).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(55));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "CcyOfTrf", 55,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"EqvtAmt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory.createEndElement("", "", "Amt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

				}
			}
			// XchgRateInf
			if ((!map.get(p).get(56).toString().isEmpty())
					|| (!map.get(p).get(57).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"XchgRateInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();
				tab = eventFactory.createDTD("\t");
				if ((!map.get(p).get(57).toString().isEmpty())) {
					childNodes.put("XchgRate", map.get(p).get(57).toString()); // change
																				// 0803
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(57));// change
																					// 0803
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "XchgRate", 57,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}
				if ((!map.get(p).get(56).toString().isEmpty())) {
					childNodes.put("CtrctId", map.get(p).get(56).toString()); // change
																				// 0803
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(56));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "CtrctId", 56,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}

				EndDocument = eventFactory.createEndElement("", "",
						"XchgRateInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// ChrgBr

			if ((!map.get(p).get(58).toString().isEmpty())) {
				childNodes = new LinkedHashMap<String, String>();
				tab = eventFactory.createDTD("\t");
				childNodes.put("ChrgBr", map.get(p).get(58).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(58));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "ChrgBr", 58, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS

				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 4);
				}
			}
			// check remit
			// ChqInstr
			if ((!map.get(p).get(59).toString().isEmpty())
					|| (!map.get(p).get(60).toString().isEmpty())
					|| (!map.get(p).get(61).toString().isEmpty())
					|| (!map.get(p).get(62).toString().isEmpty())
					|| (!map.get(p).get(63).toString().isEmpty())
					|| (!map.get(p).get(64).toString().isEmpty())
					|| (!map.get(p).get(65).toString().isEmpty())
					|| (!map.get(p).get(66).toString().isEmpty())
					|| (!map.get(p).get(67).toString().isEmpty())
					|| (!map.get(p).get(68).toString().isEmpty())
					|| (!map.get(p).get(69).toString().isEmpty())
					|| (!map.get(p).get(70).toString().isEmpty())
					|| (!map.get(p).get(71).toString().isEmpty())
					|| (!map.get(p).get(72).toString().isEmpty())
					|| (!map.get(p).get(73).toString().isEmpty())
					|| (!map.get(p).get(74).toString().isEmpty())
					|| (!map.get(p).get(75).toString().isEmpty())
					|| (!map.get(p).get(76).toString().isEmpty())
					|| (!map.get(p).get(77).toString().isEmpty())
					|| (!map.get(p).get(78).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"ChqInstr");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if (!map.get(p).get(59).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("ChqTp", map.get(p).get(59).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(59));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "ChqTp", 59,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}
				if (!map.get(p).get(60).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("ChqNb", map.get(p).get(60).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(60));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "ChqNb", 60,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}
				tempDocument = eventFactory.createStartElement("", "", "ChqFr");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				// if part

				if (!map.get(p).get(61).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(61).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(61));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 61, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}

				if (!map.get(p).get(62).toString().isEmpty()
						|| !map.get(p).get(63).toString().isEmpty()
						|| !map.get(p).get(64).toString().isEmpty()
						|| !map.get(p).get(65).toString().isEmpty()
						|| !map.get(p).get(66).toString().isEmpty()
						|| !map.get(p).get(67).toString().isEmpty()) {

					tempDocument = eventFactory.createStartElement("", "",
							"Adr");
					tabMarked(xmlEventWriter, 7);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("StrtNm", map.get(p).get(62).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(62));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "StrtNm", 62,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					childNodes.put("BldgNb", map.get(p).get(63).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(63));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "BldgNb", 63,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					childNodes.put("PstCd", map.get(p).get(64).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(64));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "PstCd", 64,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					childNodes.put("TwnNm", map.get(p).get(65).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(65));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "TwnNm", 65,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					childNodes
							.put("CtrySubDvsn", map.get(p).get(66).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(66));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "CtrySubDvsn", 66,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					childNodes.put("Ctry", map.get(p).get(67).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(67));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Ctry", 67,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 8);
					}
					EndDocument = eventFactory.createEndElement("", "", "Adr");
					tabMarked(xmlEventWriter, 7);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "ChqFr");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				if (!map.get(p).get(68).toString().isEmpty()
						|| !map.get(p).get(69).toString().isEmpty()) {

					tempDocument = eventFactory.createStartElement("", "",
							"DlvryMtd");
					tabMarked(xmlEventWriter, 7);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					if (!map.get(p).get(68).toString().isEmpty()) {
						childNodes.put("Cd", map.get(p).get(68).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(68));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Cd", 68,
									xmlEventWriter);
						}
					}
					// BAD_FILES_CHANGES ENDS
					if (!map.get(p).get(69).toString().isEmpty()) { // condition
																	// for
																	// DlvryMtd
						childNodes.put("Prtry", map.get(p).get(69).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(69));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Prtry", 69,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS

					}

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 8);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"DlvryMtd");
					tabMarked(xmlEventWriter, 7);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				if (!map.get(p).get(70).toString().isEmpty()
						|| !map.get(p).get(71).toString().isEmpty()
						|| !map.get(p).get(72).toString().isEmpty()
						|| !map.get(p).get(73).toString().isEmpty()
						|| !map.get(p).get(74).toString().isEmpty()
						|| !map.get(p).get(75).toString().isEmpty()
						|| !map.get(p).get(76).toString().isEmpty()) {

					tempDocument = eventFactory.createStartElement("", "",
							"DlvrTo");
					tabMarked(xmlEventWriter, 7);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(70).toString());

					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(70));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 70, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 8);
					}

					if (!map.get(p).get(71).toString().isEmpty()) {

						tempDocument = eventFactory.createStartElement("", "",
								"Adr");
						tabMarked(xmlEventWriter, 7);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("StrtNm", map.get(p).get(71).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(71));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "StrtNm", 71,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						childNodes.put("BldgNb", map.get(p).get(72).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(72));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "BldgNb", 72,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						childNodes.put("PstCd", map.get(p).get(73).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(73));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "PstCd", 73,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						childNodes.put("TwnNm", map.get(p).get(74).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(74));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "TwnNm", 74,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						childNodes.put("CtrySubDvsn", map.get(p).get(75)
								.toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(75));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "CtrySubDvsn", 75,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						childNodes.put("Ctry", map.get(p).get(76).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(76));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Ctry", 76,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS

						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 8);
						}
						EndDocument = eventFactory.createEndElement("", "",
								"Adr");
						tabMarked(xmlEventWriter, 7);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"DlvrTo");
					tabMarked(xmlEventWriter, 7);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				if (!map.get(p).get(77).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("MemoFld", map.get(p).get(77).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(77));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "MemoFld", 77,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}
				if (!map.get(p).get(78).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes
							.put("RgnlClrZone", map.get(p).get(78).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(78));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "RgnlClrZone", 78,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}
				EndDocument = eventFactory.createEndElement("", "", "ChqInstr");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// end check remit

			// IntrmyAgt1
			if ((!map.get(p).get(79).toString().isEmpty())
					|| (!map.get(p).get(80).toString().isEmpty())
					|| (!map.get(p).get(81).toString().isEmpty())
					|| (!map.get(p).get(82).toString().isEmpty())
					|| (!map.get(p).get(83).toString().isEmpty())
					|| (!map.get(p).get(84).toString().isEmpty())
					|| (!map.get(p).get(85).toString().isEmpty())
					|| (!map.get(p).get(86).toString().isEmpty())
					|| (!map.get(p).get(87).toString().isEmpty())
					|| (!map.get(p).get(88).toString().isEmpty())
					|| (!map.get(p).get(89).toString().isEmpty())
					|| (!map.get(p).get(90).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"IntrmyAgt1");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				tempDocument = eventFactory.createStartElement("", "",
						"FinInstnId");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				// if part

				if (!map.get(p).get(79).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("BIC", map.get(p).get(79).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(79));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "BIC", 79, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}
				// else part
				if (!map.get(p).get(80).toString().isEmpty()
						|| !map.get(p).get(81).toString().isEmpty()) { // fixed
																		// issue
																		// 36 as
																		// fgt
																		// defect
																		// tracker
					tempDocument = eventFactory.createStartElement("", "",
							"ClrSysMmbId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					tempDocument = eventFactory.createStartElement("", "",
							"ClrSysId");
					tabMarked(xmlEventWriter, 7);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Cd", map.get(p).get(80).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(80));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Cd", 80, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 8);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"ClrSysId");
					tabMarked(xmlEventWriter, 7);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("MmbId", map.get(p).get(81).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(81));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "MmbId", 81,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"ClrSysMmbId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				// end else part
				if ((!map.get(p).get(82).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(82).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(82));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 82, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}

				if ((!map.get(p).get(83).toString().isEmpty())
						|| (!map.get(p).get(84).toString().isEmpty())
						|| (!map.get(p).get(85).toString().isEmpty())
						|| (!map.get(p).get(66).toString().isEmpty())
						|| (!map.get(p).get(87).toString().isEmpty())
						|| (!map.get(p).get(88).toString().isEmpty())) {

					tempDocument = eventFactory.createStartElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					if (!map.get(p).get(83).toString().isEmpty()) {
						childNodes.put("StrtNm", map.get(p).get(83).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(83));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "StrtNm", 83,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(84).toString().isEmpty()) {
						childNodes.put("BldgNb", map.get(p).get(84).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(84));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "BldgNb", 84,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(85).toString().isEmpty()) {
						childNodes.put("PstCd", map.get(p).get(85).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(85));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "PstCd", 85,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(86).toString().isEmpty()) {
						childNodes.put("TwnNm", map.get(p).get(86).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(86));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "TwnNm", 86,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(87).toString().isEmpty()) {
						childNodes.put("CtrySubDvsn", map.get(p).get(87)
								.toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(87));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "CtrySubDvsn", 87,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(88).toString().isEmpty()) {
						childNodes.put("Ctry", map.get(p).get(88).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(88));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Ctry", 88,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				if ((!map.get(p).get(89).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Id", map.get(p).get(89).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(89));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 89, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}

					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				EndDocument = eventFactory.createEndElement("", "",
						"FinInstnId");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				if ((!map.get(p).get(90).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"BrnchId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Id", map.get(p).get(90).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(90));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 90, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"BrnchId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				EndDocument = eventFactory.createEndElement("", "",
						"IntrmyAgt1");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// IntrmyAgt1Acct
			if (!map.get(p).get(91).toString().isEmpty()
					|| (!map.get(p).get(92).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"IntrmyAgt1Acct");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				tempDocument = eventFactory.createStartElement("", "", "Id");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				// if part
				if (!map.get(p).get(91).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("IBAN", map.get(p).get(91).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(91));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "IBAN", 91,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}
				tempDocument = eventFactory.createStartElement("", "", "Othr");
				tabMarked(xmlEventWriter, 6);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Id", map.get(p).get(92).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(92));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Id", 92, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 7);
				}

				EndDocument = eventFactory.createEndElement("", "", "Othr");
				tabMarked(xmlEventWriter, 6);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				// }
				// end else part
				EndDocument = eventFactory.createEndElement("", "", "Id");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				EndDocument = eventFactory.createEndElement("", "",
						"IntrmyAgt1Acct");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// end new data

			// CdtrAgt

			if ((!map.get(p).get(93).toString().isEmpty())
					|| (!map.get(p).get(94).toString().isEmpty())
					|| (!map.get(p).get(95).toString().isEmpty())
					|| (!map.get(p).get(96).toString().isEmpty())
					|| (!map.get(p).get(97).toString().isEmpty())
					|| (!map.get(p).get(98).toString().isEmpty())
					|| (!map.get(p).get(99).toString().isEmpty())
					|| (!map.get(p).get(100).toString().isEmpty())
					|| (!map.get(p).get(101).toString().isEmpty())
					|| (!map.get(p).get(102).toString().isEmpty())
					|| (!map.get(p).get(103).toString().isEmpty())
					|| (!map.get(p).get(104).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"CdtrAgt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				tempDocument = eventFactory.createStartElement("", "",
						"FinInstnId");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				// if part
				if (!map.get(p).get(93).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("BIC", map.get(p).get(93).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(93));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "BIC", 93, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}
				if (!map.get(p).get(94).toString().isEmpty()
						|| !map.get(p).get(95).toString().isEmpty()) { // fix
																		// for
																		// defect
																		// 36 as
																		// defect
																		// tracker
					// else part
					tempDocument = eventFactory.createStartElement("", "",
							"ClrSysMmbId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					if (!map.get(p).get(94).toString().isEmpty()) {
						tempDocument = eventFactory.createStartElement("", "",
								"ClrSysId");
						tabMarked(xmlEventWriter, 7);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);

						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Cd", map.get(p).get(94).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(94));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Cd", 94,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 8);
						}
						EndDocument = eventFactory.createEndElement("", "",
								"ClrSysId");
						tabMarked(xmlEventWriter, 7);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}

					if (!map.get(p).get(95).toString().isEmpty()) {
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("MmbId", map.get(p).get(95).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(95));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "MmbId", 95,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 7);
						}
					}
					EndDocument = eventFactory.createEndElement("", "",
							"ClrSysMmbId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				// end else part
				if ((!map.get(p).get(96).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(96).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(96));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 96, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}

				if ((!map.get(p).get(97).toString().isEmpty())
						|| (!map.get(p).get(98).toString().isEmpty())
						|| (!map.get(p).get(99).toString().isEmpty())
						|| (!map.get(p).get(100).toString().isEmpty())
						|| (!map.get(p).get(101).toString().isEmpty())
						|| (!map.get(p).get(102).toString().isEmpty())) {

					tempDocument = eventFactory.createStartElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					if (!map.get(p).get(97).toString().isEmpty()) {
						childNodes.put("StrtNm", map.get(p).get(97).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(97));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "StrtNm", 97,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(98).toString().isEmpty()) {
						childNodes.put("BldgNb", map.get(p).get(98).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(98));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "BldgNb", 98,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(99).toString().isEmpty()) {
						childNodes.put("PstCd", map.get(p).get(99).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(99));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "PstCd", 99,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(100).toString().isEmpty()) {
						childNodes.put("TwnNm", map.get(p).get(100).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(100));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "TwnNm", 100,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(101).toString().isEmpty()) {
						childNodes.put("CtrySubDvsn", map.get(p).get(101)
								.toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(101));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "CtrySubDvsn", 101,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(102).toString().isEmpty()) {
						childNodes.put("Ctry", map.get(p).get(102).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(102));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Ctry", 102,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				if ((!map.get(p).get(103).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Id", map.get(p).get(103).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(103));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 103, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "",
						"FinInstnId");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				if ((!map.get(p).get(104).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"BrnchId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Id", map.get(p).get(104).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(104));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 104, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"BrnchId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "CdtrAgt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// Cdtr
			if ((!map.get(p).get(105).toString().isEmpty())
					|| (!map.get(p).get(106).toString().isEmpty())
					|| (!map.get(p).get(107).toString().isEmpty())
					|| (!map.get(p).get(108).toString().isEmpty())
					|| (!map.get(p).get(109).toString().isEmpty())
					|| (!map.get(p).get(110).toString().isEmpty())
					|| (!map.get(p).get(111).toString().isEmpty())) {

				tempDocument = eventFactory.createStartElement("", "", "Cdtr");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				if ((!map.get(p).get(105).toString().isEmpty())) {

					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(105).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(105));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 105, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}
				if ((!map.get(p).get(106).toString().isEmpty())
						|| (!map.get(p).get(107).toString().isEmpty())
						|| (!map.get(p).get(108).toString().isEmpty())
						|| (!map.get(p).get(109).toString().isEmpty())
						|| (!map.get(p).get(110).toString().isEmpty())
						|| (!map.get(p).get(111).toString().isEmpty())) {

					tempDocument = eventFactory.createStartElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();
					tab = eventFactory.createDTD("\t");
					if (!map.get(p).get(106).toString().isEmpty()) {
						childNodes
								.put("StrtNm", map.get(p).get(106).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(106));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "StrtNm", 106,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(107).toString().isEmpty()) {
						childNodes
								.put("BldgNb", map.get(p).get(107).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(107));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "BldgNb", 107,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(108).toString().isEmpty()) {
						childNodes.put("PstCd", map.get(p).get(108).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(108));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "PstCd", 108,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(109).toString().isEmpty()) {
						childNodes.put("TwnNm", map.get(p).get(109).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(109));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "TwnNm", 109,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(110).toString().isEmpty()) {
						childNodes.put("CtrySubDvsn", map.get(p).get(110)
								.toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(110));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "CtrySubDvsn", 110,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(111).toString().isEmpty()) {
						childNodes.put("Ctry", map.get(p).get(111).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(111));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Ctry", 111,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
//Jo-Column Number change starts from 114
				if ((!map.get(p).get(112).toString().isEmpty())
						|| (!map.get(p).get(113).toString().isEmpty())
						|| (!map.get(p).get(114).toString().isEmpty())//jo added 114
						) {

					tempDocument = eventFactory.createStartElement("", "",
							"CtctDtls");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();
					tab = eventFactory.createDTD("\t");
					if (!map.get(p).get(112).toString().isEmpty()) {
						childNodes.put("EmailAdr", map.get(p).get(112)
								.toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(112));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "EmailAdr", 112,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(113).toString().isEmpty()) {
						childNodes
								.put("MobNb", map.get(p).get(113).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(113));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "MobNb", 113,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					/*elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}*/
					
					//Jo-added new child node under contact details "Othr"
					if (!map.get(p).get(114).toString().isEmpty()) {
						childNodes
								.put("Othr", map.get(p).get(114).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(114));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Othr", 114,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
					//Jo-Changes end here 
					EndDocument = eventFactory.createEndElement("", "",
							"CtctDtls");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "Cdtr");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// cdtr account
			if ((!map.get(p).get(115).toString().isEmpty())//was 114
					|| (!map.get(p).get(116).toString().isEmpty())) {//was 115
				tempDocument = eventFactory.createStartElement("", "",
						"CdtrAcct");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				tempDocument = eventFactory.createStartElement("", "", "Id");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				// if part
				if (!map.get(p).get(115).toString().isEmpty()) {//was 114
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("IBAN", map.get(p).get(115).toString());//was 114
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(115));//was 114
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "IBAN", 115,//was 114
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				} else if ((!map.get(p).get(116).toString().isEmpty())) {//was 115
					// else part
					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Id", map.get(p).get(116).toString());//was 115
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(116));//was 115
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 116, xmlEventWriter);//was 115
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}
					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				// end else part
				EndDocument = eventFactory.createEndElement("", "", "Id");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				// ACH Transaction Code
				if (!map.get(p).get(117).toString().isEmpty()) {//was 116
					tempDocument = eventFactory
							.createStartElement("", "", "Tp");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					if ((!map.get(p).get(117).toString().isEmpty())) {//was 116
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Prtry", map.get(p).get(117).toString());//was 116

						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(117));//was 116
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Prtry", 117,//was 116
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS

						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
					}
					EndDocument = eventFactory.createEndElement("", "", "Tp");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "CdtrAcct");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// Transaction Purpose Code

			if (!map.get(p).get(118).toString().isEmpty()) {//was 117
				tempDocument = eventFactory.createStartElement("", "", "Purp");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Prtry", map.get(p).get(118).toString());//was 117
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(118));//was 117
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Prtry", 118, xmlEventWriter);//was 117
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}

				EndDocument = eventFactory.createEndElement("", "", "Purp");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

			}

			// remit check
	/*		if (!map.get(p).get(118).toString().isEmpty()
					|| !map.get(p).get(119).toString().isEmpty()
					|| !map.get(p).get(120).toString().isEmpty()
					|| !map.get(p).get(121).toString().isEmpty()
					|| !map.get(p).get(122).toString().isEmpty()
					|| !map.get(p).get(123).toString().isEmpty()) {*/
			if ((!map.get(p).get(119).toString().isEmpty())) {//was 118
				tempDocument = eventFactory.createStartElement("", "",
						"InstrForCdtrAgt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				childNodes = new LinkedHashMap<String, String>();
				if ((!map.get(p).get(119).toString().isEmpty())) {//was 118
					tab = eventFactory.createDTD("\t");
					childNodes.put("InstrInf", map.get(p).get(119).toString());//was 118
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(119));//was 118
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "InstrInf", 119,//was 118
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}
				//ADD END STATEMENT  FOR INSTRFORCDTR
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}
				
				EndDocument = eventFactory.createEndElement("", "",
						"InstrForCdtrAgt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
				
				if ((!map.get(p).get(120).toString().isEmpty())) {//was 119
					//JO ADDED HERE
					tempDocument = eventFactory.createStartElement("", "",
							"InstrForCdtrAgt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();
					///
					tab = eventFactory.createDTD("\t");
					childNodes.put("InstrInf", map.get(p).get(120).toString());//was 119
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(120));//was 119
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "InstrInf", 120,//was 119
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					//ADD END STATEMENT  FOR INSTRFORCDTR 2
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
					
					EndDocument = eventFactory.createEndElement("", "",
							"InstrForCdtrAgt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
					
				}
				
				
				
				if ((!map.get(p).get(121).toString().isEmpty())) {//was 120
					//JO ADDED HERE
					tempDocument = eventFactory.createStartElement("", "",
							"InstrForCdtrAgt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();
					///
					tab = eventFactory.createDTD("\t");
					childNodes.put("InstrInf", map.get(p).get(121).toString());//was 120
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(121));//was 120
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "InstrInf", 121,//was 120
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				//}
				
				//ADD END STATEMENT  FOR INSTRFORCDTR 3
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}
				
				EndDocument = eventFactory.createEndElement("", "",
						"InstrForCdtrAgt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				
				}
				if ((!map.get(p).get(122).toString().isEmpty())) {//was 121
					//JO ADDED HERE3
					tempDocument = eventFactory.createStartElement("", "",
							"InstrForCdtrAgt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					///
					childNodes = new LinkedHashMap<String, String>();
					tab = eventFactory.createDTD("\t");
					childNodes.put("InstrInf", map.get(p).get(122).toString());//was 121
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(122));//was 121
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "InstrInf", 122,//was 121
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				//}
				//ADD END STATEMENT  FOR INSTRFORCDTR 4
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}
				
				EndDocument = eventFactory.createEndElement("", "",
						"InstrForCdtrAgt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				
				}
				if ((!map.get(p).get(123).toString().isEmpty())) {//was 122
					//JO ADDED HERE4
					tempDocument = eventFactory.createStartElement("", "",
							"InstrForCdtrAgt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();
					///
					tab = eventFactory.createDTD("\t");
					childNodes.put("InstrInf", map.get(p).get(123).toString());//was 122
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(123));//was 122
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "InstrInf", 123,//was 122
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				//}
				//ADD END STATEMENT  FOR INSTRFORCDTR 5
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}
				
				EndDocument = eventFactory.createEndElement("", "",
						"InstrForCdtrAgt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			
				}
				if ((!map.get(p).get(124).toString().isEmpty())) {//was 123
					
					//JO ADDED HERE5
					tempDocument = eventFactory.createStartElement("", "",
							"InstrForCdtrAgt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();
					///
					
					
					tab = eventFactory.createDTD("\t");
					childNodes.put("InstrInf", map.get(p).get(124).toString());//was 123
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(124));//was 123
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "InstrInf", 124,//was 123
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				
				
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}
				EndDocument = eventFactory.createEndElement("", "",
						"InstrForCdtrAgt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
				
			//added two extra 
if ((!map.get(p).get(125).toString().isEmpty())) {
					
					//JO ADDED HERE5
					tempDocument = eventFactory.createStartElement("", "",
							"InstrForCdtrAgt");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();
					///
					
					
					tab = eventFactory.createDTD("\t");
					childNodes.put("InstrInf", map.get(p).get(125).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(125));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "InstrInf", 125,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				
				
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}
				EndDocument = eventFactory.createEndElement("", "",
						"InstrForCdtrAgt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			

			//8th
if ((!map.get(p).get(126).toString().isEmpty())) {
	
	//JO ADDED HERE5
	tempDocument = eventFactory.createStartElement("", "",
			"InstrForCdtrAgt");
	tabMarked(xmlEventWriter, 4);
	xmlEventWriter.add(tempDocument);
	xmlEventWriter.add(end);
	childNodes = new LinkedHashMap<String, String>();
	///
	
	
	tab = eventFactory.createDTD("\t");
	childNodes.put("InstrInf", map.get(p).get(126).toString());
	// BAD_FILES_CHANGES STARTS
	if (rowCol != null)
		rowCol.add(String.valueOf(p) + "~"
				+ String.valueOf(126));
	else {

		childNodes = applyRule(ruleRowNo, ruleColNo, p,
				ruleName, childNodes, "InstrInf", 126,
				xmlEventWriter);
	}
	// BAD_FILES_CHANGES ENDS


elementNodes = childNodes.keySet();
for (String key : elementNodes) {
	createNode(xmlEventWriter, key, childNodes.get(key), 5);
}
EndDocument = eventFactory.createEndElement("", "",
		"InstrForCdtrAgt");
tabMarked(xmlEventWriter, 4);
xmlEventWriter.add(EndDocument);
xmlEventWriter.add(end);
}

				
		  //added colums code ends here
			
			// InstrForDbtrAgt
			if ((!map.get(p).get(127).toString().isEmpty())) {//was 124
				childNodes = new LinkedHashMap<String, String>();
				tab = eventFactory.createDTD("\t");
				childNodes.put("InstrForDbtrAgt", map.get(p).get(127)//was 124
						.toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(127));//was 124
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "InstrForDbtrAgt", 127, xmlEventWriter);//was 124
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 4);
				}
			}

			if (!map.get(p).get(128).toString().isEmpty()//was 125
					|| !map.get(p).get(129).toString().isEmpty()//was 126
					|| !map.get(p).get(130).toString().isEmpty()//was 127
					|| !map.get(p).get(131).toString().isEmpty()//was 128
					|| !map.get(p).get(132).toString().isEmpty()//was 129
					|| !map.get(p).get(133).toString().isEmpty()//was 130
					|| !map.get(p).get(134).toString().isEmpty()//was 131
					|| !map.get(p).get(135).toString().isEmpty()//was 132
					|| !map.get(p).get(136).toString().isEmpty()//was 133
					|| !map.get(p).get(137).toString().isEmpty()) {//was 134
				tempDocument = eventFactory.createStartElement("", "",
						"RltdRmtInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if ((!map.get(p).get(128).toString().isEmpty())) {//was 125
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("RmtId", map.get(p).get(128).toString());//was 125
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(128));//was 125
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "RmtId", 128,//was 125
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					childNodes
							.put("RmtLctnMtd", map.get(p).get(129).toString());//was 126
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(129));//was 125
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "RmtLctnMtd", 129,//was 126
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					childNodes.put("RmtLctnElctrncAdr", map.get(p).get(130)//was 127
							.toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(130));//was 127
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "RmtLctnElctrncAdr", 130,//was 127
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}
				if (!map.get(p).get(131).toString().isEmpty()) {//was 128
					tempDocument = eventFactory.createStartElement("", "",
							"RmtLctnPstlAdr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					// if ((!map.get(p).get(135).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(131).toString());//was 128
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(131));//was 128
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 131, xmlEventWriter);//was 128
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
					// }
					if ((!map.get(p).get(132).toString().isEmpty())//was 129
							|| (!map.get(p).get(133).toString().isEmpty())//was 130
							|| (!map.get(p).get(134).toString().isEmpty())//was 131
							|| (!map.get(p).get(135).toString().isEmpty())//was 132
							|| (!map.get(p).get(136).toString().isEmpty())//was 133
							|| (!map.get(p).get(137).toString().isEmpty())) {//was 134

						tempDocument = eventFactory.createStartElement("", "",
								"Adr");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlEventWriter.add(end);
						childNodes = new LinkedHashMap<String, String>();
						tab = eventFactory.createDTD("\t");
						if (!map.get(p).get(132).toString().isEmpty()) {//was 129
							childNodes.put("StrtNm", map.get(p).get(132)//was 129
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(132));//was 129
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "StrtNm", 132,//was 129
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(133).toString().isEmpty()) {//was 130
							childNodes.put("BldgNb", map.get(p).get(133)//was 130
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(133));//was 130
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "BldgNb", 133,//was 130
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(134).toString().isEmpty()) {//was 131
							childNodes.put("PstCd", map.get(p).get(134)//was 131
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(134));//was 131
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "PstCd", 134,//was 131
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(135).toString().isEmpty()) {//was 132
							childNodes.put("TwnNm", map.get(p).get(135)//was 132
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(135));//was 132
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "TwnNm", 135,//was 132
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(136).toString().isEmpty()) {//was 133
							childNodes.put("CtrySubDvsn", map.get(p).get(136)//was 133
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(136));//was 133
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "CtrySubDvsn",
										136, xmlEventWriter);//was 133
							}
							// BAD_FILES_CHANGES ENDS
						}
						if (!map.get(p).get(137).toString().isEmpty()) {//was 134
							childNodes.put("Ctry", map.get(p).get(137)//was 134
									.toString());
							// BAD_FILES_CHANGES STARTS
							if (rowCol != null)
								rowCol.add(String.valueOf(p) + "~"
										+ String.valueOf(137));//was 134
							else {

								childNodes = applyRule(ruleRowNo, ruleColNo, p,
										ruleName, childNodes, "Ctry", 137,//was 134
										xmlEventWriter);
							}
							// BAD_FILES_CHANGES ENDS
						}
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 6);
						}
						EndDocument = eventFactory.createEndElement("", "",
								"Adr");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"RmtLctnPstlAdr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				EndDocument = eventFactory.createEndElement("", "",
						"RltdRmtInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// end remit check
			// Remittance Information
			if (!map.get(p).get(138).toString().isEmpty()) {//was 135
				tempDocument = eventFactory
						.createStartElement("", "", "RmtInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if ((!map.get(p).get(138).toString().isEmpty())) {//was 135
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Ustrd", map.get(p).get(138).toString());//was 135
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~"
								+ String.valueOf(138));//was 135
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Ustrd", 138,//was 135
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}

				// if condition add
				if ((!map.get(p).get(139).toString().isEmpty())) {//was 136

					// hellooo
					int p2 = p + 1;
					int count = 1;
					int p3 = 0;
					int flag = 0;
					int newp = p;
					while (p2 < (map.size() + 7)) {
						if (map.get(p2).get(46).toString().isEmpty()) {//no change
							if (map.get(p2).get(139).toString().isEmpty()//was 136
									&& (!map.get(p2).get(140).toString()//was 137
											.isEmpty()
											|| !map.get(p2).get(141).toString()//was 138
													.isEmpty() || !map.get(p2)
											.get(142).toString().isEmpty())//was 139
									|| !map.get(p2).get(143).toString()//was 140
											.isEmpty()) {
								count++;
								flag = 1;
							} else if (flag == 0
									&& !map.get(p2).get(139).toString()//was 136
											.isEmpty()) {
								for (int i = 0; i < count; i++) {
									strdEntry(tempDocument, EndDocument,
											xmlEventWriter, eventFactory, end,
											childNodes, elementNodes, tab,
											newp, batchCount, i, count, rowCol,
											ruleName, ruleRowNo, ruleColNo);

									newp++;
								}
							} else if (flag == 1
									&& !map.get(p2).get(139).toString()//was 136
											.isEmpty()) {
								for (int i = 0; i < count; i++) {
									strdEntry(tempDocument, EndDocument,
											xmlEventWriter, eventFactory, end,
											childNodes, elementNodes, tab,
											newp, batchCount, i, count, rowCol,
											ruleName, ruleRowNo, ruleColNo);

									newp++;
								}
								flag = 0;
								count = 1;
							}
						} else {
							for (int i = 0; i < count; i++) {
								strdEntry(tempDocument, EndDocument,
										xmlEventWriter, eventFactory, end,
										childNodes, elementNodes, tab, newp,
										batchCount, i, count, rowCol, ruleName,
										ruleRowNo, ruleColNo);

								newp++;
							}
							flag = 2;
							break;
						}
						p2 = p2 + 1;
					}
					if (flag != 2)
						for (int i = 0; i < count; i++) {
							strdEntry(tempDocument, EndDocument,
									xmlEventWriter, eventFactory, end,
									childNodes, elementNodes, tab, newp,
									batchCount, i, count, rowCol, ruleName,
									ruleRowNo, ruleColNo);

							newp++;
						}
				}
				EndDocument = eventFactory.createEndElement("", "", "RmtInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// }

			// end CdtTrfTxInf
			for (int i = 46; i <= 146; i++) {//was int i = 46; i <= 143; i++

				if ((!map.get(p).get(i).toString().isEmpty())) {
					EndDocument = eventFactory.createEndElement("", "",
							"CdtTrfTxInf");
					tabMarked(xmlEventWriter, 3);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
					break;
				}
			}
		} catch (XMLStreamException e) {
			e.printStackTrace();
			throw new XMLStreamException();
		}
		if (log.isDebugEnabled()) {
			log.debug("createTransactionNode method ends in ConvertFileISOPain001 class");
		}
	}

	public void strdEntry(StartElement tempDocument, EndElement EndDocument,
			XMLEventWriter xmlEventWriter, XMLEventFactory eventFactory,
			XMLEvent end, Map<String, String> childNodes,
			Set<String> elementNodes, XMLEvent tab, int p, int batchCount,
			int i, int count, ArrayList rowCol, String ruleName, int ruleRowNo,
			int ruleColNo) throws XMLStreamException {
		if (!map.get(p).get(139).toString().isEmpty()//was 136
				&& map.get(p).get(139).toString().equals("Strd")) {//was 136
			tempDocument = eventFactory.createStartElement("", "", "Strd");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);

		}
		if (!map.get(p).get(140).toString().isEmpty()) {//was 137
			tempDocument = eventFactory
					.createStartElement("", "", "RfrdDocInf");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			tempDocument = eventFactory.createStartElement("", "", "Tp");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			tempDocument = eventFactory.createStartElement("", "", "CdOrPrtry");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			if ((!map.get(p).get(140).toString().isEmpty())) {//was 137
				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Prtry", map.get(p).get(140).toString());//was 137
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(140));//was 137
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Prtry", 140, xmlEventWriter);//was 137
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}
			}
			EndDocument = eventFactory.createEndElement("", "", "CdOrPrtry");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
			EndDocument = eventFactory.createEndElement("", "", "Tp");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
			if ((!map.get(p).get(141).toString().isEmpty())) {//was 138
				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Nb", map.get(p).get(141).toString());//was 138
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(141));//was 138
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Nb", 141, xmlEventWriter);//was 138
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}
			}
			if ((!map.get(p).get(142).toString().isEmpty())) {//was 139
				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("RltdDt", map.get(p).get(142).toString());//was 139
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(142));//was 139
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "RltdDt", 142, xmlEventWriter);//was 139
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}
			}
			EndDocument = eventFactory.createEndElement("", "", "RfrdDocInf");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
		}

		// etransfer
		if ((!map.get(p).get(143).toString().isEmpty())) {//was 140
			childNodes = new LinkedHashMap<String, String>();

			tab = eventFactory.createDTD("\t");
			childNodes.put("AddtlRmtInf", map.get(p).get(143).toString());//was 140
			// BAD_FILES_CHANGES STARTS
			if (rowCol != null)
				rowCol.add(String.valueOf(p) + "~" + String.valueOf(143));//was 140
			else {

				childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
						childNodes, "AddtlRmtInf", 143, xmlEventWriter);//was 140
			}
			// BAD_FILES_CHANGES ENDS
			elementNodes = childNodes.keySet();
			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, childNodes.get(key), 5);
			}
		}
		// end entransfer
		int p2 = p + 1;
		if (p2 < (map.size() + 7) && !map.get(p2).get(139).toString().isEmpty()//was 136
				&& !map.get(p).get(139).toString().isEmpty() && i <= count - 1//was 136
				&& map.get(p).get(139).toString().equals("Strd")//was 136
				&& map.get(p2).get(139).toString().equals("Strd")) {//was 136
			EndDocument = eventFactory.createEndElement("", "", "Strd");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
		} else if (!map.get(p).get(117).toString().isEmpty() && i == count - 1//was 116
				&& map.get(p).get(117).toString().equals("Strd")) {//was 116
			EndDocument = eventFactory.createEndElement("", "", "Strd");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
		} else if (i == count - 1) {
			EndDocument = eventFactory.createEndElement("", "", "Strd");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
		}

	}

	public void NstrdEntry(StartElement tempDocument, EndElement EndDocument,
			XMLEventWriter xmlEventWriter, XMLEventFactory eventFactory,
			XMLEvent end, Map<String, String> childNodes,
			Set<String> elementNodes, XMLEvent tab, int p, int batchCount)
			throws XMLStreamException {
		if (!map.get(p).get(143).toString().isEmpty()) {//was 140

			tempDocument = eventFactory.createStartElement("", "", "Strd");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			if ((!map.get(p).get(143).toString().isEmpty())) {//was 140
				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");

				createNode(xmlEventWriter, "AddtlRmtInf", map.get(p).get(143)//was 140
						.toString(), 5);
				createNode(xmlEventWriter, "AddtlRmtInf", map.get(p).get(144)//was 141
						.toString(), 5);
				createNode(xmlEventWriter, "AddtlRmtInf", map.get(p).get(145)//was 142
						.toString(), 5);
			}

			EndDocument = eventFactory.createEndElement("", "", "Strd");
			tabMarked(xmlEventWriter, 4);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
		}

	}

	public void tabMarked(XMLEventWriter xmlEventWriter, int count)
			throws XMLStreamException {
		XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();
		XMLEvent tab = xmlEventFactory.createDTD("\t");

		try {
			for (int i = 0; i < count; i++)
				xmlEventWriter.add(tab);
		} catch (XMLStreamException e) {
			e.printStackTrace();
			throw new XMLStreamException();
		}
	}
	public  String getSimpleDateFormat() throws Exception{		
		Date date = new Date();		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T' HH:mm:ss");		
		String dateAsISOString = df.format(date);	
			return dateAsISOString;	
			}
	
}