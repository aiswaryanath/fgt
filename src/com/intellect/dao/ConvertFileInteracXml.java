/********************************************************************************************************/
/* Copyright �  2017 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/* 								 																		*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : ConvertFileInteracXml.java                                          					*/
/*                                                                                      				*/
/********************************************************************************************************/
/*  	   Author					    |        	  Date   			|	  Version                   */
/********************************************************************************************************/
/*         Abhishek Tiwari		        |			23:05:2017			|		1.0	               */
/*		   Sapna Jain					|			18:07:2017			| 		1.1
 /* 	   Preetam Sanjore				|			20:07:2017			|		1.2	
 /* 	   Gokaran Tiwari				|			22:03:2018			|		1.4 latest suite change
 /* 																				in main table , old move to audit
/********************************************************************************************************/
/* 											  														    */
/* Description   : For generation of Interac Xml file format 											*/
/*        																								*/
/********************************************************************************************************/

package com.intellect.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EmptyStackException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.log4j.Logger;

import com.intellect.pojo.TestToolFileNamePOJO;
import com.intellect.property.FormatConstants;
import com.intellect.service.FormatGenerateService;

public class ConvertFileInteracXml {

	private static org.apache.log4j.Logger log = Logger
			.getLogger(ConvertFileInteracXml.class);

	int m = 7, p = 7;
	int batchrowCount = 7;
	LinkedHashMap<Integer, Vector<String>> map = null;
	LinkedHashMap<Integer, Vector> map1 = null;
	ReadTemplate readTemplate = new ReadTemplate();
	int filesCrtdCnt = 0;

	// BAD_FILES_CHANGES
	private String invalidAppender = "001";

	public int main(String source, File target, String fileTypeSelected,
			LinkedHashMap<Integer, Vector<String>> map1, Connection connection,
			String db_table_file, String timeStamp, String dbTimeStamp,
			String moduleSelected, char[] variants,
			List<TestToolFileNamePOJO> testToolData) throws Exception {

		ConvertFileInteracXml xmlWriter = new ConvertFileInteracXml();
		try {
			filesCrtdCnt = xmlWriter.rootXMLFile(source, target,
					fileTypeSelected, map1, connection, db_table_file,
					timeStamp, dbTimeStamp, moduleSelected, variants,testToolData);
		} catch (Exception e) {
			throw new Exception();
		}

		return filesCrtdCnt;
	}

	public int rootXMLFile(String source, File target, String fileTypeSelected,
			LinkedHashMap map1, Connection connection, String db_table_file,
			String timeStamp, String dbTimeStamp, String moduleSelected,
			char[] variants,List<TestToolFileNamePOJO> pojoList) throws Exception {

		IwReadExcel objIwReadExcel = new IwReadExcel();
		map = (LinkedHashMap<Integer, Vector<String>>) objIwReadExcel.ReadData(
				source, fileTypeSelected, moduleSelected);
		// Change 1.4
		int rowCount = 7, colCount = 17;
		ArrayList<Integer> stockList = new ArrayList<Integer>();
		int count = 0, k = 0, fileCount = 0;
		for (int j = 0; j < map.size(); j++) {
			if (!map.get(rowCount).get(colCount).toString().isEmpty()) {
				stockList.add(count + 1);
				fileCount++;
				count = 0;
				k++;

			} else
				count++;
			rowCount++;
		}
		stockList.add(count + 1);
		int arr[] = new int[stockList.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = stockList.get(i).intValue();
		}
		ArrayList allRecList = new ArrayList();
		for (int i = 0, j2 = 1; i < fileCount; i++, j2++) {
			ArrayList rowCol = writeXML(arr[j2], target, connection,
					db_table_file, timeStamp, dbTimeStamp, null, -1, -1,pojoList);// Change 1.4
			allRecList.add(rowCol);
			for (int z = 0; z < rowCol.size(); z++) {
			}
		}

		// BAD_FILES_CHANGES STARTS
		String[] ruleNames = null;
		boolean oFind = false;
		for (int i = 0; i < variants.length; i++) {
			if (variants[i] == 'O') {
				oFind = true;
			}
		}
		if (oFind) {
			ruleNames = new String[variants.length - 1];
		} else {
			ruleNames = new String[variants.length];
		}

		for (int i = 0; i < variants.length; i++) {
			if (variants[i] == 'O')
				continue;
			else
				ruleNames[i] = variants[i] + "";
		}

		int lastRowNo = 0;
		int startNoForEachMessage = 0;
		// iterating over no of messages
		for (int allRec = 0; allRec < allRecList.size(); allRec++) {
			if (lastRowNo == 0) {
				startNoForEachMessage = 7 + lastRowNo;
			} else {
				startNoForEachMessage = 1 + lastRowNo;

			}
			p = startNoForEachMessage;
			batchrowCount = startNoForEachMessage;
			m = startNoForEachMessage;
			ArrayList rowCol = (ArrayList) allRecList.get(allRec);
			for (int rowColNo = 0; rowColNo < rowCol.size(); rowColNo++) {
				String s = (String) rowCol.get(rowColNo);

				String[] strArray = s.split("~");
				lastRowNo = Integer.valueOf(strArray[0]);
				int ruleColNo = Integer.valueOf(strArray[1]);

				for (int x = 0; x < ruleNames.length; x++) {

					p = startNoForEachMessage;
					batchrowCount = startNoForEachMessage;
					m = startNoForEachMessage;

					writeXML(arr[allRec + 1], target, connection,
							db_table_file, timeStamp, dbTimeStamp,
							ruleNames[x], lastRowNo, ruleColNo,pojoList);// Change 1.4

				}
			}
		}
		
		return fileCount;

	}

	public ArrayList writeXML(int batchCount, File target,
			Connection connection, String db_table_file, String timeStamp,
			String dbTimeStamp, String ruleName, int ruleRowNo, int ruleColNo, List<TestToolFileNamePOJO> pojoList)
			throws Exception {

		XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
		String XMLPath = target + "/";
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMdd");

		String fileName = null, dbfileName = null;
		// BAD_FILES_CHANGES STARTS
		ArrayList<String> rowCol = null;
		// BAD_FILES_CHANGES ENDS

		String interac_applied_rule_pos = FormatConstants.interacAppliedRulePosition;
		int interac_applied_rule_pos_no = Integer
				.parseInt(interac_applied_rule_pos);

		if (map.get(p).get(2) != null
				&& map.get(p).get(2).toString().length() > 0) {

			String temp = FormatGenerateService.getSequenceNumber(connection,
					map.get(p).get(2).toString());
			if (map.get(p).get(3) != null
					&& map.get(p).get(3).toString().replace(" ", "")
							.equalsIgnoreCase("FTS")) {

				File filefolder = new File(XMLPath + "/FTS");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();
				// BAD_FILES_CHANGES STARTS
				fileName = XMLPath
						+ "/"
						+ map.get(p).get(7).toString()
						+ "_"
						+ map.get(p).get(10).toString()
						+ "_"
						+ formatter.format(date)
						+ "_111213_"
						+ map.get(p)
								.get(8)
								.toString()
								.replace(".0", "")
								.substring(
										0,
										map.get(p).get(8).toString().length() - 6)
						+ temp + "." + map.get(p).get(2).toString() + "."
						+ map.get(p).get(11).toString() + ".xml";
				// BAD_FILES_CHANGES ENDS
				if (map.get(p).get(12) != null
						&& !map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(7).toString()
							+ "_"
							+ map.get(p).get(10).toString()
							+ "_"
							+ formatter.format(date)
							+ "_111213_"
							+ map.get(p)
									.get(8)
									.toString()
									.replace(".0", "")
									.substring(
											0,
											map.get(p).get(8).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();

				} else {
					dbfileName = map.get(p).get(7).toString()
							+ "_"
							+ map.get(p).get(10).toString()
							+ "_"
							+ formatter.format(date)
							+ "_111213_"
							+ map.get(p)
									.get(8)
									.toString()
									.replace(".0", "")
									.substring(
											0,
											map.get(p).get(8).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + ".xml";
				}

			} else if (map.get(p).get(3).toString().replace(" ", "")
					.equalsIgnoreCase("SCA")) {

				File filefolder = new File(XMLPath + "/SCA");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();

				fileName = XMLPath
						+ "/"
						+ map.get(p).get(6).toString()
						+ "."
						+ formatter1.format(date)
						+ "111213"
						+ "."
						+ map.get(p)
								.get(10)
								.toString()
								.substring(
										0,
										map.get(p).get(10).toString().length() - 6)
						+ temp + "."
						+ map.get(p).get(2).toString().replace(".0", "") + "."
						+ map.get(p).get(11).toString() + ".xml";
				if (!map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();

					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();

				} else {
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";
				}
			} else if (map.get(p).get(3).toString().replace(" ", "")
					.equalsIgnoreCase("FTT")) {

				File filefolder = new File(XMLPath + "/FTT");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();
				// BAD_FILES_CHANGES STARTS
				if (ruleName != null) {
					fileName = XMLPath + "/" + map.get(p).get(7).toString()
							+ "_" + map.get(p).get(10).toString() + "_"
							+ map.get(p).get(9).toString() + "_"
							+ map.get(p).get(8).toString().replace(".0", "")
							+ "." + map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + "_" + ruleName
							+ String.valueOf(ruleRowNo)
							+ String.valueOf(ruleColNo) + ".xml";

				} else {
					fileName = XMLPath + "/" + map.get(p).get(6).toString()
							+ "." + map.get(p).get(9).toString() + "."
							+ map.get(p).get(10).toString() + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";
				}
				// BAD_FILES_CHANGES ENDS
				if (!map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(6).toString() + "."
							+ map.get(p).get(9).toString() + "."
							+ map.get(p).get(10).toString() + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();

				} else {
					dbfileName = map.get(p).get(6).toString() + "."
							+ map.get(p).get(9).toString() + "."
							+ map.get(p).get(10).toString() + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";

				}
			}
			// BAD_FILES_CHANGES (S)
			StringBuffer strModifiedField = new StringBuffer();
			if (ruleName != null && !"".equals(ruleName)) {
				strModifiedField.append(ruleRowNo).append("~")
						.append(ruleColNo);
			}
			String errorCode = null;
			String ruleType = null;
			if ("M".equals(ruleName)) {
				ruleType = "TAG_MISSING";
				errorCode = "TAG_MISSING_ERRORCODE";
			} else if ("D".equals(ruleName)) {
				ruleType = "DUPLICATE_TAG";
				errorCode = "DUPLICATE_TAG_ERRORCODE";
			} else if ("I".equals(ruleName)) {
				ruleType = "INVALID_TAG";
				errorCode = "INVALID_TAG_ERRORCODE";
			} else if ("O".equals(ruleName)) {
				ruleType = "TAG_ORDER_CHANGE";
				errorCode = "TAG_ORDER_CHANGE_ERRORCODE";
			} // BAD_FILES_CHANGES (E)
			// Change 1.4 starts
			if (ruleName != null && !"".equals(ruleName)) {
				TestToolFileNamePOJO pojoObj = new TestToolFileNamePOJO();
				pojoObj.setTestCaseid(map.get(p).get(0).toString());
				pojoObj.setFormatName(map.get(p).get(2).toString());
				pojoObj.setChannel(map.get(p).get(3).toString());
				pojoObj.setLsi(map.get(p).get(4).toString());
				pojoObj.setClientRefNum(map.get(p).get(17).toString());
				pojoObj.setBicCode(map.get(p).get(6).toString());
				pojoObj.setBbdId(map.get(p).get(7).toString());
				pojoObj.setCustRef(map.get(p).get(8).toString());
				pojoObj.setUniqueId(map.get(p).get(10).toString());
				pojoObj.setFileName(dbfileName);
				pojoObj.setAppliedRules(ruleType);
				pojoObj.setAppliedRules(errorCode);
				pojoObj.setModifiedField(strModifiedField.toString());
				pojoObj.setTimeStamp(dbTimeStamp);
				pojoList.add(pojoObj);
			} else {
				TestToolFileNamePOJO pojoObj = new TestToolFileNamePOJO();
				pojoObj.setTestCaseid(map.get(p).get(0).toString());
				pojoObj.setFormatName(map.get(p).get(2).toString());
				pojoObj.setChannel(map.get(p).get(3).toString());
				pojoObj.setLsi(map.get(p).get(4).toString());
				pojoObj.setClientRefNum(map.get(p).get(17).toString());
				pojoObj.setBicCode(map.get(p).get(6).toString());
				pojoObj.setBbdId(map.get(p).get(7).toString());
				pojoObj.setCustRef(map.get(p).get(8).toString());
				pojoObj.setUniqueId(map.get(p).get(10).toString());
				pojoObj.setFileName(dbfileName);
				pojoObj.setAppliedRules(map.get(p).get(interac_applied_rule_pos_no).toString());
				pojoObj.setAppliedRules(map.get(p).get(interac_applied_rule_pos_no + 1).toString());
				pojoObj.setModifiedField(map.get(p).get(interac_applied_rule_pos_no + 3).toString());
				pojoObj.setTimeStamp(dbTimeStamp);
				pojoList.add(pojoObj);
			}
			// Change 1.4 ends
		} else {

			if (log.isDebugEnabled()) {
				log.debug("FormatName is empty");
			}
		}
		m++;
		String rootElement = "header";

		try {
			XMLEventWriter xmlEventWriter = xmlOutputFactory
					.createXMLEventWriter(new FileOutputStream(fileName),
							"UTF-8");
			XMLEventFactory eventFactory = XMLEventFactory.newInstance();
			XMLEvent end = eventFactory.createDTD(System
					.getProperty("line.separator"));
			XMLEvent tab = eventFactory.createDTD("\t");
			StartDocument startDocument = eventFactory.createStartDocument();
			xmlEventWriter.add(startDocument);
			xmlEventWriter.add(end);
			StartElement tempDocument = eventFactory.createStartElement("", "",
					"interac-bulk-send-transfers-request");

			xmlEventWriter.add(tempDocument);
			XMLEvent xmlDocs2 = eventFactory.createAttribute("xmlns",
					"urn:iso:std:iso:20022:tech:xsd:pain.001.001.03");
			xmlDocs2 = eventFactory.createNamespace("xsi",
					"http://www.w3.org/2001/XMLSchema-instance");
			xmlEventWriter.add(end);

			EndElement EndDocument = null;
			Map<String, String> childNodes = null;
			Set<String> elementNodes = null;
			tempDocument = eventFactory.createStartElement("", "", "header");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			Map<String, String> elementsMap = new LinkedHashMap<String, String>();

			// BAD_FILES_CHANGES STARTS
			if (ruleName == null) {
				rowCol = new ArrayList();
			}
			// BAD_FILES_CHANGES ENDS

			// Write the element nodes

			// <GrpHdr> start

			// MsgId
			if (!map.get(p).get(17).toString().isEmpty()) {
				elementsMap.put("bulk-request-reference-number", (String) map
						.get(p).get(17));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(17));
				else {
					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "bulk-request-reference-number", 17,
							xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}

			// CreDtTm
			if (!map.get(p).get(18).toString().isEmpty()) {
				elementsMap.put("creation-date", (String) map.get(p).get(18));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(18));
				else {
					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "creation-date", 18, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}

			// NbOfTxs

			elementNodes = elementsMap.keySet();
			for (String key : elementNodes) {

				createNode(xmlEventWriter, key, elementsMap.get(key), 3);
			}

			EndDocument = eventFactory.createEndElement("", "", "header");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

			if (!map.get(p).get(20).toString().isEmpty()
					|| !map.get(p).get(21).toString().isEmpty()
					|| !map.get(p).get(22).toString().isEmpty()) {

				tempDocument = eventFactory.createStartElement("", "",
						"customer");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				if (!map.get(p).get(20).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					childNodes.put("fi-user-id", (String) map.get(p).get(20));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(20));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "fi-user-id", 20,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					childNodes.put("account-holder-name", (String) map.get(p)
							.get(21));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(21));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "account-holder-name",
								21, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					childNodes.put("account-number", (String) map.get(p)
							.get(22));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(22));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "account-number", 22,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 4);
					}
				}
				EndDocument = eventFactory.createEndElement("", "", "customer");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// sender name
			if (!map.get(p).get(23).toString().isEmpty()) {

				tempDocument = eventFactory
						.createStartElement("", "", "sender");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				if (!map.get(p).get(23).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					childNodes.put("name", (String) map.get(p).get(23));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(23));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "name", 23,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 4);
					}
				}
				EndDocument = eventFactory.createEndElement("", "", "sender");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// end sender name

			if (!map.get(p).get(24).toString().isEmpty()
					|| !map.get(p).get(25).toString().isEmpty()
					|| !map.get(p).get(26).toString().isEmpty()
					|| !map.get(p).get(27).toString().isEmpty()
					|| !map.get(p).get(28).toString().isEmpty()
					|| !map.get(p).get(29).toString().isEmpty()) {
				tempDocument = eventFactory.createStartElement("", "",
						"processing-options");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();

				childNodes.put("cancellation-period",
						(String) map.get(p).get(24));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(24));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "cancellation-period", 24,
							xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				childNodes.put("recipient-reminder-frequency", (String) map
						.get(p).get(25));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(25));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "recipient-reminder-frequency", 25,
							xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				childNodes.put("recipient-reminder-max-count", (String) map
						.get(p).get(26));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(26));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "recipient-reminder-max-count", 26,
							xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				childNodes.put("enable-sender-notifications",
						(String) map.get(p).get(27));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(27));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "enable-sender-notifications", 27,
							xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				childNodes.put("accept-auto-deposit-delivery", (String) map
						.get(p).get(28));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(28));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "accept-auto-deposit-delivery", 28,
							xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				childNodes.put("custom-notification-template-id", (String) map
						.get(p).get(29));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(29));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "custom-notification-template-id", 29,
							xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 7);
				}

				EndDocument = eventFactory.createEndElement("", "",
						"processing-options");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			int colCount = 30;

			ArrayList<Integer> stockList = new ArrayList<Integer>();

			int count = 0, k = 0, fileCount = 0;
			for (int j = 0; j < batchCount; j++) {
				if (!map.get(batchrowCount).get(colCount).toString().isEmpty()) {
					stockList.add(count + 1);
					fileCount++;
					count = 0;
					k++;
				} else
					count++;
				batchrowCount++;
			}
			stockList.add(count + 1);
			int arr[] = new int[stockList.size()];
			for (int i2 = 0; i2 < arr.length; i2++) {
				arr[i2] = stockList.get(i2).intValue();
			}
			int p1 = p;

			for (int i2 = 0, k2 = 1; i2 < fileCount; i2++, k2++) {
				paymentInfo(tempDocument, EndDocument, xmlEventWriter,
						eventFactory, end, childNodes, elementNodes, tab, p,
						arr[k2], rowCol, ruleName, ruleRowNo, ruleColNo);
				if (!map.get(p).get(46).toString().isEmpty()) {
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null) {
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(46));
					}

				}
				if (!map.get(p).get(47).toString().isEmpty()) {
					if (rowCol != null) {
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(47));
					}
					// BAD_FILES_CHANGES ENDS
				}

				p++;
			}
			tabMarked(xmlEventWriter, 1);
			// trailer
			if ((!map.get(p1).get(46).toString().isEmpty())
					|| (!map.get(p1).get(47).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"trailer");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				if (!map.get(p1).get(46).toString().isEmpty()
						|| !map.get(p1).get(47).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("number-of-transfers", map.get(p1).get(46)
							.toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol == null) {

						childNodes = applyRule(ruleRowNo, ruleColNo, p1,
								ruleName, childNodes, "number-of-transfers",
								46, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					childNodes.put("transfers-total-amount", map.get(p1)
							.get(47).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol == null) {

						childNodes = applyRule(ruleRowNo, ruleColNo, p1,
								ruleName, childNodes, "transfers-total-amount",
								47, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}

			}
			EndDocument = eventFactory.createEndElement("", "", "trailer");
			tabMarked(xmlEventWriter, 3);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
			// end trailer
			xmlEventWriter.add(eventFactory.createEndDocument());
			eventFactory.createEndDocument();
			// Changes done as getting exception
			// EndDocument = eventFactory.createEndElement("", "",
			// "interac-bulk-send-transfers-request");
			// xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

			String rp = xmlEventWriter.toString();
			rp = rp.replaceAll(
					"</interac-bulk-send-transfers-request xmlns=urn:iso:std:iso:20022:tech:xsd:pain.001.001.03 xmlns:xsi=http://www.w3.org/2001/XMLSchema-instance>",
					"</interac-bulk-send-transfers-request>");

			xmlEventWriter.close();

		} catch (EmptyStackException exx) {
			log.fatal("EmptyStackException: ", exx);
			throw new EmptyStackException();
		} catch (FileNotFoundException e) {
			log.fatal("FileNotFoundException: ", e);
			throw new FileNotFoundException();
			// e.printStackTrace();
		} catch (XMLStreamException ex) {
			log.fatal("XMLStreamException: ", ex);
			throw new XMLStreamException();
		}
		return rowCol;

	}

	// BAD_FILES_CHANGES
	private Map applyRule(int ruleRowNo, int ruleColNo, int p2,
			String ruleName, Map<String, String> elementsMap, String key,
			int actualColNo, XMLEventWriter xmlEventWriter) throws Exception {
		if (log.isDebugEnabled())
			if (p2 == ruleRowNo && ruleColNo == actualColNo) {
				if ("D".equals(ruleName)) {
					createNode(xmlEventWriter, key, elementsMap.get(key), 3);
				} else if ("M".equals(ruleName)) {
					elementsMap.remove(key);
				} else if ("I".equals(ruleName)) {
					elementsMap.put(key + invalidAppender,
							(String) elementsMap.get(key));
					elementsMap.remove(key);
				}
			}
		if (log.isDebugEnabled())
			log.debug("Leaving applyRule: elementsMap " + elementsMap);
		return elementsMap;

	}

	public void paymentInfo(StartElement tempDocument, EndElement EndDocument,
			XMLEventWriter xmlEventWriter, XMLEventFactory eventFactory,
			XMLEvent end, Map<String, String> childNodes,
			Set<String> elementNodes, XMLEvent tab, int tempP, int batchCount,
			ArrayList rowCol, String ruleName, int ruleRowNo, int ruleColNo)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("paymentInfo method starts in ConvertFileISOPain001 class");
		}
		// start <PmtInf>
		tempDocument = eventFactory.createStartElement("", "", "transfers");
		try {
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(tempDocument);

			xmlEventWriter.add(end);
			childNodes = new LinkedHashMap<String, String>();

			tab = eventFactory.createDTD("\t");

			// PmtInfId
			if (!map.get(p).get(30).toString().isEmpty()) {
				childNodes.put("Txn-Ref-number", map.get(p).get(30).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(30));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Txn-Ref-number", 30, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			// PmtMtd
			if (!map.get(p).get(31).toString().isEmpty()) {
				childNodes.put("value-date", map.get(p).get(31).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(31));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "value-date", 31, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			elementNodes = childNodes.keySet();
			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, childNodes.get(key), 3);
			}
			// DbtrAgt
			if ((!map.get(p).get(32).toString().isEmpty())
					|| (!map.get(p).get(33).toString().isEmpty())
					|| (!map.get(p).get(34).toString().isEmpty())
					|| (!map.get(p).get(35).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"processing-options");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				if (!map.get(p).get(32).toString().isEmpty()
						|| !map.get(p).get(33).toString().isEmpty()
						|| !map.get(p).get(34).toString().isEmpty()
						|| !map.get(p).get(35).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					if (!map.get(p).get(32).toString().isEmpty()) {
						childNodes.put("priority", map.get(p).get(32)
								.toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(32));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "priority", 32,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(33).toString().isEmpty()) {
						childNodes.put("cancellation-period", map.get(p)
								.get(33).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(33));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes,
									"cancellation-period", 33, xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(34).toString().isEmpty()) {
						childNodes.put("accept-auto-deposit-delivery",
								map.get(p).get(34).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(34));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes,
									"accept-auto-deposit-delivery", 34,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(35).toString().isEmpty()) {
						childNodes.put("custom-notification-template-id", map
								.get(p).get(35).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(35));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes,
									"custom-notification-template-id", 35,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}

			}
			EndDocument = eventFactory.createEndElement("", "",
					"processing-options");
			tabMarked(xmlEventWriter, 3);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

			// sender name
			if (!map.get(p).get(36).toString().isEmpty()) {

				tempDocument = eventFactory
						.createStartElement("", "", "sender");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				if (!map.get(p).get(36).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					childNodes.put("name", (String) map.get(p).get(36));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(36));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "name", 36,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 4);
					}
				}
				EndDocument = eventFactory.createEndElement("", "", "sender");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// end sender name
			// recipient
			if ((!map.get(p).get(37).toString().isEmpty())
					|| (!map.get(p).get(38).toString().isEmpty())
					|| (!map.get(p).get(39).toString().isEmpty())
					|| (!map.get(p).get(40).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"recipient");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				if (!map.get(p).get(37).toString().isEmpty()
						|| !map.get(p).get(38).toString().isEmpty()
						|| !map.get(p).get(39).toString().isEmpty()
						|| !map.get(p).get(40).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("name", map.get(p).get(37).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(37));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "name", 37,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					childNodes.put("email-address", map.get(p).get(38)
							.toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(38));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "email-address", 38,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					childNodes.put("mobile-phone-number", map.get(p).get(39)
							.toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(39));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "mobile-phone-number",
								39, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					childNodes.put("language", map.get(p).get(40).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(40));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "language", 40,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}

			}
			EndDocument = eventFactory.createEndElement("", "", "recipient");
			tabMarked(xmlEventWriter, 3);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
			childNodes = new LinkedHashMap<String, String>();
			if (!map.get(p).get(41).toString().isEmpty()) {
				childNodes.put("question", map.get(p).get(41).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(41));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "question", 41, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			// PmtMtd
			if (!map.get(p).get(42).toString().isEmpty()) {
				childNodes.put("answer", map.get(p).get(42).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(42));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "answer", 42, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			if (!map.get(p).get(43).toString().isEmpty()) {
				childNodes.put("currency", map.get(p).get(43).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(43));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "currency", 43, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			if (!map.get(p).get(44).toString().isEmpty()) {
				childNodes.put("amount", map.get(p).get(44).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(44));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "amount", 44, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			if (!map.get(p).get(45).toString().isEmpty()) {
				childNodes.put("memo", map.get(p).get(45).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(45));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "memo", 45, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			elementNodes = childNodes.keySet();
			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, childNodes.get(key), 3);
			}
			// end recipient

			EndDocument = eventFactory.createEndElement("", "", "transfers");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
			// end </PmtInf>

		} catch (XMLStreamException e) {
			e.printStackTrace();
			throw new XMLStreamException();
		}

	}

	private static void createNode(XMLEventWriter eventWriter, String element,
			String value, int tab2) throws XMLStreamException {

		XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();
		XMLEvent end = xmlEventFactory.createDTD("\n");
		XMLEvent tab = xmlEventFactory.createDTD("\t");
		// Create Start node
		StartElement sElement = xmlEventFactory.createStartElement("", "",
				element);
		for (int i = 0; i < tab2; i++)
			eventWriter.add(tab);
		eventWriter.add(sElement);
		// Create Content
		Characters characters = xmlEventFactory.createCharacters(value);
		eventWriter.add(characters);
		// Create End node
		EndElement eElement = xmlEventFactory.createEndElement("", "", element);
		eventWriter.add(eElement);
		eventWriter.add(end);

	}

	public void tabMarked(XMLEventWriter xmlEventWriter, int count)
			throws XMLStreamException {
		XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();
		XMLEvent tab = xmlEventFactory.createDTD("\t");

		try {
			for (int i = 0; i < count; i++)
				xmlEventWriter.add(tab);
		} catch (XMLStreamException e) {
			e.printStackTrace();
			throw new XMLStreamException();
		}
	}
}