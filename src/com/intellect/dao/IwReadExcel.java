/********************************************************************************************************/
/* Copyright �  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/* 													 													*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : IwReadExcel.java                                           					        */
/********************************************************************************************************/
/*                 Author					    |        	  Date   			|	  Version           */
/********************************************************************************************************/
/* 	           Veera Kumar Reddy.V	           	|			20:05:2016			|		1.0	            */
/*				Preetam Sanjore					|			18:07:2017			|		1.1				*/
/********************************************************************************************************/
/*																										*/
/* 	Description  : To read input Template file data														*/
/*        																								*/
/********************************************************************************************************/
package com.intellect.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.intellect.property.FormatConstants;

public class IwReadExcel {
	private static org.apache.log4j.Logger log = Logger
			.getLogger(IwReadExcel.class);

	int rowCnt = 0;
	int columnCnt = 0;

	// Read data from Sheet
	public Map<Integer, Vector<String>> ReadData(String source,
			String fileTypeSelected, String moduleSelected) throws Exception {

		if (log.isDebugEnabled()) {
			log.debug("ReadData method starts in IwReadExcel class");
			log.debug("source:==>" + source + "<=fileTypeSelected=>"
					+ fileTypeSelected);
		}

		Map<Integer, Vector<String>> map = new LinkedHashMap<Integer, Vector<String>>();
		int rowId = getTestCaseRow(fileTypeSelected);
		int[] vectorArr = new int[4];
		Sheet sheet;
		try {
			int count = 0;
			int index = 0, FLAG = 0;
			String XLSPath = source;
			InputStream in = new FileInputStream(XLSPath);
			String xlsNameExtension = XLSPath.substring(XLSPath
					.lastIndexOf(".") + 1);

			if (xlsNameExtension.equalsIgnoreCase("xls")) {
				HSSFWorkbook workbook = new HSSFWorkbook(in);
				int w_no_of_sheet = workbook.getNumberOfSheets();
				for (int i = 0; i < w_no_of_sheet; i++) {
					if (workbook.getSheetName(i).equals(fileTypeSelected)) {
						index = i;
						FLAG = 1;
					} else if (workbook.getSheetName(i)
							.equals(fileTypeSelected)) {
						index = i;
						FLAG = 1;
					}
				}
			}

			// HSSFSheet sheet=null;
			Row myRow = null;
			int ff = 7;
			Vector<String> cellStoreVector = null;
			Date date = new Date();
			SimpleDateFormat formatter = null;

			if (FLAG == 1) {
				FileInputStream file = new FileInputStream(new File(source));
				String fileNamePath = source;
				String fileNameExtension = fileNamePath.substring(fileNamePath
						.lastIndexOf(".") + 1);

				if (fileNameExtension.equalsIgnoreCase("xls")) {
					// Get the workbook instance for XLS file
					HSSFWorkbook workbook1 = new HSSFWorkbook(file);
					// Get second sheet from the workbook
					sheet = workbook1.getSheetAt(index);
				} else {
					// Get the workbook instance for XLSX file
					XSSFWorkbook workbook1 = new XSSFWorkbook(source);
					// Get second sheet from the workbook
					sheet = workbook1.getSheetAt(index);
				}
				Row firstRow = sheet.getRow(0);
				for (int i = 7; i < sheet.getPhysicalNumberOfRows(); i++) {
					rowCnt = i;
					// log.debug("Row number :::" + rowCnt);
					myRow = sheet.getRow(i);
					if (myRow != null
							&& (moduleSelected.equals("N") || (moduleSelected
									.equals("Y") && myRow.getCell(rowId) != null 
									&& myRow.getCell(rowId).toString().trim().equals("Y")))) {// myRow.getcell()

						cellStoreVector = new Vector<String>();
						int count1 = 0;
						for (int column = 0; column < myRow
								.getPhysicalNumberOfCells(); column++) {

							Cell recodCell = myRow.getCell(column,
									Row.CREATE_NULL_AS_BLANK);
							String colVal = recodCell.toString();
							if (colVal.equalsIgnoreCase("")) {
								count1++;
							}
						}
						if (count1 == myRow.getPhysicalNumberOfCells()) {

						} else {
							for (int j = 0; j < myRow
									.getPhysicalNumberOfCells(); j++) {
								columnCnt = j;
								// log.debug("columnCnt:::" + columnCnt);
								Cell myCell = myRow.getCell(j,Row.CREATE_NULL_AS_BLANK);
								String cellvalue = "";
								switch (myCell.getCellType()) {

								case Cell.CELL_TYPE_BOOLEAN:

									cellvalue = String.valueOf(myCell
											.getBooleanCellValue());
									break;
								case Cell.CELL_TYPE_NUMERIC:
									cellvalue = String.valueOf(myCell
											.getNumericCellValue());
									log.debug("getNumericCellValue() :::"
											+ myCell.getNumericCellValue());
									log.debug("cellvalue for CELL_TYPE_NUMERIC:::"
											+ cellvalue);

									break;
								case Cell.CELL_TYPE_STRING:
									cellvalue = String.valueOf(myCell
											.getStringCellValue());
									break;
								case Cell.CELL_TYPE_FORMULA:
									handleCell(myCell.getCellType(), myCell);// 1.1
																				// version
																				// fix
																				// for
																				// integrated
																				// transactions
									break;
								case Cell.CELL_TYPE_ERROR:
									cellvalue = String.valueOf(myCell
											.getStringCellValue());
									break;
								}

								cellStoreVector.addElement(cellvalue);

							}
							// System.out.println("111");
							if (FormatConstants.pain1SheetName
									.equals(fileTypeSelected)) {
								vectorArr[0] = 19;
								// vectorArr[1]=96;
								vectorArr[1] = 106;
								vectorArr[2] = 28;
								// vectorArr[3]=97;
								vectorArr[3] = 107; // new columns added changes

							} else if (FormatConstants.pain8SheetName
									.equals(fileTypeSelected)) {
								vectorArr[0] = 19;
								vectorArr[1] = 95;
								vectorArr[2] = 28;
								vectorArr[3] = 96;

							} else if (FormatConstants.PAIN1_REMIT_SHEET_NAME
									.equals(fileTypeSelected)) {

								vectorArr[0] = 19;
								vectorArr[1] = 154;//Jo Changed here was 151
								vectorArr[2] = 28;
								vectorArr[3] = 155;//Jo Changed here was 152

							} else if (FormatConstants.PAIN8_REMIT_SHEET_NAME
									.equals(fileTypeSelected)) {

								vectorArr[0] = 19;
								vectorArr[1] = 122;
								vectorArr[2] = 32;
								vectorArr[3] = 123;
							} else if (FormatConstants.InteracSheetName
									.equals(fileTypeSelected)) {

								vectorArr[0] = 18;
								vectorArr[1] = 52;
								vectorArr[2] = 31;
								vectorArr[3] = 53;

							} else if (FormatConstants.IsoRemitSheetName
									.equals(fileTypeSelected)) {

								vectorArr[0] = 19;
								// vectorArr[1]=60;
								vectorArr[1] = 65;
								vectorArr[2] = 36;
								// vectorArr[3]=61;
								vectorArr[3] = 66;

							}
							
							//jO ADDED HERE
							else if (FormatConstants.CAMT_SHEET_NAME
									.equals(fileTypeSelected)) {

								vectorArr[0] = 21;//CREATION DATE TIME
						
								vectorArr[1] = 39;//CREATION DATE +-
								vectorArr[2] = 33;//VALUE DATE
								
								vectorArr[3] = 40;//VALUE DATE +-

							}
							if (log.isDebugEnabled())
								log.debug("SIZE::::" + cellStoreVector);
							formatter = new SimpleDateFormat(
									"yyyy-MM-dd'T'HH:mm:ss");
							if (!cellStoreVector.get(vectorArr[0]).equals("")) {
								cellStoreVector.remove(vectorArr[0]);

//								int value = (int) Math.floor(Float.valueOf(cellStoreVector.get(vectorArr[1] - 1)));
								int value = 0;//jo changed here WAS 10
								 System.out.println("value1"+value);
								cellStoreVector
										.add(vectorArr[0],
												formatter.format(date.getTime()
														+ ((long) value * 24 * 60 * 60 * 1000))

										);
							}
							formatter = new SimpleDateFormat("yyyy-MM-dd");
							if (!cellStoreVector.get(vectorArr[2]).equals("")) {
								cellStoreVector.remove(vectorArr[2]);

								int value = (int) Math.floor(Float
										.valueOf(cellStoreVector
												.get(vectorArr[3] - 1)));
								cellStoreVector
										.add(vectorArr[2],
												formatter.format(date.getTime()
														+ ((long) value * 24 * 60 * 60 * 1000)));
							}
							map.put(ff, cellStoreVector);
							if (log.isDebugEnabled())
								log.debug("hashmap----Map->" + map);

							ff = ff + 1;
						}
					}

				}
				ff = 7;
				count++;
			}
			// log.debug("Exception at  Row Number:: " + rowCnt +
			// "Column number::" + columnCnt);
			// } catch (IOException e) {
			// if (log.isDebugEnabled()) {
			// log.error("IOException while reading Excel file");
			// }
			// e.printStackTrace();
			// throw new
			// Exception("Exception at  Row Number "+rowCnt+"Column number::"+columnCnt);
			// }
			// System.out.println("try() ends...");
		} catch (Exception e) {
			if (e instanceof NullPointerException) {
				System.out.println("NullPointerException");
				throw new NullPointerException("Please fill value at Column :"
						+ columnCnt + " and row :" + rowCnt);
			} else if (e instanceof StringIndexOutOfBoundsException) {
				System.out.println("StringIndexOutOfBoundsException");
				throw new StringIndexOutOfBoundsException(
						"String length exceed at Column :" + columnCnt
								+ " and row :" + rowCnt);
			} else if (e instanceof FileNotFoundException) {
				System.out.println("FileNotFoundException");
				throw new FileNotFoundException(
						"File Not Present at specified Location");
			} else if (e instanceof NumberFormatException) {
				System.out.println("NumberFormatException");
				throw new NumberFormatException(
						"String value is not present at Column:" + columnCnt
								+ " and Row : " + rowCnt);
			} else {
				e.getLocalizedMessage();
				throw new Exception(e.getLocalizedMessage()
						+ "at Column position" + columnCnt + " and row :::"
						+ rowCnt);
				// throw new
				// Exception("Exception at Column position"+globalColumn+" and row :::"+globalCntRow);
			}

		}

		return map;
	}

	public int getTestCaseRow(String fileTypeSelected) {

		log.debug("Entering inside getTestCaseRow with fileTypeSelected"
				+ fileTypeSelected);
		int rowNum = 0;
		if (FormatConstants.PAIN1_REMIT_SHEET_NAME.equals(fileTypeSelected)) {

			rowNum = Integer
					.parseInt(FormatConstants.pain1remitModularPosition);

		} else if (FormatConstants.pain1SheetName.equals(fileTypeSelected)) {

			rowNum = Integer.parseInt(FormatConstants.pain001ModularPosition);

		} else if (FormatConstants.pain8SheetName.equals(fileTypeSelected)) {

			rowNum = Integer.parseInt(FormatConstants.pain008ModularPosition);

		} else if (FormatConstants.InteracSheetName.equals(fileTypeSelected)) {

			rowNum = Integer.parseInt(FormatConstants.interacModularPosition);

		} else if (FormatConstants.IsoRemitSheetName.equals(fileTypeSelected)) {

			rowNum = Integer.parseInt(FormatConstants.isoremitModularPosition);

		}
		//JO ADDED HERE
		else if (FormatConstants.CAMT_SHEET_NAME.equals(fileTypeSelected)) {

			rowNum = Integer.parseInt(FormatConstants.CAMTModularPosition);

		} 
		else if (FormatConstants.PAIN8_REMIT_SHEET_NAME
				.equals(fileTypeSelected)) {

			rowNum = Integer
					.parseInt(FormatConstants.pain8remitModularPosition);

		}
		// log.debug("leaving with rowNum" + rowNum);
		return rowNum;

	}

	// 1.1 version (S)fix for integrated transactions
	private void handleCell(int type, Cell cell) {
		if (type == Cell.CELL_TYPE_FORMULA) {
			handleCell(cell.getCachedFormulaResultType(), cell);
		}
	}
	// (E)fix for integrated transactions
}