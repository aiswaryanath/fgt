/********************************************************************************************************/
/* Copyright �  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/* 													 													*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : ReadTemplate.java                                           					        */
/*                                                                                      				*/
/********************************************************************************************************/
/*  			 Author					         |        	  Date   			|	  Version           */
/********************************************************************************************************/
/* 	         Surendra Reddy.M  		             |			20:05:2016			|		1.0	            */
/*           Preetam Sanjore					 |          04:09:2017			|		1.1             */
/*		     Shruti Gupta						 |			20:01:2018			|   1.2(Code changes 
 * 																					for maintain an audit table)*/
/********************************************************************************************************/
/* 																									    */
/* 	Description  : To read input Template file data and as well as Database data						*/
/*        																								*/
/********************************************************************************************************/

package com.intellect.dao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.intellect.db.DBUtility;
import com.intellect.property.FormatConstants;
import com.intellect.service.UpdateFieldValues;
import com.sun.corba.se.impl.orb.ParserTable.TestAcceptor1;

public class ReadTemplate {

	private static org.apache.log4j.Logger log = Logger
			.getLogger(ReadTemplate.class);
	private static final DateFormat dateFormat = new SimpleDateFormat(
			"yyyy/MM/dd HH:mm:ss");

	int globalColumn = 0;
	int globalCntRow = 0;

	// To read data(field names,section names and records) from template
	public Map[] readDataFromTemplate(String inputTemplatePath,
			String sheetName, int fieldsRow_No, int formatCol,
			int recordStartedRow, int sectionNameRow_No) throws Exception {

		Map templateData[] = null;
		SimpleDateFormat formatter = null;

		templateData = new Map[3];
		// For field Names
		templateData[0] = new LinkedHashMap<String, Integer>();
		// For records Data
		templateData[1] = new LinkedHashMap<Integer, Map>();
		// For section Names
		templateData[2] = new LinkedHashMap<String, Integer>();

		try {
			InputStream inputStream = new FileInputStream(inputTemplatePath);
			Workbook workbook = null;
			if (inputTemplatePath.endsWith(".xls")) {
				workbook = new HSSFWorkbook(inputStream);
			}
			int workbook_no_of_sheet = workbook.getNumberOfSheets();
			int sheetIndex = 0, tempFlag = 0;
			for (int sheetNo = 0; sheetNo < workbook_no_of_sheet; sheetNo++) {
				if (workbook.getSheetName(sheetNo).trim()
						.equalsIgnoreCase(sheetName.trim())) {
					sheetIndex = sheetNo;
					tempFlag = 1;
				}
			}
			Sheet sheet = null;
			Row hssfRow = null;
			int temp_count = 0;
			Map cellDataMap = null;
			int[] vectorArr = new int[5];
			if (tempFlag == 1) {
				if (inputTemplatePath.endsWith(".xls")) {
					sheet = workbook.getSheetAt(sheetIndex);
				}

				Row firstRow = (Row) sheet.getRow(fieldsRow_No);
				Row sectionNameRow = (Row) sheet.getRow(sectionNameRow_No);
				for (int column = formatCol; column < firstRow.getPhysicalNumberOfCells(); column++) {
					Cell fieldCell = (Cell) firstRow.getCell(column,Row.CREATE_NULL_AS_BLANK);
					templateData[0].put(column, fieldCell.toString());
					globalColumn = column;
				}

				for (int column = formatCol; column < sectionNameRow.getPhysicalNumberOfCells(); column++) {
					Cell sectionNameCell = (Cell) sectionNameRow.getCell(column, Row.CREATE_NULL_AS_BLANK);
					if (sectionNameCell == null) {
					} else if (!sectionNameCell.toString().trim().isEmpty()) {
						if (!templateData[2].containsKey(sectionNameCell
								.toString().trim()))
							templateData[2].put(sectionNameCell.toString()
									.trim(), column);
					}
				}

				for (int row = recordStartedRow; row < sheet
						.getPhysicalNumberOfRows(); row++) {

					hssfRow = (Row) sheet.getRow(row);
					cellDataMap = new HashMap();
					int tempCount = formatCol, count = formatCol;
					globalCntRow = (row + 1);
					for (int column = formatCol; column < hssfRow
							.getPhysicalNumberOfCells(); column++) {
						Cell recodCell = hssfRow.getCell(column,
								Row.CREATE_NULL_AS_BLANK);
						String colVal = recodCell.toString();
						if (colVal.equalsIgnoreCase("")) {
							count++;
						}
					}
					if (count == hssfRow.getPhysicalNumberOfCells()) {

					} else {
						for (int column = 0; column < firstRow
								.getPhysicalNumberOfCells(); column++) {
							Cell recordCell = hssfRow.getCell(column,
									Row.CREATE_NULL_AS_BLANK);
							if (recordCell == null) {
								tempCount = tempCount + 1;
							} else {
								if (recordCell.toString().trim().isEmpty()) {
									tempCount = tempCount + 1;
								}
							}
							switch (recordCell.getCellType()) {
							case Cell.CELL_TYPE_NUMERIC:
								String str = new BigDecimal(
										recordCell.getNumericCellValue())
										.toPlainString();
								if (str.contains(".")) {
									int loc = str.indexOf(".");
									if (str.length() >= loc + 2) {
										cellDataMap.put(column, recordCell
												.getNumericCellValue());
									} else
										cellDataMap.put(
												column,
												new BigDecimal(recordCell
														.getNumericCellValue())
														.toPlainString());
								} else {
									cellDataMap.put(column, new BigDecimal(
											recordCell.getNumericCellValue())
											.toPlainString());
								}
								break;
							case Cell.CELL_TYPE_STRING:
								String cell = recordCell.toString();
								cellDataMap.put(column, cell);
								break;
							default:
								if (recordCell.toString().contains(".")) {
									int index = recordCell.toString().indexOf(
											".");
									cellDataMap.put(column, recordCell
											.toString().substring(index + 2));
								} else {
									cellDataMap.put(column,
											recordCell.toString());
								}
							}
						}

						if (sheetName.equals("Native Formats")) {
							// log.debug("sheetName ::"+sheetName);
							vectorArr[0] = 20;
							vectorArr[1] = 223;
							vectorArr[2] = 75;
							vectorArr[3] = 224;
							vectorArr[4] = 108;

						} else if (sheetName.equals("SWIFT")) {

							vectorArr[0] = 50;
							vectorArr[1] = 135;
							vectorArr[3] = 69;

						} else if (sheetName.equals("ANSI")) {
							vectorArr[0] = 28;
							vectorArr[1] = 165;
							vectorArr[2] = 64;
							vectorArr[3] = 166;

						}
						String strCreationDate = (String) cellDataMap
								.get(vectorArr[0]);
						String cmoCreationDate = (String) cellDataMap
								.get(vectorArr[3]);
						String valueDate = (String) cellDataMap
								.get(vectorArr[2]);
						String DateFundAvailable = (String) cellDataMap
								.get(vectorArr[4]);

						if ((!sheetName.equals("Native Formats") || temp_count != 0)
								&& strCreationDate != null
								&& !strCreationDate.equals("")) {
							if (strCreationDate.substring(0, 1).equals("0")) {

								cellDataMap.put(
										vectorArr[0],
										convertToJulian(new Date(), Integer
												.parseInt((String) cellDataMap
														.get(vectorArr[1]))));
							} else {
								formatter = new SimpleDateFormat("yyMMdd");
								cellDataMap
										.put(vectorArr[0],
												formatter.format(new Date()
														.getTime()
														+ (Long.parseLong((String) cellDataMap
																.get(vectorArr[1])) * 24 * 60 * 60 * 1000)));
							}
						} else if (!cmoCreationDate.equals("")
								&& sheetName.equals("SWIFT")
								&& cmoCreationDate.substring(0, 1).equals("1")) {
							String usdTime = cmoCreationDate.substring(6,
									cmoCreationDate.length());
							formatter = new SimpleDateFormat("yyMMdd");

							cellDataMap
									.put(vectorArr[3],
											formatter.format(new Date()
													.getTime()
													+ (Long.parseLong((String) cellDataMap
															.get(vectorArr[1])) * 24 * 60 * 60 * 1000))
													+ usdTime);

						}
						if (!sheetName.equals("SWIFT")
								&& (!sheetName.equals("Native Formats") || temp_count != 0)
								&& (valueDate != null && !valueDate.equals(""))) {
							if (sheetName.equals("ANSI")) {
								formatter = new SimpleDateFormat("yyyyMMdd");
								cellDataMap
										.put(vectorArr[2],
												formatter.format(new Date()
														.getTime()
														+ (Long.parseLong((String) cellDataMap
																.get(vectorArr[3])) * 24 * 60 * 60 * 1000)));
							} else if (valueDate.substring(0, 1).equals("0")) {
								cellDataMap.put(
										vectorArr[2],
										convertToJulian(new Date(), Integer
												.parseInt((String) cellDataMap
														.get(vectorArr[3]))));

							} else {
								formatter = new SimpleDateFormat("yyMMdd");

								cellDataMap
										.put(vectorArr[2],
												formatter.format(new Date()
														.getTime()
														+ (Long.parseLong((String) cellDataMap
																.get(vectorArr[3])) * 24 * 60 * 60 * 1000)));

							}

						}

						if (sheetName.equals("Native Formats")
								&& temp_count != 0 && DateFundAvailable != null
								&& !DateFundAvailable.equals("")) {
							if (DateFundAvailable.substring(0, 1).equals("0")) {
								cellDataMap.put(
										vectorArr[4],
										convertToJulian(new Date(), Integer
												.parseInt((String) cellDataMap
														.get(vectorArr[3]))));
							} else {
								formatter = new SimpleDateFormat("yyMMdd");
								cellDataMap
										.put(vectorArr[4],
												formatter.format(new Date()
														.getTime()
														+ (Long.parseLong((String) cellDataMap
																.get(vectorArr[3])) * 24 * 60 * 60 * 1000)));
							}
						}
						if (firstRow.getPhysicalNumberOfCells() != tempCount) {
							// String formatName =(String) cellDataMap.get(2);
							//
							// cellDataMap =
							// UpdateFieldValues.update(cellDataMap,formatName,sheetName);
							// log.debug("temp_count ::"+temp_count);
							// log.debug("cellDataMap ::"+cellDataMap);
							templateData[1].put(temp_count, cellDataMap);

							temp_count++;
							// log.debug("temp_count :::"+temp_count);

						}
					}
				}

				temp_count = 0;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new FileNotFoundException();

		} catch (Exception e) {
			log.error("exception occurred : ", e);
			// log.fatal("Exception at Column position"+globalColumn+" and row :::"+globalCntRow);
			throw new Exception("Exception at Column position" + globalColumn
					+ " and row :::" + globalCntRow);

		}
		//log.debug("Fields name  :::"+templateData[0]);
		//log.debug("Records Data :::"+templateData[1]);
		//log.debug("Section Name :::"+templateData[2]);
		
		return templateData;

	}

	// To get Field Names from DB
	public Map<String, Integer> getFieldsFromDB(Connection connection,
			String tableName, String formatName, String sectionName,
			Map<String, Integer> sectionTagsMap) throws SQLException {

		Map<String, Integer> dbFieldsMap = null;
		String query = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			query = "select * from " + tableName + " where format_name='"
					+ formatName.trim() + "'" + " and section='" + sectionName
					+ "' order by order_sequence";
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			dbFieldsMap = new LinkedHashMap<String, Integer>();

			while (resultSet.next()) {
				String fieldName = resultSet.getString("FIELD_NAME");
				int length = resultSet.getInt("length");
				if (fieldName == null) {
				} else if (!fieldName.trim().isEmpty()) {
					dbFieldsMap.put(fieldName, length);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			DBUtility.closeDBResourcePstmt(resultSet, preparedStatement);
		}

		return dbFieldsMap;
	}

	// To get format names from DB
	public Set<String> getFormatsNameList(Connection connection,
			String tableName) throws Exception {

		Set<String> formatNames = null;
		String query = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			query = "select * from " + tableName + " order by format_name asc";
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			formatNames = new TreeSet<String>();

			while (resultSet.next()) {
				String formatName = resultSet.getString("format_name");
				if (formatName == null) {
				} else if (!formatName.trim().isEmpty())
					formatNames.add(formatName);

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		} finally {
			DBUtility.closeDBResourcePstmt(resultSet, preparedStatement);
		}
		return formatNames;
	}

	// To get section from DB
	public Map<String, Integer> getSectionsMapFromDB(Connection connection,
			String tableName, String formatName) throws SQLException {
		String query = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Map<String, Integer> sectionMap = new HashMap<String, Integer>();

		try {
			query = "select * from " + tableName + " where format_name='"
					+ formatName.trim() + "'" + " order by order_sequence";
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			int section = 0;
			while (resultSet.next()) {
				if (resultSet.getString("section") == null) {
				} else if (!resultSet.getString("section").trim().isEmpty()) {
					if (!sectionMap.containsKey(resultSet.getString("section"))) {
						sectionMap.put(resultSet.getString("section"), section);
						section = section + 1;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			DBUtility.closeDBResourcePstmt(resultSet, preparedStatement);
		}
		return sectionMap;
	}

	// To get index from input template file after comparing fields from
	// template with DB fields
	public Map<Integer, ArrayList<Integer>> getIndexOfInputFile(
			Map<String, Integer> fieldsMapFromDB,
			Map<Integer, String> fieldsMapFromTemplate, int indexStarted) {
		Map<Integer, ArrayList<Integer>> indexDataMap = new HashMap<Integer, ArrayList<Integer>>();
		int count = 0;
		// log.debug("fieldsMapFromDB :::"+fieldsMapFromDB);
		// log.debug("fieldsMapFromTemplate :::"+fieldsMapFromTemplate);
		for (Map.Entry<String, Integer> entry : fieldsMapFromDB.entrySet()) {
			String key_fieldDB = entry.getKey();
			int value_fieldDB = entry.getValue();
			boolean status = false;
			ArrayList<Integer> index = new ArrayList<Integer>();
			int tempIndex = 0;
			int keyfield_Template = 0;

			for (Map.Entry<Integer, String> entry1 : fieldsMapFromTemplate
					.entrySet()) {

				keyfield_Template = entry1.getKey();
				String valuefield_Template = entry1.getValue();
				// log.debug("keyfield_Template :::"+keyfield_Template);
				// log.debug("valuefield_Template :::"+valuefield_Template);
				// log.debug("indexStarted :::"+indexStarted);

				if (indexStarted <= keyfield_Template) {
					tempIndex = keyfield_Template;

					if (key_fieldDB
							.toString()
							.trim()
							.replace(" ", "")
							.equalsIgnoreCase(
									valuefield_Template.trim().replace(" ", ""))) {

						index.add(keyfield_Template);
						index.add(value_fieldDB);
						indexDataMap.put(count, index);
						count = count + 1;
						status = true;
						break;
					}

				}
			}
			if (!status) {
				// log.debug("tempIndex  ::"+tempIndex);
				// log.debug("value_fieldDB  ::"+value_fieldDB);
				index.add(tempIndex);
				index.add(value_fieldDB);
				// log.debug("count  ::"+count);
				// log.debug("index  ::"+index);
				indexDataMap.put(count, index);
				count = count + 1;
			}
		}

		// log.debug("indexDataMap  ::"+indexDataMap);
		return indexDataMap;
	}

	// To get started index number of section name from input template file
	public int getOfSectiontagIndex(Connection connection, String tableName,
			String formatName, String sectionName,
			Map<String, Integer> sectionTagsMap) throws SQLException {
		int num = 0;
		String query = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			query = "select * from " + tableName + " where format_name='"
					+ formatName.trim() + "'" + " and section='" + sectionName
					+ "' order by order_sequence";
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			boolean status = false;
			int j = 0;
			Map<String, Integer> tempSectionNameSet = new LinkedHashMap<String, Integer>();
			while (resultSet.next()) {
				String tempSecName = resultSet.getString("SECTION_NAME");
				if (tempSecName == null) {
				} else if (!tempSecName.trim().isEmpty()) {
					if (!tempSectionNameSet.containsKey(tempSecName.trim())) {
						tempSectionNameSet.put(tempSecName, j);
						j = j++;
					}
				}
			}
			for (Map.Entry<String, Integer> entry1 : tempSectionNameSet
					.entrySet()) {
				String sectionNameStr = entry1.getKey();
				for (Map.Entry<String, Integer> entry : sectionTagsMap
						.entrySet()) {
					String key = entry.getKey();
					int value = entry.getValue();
					if (sectionNameStr.trim().replace(" ", "")
							.equalsIgnoreCase(key.trim().replace(" ", ""))) {
						status = true;
						num = value;
						break;
					}
				}
				if (status) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			DBUtility.closeDBResourcePstmt(resultSet, preparedStatement);
		}

		return num;
	}

	// To get Template Value and DataBase Tags.
	public Map<String, String> getIndexOfInputFileMTCMO(
			Map<String, Integer> fieldsMapFromDB,
			Map<Integer, String> fieldsMapFromTemplate, int indexStarted,
			Connection connection, String formatName, String tableName,
			Map<Integer, String> templateRecordsData) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("getIndexOfInputFileMTCMO method start in ReadTemplate");
		}
		String sectionTag = "", tmpData = "";
		Map<String, String> indexDataMap = new LinkedHashMap<String, String>();
		Map<String, String> indexDataMapValue = new LinkedHashMap<String, String>();
		Map<String, String> indexDataMapTag = new LinkedHashMap<String, String>();
		int count = 0;
		String formatNeededColumnNo = FormatConstants.formatColumn;
		int formatNeededColumn_No = Integer.parseInt(formatNeededColumnNo);
		for (Map.Entry<String, Integer> entry : fieldsMapFromDB.entrySet()) {
			String key_fieldDB = entry.getKey();
			int keyfield_Template = 0;

			for (Map.Entry<Integer, String> entry1 : fieldsMapFromTemplate
					.entrySet()) {
				keyfield_Template = entry1.getKey();
				String valuefield_Template = entry1.getValue();
				if (indexStarted <= keyfield_Template) {
					if (key_fieldDB
							.toString()
							.trim()
							.replace(" ", "")
							.equalsIgnoreCase(
									valuefield_Template.trim().replace(" ", ""))) {
						sectionTag = getSectionTagFromDb(connection, tableName,
								formatName, key_fieldDB);
						tmpData = (String) templateRecordsData
								.get(keyfield_Template);
						indexDataMapValue.put(key_fieldDB, tmpData);
						indexDataMapTag.put(key_fieldDB, sectionTag);
						count = count + 1;
						break;
					}
				}
			}
		}
		String oldTag = "", newTag = "";
		Set<String> keys = indexDataMapValue.keySet();
		for (String key : keys) {
			String tag = indexDataMapTag.get(key);
			String value = indexDataMapValue.get(key).trim();
			if (tag != null) {
				if (tag.endsWith("?")) {
					if (indexDataMap.containsKey(tag)) {
						String val = "";
						String tmpValue = indexDataMap.get(tag);
						if (tag.equalsIgnoreCase("1")
								|| tag.equalsIgnoreCase("2")
								|| tag.equalsIgnoreCase("3")
								|| tag.equalsIgnoreCase("5")) {
							val = tmpValue + value;
						} else {
							val = tmpValue + "\n" + value;
						}
						if (!value.isEmpty() && !value.equalsIgnoreCase("")
								&& value != null) {
							indexDataMap.put(tag, val);
						}
					} else {
						if (tag.endsWith("?")) {
							if (value.trim().length() == 1
									&& (!value.matches("@|~|%|#|!|^|~|=|`"))) {
								oldTag = tag;
								newTag = tag.replace("?", value);
							} else if (value.length() == 0) {
								if(formatName.equalsIgnoreCase(FormatConstants.cmo103.replace(" ", "")) 
										&& templateRecordsData.containsKey(formatNeededColumn_No+87) 
										&& templateRecordsData.get(formatNeededColumn_No+87).equalsIgnoreCase("D")
										&& (templateRecordsData.get(formatNeededColumn_No+88) == null || templateRecordsData.get(formatNeededColumn_No+88).isEmpty()) 
										&& !tag.contains("59")
										){
									continue;
								}
								if (value.trim().equalsIgnoreCase("")) {
									oldTag = tag;
									newTag = tag.replace("?", "");
								}
							} else {
								if (!value.isEmpty()
										&& !value.equalsIgnoreCase("")
										&& value != null) {
									if (oldTag.equalsIgnoreCase(tag)) {
										if (indexDataMap.containsKey(newTag)) {
											String val = indexDataMap
													.get(newTag);
											indexDataMap.put(newTag, val + "\n"
													+ value);
										} else {
											indexDataMap.put(newTag, value);
										}
									}
								}
							}
						}
					}
				} else {
					if (indexDataMap.containsKey(tag)) {
						String val = "";
						String tmpValue = indexDataMap.get(tag);
						if (tag.equalsIgnoreCase("1")
								|| tag.equalsIgnoreCase("2")
								|| tag.equalsIgnoreCase("3")
								|| tag.equalsIgnoreCase("5")) {
							val = tmpValue + value;
						} else {
							val = tmpValue + "\n" + value;
						}
						if (!value.isEmpty() && !value.equalsIgnoreCase("")
								&& value != null) {
							indexDataMap.put(tag, val);
						}
					} else {
						if (!value.isEmpty() && !value.equalsIgnoreCase("")
								&& value != null) {
							indexDataMap.put(tag, value);
						}
					}
				}
			}
		}
		return indexDataMap;
	}

	// To get SectionTag from DataBase.
	private String getSectionTagFromDb(Connection connection, String tableName,
			String formatName, String oldfieldName) throws Exception {
		String sectiontag = null;
		String query = null;
		String fieldName = oldfieldName;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			query = "select * from "
					+ tableName
					+ " where format_name=? and field_name=? order by order_sequence";
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, formatName);
			preparedStatement.setString(2, fieldName);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				sectiontag = resultSet.getString("section_tag");
			}
		} catch (Exception e) {
			// e.printStackTrace();
			throw new Exception(e);
		} finally {
			DBUtility.closeDBResourcePstmt(resultSet, preparedStatement);
		}
		return sectiontag;
	}

	// To get the FolderName from template ( Excel file )
	public String getFolderName(String inputTemplatePath, String sheetName)
			throws IOException {

		String folderName = "";
		InputStream inputStream = new FileInputStream(inputTemplatePath);
		Workbook workbook = null;
		if (inputTemplatePath.endsWith(".xls")) {
			workbook = new HSSFWorkbook(inputStream);
		}
		int workbook_no_of_sheet = workbook.getNumberOfSheets();
		// log.debug("workbook_no_of_sheet ::"+workbook_no_of_sheet);
		int sheetIndex = 0, tempFlag = 0;
		for (int sheetNo = 0; sheetNo < workbook_no_of_sheet; sheetNo++) {
			if (workbook.getSheetName(sheetNo).trim()
					.equalsIgnoreCase(sheetName.trim())) {
				sheetIndex = sheetNo;
				tempFlag = 1;
			}
		}
		Sheet sheet = null;

		if (tempFlag == 1) {
			if (inputTemplatePath.endsWith(".xls")) {
				sheet = workbook.getSheetAt(sheetIndex);
			}
			Row firstRow = (Row) sheet.getRow(0);
			Cell fieldCell = (Cell) firstRow.getCell(1,
					Row.CREATE_NULL_AS_BLANK);
			folderName = fieldCell.toString();
		}
		return folderName;
	}

	// 1.2 start
	public void insertIntoAuditTable(String suiteName , Connection conn) throws Exception {

		if (log.isDebugEnabled()) {
			log.debug("insertDataFromDB method start in ReadTemplate");
		}

		int seqnum = 0;
		String query = null;
		String  selectMainTable=null;
		String selectQuery=null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		PreparedStatement psmt1=null;
		ResultSet rs1=null;
		try {

			selectQuery = "select nvl(max(SEQNUM),0)+1 from  "
					+ FormatConstants.auditTableName + "  where suite_name =?";
			psmt = conn.prepareStatement(selectQuery);
			psmt.setString(1, suiteName);
			rs=psmt.executeQuery();

			while (rs.next()){
				seqnum = rs.getInt(1);
			}
			rs.close();
			psmt.close();
			selectMainTable = "select * from test_tool_file_name where suite_name =?";
			psmt  =conn.prepareStatement(selectMainTable);
			psmt.setString(1, suiteName);
			rs= psmt.executeQuery();
			while(rs.next()){
				query = "Insert into "+ FormatConstants.auditTableName+"(SEQNUM,TESTCASEID,FORMATNAME,CHANNEL,LSI,CLIENT_FILE_REF,BIC_CODE,BBDID,"
						+ "CUST_REF,UNIQUE_ID,FILENAME,TIMESTAMP,APPLIED_RULES,ERROR_CODES,MODIFIED_FIELD,FOLDER_NAME,SUITE_NAME) "
						+ "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				psmt1=conn.prepareStatement(query);
				psmt1.setInt(1, seqnum);
				psmt1.setString(2, rs.getString("TESTCASEID"));
				psmt1.setString(3, rs.getString("FORMATNAME"));
				psmt1.setString(4, rs.getString("CHANNEL"));
				psmt1.setString(5, rs.getString("LSI"));

				psmt1.setString(6, rs.getString("CLIENT_FILE_REF"));
				psmt1.setString(7, rs.getString("BIC_CODE"));
				psmt1.setString(8, rs.getString("BBDID"));
				psmt1.setString(9, rs.getString("CUST_REF"));

				psmt1.setString(10, rs.getString("UNIQUE_ID"));
				psmt1.setString(11, rs.getString("FILENAME"));
				psmt1.setTimestamp(12, rs.getTimestamp("TIMESTAMP"));
				psmt1.setString(13, rs.getString("APPLIED_RULES"));

				psmt1.setString(14, rs.getString("ERROR_CODES"));
				psmt1.setString(15, rs.getString("MODIFIED_FIELD"));
				psmt1.setString(16, rs.getString("FOLDER_NAME"));
				psmt1.setString(17, rs.getString("SUITE_NAME"));
				psmt1.executeUpdate();
			}
		}catch (NullPointerException np) {
			np.printStackTrace();
		} catch (NumberFormatException ex) {
			throw (ex);
		} catch (Exception e) {
			throw (e);
		} finally {
			DBUtility.closePreparedStatement(psmt);
			DBUtility.closePreparedStatement(psmt1);
			DBUtility.closeResultSet(rs);
			DBUtility.closeResultSet(rs1);
	}
}

	public void insertFileInDB(String testCaseId, String format, String chanel,
			String lsi, String seq_num, String BIC_Code, String BBDID,
			String Cust_Ref, String Unique_Id, String fileName,
			String appliedRules, String errorCodes, String modifiedField,
			String db_table_file, String suite_name ,Connection connection, String timeStamp)
			throws Exception {

		if (log.isDebugEnabled()) {
			log.debug("insertFileInDB method start in ReadTemplate");
		}
		String folderName = "";
		String query = null;
		PreparedStatement statement = null;

		ReadTemplate readTemplate1 = new ReadTemplate();
		String filname1 = fileName;
		String fileSheetName = FormatConstants.folderNameSheetName;
		String inputTemplatePath = FormatConstants.inputFile;
		if (fileName.contains("&")) {
			fileName = fileName.replace("&", ":");
			if (fileName.contains(".tmp")) {
				fileName = fileName.replace(".tmp", "");
			}
		}
		try {
			filname1 = inputTemplatePath + FormatConstants.tempFileName
					+ ".xls";
			folderName = readTemplate1.getFolderName(filname1, fileSheetName);
			query = "insert into  " + db_table_file + "  values ('" 
					+ testCaseId + "','" + format + "','" + chanel + "','"
					+ lsi + "','" + seq_num + "','" + BIC_Code + "','" + BBDID
					+ "','" + Cust_Ref + "','" + Unique_Id + "','" + fileName
					+ "',to_date('" + getCurrentTimeStamp()
					+ "', 'yyyy/mm/dd hh24:mi:ss'),'" + appliedRules + "','"
					+ errorCodes + "','" + modifiedField + "','" + folderName + "','"+suite_name
					+ "')";
			statement = connection.prepareStatement(query);
			statement.executeUpdate(query);

		} catch (Exception e) {
			log.error("Error occured while inserting in insertFileInDB !!!!", e);
			throw e;
		} finally {
			DBUtility.closeStatement(statement);
		}

	}
	
	public Integer fetchDataDetails(String suiteName ,Connection con) throws Exception {

		if (log.isDebugEnabled()) {
			log.debug("SelectDataFromDB method start in ReadTemplate");
		}
		
		String query = null;
		PreparedStatement pStmt = null;
		ResultSet resSet = null;
		int rowCount=0;
		try {
			 System.out.println("suiteName ::"+suiteName);
			query = "select count(*) from "+FormatConstants.fileNamesDbName+"  where suite_name  =? ";
			System.out.println("query ::"+query);
			pStmt = con.prepareStatement(query);

			pStmt.setString(1, suiteName);
			resSet = pStmt.executeQuery();
			
				resSet.next();
				rowCount = resSet.getInt(1);
			    System.out.println(rowCount);
	
	} catch (Exception e) {
		log.error(
				"Error occured while fetching from  fetchDataDetails !!!!",
				e);
		throw e;
	} finally {
		DBUtility.closeDBResourcePstmt(resSet, pStmt);
		// DBUtility.closeConnection(conn);
	}
		return rowCount;
}
	
	public void deleteDataFromDB(String suiteName,Connection conn)
			throws Exception {

		String deleteSQL = null;
		PreparedStatement psmt = null;
		try {
			deleteSQL = "DELETE from  " + FormatConstants.fileNamesDbName
					+ "  WHERE suite_name  = ? ";
			psmt = conn.prepareStatement(deleteSQL);
			psmt.setString(1, suiteName);
			psmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			// throw e;
		} finally {
			DBUtility.closePreparedStatement(psmt);
	

		}
	}

	// code changes for select from test_tool_file_name table
	public Map<String, String> fetchDataDetails(String testCaseId,
			String formatname) throws Exception {

		if (log.isDebugEnabled()) {
			log.debug("SelectDataFromDB method start in ReadTemplate");
		}
		Connection conn = null;

		String query = null;
		PreparedStatement pStmt = null;
		ResultSet resSet = null;
		Map<String, String> map1 = new HashMap<String, String>();
		try {
			conn = DBUtility.openConnection();
			query = "select * from " + FormatConstants.fileNamesDbName
					+ " where TESTCASEID =? and FORMATNAME = ? ";
			pStmt = conn.prepareStatement(query);

			pStmt.setString(1, testCaseId);
			pStmt.setString(2, formatname);
			resSet = pStmt.executeQuery();

			while (resSet.next()) {
				map1.put("TESTCASEID", resSet.getString("TESTCASEID"));
				map1.put("FORMATNAME", resSet.getString("FORMATNAME"));
				map1.put("CHANNEL", resSet.getString("CHANNEL"));
				map1.put("LSI", resSet.getString("LSI"));
				map1.put("CLIENT_FILE_REF", resSet.getString("CLIENT_FILE_REF"));
				map1.put("BIC_CODE", resSet.getString("BIC_CODE"));
				map1.put("BBDID", resSet.getString("BBDID"));
				map1.put("CUST_REF", resSet.getString("CUST_REF"));
				map1.put("UNIQUE_ID", resSet.getString("UNIQUE_ID"));
				map1.put("FILENAME", resSet.getString("FILENAME"));
				map1.put("TIMESTAMP", String.valueOf(resSet.getTimestamp(
						"TIMESTAMP").getTime()));
				// map1.put("TIMESTAMP", resSet.getString("TIMESTAMP"));
				map1.put("APPLIED_RULES", resSet.getString("APPLIED_RULES"));
				map1.put("ERROR_CODES", resSet.getString("ERROR_CODES"));
				map1.put("MODIFIED_FIELD", resSet.getString("MODIFIED_FIELD"));
				map1.put("FOLDER_NAME", resSet.getString("FOLDER_NAME"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			DBUtility.closeDBResourcePstmt(resSet, pStmt);
			DBUtility.closeConnection(conn);
		}
		return map1;

	}

	// Insert records into test_tool_file_name_audit table
	public void insertDataFromDB(String testCaseId, String format,
			Map<String, String> insertMap) throws Exception {

		if (log.isDebugEnabled()) {
			log.debug("insertDataFromDB method start in ReadTemplate");
		}

		insertMap = new HashMap<String, String>();
		insertMap = fetchDataDetails(testCaseId, format);
		int seqnum = 0;
		Connection conn = null;
		String query = null;
		Statement psmt = null;
		PreparedStatement psmt1 = null;
		ResultSet rs1 = null;

		try {
			conn = DBUtility.openConnection();
			String selectQuery = "select nvl(max(SEQNUM),0)+1 from  "
					+ FormatConstants.auditTableName + "  where testCaseId =?";

			psmt1 = conn.prepareStatement(selectQuery);
			psmt1.setString(1, testCaseId);
			rs1 = psmt1.executeQuery();
			while (rs1.next())
				seqnum = rs1.getInt(1);

			query = "insert into  "
					+ FormatConstants.auditTableName
					+ "  values ('"
					+ seqnum
					+ "','"
					+ insertMap.get("TESTCASEID")
					+ "','"
					+ insertMap.get("FORMATNAME")
					+ "','"
					+ insertMap.get("CHANNEL")
					+ "','"
					+ insertMap.get("LSI")
					+ "','"
					+ insertMap.get("CLIENT_FILE_REF")
					+ "','"
					+ insertMap.get("BIC_CODE")
					+ "','"
					+ insertMap.get("BBDID")
					+ "','"
					+ insertMap.get("CUST_REF")
					+ "','"
					+ insertMap.get("UNIQUE_ID")
					+ "','"
					+ insertMap.get("FILENAME")
					+ "',to_date('"
					+ new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Long
							.parseLong(insertMap.get("TIMESTAMP")))
					+ "', 'yyyy/mm/dd hh24:mi:ss'),'"
					+ insertMap.get("APPLIED_RULES") + "','"
					+ insertMap.get("ERROR_CODES") + "','"
					+ insertMap.get("MODIFIED_FIELD") + "','"
					+ insertMap.get("FOLDER_NAME") + "')";
			psmt = conn.createStatement();
			psmt.executeUpdate(query);

		} catch (NullPointerException np) {
			np.printStackTrace();
		} catch (NumberFormatException ex) {
			throw (ex);
		} catch (Exception e) {
			throw (e);
		} finally {
			DBUtility.closeStatement(psmt);
			DBUtility.closeConnection(conn);

		}

	}

	// Code changes for delete records from test_tool_file_name table
	public void deleteDataFromDB(String testcaseid, String formatname)
			throws Exception {

		Connection conn = null;
		String deleteSQL = null;
		PreparedStatement psmt = null;
		try {
			conn = DBUtility.openConnection();

			deleteSQL = "DELETE from  " + FormatConstants.fileNamesDbName
					+ "  WHERE testcaseid  = ? ";

			psmt = conn.prepareStatement(deleteSQL);
			psmt.setString(1, testcaseid);
			psmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			// throw e;
		} finally {
			DBUtility.closePreparedStatement(psmt);
			DBUtility.closeConnection(conn);

		}
	}

	// 1.2 end

	/*
	 * converts current date to julian
	 */

	public static String dateToJulian(String unformattedDate) {
		String resultJulian = null;
		if (unformattedDate.length() > 0) {
			int[] monthValues = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30,
					31 };

			String dayS, monthS, yearS;
			dayS = unformattedDate.substring(4, 6);
			monthS = unformattedDate.substring(2, 4);
			yearS = unformattedDate.substring(0, 2);

			int day = Integer.valueOf(dayS);
			int month = Integer.valueOf(monthS);
			int year = Integer.valueOf(yearS);
			// log.debug("day ::"+day +"month "+ month +"year"+year) ;
			if (year % 4 == 0) {
				monthValues[1] = 29;
			}
			String julianDate = "0";
			julianDate += yearS.substring(0, 2);
			// log.debug("julianDate :::"+julianDate);
			int julianDays = 0;
			for (int i = 0; i < month - 1; i++) {
				julianDays += monthValues[i];
			}
			// log.debug("julianDays :::"+julianDays);
			// log.debug("String.valueOf(julianDays).length()"+String.valueOf(julianDays).length());
			julianDays += day;
			if (String.valueOf(julianDays).length() < 2) {
				julianDate += "00";
			} else if (String.valueOf(julianDays).length() < 3) {
				julianDate += "0";
			}

			julianDate += String.valueOf(julianDays);
			// log.debug("julianDate::::"+julianDate);
			resultJulian = julianDate;
		}
		// log.debug("resultJulian::::"+resultJulian);
		return resultJulian;
	}

	public static String convertToJulian(Date date, int days)
			throws ParseException {
		// log.debug("Inside convert to julian:::");
		DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
		Calendar c = Calendar.getInstance();
		String dateOutput = dateFormat.format(new Date());
		Date date2 = new SimpleDateFormat("yyMMdd").parse(dateOutput);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date2);
		int year = calendar.get(Calendar.YEAR);
		String syear = String.format("%04d", year).substring(2);
		int century = Integer.parseInt(String.valueOf((year)).substring(2));
		calendar.setTime(dateFormat.parse(dateOutput));
		calendar.add(Calendar.DATE, days); // number of days to add
		dateOutput = dateFormat.format(calendar.getTime()); // dt is now the new
															// date
		// log.debug("dateToJulian(dateOutput) ::"+dateToJulian(dateOutput));
		return dateToJulian(dateOutput);

	}

	private static String getCurrentTimeStamp() {

		java.util.Date today = new java.util.Date();
		return dateFormat.format(today.getTime());

	}
	
	// 1.3 start
	public void insertIntoTestCasesAuditTable(String testCaseId, String testCaseType,
			String excelFileName, String filePath, String exceutionTime,
			 Connection connection) {
		if (log.isDebugEnabled()) {
			log.debug("insertIntoTestCasesAuditTable method start in ReadTemplate  testCaseId "+testCaseId);
		}

		Map<String, String> outerInsertMap = null;
		try {

			// Select records from TEST_CASE_DETAILS table
			outerInsertMap = fetchTestcaseDetails(testCaseId,connection);
			System.out.println("outerInsertMap ::"+outerInsertMap);
			if (outerInsertMap.size() != 0) {
				insertDataIntoAuditFromDB(testCaseId, outerInsertMap,connection);
				deleteDataFromTestCaseDB(testCaseId,connection);
			}
			
			// Insert records into TEST_CASE_DETAILS
			insertTestCasesInDB(testCaseId,testCaseType,excelFileName,filePath,exceutionTime,connection);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.fatal("Exception Occured :: ", e);

		}

	}

	public void insertTestCasesInDB(String testCaseId, String testCaseType,
			String excelFileName, String filePath, String exceutionTime,
			Connection connection)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("insertTestCasesInDB method start in ReadTemplate");
		}
		String query = null;
		PreparedStatement statement = null;

		try {
			
			/*query = "insert into " + FormatConstants.testcaseTableName + " values ('" + testCaseId
					+ "','" + testCaseType + "','" + excelFileName + "','" + filePath + "',to_date('" + getCurrentTimeStamp()+", 'yyyy/mm/dd hh24:mi:ss'))";
			// 1.1 end
*/
			query = "insert into " + FormatConstants.testcaseTableName + " values (?,?,?,?,?)";
			statement = connection.prepareStatement(query);
			statement.setString(1, testCaseId);
			statement.setString(2, testCaseType);
			statement.setString(3, excelFileName);
			statement.setString(4, filePath);
			statement.setString(5, getCurrentTimeStamp());
			// execute insert SQL stetement
			statement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtility.closeStatement(statement);
		}

	}

	// code changes for select from TEST_CASE_DETAILS table
	public Map<String, String> fetchTestcaseDetails(String testCaseId,Connection connection) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("fetchTestcaseDetails method start in ReadTemplate");
		}
		
		String query = null;
		PreparedStatement pStmt = null;
		ResultSet resSet = null;
		Map<String, String> map1 = new HashMap<String, String>();
		try {
			query = "select * from " + FormatConstants.testcaseTableName
					+ " where TESTCASEID =? ";
			pStmt = connection.prepareStatement(query);

			pStmt.setString(1, testCaseId);
			resSet = pStmt.executeQuery();

			while (resSet.next()) {
				map1.put("TESTCASEID", resSet.getString("TESTCASEID"));
				map1.put("TESTCASETYPE", resSet.getString("TESTCASETYPE"));
				map1.put("EXCELFILENAME", resSet.getString("EXCELFILENAME"));
				map1.put("FILEPATH", resSet.getString("FILEPATH"));
				map1.put("EXECUTEDTIME",resSet.getString("EXECUTEDTIME"));
				
			}
		} catch (Exception e) {
			 e.printStackTrace();
			//throw e;
		} finally {
			DBUtility.closeDBResourcePstmt(resSet, pStmt);
			
		}
		return map1;

	}

	// Insert records into TEST_CASE_DETAILS_AUDIT table
	public void insertDataIntoAuditFromDB(String testCaseId,
			Map<String, String> insertMap,Connection connection) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("insertDataIntoAuditFromDB method start in ReadTemplate");
		}

		insertMap = new HashMap<String, String>();
		insertMap = fetchTestcaseDetails(testCaseId,connection);
		
		int seqnum = 0;
		String query = null;
		PreparedStatement psmt = null;
		PreparedStatement psmt1 = null;
		ResultSet rs1 = null;

		try {
			String selectQuery = "select nvl(max(SEQNUM),0)+1 from  "
					+ FormatConstants.testcaseAuditTableName + "  where testCaseId =?";
			System.out.println("select Query ::"+selectQuery);
			psmt1 = connection.prepareStatement(selectQuery);
			psmt1.setString(1, testCaseId);
			rs1 = psmt1.executeQuery();
			while (rs1.next())
				seqnum = rs1.getInt(1);
				
			query ="INSERT INTO "+FormatConstants.testcaseAuditTableName+" VALUES (?,?,?,?,?,?)";
			psmt = connection.prepareStatement(query);
			psmt.setInt(1, seqnum);
			psmt.setString(2, insertMap.get("TESTCASEID"));
			psmt.setString(3, insertMap.get("TESTCASETYPE"));
			psmt.setString(4, insertMap.get("EXCELFILENAME"));
			psmt.setString(5, insertMap.get("FILEPATH"));
			psmt.setString(6, insertMap.get("EXECUTEDTIME"));
			psmt.executeUpdate();

		} catch (NullPointerException np) {
			np.printStackTrace();
		} catch (NumberFormatException ex) {
			throw (ex);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtility.closePreparedStatement(psmt);
			DBUtility.closePreparedStatement(psmt1);
			DBUtility.closeResultSet(rs1);
			
		}

	}

	// Code changes for delete records from TEST_CASE_DETAILS table
	public void deleteDataFromTestCaseDB(String testcaseid,Connection connection)
			throws Exception {
		String deleteSQL = null;
		PreparedStatement psmt = null;
		try {

			deleteSQL = "DELETE from  " + FormatConstants.testcaseTableName
					+ "  WHERE testcaseid  = ? ";
			psmt = connection.prepareStatement(deleteSQL);
			psmt.setString(1, testcaseid);
			psmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			// throw e;
		} finally {
			DBUtility.closePreparedStatement(psmt);
			
		}
	}

	// 1.3 end


	public Map<String,String> readRegressionSuitesConfigData()
			throws Exception {
		Map<String,String> configData=new HashMap<String,String>();
		String queryStr = null;
		PreparedStatement psmt = null;
		ResultSet resultSet=null;
		Connection connection=null;
		try {
			connection=DBUtility.openConnection();
			queryStr = "Select SUITE_NAME , INPUT_FILE_PATH from  " + FormatConstants.testSuiteConfigTable;
			System.out.println("here");
			psmt = connection.prepareStatement(queryStr);
			resultSet=psmt.executeQuery();
			while(resultSet.next()){
				configData.put(resultSet.getString("SUITE_NAME"), resultSet.getString("INPUT_FILE_PATH"));
			}
		} catch (Exception e) {
			log.error("Error occured while fetching regression suite names from database!!!!", e);
		} finally {
			DBUtility.closePreparedStatement(psmt);
			DBUtility.closeConnection(connection);
			
		}
		return configData;
	}
	
	public String getRegressionSuiteInputFile(String suiteName)
			throws Exception {
		String fileName="";
		String queryStr = null;
		Connection connection=null;
		PreparedStatement psmt = null;
		ResultSet resultSet=null;
		try {
			connection=DBUtility.openConnection();
			queryStr = "Select INPUT_FILE_PATH from  " + FormatConstants.testSuiteConfigTable+" where SUITE_NAME=?";
			psmt = connection.prepareStatement(queryStr);
			psmt.setString(1, suiteName);
			resultSet=psmt.executeQuery();
			if(resultSet.next()){
				fileName=resultSet.getString("INPUT_FILE_PATH");
			}
		} catch (Exception e) {
			log.error("Error occured while fetching regression suite names from database!!!!", e);
		} finally {
			DBUtility.closePreparedStatement(psmt);
			DBUtility.closeConnection(connection);
			
		}
		return fileName;
	}
	
}
