/********************************************************************************************************/
/* Copyright �  2016 Intellect Design Arena Ltd. All rights reserved                        			*/
/*                                                                                  				    */
/********************************************************************************************************/
/*  Application  : Intellect Payment Engine																*/
/* 								 																		*/
/*  Module Name  : Generating Files for Testing															*/
/*                                                                                     					*/
/*  File Name    : ConvertFileISOPain008.java                                          					*/
/*                                                                                      				*/
/********************************************************************************************************/
/*  			  Author					    |        	  Date   			|	  Version           */
/********************************************************************************************************/
/*         Veera Kumar Reddy.V		            |			20:05:2016			|		1.0	            */
/*         Preetam Sanjore  		            |			07:03:2017			|		1.1	            */
/*		   Sapna Jain							|			18:07:2017			|		1.2
 /* 	   Preetam Sanjore				    	|			20:07:2017			|		1.3	
 /* 	   Gokaran Tiwari						|			22:03:2018			|		1.4 latest suite change
 /* 																					 in main table , old move to audit
/********************************************************************************************************/
/* 											  														    */
/* Description   : For generation of PAIN 008 format 													*/
/*        																								*/
/********************************************************************************************************/
package com.intellect.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EmptyStackException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.log4j.Logger;

import com.intellect.pojo.TestToolFileNamePOJO;
import com.intellect.property.FormatConstants;
import com.intellect.service.FormatGenerateService;
import com.intellect.service.UpdateFormatXlsService;

public class ConvertFileISOPain008 {

	private static org.apache.log4j.Logger log = Logger
			.getLogger(ConvertFileISOPain008.class);

	int m = 7, p = 7;
	int batchrowCount = 7;
	LinkedHashMap<Integer, Vector<String>> map = null;
	LinkedHashMap<Integer, Vector<String>> map2 = null;
	ReadTemplate readTemplate = new ReadTemplate();
	int filesCrtdCnt = 0;

	// BAD_FILES_CHANGES
	private String invalidAppender = "001";

	public int main(String source, File target, String fileTypeSelected,
			LinkedHashMap<Integer, Vector<String>> map2, Connection connection,
			String db_table_file, String timeStamp, String dbTimeStamp,
			String moduleSelected, char[] variants, List<TestToolFileNamePOJO> testToolData) throws Exception {

		ConvertFileISOPain008 xmlWriter = new ConvertFileISOPain008();
		try {
			filesCrtdCnt = xmlWriter.rootXMLFile(source, target,
					fileTypeSelected, map2, connection, db_table_file,
					timeStamp, dbTimeStamp, moduleSelected, variants,testToolData);
		} catch (Exception e) {
			log.fatal("Exception: ", e);
			throw new Exception(e);
		}
		return filesCrtdCnt;
	}

	public int rootXMLFile(String source, File target, String fileTypeSelected,
			LinkedHashMap<Integer, Vector<String>> map2, Connection connection,
			String db_table_file, String timeStamp, String dbTimeStamp,
			String moduleSelected, char[] variants, List<TestToolFileNamePOJO> pojoList) throws Exception {

		IwReadExcel objIwReadExcel = new IwReadExcel();
		map = (LinkedHashMap<Integer, Vector<String>>) objIwReadExcel.ReadData(
				source, fileTypeSelected, moduleSelected);
		int rowCount = 7, colCount = 17;
		ArrayList<Integer> stockList = new ArrayList<Integer>();
		int count = 0, k = 0, fileCount = 0;
		for (int j = 0; j < map.size(); j++) {
			if (!map.get(rowCount).get(colCount).toString().isEmpty()) {
				stockList.add(count + 1);
				fileCount++;
				count = 0;
				k++;
			} else
				count++;
			rowCount++;
		}

		stockList.add(count + 1);
		int arr[] = new int[stockList.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = stockList.get(i).intValue();
		}

		ArrayList allRecList = new ArrayList();
		for (int i = 0, j2 = 1; i < fileCount; i++, j2++) {
			ArrayList rowCol = writeXML(arr[j2], target, connection,
					db_table_file, timeStamp, dbTimeStamp, null, -1, -1,pojoList);// Changes 1.4
			// log.debug("RowCol size after 1st loop: "+rowCol.size());
			allRecList.add(rowCol);
		}
		log.debug("ALL good files generated");

		// BAD_FILES_CHANGES STARTS
		String[] ruleNames = null;
		boolean oFind = false;
		for (int i = 0; i < variants.length; i++) {
			if (variants[i] == 'O') {
				oFind = true;
			}
		}
		if (oFind) {
			ruleNames = new String[variants.length - 1];
		} else {
			ruleNames = new String[variants.length];
		}

		for (int i = 0; i < variants.length; i++) {
			if (variants[i] == 'O')
				continue;
			else
				ruleNames[i] = variants[i] + "";
		}
		int lastRowNo = 0;
		int startNoForEachMessage = 0;
		// iterating over no of messages
		for (int allRec = 0; allRec < allRecList.size(); allRec++) {
			if (lastRowNo == 0) {
				startNoForEachMessage = 7 + lastRowNo;
			} else {
				startNoForEachMessage = 1 + lastRowNo;

			}
			p = startNoForEachMessage;
			batchrowCount = startNoForEachMessage;
			m = startNoForEachMessage;
			ArrayList rowCol = (ArrayList) allRecList.get(allRec);
			for (int rowColNo = 0; rowColNo < rowCol.size(); rowColNo++) {
				String s = (String) rowCol.get(rowColNo);
				// log.debug("Starting loop: "+s);

				String[] strArray = s.split("~");
				lastRowNo = Integer.valueOf(strArray[0]);
				int ruleColNo = Integer.valueOf(strArray[1]);

				for (int x = 0; x < ruleNames.length; x++) {

					p = startNoForEachMessage;
					batchrowCount = startNoForEachMessage;
					m = startNoForEachMessage;

					writeXML(arr[allRec + 1], target, connection,
							db_table_file, timeStamp, dbTimeStamp,
							ruleNames[x], lastRowNo, ruleColNo,pojoList);// Changes 1.4

				}
			}
		}
		
		return fileCount;
	}

	public ArrayList writeXML(int batchCount, File target,
			Connection connection, String db_table_file, String timeStamp,
			String dbTimeStamp, String ruleName, int ruleRowNo, int ruleColNo,List<TestToolFileNamePOJO> pojoList)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("writeXML method starts in ConvertFileISOPain001 class: batchCount: "
					+ batchCount
					+ "ruleName: "
					+ ruleName
					+ " ruleRowNo: "
					+ ruleRowNo + " ruleColNo: " + ruleColNo + " p: " + p);
		}
		XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMdd");
		String XMLPath = target + "/";
		XMLPath = XMLPath.replaceAll(".xml", m + ".xml");
		String fileName = null, dbfileName = null;
		// BAD_FILES_CHANGES STARTS
		ArrayList<String> rowCol = null;
		// BAD_FILES_CHANGES ENDS

		if (map.get(p).get(2) != null
				&& map.get(p).get(2).toString().length() > 0) {

			String temp = FormatGenerateService.getSequenceNumber(connection,
					map.get(p).get(2).toString());
			if (map.get(p).get(3) != null
					&& map.get(p).get(3).toString().replace(" ", "")
							.equalsIgnoreCase("FTS")) {
				File filefolder = new File(XMLPath + "/FTS");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();
				fileName = XMLPath
						+ "/"
						+ map.get(p).get(7).toString()
						+ "_"
						+ map.get(p).get(10).toString()
						+ "_"
						+ formatter.format(date)
						+ "_111213"
						+ "_"
						+ map.get(p)
								.get(8)
								.toString()
								.replace(".0", "")
								.substring(
										0,
										map.get(p).get(8).toString().length() - 6)
						+ temp + "." + map.get(p).get(2).toString() + "."
						+ map.get(p).get(11).toString() + ".xml";
				log.debug("fileName:" + fileName);
				// BAD_FILES_CHANGES ENDS
				if (map.get(p).get(12) != null
						&& !map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(7).toString()
							+ "_"
							+ map.get(p).get(10).toString()
							+ "_"
							+ formatter.format(date)
							+ "_111213"
							+ "_"
							+ map.get(p)
									.get(8)
									.toString()
									.replace(".0", "")
									.substring(
											0,
											map.get(p).get(8).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();
				} else {
					dbfileName = map.get(p).get(7).toString()
							+ "_"
							+ map.get(p).get(10).toString()
							+ "_"
							+ formatter.format(date)
							+ "_111213"
							+ "_"
							+ map.get(p)
									.get(8)
									.toString()
									.replace(".0", "")
									.substring(
											0,
											map.get(p).get(8).toString()
													.length() - 6)
							+ FormatGenerateService.getSequenceNumber(
									connection, map.get(p).get(2).toString())
							+ "." + map.get(p).get(2).toString() + "."
							+ map.get(p).get(11).toString() + ".xml";
				}

			} else if (map.get(p).get(3).toString().replace(" ", "")
					.equalsIgnoreCase("SCA")) {

				File filefolder = new File(XMLPath + "/SCA");
				if (!filefolder.exists()) {
					filefolder.mkdirs();
				}
				XMLPath = filefolder.toString();

				fileName = XMLPath
						+ "/"
						+ map.get(p).get(6).toString()
						+ "."
						+ formatter1.format(date)
						+ "111213"
						+ "."
						+ map.get(p)
								.get(10)
								.toString()
								.substring(
										0,
										map.get(p).get(10).toString().length() - 6)
						+ temp + "."
						+ map.get(p).get(2).toString().replace(".0", "") + "."
						+ map.get(p).get(11).toString() + ".xml";
				log.debug("fileName:" + fileName);
				// BAD_FILES_CHANGES ENDS
				if (!map.get(p).get(12).toString().isEmpty()) {

					fileName = fileName + "." + map.get(p).get(12).toString();
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml."
							+ map.get(p).get(12).toString();
				} else {
					dbfileName = map.get(p).get(6).toString()
							+ "."
							+ formatter1.format(date)
							+ "111213"
							+ "."
							+ map.get(p)
									.get(10)
									.toString()
									.substring(
											0,
											map.get(p).get(10).toString()
													.length() - 6) + temp + "."
							+ map.get(p).get(2).toString().replace(".0", "")
							+ "." + map.get(p).get(11).toString() + ".xml";
				}
			}
			// BAD_FILES_CHANGES
			StringBuffer strModifiedField = new StringBuffer();
			if (ruleName != null && !"".equals(ruleName)) {
				strModifiedField.append(ruleRowNo).append("~")
						.append(ruleColNo);
			}
			String errorCode = null;
			String ruleType = null;
			if ("M".equals(ruleName)) {
				ruleType = "TAG_MISSING";
				errorCode = "TAG_MISSING_ERRORCODE";
			} else if ("D".equals(ruleName)) {
				ruleType = "DUPLICATE_TAG";
				errorCode = "DUPLICATE_TAG_ERRORCODE";
			} else if ("I".equals(ruleName)) {
				ruleType = "INVALID_TAG";
				errorCode = "INVALID_TAG_ERRORCODE";
			} else if ("O".equals(ruleName)) {
				ruleType = "TAG_ORDER_CHANGE";
				errorCode = "TAG_ORDER_CHANGE_ERRORCODE";
			}

			String pain008_applied_rule_pos = FormatConstants.pain008AppliedRulePosition;
			int pain008_applied_rule_pos_no = Integer
					.parseInt(pain008_applied_rule_pos);
			
			// Changes 1.4 starts
			if (ruleName != null && !"".equals(ruleName)) {
				TestToolFileNamePOJO pojoObj = new TestToolFileNamePOJO();
				pojoObj.setTestCaseid(map.get(p).get(0).toString());
				pojoObj.setFormatName(map.get(p).get(2).toString());
				pojoObj.setChannel(map.get(p).get(3).toString());
				pojoObj.setLsi(map.get(p).get(4).toString());
				pojoObj.setClientRefNum(map.get(p).get(17).toString());
				pojoObj.setBicCode(map.get(p).get(6).toString());
				pojoObj.setBbdId(map.get(p).get(7).toString());
				pojoObj.setCustRef(map.get(p).get(8).toString());
				pojoObj.setUniqueId(map.get(p).get(10).toString());
				pojoObj.setFileName(dbfileName);
				pojoObj.setAppliedRules(ruleType);
				pojoObj.setAppliedRules(errorCode);
				pojoObj.setModifiedField(strModifiedField.toString());
				pojoObj.setTimeStamp(dbTimeStamp);
				pojoList.add(pojoObj);
			} else {
				TestToolFileNamePOJO pojoObj = new TestToolFileNamePOJO();
				pojoObj.setTestCaseid(map.get(p).get(0).toString());
				pojoObj.setFormatName(map.get(p).get(2).toString());
				pojoObj.setChannel(map.get(p).get(3).toString());
				pojoObj.setLsi(map.get(p).get(4).toString());
				pojoObj.setClientRefNum(map.get(p).get(17).toString());
				pojoObj.setBicCode(map.get(p).get(6).toString());
				pojoObj.setBbdId(map.get(p).get(7).toString());
				pojoObj.setCustRef(map.get(p).get(8).toString());
				pojoObj.setUniqueId(map.get(p).get(10).toString());
				pojoObj.setFileName(dbfileName);
				pojoObj.setAppliedRules(map.get(p).get(pain008_applied_rule_pos_no).toString());
				pojoObj.setAppliedRules(map.get(p).get(pain008_applied_rule_pos_no + 1).toString());
				pojoObj.setModifiedField(map.get(p).get(pain008_applied_rule_pos_no + 3).toString());
				pojoObj.setTimeStamp(dbTimeStamp);
				pojoList.add(pojoObj);
			}
			// Changes 1.4 ends
		} else {

			if (log.isDebugEnabled()) {
				log.debug("FormatName is empty");
			}
		}
		m++;
		String rootElement = "CstmrDrctDbtInitn";
		try {
			XMLEventWriter xmlEventWriter = xmlOutputFactory
					.createXMLEventWriter(new FileOutputStream(fileName),
							"UTF-8");
			XMLEventFactory eventFactory = XMLEventFactory.newInstance();
			XMLEvent end = eventFactory.createDTD("\r\n");
			XMLEvent tab = eventFactory.createDTD("\t");

			ArrayList<Namespace> ns = new ArrayList<Namespace>();

			ArrayList<Attribute> atts = new ArrayList<Attribute>();
			atts.add(eventFactory
					.createAttribute(
							"xsi:schemaLocation",
							"urn:iso:std:iso:20022:tech:xsd:pain.008.001.02 file:///C:/Users/IBM_ADMIN/Box%20Sync/My%20Files/PSH/pain.008.001.02.xsd"));
			atts.add(eventFactory.createAttribute("xmlns",
					"urn:iso:std:iso:20022:tech:xsd:pain.008.001.02"));
			atts.add(eventFactory.createAttribute("xmlns:xsi",
					"http://www.w3.org/2001/XMLSchema-instance"));

			StartElement tempDocument = eventFactory.createStartElement("", "",
					"Document", atts.iterator(), ns.iterator());
			// 1.1 ends
			xmlEventWriter.add(tempDocument);
			XMLEvent xmlDocs2 = eventFactory.createAttribute("xmlns",
					"urn:iso:std:iso:20022:tech:xsd:pain.001.001.03");
			// xmlEventWriter.add(xmlDocs2);
			xmlDocs2 = eventFactory.createNamespace("xsi",
					"http://www.w3.org/2001/XMLSchema-instance");
			// xmlEventWriter.add(xmlDocs2);
			xmlEventWriter.add(end);
			StartElement configStartElement = eventFactory.createStartElement(
					"", "", rootElement);
			xmlEventWriter.add(tab);
			xmlEventWriter.add(configStartElement);
			xmlEventWriter.add(end);
			EndElement EndDocument = null;
			Map<String, String> childNodes = null;
			Set<String> elementNodes = null;

			tempDocument = eventFactory.createStartElement("", "", "GrpHdr");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			Map<String, String> elementsMap = new LinkedHashMap<String, String>();
			String[] Msg_id = { "", "" };// messageid changes
			// BAD_FILES_CHANGES STARTS
			if (ruleName == null) {
				rowCol = new ArrayList();
			}
			// BAD_FILES_CHANGES ENDS

			// start GrpHdr
			if (!map.get(p).get(17).toString().isEmpty()) {
				elementsMap.put("MsgId", (String) map.get(p).get(17));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(17));
				else {
					Msg_id = UpdateFormatXlsService.getData(connection,
							FormatConstants.pain008);// messageid changes
					elementsMap.put("MsgId", Msg_id[0]);
					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "MsgId", 17, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS

			}
			// CreDtTm
			if (!map.get(p).get(19).toString().isEmpty()) {
				elementsMap.put("CreDtTm", (String) map.get(p).get(19));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(19));
				else {

					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "CreDtTm", 19, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS

			}
			elementNodes = elementsMap.keySet();
			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, elementsMap.get(key), 3);
			}
			elementsMap = new LinkedHashMap<String, String>();

			// Authstn
			if (!map.get(p).get(19).toString().isEmpty()) {
				tab = eventFactory.createDTD("\t");
				tempDocument = eventFactory.createStartElement("", "",
						"Authstn");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if ((!map.get(p).get(18).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Prtry", map.get(p).get(18).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(18));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Prtry", 18,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}
				EndDocument = eventFactory.createEndElement("", "", "Authstn");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

			}

			// NbOfTxs
			if (!map.get(p).get(20).toString().isEmpty()) {
				elementsMap.put("NbOfTxs", (String) map.get(p).get(20));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(20));
				else {

					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "NbOfTxs", 20, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}

			// CtrlSum
			if (!map.get(p).get(21).toString().isEmpty()) {
				elementsMap.put("CtrlSum", (String) map.get(p).get(21));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(21));
				else {

					elementsMap = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							elementsMap, "CtrlSum", 21, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}

			elementNodes = elementsMap.keySet();
			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, elementsMap.get(key), 3);
			}
			if (!map.get(p).get(22).toString().isEmpty()
					|| !map.get(p).get(22).toString().isEmpty()) {
				tempDocument = eventFactory.createStartElement("", "",
						"InitgPty");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if (!map.get(p).get(22).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					// XMLEvent tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", (String) map.get(p).get(22));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(22));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 22, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 4);
					}
				}

				if (!map.get(p).get(23).toString().isEmpty()) {
					tempDocument = eventFactory
							.createStartElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					tempDocument = eventFactory.createStartElement("", "",
							"OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					childNodes.put("Id", (String) map.get(p).get(23));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(23));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 23, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}

					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory
							.createEndElement("", "", "OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory.createEndElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "InitgPty");
				tabMarked(xmlEventWriter, 3);

				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			EndDocument = eventFactory.createEndElement("", "", "GrpHdr");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

			// <GrpHdr> end
			int colCount = 24;
			ArrayList<Integer> stockList = new ArrayList<Integer>();
			int count = 0, k = 0, fileCount = 0;

			for (int j = 0; j < batchCount; j++) {
				if (!map.get(batchrowCount).get(colCount).toString().isEmpty()) {
					// arr[k]=count+1;
					stockList.add(count + 1);
					fileCount++;
					count = 0;
					k++;
				} else
					count++;
				batchrowCount++;
			}
			stockList.add(count + 1);
			int arr[] = new int[stockList.size()];
			for (int i2 = 0; i2 < arr.length; i2++) {
				arr[i2] = stockList.get(i2).intValue();
			}
			for (int j2 = 1; j2 <= k; j2++) {
			}
			// end of logics

			for (int i2 = 0, k2 = 1; i2 < fileCount; i2++, k2++)
				paymentInfo(tempDocument, EndDocument, xmlEventWriter,
						eventFactory, end, childNodes, elementNodes, tab, p,
						arr[k2], rowCol, ruleName, ruleRowNo, ruleColNo);

			tabMarked(xmlEventWriter, 1);
			xmlEventWriter.add(eventFactory.createEndElement("", "",
					rootElement));
			xmlEventWriter.add(end);
			xmlEventWriter.add(eventFactory.createEndDocument());
			eventFactory.createEndDocument();
			EndDocument = eventFactory.createEndElement("", "", "Document");
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

			String rp = xmlEventWriter.toString();
			rp = rp.replaceAll(
					"</Document xmlns=urn:iso:std:iso:20022:tech:xsd:pain.001.001.03 xmlns:xsi=http://www.w3.org/2001/XMLSchema-instance>",
					"</Document>");

			xmlEventWriter.close();

		} catch (EmptyStackException exx) {
			log.fatal("EmptyStackException: ", exx);
			throw new EmptyStackException();

		} catch (FileNotFoundException e) {
			log.fatal("FileNotFoundException: ", e);
			throw new FileNotFoundException();
			// e.printStackTrace();
		} catch (XMLStreamException ex) {
			log.fatal("XMLStreamException: ", ex);
			throw new XMLStreamException();
		}
		return rowCol;

	}

	// BAD_FILES_CHANGES
	private Map applyRule(int ruleRowNo, int ruleColNo, int p2,
			String ruleName, Map<String, String> elementsMap, String key,
			int actualColNo, XMLEventWriter xmlEventWriter) throws Exception {
		if (log.isDebugEnabled())
			log.debug("in applyRule ruleRowNo: " + ruleRowNo + " ruleColNo: "
					+ ruleColNo + "p: " + p2 + " ruleName: " + ruleName
					+ " elementsMap: " + elementsMap + " key: " + key
					+ " actualColNo: " + actualColNo);
		if (p2 == ruleRowNo && ruleColNo == actualColNo) {
			if ("D".equals(ruleName)) {
				// elementsMap.put(key, (String) map.get(p).get(actualColNo));
				createNode(xmlEventWriter, key, elementsMap.get(key), 3);

			} else if ("M".equals(ruleName)) {
				elementsMap.remove(key);

			} else if ("I".equals(ruleName)) {
				elementsMap.put(key + invalidAppender,
						(String) map.get(p).get(actualColNo));
				elementsMap.remove(key);

			}
		}
		if (log.isDebugEnabled())
			log.debug("Leaving applyRule: elementsMap " + elementsMap);
		return elementsMap;

	}

	public void paymentInfo(StartElement tempDocument, EndElement EndDocument,
			XMLEventWriter xmlEventWriter, XMLEventFactory eventFactory,
			XMLEvent end, Map<String, String> childNodes,
			Set<String> elementNodes, XMLEvent tab, int tempP, int batchCount,
			ArrayList rowCol, String ruleName, int ruleRowNo, int ruleColNo)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("paymentInfo method starts in ConvertFileISOPain008 class");
		}
		// start <PmtInf>
		tempDocument = eventFactory.createStartElement("", "", "PmtInf");
		try {
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(tempDocument);
			xmlEventWriter.add(end);
			childNodes = new LinkedHashMap<String, String>();
			tab = eventFactory.createDTD("\t");
			if (!map.get(p).get(24).toString().isEmpty()) {
				childNodes.put("PmtInfId", map.get(p).get(24).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(24));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "PmtInfId", 24, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			if (!map.get(p).get(25).toString().isEmpty()) {
				childNodes.put("PmtMtd", map.get(p).get(25).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(25));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "PmtMtd", 25, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			if (!map.get(p).get(26).toString().isEmpty()) {
				childNodes.put("NbOfTxs", (String) map.get(p).get(26));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(26));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "NbOfTxs", 26, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			if (!map.get(p).get(27).toString().isEmpty()) {
				childNodes.put("CtrlSum", (String) map.get(p).get(27));
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(27));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "CtrlSum", 27, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}

			// Requested Execution Date
			if (!map.get(p).get(28).toString().isEmpty()) {
				childNodes.put("ReqdColltnDt", map.get(p).get(28).toString());

				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(28));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "ReqdColltnDt", 28, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
			}
			elementNodes = childNodes.keySet();

			for (String key : elementNodes) {
				createNode(xmlEventWriter, key, childNodes.get(key), 3);
			}

			// Cdtr
			if ((!map.get(p).get(29).toString().isEmpty())
					|| (!map.get(p).get(30).toString().isEmpty())
					|| (!map.get(p).get(31).toString().isEmpty())
					|| (!map.get(p).get(32).toString().isEmpty())
					|| (!map.get(p).get(33).toString().isEmpty())
					|| (!map.get(p).get(34).toString().isEmpty())
					|| (!map.get(p).get(35).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "", "Cdtr");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if (!map.get(p).get(29).toString().isEmpty()) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(29).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(29));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 29, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 4);
					}
				}
				if ((!map.get(p).get(30).toString().isEmpty())
						|| (!map.get(p).get(31).toString().isEmpty())
						|| (!map.get(p).get(32).toString().isEmpty())
						|| (!map.get(p).get(33).toString().isEmpty())
						|| (!map.get(p).get(34).toString().isEmpty())
						|| (!map.get(p).get(35).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();
					tab = eventFactory.createDTD("\t");
					if ((!map.get(p).get(30).toString().isEmpty())) {
						childNodes.put("StrtNm", map.get(p).get(30).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(30));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "StrtNm", 30,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if ((!map.get(p).get(32).toString().isEmpty())) {
						childNodes.put("PstCd", map.get(p).get(32).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(32));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "PstCd", 32,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if ((!map.get(p).get(33).toString().isEmpty())) {
						childNodes.put("TwnNm", map.get(p).get(33).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(33));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "TwnNm", 33,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if ((!map.get(p).get(31).toString().isEmpty())) {
						childNodes.put("CtrySubDvsn", map.get(p).get(31)
								.toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(31));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "CtrySubDvsn", 31,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if ((!map.get(p).get(35).toString().isEmpty())) {
						childNodes.put("Ctry", map.get(p).get(35).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(35));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Ctry", 35,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				if (!map.get(p).get(36).toString().isEmpty()) {
					tempDocument = eventFactory
							.createStartElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					tempDocument = eventFactory.createStartElement("", "",
							"OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					// XMLEvent tab = eventFactory.createDTD("\t");
					childNodes.put("Id", (String) map.get(p).get(36));
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(36));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 36, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}

					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
					EndDocument = eventFactory
							.createEndElement("", "", "OrgId");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					EndDocument = eventFactory.createEndElement("", "", "Id");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "Cdtr");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// CdtrAcct
			if (!map.get(p).get(37).toString().isEmpty()) {
				tempDocument = eventFactory.createStartElement("", "",
						"CdtrAcct");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				tempDocument = eventFactory.createStartElement("", "", "Id");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				tempDocument = eventFactory.createStartElement("", "", "Othr");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Id", map.get(p).get(37).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(37));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Id", 37, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 6);
				}
				EndDocument = eventFactory.createEndElement("", "", "Othr");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				EndDocument = eventFactory.createEndElement("", "", "Id");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
				EndDocument = eventFactory.createEndElement("", "", "CdtrAcct");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// CdtrAgt

			if ((!map.get(p).get(38).toString().isEmpty())
					|| (!map.get(p).get(39).toString().isEmpty())
					|| (!map.get(p).get(40).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"CdtrAgt");
				tabMarked(xmlEventWriter, 3);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				// new data
				if ((!map.get(p).get(38).toString().isEmpty())
						|| (!map.get(p).get(39).toString().isEmpty())
						|| (!map.get(p).get(40).toString().isEmpty())) {

					tempDocument = eventFactory.createStartElement("", "",
							"FinInstnId");
					tabMarked(xmlEventWriter, 4);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					// IF part

					if (!map.get(p).get(38).toString().isEmpty()) {
						childNodes = new LinkedHashMap<String, String>();

						tab = eventFactory.createDTD("\t");
						childNodes.put("Nm", map.get(p).get(38).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(38));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Nm", 38,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
						elementNodes = childNodes.keySet();
						for (String key : elementNodes) {
							createNode(xmlEventWriter, key,
									childNodes.get(key), 5);
						}
					}

					tempDocument = eventFactory.createStartElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					if ((!map.get(p).get(39).toString().isEmpty())) {
						childNodes.put("Ctry", map.get(p).get(39).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(39));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Ctry", 39,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

				}
				EndDocument = eventFactory.createEndElement("", "",
						"FinInstnId");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

			}

			// BrnchId

			if (!map.get(p).get(40).toString().isEmpty()) {
				tempDocument = eventFactory.createStartElement("", "",
						"BrnchId");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Id", map.get(p).get(40).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(40));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Id", 40, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}

				EndDocument = eventFactory.createEndElement("", "", "BrnchId");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			EndDocument = eventFactory.createEndElement("", "", "CdtrAgt");
			tabMarked(xmlEventWriter, 3);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);

			for (int transC = 0; transC < batchCount; transC++) {
				createTransactionNode(tempDocument, EndDocument,
						xmlEventWriter, eventFactory, end, childNodes,
						elementNodes, tab, p, batchCount, rowCol, ruleName,
						ruleRowNo, ruleColNo);
				p++;
			}

			EndDocument = eventFactory.createEndElement("", "", "PmtInf");
			tabMarked(xmlEventWriter, 2);
			xmlEventWriter.add(EndDocument);
			xmlEventWriter.add(end);
			// end </PmtInf>

		} catch (XMLStreamException e) {
			log.fatal("Exception: ", e);
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	private static void createNode(XMLEventWriter eventWriter, String element,
			String value, int tab2) throws XMLStreamException {

		XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();
		XMLEvent end = xmlEventFactory.createDTD("\n");
		XMLEvent tab = xmlEventFactory.createDTD("\t");
		// Create Start node
		StartElement sElement = xmlEventFactory.createStartElement("", "",
				element);
		for (int i = 0; i < tab2; i++)
			eventWriter.add(tab);
		eventWriter.add(sElement);
		// Create Content
		Characters characters = xmlEventFactory.createCharacters(value);
		eventWriter.add(characters);
		// Create End node
		EndElement eElement = xmlEventFactory.createEndElement("", "", element);
		eventWriter.add(eElement);
		eventWriter.add(end);
	}

	public void createTransactionNode(StartElement tempDocument,
			EndElement EndDocument, XMLEventWriter xmlEventWriter,
			XMLEventFactory eventFactory, XMLEvent end,
			Map<String, String> childNodes, Set<String> elementNodes,
			XMLEvent tab, int p, int batchCount, ArrayList rowCol,
			String ruleName, int ruleRowNo, int ruleColNo) throws Exception {

		// start DrctDbtTxInf

		try {
			for (int i = 41; i <= 90; i++) {
				if ((!map.get(p).get(i).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"DrctDbtTxInf");
					tabMarked(xmlEventWriter, 3);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					break;
				}
			}

			if ((!map.get(p).get(41).toString().isEmpty())
					|| (!map.get(p).get(42).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "", "PmtId");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();
				tab = eventFactory.createDTD("\t");
				if ((!map.get(p).get(41).toString().isEmpty())) {
					childNodes.put("InstrId", map.get(p).get(41).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(41));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "InstrId", 41,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}
				if ((!map.get(p).get(42).toString().isEmpty())) {
					childNodes.put("EndToEndId", map.get(p).get(42).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(42));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "EndToEndId", 42,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
				}
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}

				EndDocument = eventFactory.createEndElement("", "", "PmtId");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// PmtTpInf

			if ((!map.get(p).get(44).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"PmtTpInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				tempDocument = eventFactory.createStartElement("", "",
						"LclInstrm");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Cd", map.get(p).get(44).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(44));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Cd", 44, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 6);
				}

				EndDocument = eventFactory
						.createEndElement("", "", "LclInstrm");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				// Category Purpose Code Proprietary
				if ((!map.get(p).get(45).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"CtgyPurp");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Prtry", map.get(p).get(45).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(45));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Prtry", 45,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"CtgyPurp");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "PmtTpInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// Amt

			XMLEvent xmlDocs;
			Characters characters;
			if (!map.get(p).get(46).toString().isEmpty())

			{
				tempDocument = eventFactory.createStartElement("", "", "Amt");
				tabMarked(xmlEventWriter, 4);
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null) {

					tempDocument = eventFactory.createStartElement("", "",
							"InstdAmt");

					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlDocs = eventFactory.createAttribute("Ccy", map.get(p)
							.get(46).toString());
					xmlEventWriter.add(xmlDocs);

					characters = eventFactory.createCharacters(map.get(p)
							.get(47).toString());
					xmlEventWriter.add(characters);

					EndDocument = eventFactory.createEndElement("", "",
							"InstdAmt");
					EndDocument.asEndElement();

					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);

					rowCol.add(String.valueOf(p) + "~" + String.valueOf(47));
				} else {

					if (p == ruleRowNo && ruleColNo == 47) {
						if ("D".equals(ruleName)) {
							tempDocument = eventFactory.createStartElement("",
									"", "InstdAmt");
							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							xmlDocs = eventFactory.createAttribute("Ccy", map
									.get(p).get(46).toString());
							xmlEventWriter.add(xmlDocs);

							characters = eventFactory.createCharacters(map
									.get(p).get(47).toString());
							xmlEventWriter.add(characters);
							EndDocument = eventFactory.createEndElement("", "",
									"InstdAmt");
							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

							tempDocument = eventFactory.createStartElement("",
									"", "InstdAmt");
							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							xmlDocs = eventFactory.createAttribute("Ccy", map
									.get(p).get(46).toString());
							xmlEventWriter.add(xmlDocs);

							characters = eventFactory.createCharacters(map
									.get(p).get(47).toString());
							xmlEventWriter.add(characters);
							EndDocument = eventFactory.createEndElement("", "",
									"InstdAmt");
							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

						} else if ("M".equals(ruleName)) {

						} else if ("I".equals(ruleName)) {
							tempDocument = eventFactory.createStartElement("",
									"", "InstdAmt" + invalidAppender);
							tabMarked(xmlEventWriter, 5);
							xmlEventWriter.add(tempDocument);
							xmlDocs = eventFactory.createAttribute("Ccy", map
									.get(p).get(46).toString());
							xmlEventWriter.add(xmlDocs);

							characters = eventFactory.createCharacters(map
									.get(p).get(47).toString());
							xmlEventWriter.add(characters);
							EndDocument = eventFactory.createEndElement("", "",
									"InstdAmt" + invalidAppender);
							EndDocument.asEndElement();

							xmlEventWriter.add(EndDocument);
							xmlEventWriter.add(end);

						}
					} else {
						tempDocument = eventFactory.createStartElement("", "",
								"InstdAmt");
						tabMarked(xmlEventWriter, 5);
						xmlEventWriter.add(tempDocument);
						xmlDocs = eventFactory.createAttribute("Ccy", map
								.get(p).get(46).toString());
						xmlEventWriter.add(xmlDocs);

						characters = eventFactory.createCharacters(map.get(p)
								.get(47).toString());
						xmlEventWriter.add(characters);
						EndDocument = eventFactory.createEndElement("", "",
								"InstdAmt");
						EndDocument.asEndElement();

						xmlEventWriter.add(EndDocument);
						xmlEventWriter.add(end);

					}
					// pending
				}
				// BAD_FILES_CHANGES ENDS

				EndDocument = eventFactory.createEndElement("", "", "Amt");
				tabMarked(xmlEventWriter, 4);

			}

			// DbtrAgt
			if ((!map.get(p).get(67).toString().isEmpty())
					|| (!map.get(p).get(68).toString().isEmpty())
					|| (!map.get(p).get(69).toString().isEmpty())
					|| (!map.get(p).get(70).toString().isEmpty())
					|| (!map.get(p).get(71).toString().isEmpty())
					|| (!map.get(p).get(72).toString().isEmpty())
					|| (!map.get(p).get(73).toString().isEmpty())
					|| (!map.get(p).get(74).toString().isEmpty())
					|| (!map.get(p).get(75).toString().isEmpty())
					|| (!map.get(p).get(76).toString().isEmpty())
					|| (!map.get(p).get(77).toString().isEmpty())
					|| (!map.get(p).get(78).toString().isEmpty())) {
				tempDocument = eventFactory.createStartElement("", "",
						"DbtrAgt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				tempDocument = eventFactory.createStartElement("", "",
						"FinInstnId");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				if ((!map.get(p).get(70).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(70).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(70));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 70, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
				}

				if ((!map.get(p).get(71).toString().isEmpty())
						|| (!map.get(p).get(72).toString().isEmpty())
						|| (!map.get(p).get(73).toString().isEmpty())
						|| (!map.get(p).get(74).toString().isEmpty())
						|| (!map.get(p).get(75).toString().isEmpty())
						|| (!map.get(p).get(76).toString().isEmpty())) {

					tempDocument = eventFactory.createStartElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					if (!map.get(p).get(71).toString().isEmpty()) {
						childNodes.put("StrtNm", map.get(p).get(71).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(71));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "StrtNm", 71,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(73).toString().isEmpty()) {
						childNodes.put("PstCd", map.get(p).get(73).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(73));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "PstCd", 73,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(74).toString().isEmpty()) {
						childNodes.put("TwnNm", map.get(p).get(74).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(74));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "TwnNm", 74,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(75).toString().isEmpty()) {
						childNodes.put("CtrySubDvsn", map.get(p).get(75)
								.toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(75));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "CtrySubDvsn", 75,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(76).toString().isEmpty()) {
						childNodes.put("Ctry", map.get(p).get(76).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(76));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Ctry", 76,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 7);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				if ((!map.get(p).get(77).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Id", map.get(p).get(77).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(77));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 77, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS

					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "", "Othr");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "",
						"FinInstnId");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				if ((!map.get(p).get(78).toString().isEmpty())) {
					tempDocument = eventFactory.createStartElement("", "",
							"BrnchId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Id", map.get(p).get(78).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(78));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Id", 78, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "",
							"BrnchId");
					tabMarked(xmlEventWriter, 6);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "DbtrAgt");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// <Dbtr>
			if ((!map.get(p).get(79).toString().isEmpty())
					|| (!map.get(p).get(80).toString().isEmpty())
					|| (!map.get(p).get(81).toString().isEmpty())
					|| (!map.get(p).get(82).toString().isEmpty())
					|| (!map.get(p).get(83).toString().isEmpty())
					|| (!map.get(p).get(84).toString().isEmpty())
					|| (!map.get(p).get(85).toString().isEmpty())) {

				tempDocument = eventFactory.createStartElement("", "", "Dbtr");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				if ((!map.get(p).get(79).toString().isEmpty())) {
					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Nm", map.get(p).get(79).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(79));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Nm", 79, xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 5);
					}
				}
				if ((!map.get(p).get(80).toString().isEmpty())
						|| (!map.get(p).get(81).toString().isEmpty())
						|| (!map.get(p).get(82).toString().isEmpty())
						|| (!map.get(p).get(83).toString().isEmpty())
						|| (!map.get(p).get(84).toString().isEmpty())
						|| (!map.get(p).get(85).toString().isEmpty())) {

					tempDocument = eventFactory.createStartElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);
					childNodes = new LinkedHashMap<String, String>();
					tab = eventFactory.createDTD("\t");
					if (!map.get(p).get(80).toString().isEmpty()) {
						childNodes.put("StrtNm", map.get(p).get(80).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(80));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "StrtNm", 80,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}

					if (!map.get(p).get(82).toString().isEmpty()) {
						childNodes.put("PstCd", map.get(p).get(82).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(82));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "PstCd", 82,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(83).toString().isEmpty()) {
						childNodes.put("TwnNm", map.get(p).get(83).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(83));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "TwnNm", 83,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(84).toString().isEmpty()) {
						childNodes.put("CtrySubDvsn", map.get(p).get(84)
								.toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(84));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "CtrySubDvsn", 84,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					if (!map.get(p).get(85).toString().isEmpty()) {
						childNodes.put("Ctry", map.get(p).get(85).toString());
						// BAD_FILES_CHANGES STARTS
						if (rowCol != null)
							rowCol.add(String.valueOf(p) + "~"
									+ String.valueOf(85));
						else {

							childNodes = applyRule(ruleRowNo, ruleColNo, p,
									ruleName, childNodes, "Ctry", 85,
									xmlEventWriter);
						}
						// BAD_FILES_CHANGES ENDS
					}
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}
					EndDocument = eventFactory.createEndElement("", "",
							"PstlAdr");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}
				EndDocument = eventFactory.createEndElement("", "", "Dbtr");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}
			// cdtr account

			// if part
			if (!map.get(p).get(88).toString().isEmpty()
					|| !map.get(p).get(87).toString().isEmpty()) {
				tempDocument = eventFactory.createStartElement("", "",
						"DbtrAcct");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				tempDocument = eventFactory.createStartElement("", "", "Id");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				childNodes = new LinkedHashMap<String, String>();

				tempDocument = eventFactory.createStartElement("", "", "Othr");
				tabMarked(xmlEventWriter, 6);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);
				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Id", map.get(p).get(87).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(87));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Id", 87, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 7);
				}
				EndDocument = eventFactory.createEndElement("", "", "Othr");
				tabMarked(xmlEventWriter, 6);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				// end else part
				EndDocument = eventFactory.createEndElement("", "", "Id");
				tabMarked(xmlEventWriter, 5);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

				// ACH Transaction Code

				if ((!map.get(p).get(88).toString().isEmpty())) {
					tempDocument = eventFactory
							.createStartElement("", "", "Tp");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(tempDocument);
					xmlEventWriter.add(end);

					childNodes = new LinkedHashMap<String, String>();

					tab = eventFactory.createDTD("\t");
					childNodes.put("Prtry", map.get(p).get(88).toString());
					// BAD_FILES_CHANGES STARTS
					if (rowCol != null)
						rowCol.add(String.valueOf(p) + "~" + String.valueOf(88));
					else {

						childNodes = applyRule(ruleRowNo, ruleColNo, p,
								ruleName, childNodes, "Prtry", 88,
								xmlEventWriter);
					}
					// BAD_FILES_CHANGES ENDS
					elementNodes = childNodes.keySet();
					for (String key : elementNodes) {
						createNode(xmlEventWriter, key, childNodes.get(key), 6);
					}

					EndDocument = eventFactory.createEndElement("", "", "Tp");
					tabMarked(xmlEventWriter, 5);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
				}

				EndDocument = eventFactory.createEndElement("", "", "DbtrAcct");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

			}
			// }
			// Transaction Purpose Code

			if (!map.get(p).get(89).toString().isEmpty()) {
				tempDocument = eventFactory.createStartElement("", "", "Purp");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Prtry", map.get(p).get(89).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(89));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Prtry", 89, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}

				EndDocument = eventFactory.createEndElement("", "", "Purp");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);

			}
			// Remittance Information

			if ((!map.get(p).get(90).toString().isEmpty())) {

				tempDocument = eventFactory
						.createStartElement("", "", "RmtInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(tempDocument);
				xmlEventWriter.add(end);

				childNodes = new LinkedHashMap<String, String>();

				tab = eventFactory.createDTD("\t");
				childNodes.put("Ustrd", map.get(p).get(90).toString());
				// BAD_FILES_CHANGES STARTS
				if (rowCol != null)
					rowCol.add(String.valueOf(p) + "~" + String.valueOf(90));
				else {

					childNodes = applyRule(ruleRowNo, ruleColNo, p, ruleName,
							childNodes, "Ustrd", 90, xmlEventWriter);
				}
				// BAD_FILES_CHANGES ENDS
				elementNodes = childNodes.keySet();
				for (String key : elementNodes) {
					createNode(xmlEventWriter, key, childNodes.get(key), 5);
				}

				EndDocument = eventFactory.createEndElement("", "", "RmtInf");
				tabMarked(xmlEventWriter, 4);
				xmlEventWriter.add(EndDocument);
				xmlEventWriter.add(end);
			}

			// DrctDbtTxInf

			for (int i = 41; i <= 90; i++) {
				if ((!map.get(p).get(i).toString().isEmpty())) {
					EndDocument = eventFactory.createEndElement("", "",
							"DrctDbtTxInf");
					tabMarked(xmlEventWriter, 3);
					xmlEventWriter.add(EndDocument);
					xmlEventWriter.add(end);
					break;
				}
			}

		} catch (XMLStreamException e) {
			log.fatal("Exception: ", e);
			throw new Exception(e);
			// e.printStackTrace();
		}

	}

	public void tabMarked(XMLEventWriter xmlEventWriter, int count)
			throws Exception {
		XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();
		// XMLEvent end = xmlEventFactory.createDTD("\n");
		XMLEvent tab = xmlEventFactory.createDTD("\t");

		try {
			for (int i = 0; i < count; i++)
				xmlEventWriter.add(tab);
		} catch (XMLStreamException e) {
			log.fatal("Exception: ", e);
			throw new Exception(e);
			// e.printStackTrace();
		}
	}
}